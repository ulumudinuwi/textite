# ![Logo](assets/img/logo.png) Sistem Pos Retail Electronic

## Instalasi
Pastikan [Composer](https://getcomposer.org) dan [Bower](https://bower.io) telah terinstall di komputer, sebelum 
menggunakan CI-Beam. [Min. PHP 7.1]

Lakukan langkah-langkah berikut untuk melakukan instalasi CI-Beam:

1. Download CI-Beam ke direktori tujuan.
2. Pindah ke direktori CI-Beam.
3. Jalankan `composer install`.
4. Pindah ke direktori `assets`.
5. Jalankan `bower update`.

## Environment
Untuk pengerjaan di lokal, pastikan Anda membuat folder `development` di `application/config`.
Sebagai contoh untuk menset konfigurasi database di lokal, buat file `application/config/development/database.php`.
File konfigurasi di folder `application/config/development` akan menimpa konfigurasi di `application/config`.

## Catatan
CodeIgniter selalu me-load file config global terlebih dahulu (yaitu yang ada di `application/config/`), kemudian ia akan mencoba me-load file-file konfigurasi untuk environment aktif. Hal ini berarti Anda tidak perlu menimpa semua file konfigurasi dalam folder environment. Hanya file-file yang berubah saja yang perlu dibuat. Anda juga tidak perlu mengkopi semua item config ke dalam file config environment. Hanya item-item config yang ingin diubah saja yang perlu dimasukkan dalam file environment Anda. Item-item config yang dideklarasikan dalam folder environment akan selalu menimpa item-item yang terdapat dalam file-file config global.

## Instalasi Database
- Buka file sql di  `application/database/db_textile.sql`
- Buat database di Mysql dengan nama db_textile
- import file db_textile.sql

