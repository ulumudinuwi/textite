// BETA
function PembukaanServiks(canvasId, x_count, y_count, options) {
    var me = this;
    this.canvas = $('#' + canvasId);

    var canvas = this.canvas.get(0);
    var ctx = canvas.getContext('2d');

    this.labelX = 25;
    this.labelY = 25;
    this.shapes = {};

    this.settings = $.extend({
        boxWidth: (ctx.canvas.width - this.labelX) / (x_count * 2),
        boxHeight: (ctx.canvas.height - this.labelY) / y_count,
        boxPadding: 4,
        x_count: x_count,
        y_count: y_count
    }, options);

    this.canvas.mousemove(_pembukaan_serviks_on_mouse_move);
    this.canvas.click(_pembukaan_serviks_on_mouse_click);

    this.canvas.data('pembukaan_serviks', this);

    this.redraw();
}

PembukaanServiks.prototype.redraw = function() {
    var canvas = this.canvas.get(0);
    var ctx = canvas.getContext('2d');

    var waspada = {
        x1: 0,
        y1: 0,
        x2: 0,
        y2: 0
    };
    var bertindak = {
        x1: 0,
        y1: 0,
        x2: 0,
        y2: 0
    }

    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // Clears the canvas


    ctx.rect(0, 0, ctx.canvas.width, ctx.canvas.height);
    ctx.stroke();

    var bw = this.settings.boxWidth;
    var bh = this.settings.boxHeight;
    var p = this.settings.boxPadding;

    // Render GRID
    ctx.strokeStyle = 'rgba(0, 0, 0, .5)';
    ctx.lineWidth = 1;
    var xi = 0;
    for (var x = 0; x < this.settings.x_count * 2; x++) {
        ctx.moveTo(this.labelY + x * bw, 0);
        ctx.lineTo(this.labelY + x * bw, ctx.canvas.height - this.labelX);
        ctx.stroke();

        if ((x+1) % 2 == 0) {
            ctx.fillStyle = '#000';
            ctx.font = "14px Algerian";
            ctx.textBaseline = "bottom";
            ctx.textAlign = "left";
            ctx.fillText(xi+1, this.labelY + x * bw - (14/2), ctx.canvas.height - this.labelX/2 + (14/2));
            xi++;
        }

        // Waspada
        if (x == 0) {
            waspada.x1 = this.labelY + x * bw;
        }
        if ((x + 1) / 2 == 6) {
            waspada.x2 = this.labelY + x * bw;
        }

        // Bertindak
        if ((x + 1) / 2 == 4) {
            bertindak.x1 = this.labelY + x * bw;
        }

        if ((x + 1) / 2 == 10) {
            bertindak.x2 = this.labelY + x * bw;
        }
    }

    for (var y = 0; y < this.settings.y_count; y++) {
        ctx.moveTo(this.labelY, y * bh + bh);
        ctx.lineTo(ctx.canvas.width, y * bh + bh);
        ctx.stroke();

        ctx.fillStyle = '#000';
        ctx.font = "14px Algerian";
        ctx.textBaseline = "bottom";
        ctx.textAlign = "left";
        ctx.fillText(y+1, this.labelY - 18, y * bh + bh + (14/2));

        if (y + 1 == 4) {
            waspada.y1 = y * bh + bh;
            bertindak.y1 = y * bh + bh;
        }
    }

    // Render Garis Waspada & Bertindak
    ctx.strokeStyle = '#555';
    ctx.lineWidth = 1;
    ctx.moveTo(waspada.x1, waspada.y1);
    ctx.lineTo(waspada.x2, waspada.y2);
    ctx.stroke();

    ctx.moveTo(bertindak.x1, bertindak.y1);
    ctx.lineTo(bertindak.x2, bertindak.y2);
    ctx.stroke();

    // Text Waspada
    ctx.save();
    ctx.fillStyle = '#000';
    ctx.font = "18px Algerian";
    ctx.textBaseline = "bottom";
    ctx.textAlign = "left";
    ctx.translate(waspada.x1, waspada.y1);
    ctx.rotate(-Math.PI / 8);
    ctx.fillText('                 W A S P A D A', 0, 0);
    ctx.restore();

    // Text Bertindak
    ctx.save();
    ctx.fillStyle = '#000';
    ctx.font = "18px Algerian";
    ctx.textBaseline = "bottom";
    ctx.textAlign = "left";
    ctx.translate(bertindak.x1, bertindak.y1);
    ctx.rotate(-Math.PI / 8);
    ctx.fillText('                 B E R T I N D A K', 0, 0);
    ctx.restore();

    this.renderShapes();
};

PembukaanServiks.prototype.renderShapes = function() {
    var canvas = this.canvas.get(0);
    var ctx = canvas.getContext('2d');

    var shape;
    for (var key in this.shapes) {
        shape = this.shapes[key];
        if (shape) {
            ctx.fillStyle = shape.fillStyle;
            ctx.font = shape.font;
            ctx.textBaseline = shape.textBaseline;
            ctx.textAlign = shape.textAlign;
            ctx.fillText(shape.text, shape.x, shape.y);
        }
    }
};

PembukaanServiks.prototype.toDataURL = function () {
    return this.canvas.get(0).toDataURL();
}

function _pembukaan_serviks_on_mouse_move(e) {
    var canvas = this;
    var me = $(this).data('pembukaan_serviks');
    var ctx = canvas.getContext('2d');
    var offset = getOffsets(e);
    var mouseX = offset.offsetX;
    var mouseY = offset.offsetY;
    var mouse = {x: mouseX, y: mouseY};
    var bw = me.settings.boxWidth;
    var bh = me.settings.boxHeight;
    var p = me.settings.boxPadding;


    var xpos, ypos;
    var focusCoord = null;
    for (var y = 0; y < me.settings.y_count; y++) {
        for (var x = 0; x < me.settings.x_count * 2; x++) {
            xpos = me.labelY + x * bw;
            ypos = y * bh + bh;

            if (isInCircle(mouse, {x: xpos, y: ypos})) {
                focusCoord = {
                    x: xpos,
                    y: ypos,
                    r: 5
                };
            }
        }
    }

    if (focusCoord) {
        $(this).css('cursor', 'pointer');
    } else {
        $(this).css('cursor', 'default');
    }


    // me.setActiveShape(focusCoord);
    // me.redraw();
}

function _pembukaan_serviks_on_mouse_click(e) {
    var canvas = this;
    var me = $(this).data('pembukaan_serviks');
    var ctx = canvas.getContext('2d');
    var offset = getOffsets(e);
    var mouseX = offset.offsetX;
    var mouseY = offset.offsetY;
    var mouse = {x: mouseX, y: mouseY};
    var bw = me.settings.boxWidth;
    var bh = me.settings.boxHeight;
    var p = me.settings.boxPadding;


    var xpos, ypos;
    var shape;
    for (var y = 0; y < me.settings.y_count; y++) {
        for (var x = 0; x < me.settings.x_count * 2; x++) {
            xpos = me.labelY + x * bw;
            ypos = y * bh + bh;

            if (isInCircle(mouse, {x: xpos, y: ypos})) {
                shape = me.shapes[xpos+':'+ypos];

                if (shape) {
                    if (shape.text == 'X') {
                        shape.text = 'O';
                    } else {
                        shape = null;
                    }
                } else {
                    shape = {
                        fillStyle: '#333672',
                        font: "bold 18px Arial",
                        textBaseline: 'bottom',
                        textAlign: 'left',
                        x: xpos - 7,
                        y: ypos + 9,
                        text: 'X'
                    }
                }

                me.shapes[xpos+':'+ypos] = shape;
            }
        }
    }

    me.redraw();
}

function getOffsets(evt) {
    var offsetX, offsetY;
    if (typeof evt.offsetX != 'undefined') {
        offsetX = evt.offsetX;
        offsetY = evt.offsetY;
    } else if (typeof evt.layerX != 'undefined') {
        offsetX = evt.layerX;
        offsetY = evt.layerY;
    }

    return { 'offsetX': offsetX, 'offsetY': offsetY };
}

function isInCircle(mouse, circle) {
    var a=10;
    var b=10;

    var dx = mouse.x - circle.x;
    var dy = mouse.y - circle.y;
    return ((dx*dx)/(a*a)+(dy*dy)/(b*b)<=1);
}