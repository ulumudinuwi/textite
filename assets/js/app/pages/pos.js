var pos = new function(){
    this.var = {
    };
    this.el = {
        
    };
    this.method = {
       
    };
    this.onCreate = new function(){
        this.mockup = function(){
            var dispInputQuantity = $("<input/>")
                .prop('type', 'text')
                .addClass('form-control')
                .appendTo('#check-form');
            var inputQuantity = $("<input/>")
                .prop('type', 'hidden')
                .prop('name', 'tarif_pelayanan_quantity[]')
                .appendTo('#check-form');
            dispInputQuantity.autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': 'Rp.', 'pSign': 'p',});
            dispInputQuantity.autoNumeric('set', '10710880');

            $('#mock').DataTable();
       }
    };
    this.onCreated = function(){
        pos.onCreate.mockup();
        global.onCreate.productInput();
    };
}

$(function(){
    $('.daterange-basic').daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default'
    });
    $('.daterange-basic').blur();
    pos.onCreated();
})