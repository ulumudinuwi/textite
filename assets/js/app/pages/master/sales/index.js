var INDEX_SALES = new function(){
    this.el = {
        table : "#myTable",
        btn_status : '.btn-status',
        
    };
    this.data = {

    };
    this.method = {
        switch_status(sales_uid,e) {
            // alert(sales_uid);
            let el = $(e);
            el.html('<i></i>')
            el.prop('disabled', true);
            provider.service.api.master.sales.switch_status(sales_uid).done((res) => {
                let status = el.data('status') == 1 ? 0 : 1;
                let uid = el.data('uid');
                let new_btn = this.status_type(status,uid);
                el.replaceWith(new_btn);
                successMessage('Berhasil', res.message);
                $('#myTable').DataTable().draw();
                $('#myTable_inactive').DataTable().draw();
                // el.prop('disabled', false);
            }).fail((xhr) => {
                // console.log(xhr)
                if(xhr.status == 500) xhr.responseJSON.message = "Internal Server Error";
                errorMessage('Peringatan !',xhr.responseJSON.message);
                // el.prop('disabled', false);
            });
        },
        status_type(status,uid) {
            let btn_class = status == 1 ? 'btn-success' : 'bg-slate-400';
            let status_text = status == 1 ? 'Active' : 'Inactive';
            let button = `<button class="btn btn-xs ${btn_class} btn-status" data-status="${status}" data-uid="${uid}" onclick="INDEX_SALES.method.switch_status('${uid}',this)">${status_text}</button>`;
            return button;
        }
    };
    this.onCreate = new function(){
        this.setup = () => {
            $(INDEX_SALES.el.btn_status).on('click', (e) => {
                let el = $(e.target);
                let uid = el.data('uid');
                INDEX_SALES.method.switch_status(uid,el);
            });
        };
        this.datatable = () => {
            $(INDEX_SALES.el.table).DataTable({
                serverSide: true,
                processing: true,
                ajax: {
                    url: provider.url.api.master.sales.list_dt,
                    type: "POST",
                    data: (data) => {
                        global.method.blockElement(INDEX_SALES.el.table);
                        data.status = "1";
                    }
                },
                searchDelay: 600,
                processing: true,
                columns: [
                    {data: 'nama', name: 'nama', 
                        render: (data, type, row, meta) => {
                            console.log(row.uid)
                            let img  = `<img src="${global.assetsUrl}img/icon/sales.png" alt="">`;
                            let html = '<div class="column_nama">'+img+' <a href="'+provider.url.page.master.sales.form.replace(':uid',row.uid)+'">'+data+'</a></div>';
                            return html;
                        }
                    },
                    {data: 'email', name: 'email', 
                        render: (data, type, row, meta) => {
                            let html = '<ul class="column_kontak">';
                            html += '<li><i class="icon-envelop5"></i> '+data+'</li>';
                            html += '<li><i class="icon-phone2"></i> '+row.no_hp+'</li>';
                            html += '</ul>';
                            return html;
                        }
                    },
                    {data: 'alamat', name: 'alamat',
                        render: (data, type, row, meta) => {
                            let html = '<span class="column_alamat"><i class="icon-map5"></i>  '+data+'</span>';
                            return html;
                        }
                    },
                    {data: 'status', name: 'status', className: 'text-center',
                        render: (data, type, row, meta) => {
                            return INDEX_SALES.method.status_type(data,row.uid);
                        },
                        // orderable: false, searchable: false,
                    },
                    {data: 'uid', name: 'uid', className: 'text-center',
                        render: (data, type, row, meta) => {
                            return `<a href="${provider.url.page.master.sales.detail.replace(':uid',data)}">DETAIL</a>`;
                        },
                        orderable: false, searchable: false,
                    },
                ],
                aaSorting: [],
                drawCallback: (settings) => {
                    $(INDEX_SALES.el.table).unblock();
                }
            });

        };
    };
    this.onCreated = () => {
        INDEX_SALES.onCreate.setup();
        INDEX_SALES.onCreate.datatable();
    };
}
var INDEX_SALES_INACTIVE = new function(){
    this.el = {
        table : "#myTable_inactive",
        btn_status : '.btn-status',
        
    };
    this.data = {

    };
    this.method = {
        switch_status(sales_uid,e) {
            // alert(sales_uid);
            let el = $(e);
            el.html('<i></i>')
            el.prop('disabled', true);
            provider.service.api.master.sales.switch_status(sales_uid).done((res) => {
                let status = el.data('status') == 1 ? 0 : 1;
                let uid = el.data('uid');
                let new_btn = this.status_type(status,uid);
                el.replaceWith(new_btn);
                successMessage('Berhasil', res.message);
                $('#myTable').DataTable().draw();
                $('#myTable_inactive').DataTable().draw();
                // el.prop('disabled', false);
            }).fail((xhr) => {
                // console.log(xhr)
                if(xhr.status == 500) xhr.responseJSON.message = "Internal Server Error";
                errorMessage('Peringatan !',xhr.responseJSON.message);
                // el.prop('disabled', false);
            });
        },
        status_type(status,uid) {
            let btn_class = status == 1 ? 'btn-success' : 'bg-slate-400';
            let status_text = status == 1 ? 'Active' : 'Inactive';
            let button = `<button class="btn btn-xs ${btn_class} btn-status" data-status="${status}" data-uid="${uid}" onclick="INDEX_SALES_INACTIVE.method.switch_status('${uid}',this)">${status_text}</button>`;
            return button;
        }
    };
    this.onCreate = new function(){
        this.setup = () => {
            $(INDEX_SALES_INACTIVE.el.btn_status).on('click', (e) => {
                let el = $(e.target);
                let uid = el.data('uid');
                INDEX_SALES_INACTIVE.method.switch_status(uid,el);
            });
        };
        this.datatable = () => {
            $(INDEX_SALES_INACTIVE.el.table).DataTable({
                serverSide: true,
                processing: true,
                ajax: {
                    url: provider.url.api.master.sales.list_dt,
                    type: "POST",
                    data: (data) => {
                        global.method.blockElement(INDEX_SALES_INACTIVE.el.table);
                        data.status = '0';
                    }
                },
                searchDelay: 600,
                processing: true,
                columns: [
                    {data: 'nama', name: 'nama', 
                        render: (data, type, row, meta) => {
                            console.log(row.uid)
                            let img  = `<img src="${global.assetsUrl}img/icon/sales.png" alt="">`;
                            let html = '<div class="column_nama">'+img+' <a href="'+provider.url.page.master.sales.form.replace(':uid',row.uid)+'">'+data+'</a></div>';
                            return html;
                        }
                    },
                    {data: 'email', name: 'email', 
                        render: (data, type, row, meta) => {
                            let html = '<ul class="column_kontak">';
                            html += '<li><i class="icon-envelop5"></i> '+data+'</li>';
                            html += '<li><i class="icon-phone2"></i> '+row.no_hp+'</li>';
                            html += '</ul>';
                            return html;
                        }
                    },
                    {data: 'alamat', name: 'alamat',
                        render: (data, type, row, meta) => {
                            let html = '<span class="column_alamat"><i class="icon-map5"></i>  '+data+'</span>';
                            return html;
                        }
                    },
                    {data: 'status', name: 'status', className: 'text-center',
                        render: (data, type, row, meta) => {
                            return INDEX_SALES_INACTIVE.method.status_type(data,row.uid);
                        },
                        // orderable: false, searchable: false,
                    },
                    {data: 'uid', name: 'uid', className: 'text-center',
                        render: (data, type, row, meta) => {
                            return `<a href="${provider.url.page.master.sales.detail.replace(':uid',data)}">DETAIL</a>`;
                        },
                        orderable: false, searchable: false,
                    },
                ],
                aaSorting: [],
                drawCallback: (settings) => {
                    $(INDEX_SALES_INACTIVE.el.table).unblock();
                }
            });

        };
    };
    this.onCreated = () => {
        INDEX_SALES_INACTIVE.onCreate.setup();
        INDEX_SALES_INACTIVE.onCreate.datatable();
    };
}

$(() => {
    INDEX_SALES.onCreated();
    INDEX_SALES_INACTIVE.onCreated();
})