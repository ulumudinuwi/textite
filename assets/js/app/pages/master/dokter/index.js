var INDEX_DOKTER = new function(){
    this.el = {
        table : "#myTable",
        btn_status : '.btn-status',
        
    };
    this.data = {

    };
    this.method = {
        switch_status(sales_uid,e) {
            // alert(sales_uid);
            let el = $(e);
            el.html('<i></i>')
            el.prop('disabled', true);
            provider.service.api.master.dokter.switch_status(sales_uid).done((res) => {
                let status = el.data('status') == 1 ? 0 : 1;
                let uid = el.data('uid');
                let new_btn = this.status_type(status,uid);
                el.replaceWith(new_btn);
                successMessage('Berhasil', res.message);
                // el.prop('disabled', false);
            }).fail((xhr) => {
                // console.log(xhr)
                if(xhr.status == 500) xhr.responseJSON.message = "Internal Server Error";
                errorMessage('Peringatan !',xhr.responseJSON.message);
                // el.prop('disabled', false);
            });
        },
        status_type(status,uid) {
            let btn_class = status == 1 ? 'btn-success' : 'bg-slate-400';
            let status_text = status == 1 ? 'Active' : 'Inactive';
            let button = `<button class="btn btn-xs ${btn_class} btn-status" data-status="${status}" data-uid="${uid}" onclick="INDEX_DOKTER.method.switch_status('${uid}',this)">${status_text}</button>`;
            return button;
        }
    };
    this.onCreate = new function(){
        this.setup = () => {
            $(INDEX_DOKTER.el.btn_status).on('click', (e) => {
                let el = $(e.target);
                let uid = el.data('uid');
                INDEX_DOKTER.method.switch_status(uid,el);
            });
        };
        this.datatable = () => {
            $(INDEX_DOKTER.el.table).DataTable({
                serverSide: true,
                processing: true,
                ajax: {
                    url: provider.url.api.master.dokter.list_dt,
                    type: "POST",
                    data: (data) => {
                        global.method.blockElement(INDEX_DOKTER.el.table);
                        // data.nama = "ujang";
                    }
                },
                searchDelay: 600,
                processing: true,
                columns: [
                    {data: 'no_member', name: 'no_member'},
                    {data: 'nama', name: 'nama', 
                        render: (data, type, row, meta) => {
                            console.log(row.uid)
                            let img  = `<img src="${global.assetsUrl}img/icon/dokter.png" alt="">`;
                            let html = '<div class="column_nama">'+img+' <a href="'+provider.url.page.master.dokter.form.replace(':uid',row.uid)+'">'+data+'</a></div>';
                            return html;
                        }
                    },
                    {data: 'email', name: 'email', 
                        render: (data, type, row, meta) => {
                            let html = '<ul class="column_kontak">';
                            html += '<li><i class="icon-envelop5"></i> '+data+'</li>';
                            html += '<li><i class="icon-phone2"></i> '+row.no_hp+'</li>';
                            html += '</ul>';
                            return html;
                        }
                    },
                    {data: 'alamat', name: 'alamat',
                        render: (data, type, row, meta) => {
                            let html = '<span class="column_alamat"><i class="icon-map5"></i>  '+data+'</span>';
                            return html;
                        }
                    },
                    {data: 'status', name: 'status', className: 'text-center',
                        render: (data, type, row, meta) => {
                            return INDEX_DOKTER.method.status_type(data,row.uid);
                        }
                    },
                ],
                aaSorting: [],
                drawCallback: (settings) => {
                    $(INDEX_DOKTER.el.table).unblock();
                }
            });

        };
    };
    this.onCreated = () => {
        INDEX_DOKTER.onCreate.setup();
        INDEX_DOKTER.onCreate.datatable();
    };
}

$(() => {
    INDEX_DOKTER.onCreated();
})