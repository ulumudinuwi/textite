var FORM_DOKTER = new function(){
    this.el = {
        'form' : '#myForm',
        'uid' : '[name="uid"]',
    };
    this.data = {

    };
    this.method = {
        store(formData) {
            provider.service.api.master.dokter.store(formData).done((res) => {
                let message = res.message;
                console.log(message)
                successMessage('Berhasil',message);
                window.location.href = provider.url.page.master.dokter.index;
            }).fail((xhr) => {
                console.log(xhr);
                $.unblockUI();
                global.method._throw_error.form_error(xhr);
                $('button').prop('disabled',false);
            })
        },
        update(formData) {
            provider.service.api.master.dokter.update(formData).done((res) => {
                let message = res.message;
                console.log(message)
                successMessage('Berhasil',message);
                window.location.href = provider.url.page.master.dokter.index;
            }).fail((xhr) => {
                console.log(xhr);
                $.unblockUI();
                global.method._throw_error.form_error(xhr);
                $('button').prop('disabled',false);
            })
        },
    };
    this.onCreate = new function(){
        this.setup = () => {
            $(FORM_DOKTER.el.form).on('submit', (e) => {
                e.preventDefault();
                // console.log($('#plafon').val())
                $('button').prop('disabled',true);
                global.method.blockUI();
                global.method.clear_error(e.target);
                let el_form = $(FORM_DOKTER.el.form);
                let formData = new FormData(el_form[0]);
                let uid = $('[name="uid"]').val();
                if(!uid) {
                    FORM_DOKTER.method.store(formData);
                }else{
                    FORM_DOKTER.method.update(formData);
                }
            })
        };
    };
    this.onCreated = () => {
        FORM_DOKTER.onCreate.setup();
    };
}

$(() => {
    FORM_DOKTER.onCreated();
    $("#plafon").autoNumeric('init', {
        aSep: '.',
        aDec: ',',
        aSign: 'Rp. ',
        pSign: 'p',
    });
})

$('[name="st_diskon_mou"]').on('click', function(){
    let val = $(this).val();
    if (val == 1) {
        $('[name="diskon_mou"]').prop('required', true);
        $('[name="diskon_mou"]').prop('readonly', false);
    }else{
        $('[name="diskon_mou"]').prop('required', false);
        $('[name="diskon_mou"]').prop('readonly', true);
        $('[name="diskon_mou"]').val('0');
    }
});

$('[name="tgl_jatuh_tempo"]').on('keyup', function(){
    let thisVall = $(this).val(),
        mulaiVall = $('[name="tgl_mulai_mou"]').val();
        if (mulaiVall == '') {
            warningMessage('Peringatan', 'Isi Tanggal Mulai MOU');
            return $('[name="tgl_jatuh_tempo"]').val('');
        }

        if (thisVall <= mulaiVall) {
            warningMessage('Peringatan', 'Tanggal Jatuh Tempo MOU tidak boleh kurang dari tanggal mulai');
            return $('[name="tgl_jatuh_tempo"]').val('');
        }
});
