const cariMember = {
    name: "carimember",
    template: "#carimember",
    props: {
        items: {
            type: Array,
            required: false,
            default: () => []
        },
        isAsync: {
            type: Boolean,
            required: false,
            default: false
        },
        placeHolder : '',
    },
  
    data() {
        return {
            isOpen: false,
            results: [],
            search: "",
            isLoading: false,
            arrowCounter: 0
        };
    },
    methods: {
        onChange() {
            // Let's warn the parent that a change was made
            this.$emit("input", this.search);
            // Is the data given by an outside ajax request?
            if (this.isAsync) {
            this.isLoading = true;
            } else {
            // Let's search our flat array
            this.filterResults();
            this.isOpen = true;
            }
        },
  
        filterResults() {
            // first uncapitalize all the things
            this.results = this.items.filter(item => {
                return item.nama_dokter.toLowerCase().indexOf(this.search.toLowerCase()) > -1;
            });
            // this.results = this.items; 
        },
        setResult(result) {
            if(result.type == "search_null"){
                return false;
            }
            this.search = result.nama_dokter.toUpperCase();
            this.isOpen = false;
            this.$emit("setresult", result);
        },
        onArrowDown() {
            if (this.arrowCounter < this.results.length - 1) {
                this.arrowCounter = this.arrowCounter + 1;
            }else{
                this.arrowCounter = 0;
            }
        },
        onArrowUp() {
            if (this.arrowCounter > 0) {
                this.arrowCounter = this.arrowCounter - 1;
            }else if(this.arrowCounter == 0){
                this.arrowCounter = (this.results.length - 1) ;
            }
        },
        onEnter() {
            // this.search = this.results[this.arrowCounter].nama_dokter.toUpperCase();
            this.setResult(this.results[this.arrowCounter]);
            this.isOpen = false;
            this.arrowCounter = -1;
        },
        handleClickOutside(evt) {
            if (!this.$el.contains(evt.target)) {
            this.isOpen = false;
            this.arrowCounter = -1;
            }
        }
    },
    watch: {
        items: function(val, oldValue) {
            // actually compare them
            if (val.length !== oldValue.length) {
            this.results = val.nama_dokter;
            this.isLoading = false;
            }
        }
    },
    mounted() {
        document.addEventListener("click", this.handleClickOutside);
    },
    destroyed() {
        document.removeEventListener("click", this.handleClickOutside);
    }
};

const modalDiskon = {
    template: '#modal-diskon',
    props: {
        param: '',
    },
    data() {
        return {
            disc: 0,
        }
    },
    methods: {
        close() {
            this.$emit("close");
        },
        save() {
            if(this.disc <= 0){
                Swal.fire({
                    type: 'error',
                    title: 'Tidak boleh 0',
                });
            }else if(this.disc > 100){
                Swal.fire({
                    type: 'error',
                    title: 'Maksimal 100 %',
                })
            }else{
                let result = {
                    i : this.param.i,
                    disc : this.disc, 
                    mustApprove: this.getStatus(),
                };
                this.$emit("save",result);
            }
        },
        getStatus(){
            let status = '';
            if(this.disc >= 0 && this.disc < 25){
                status = 'without_approve';
            }else if(parseInt(this.disc) === 25){
            //}else if(this.disc >= 21 && this.disc <= 25){
                status = 'with_manager';
            }else if(this.disc > 25){
                status = 'with_direktur';
            }

            return status;
        }
    },
    created: function () {
        this.disc = this.param.disc;
    },
};

const modalHarga = {
    template: '#modal-harga',
    props: {
        param: '',
    },
    data() {
        return {
            harga: 0,
        }
    },
    methods: {
        close() {
            this.$emit("close");
        },
        save() {
            let result = {
                i : this.param.i,
                harga : this.harga, 
                mustApprove: this.getStatus(),
            };
            this.$emit("save",result);
        },
        getStatus(){
            let status = 'Harga Penawaran';

            return status;
        }
    },
    created: function () {
        this.harga = this.param.harga;
    },
};


var VM_POS = new Vue({
    el: '#pos_menu',
    components: {
        carimember: cariMember,
        modaldiskon: modalDiskon,
        modalharga: modalHarga,
    },
    data : {
        page : 1,
        modal : {
            'diskon' : {
                open: false,
                param: '',
            }, 
            'harga' : {
                open: false,
                param: '',
            }, 
        },
        detail_member : false, 
        barang: {
            'total_page': 0,
            'current_page': 0,
            'list_barang': [],
            'more_page': true,
            'isLoading': false,
            'q': '',
        },
        list_person : [],
        selected_person : {
            'nama_member' : '',
            'nama_sales' : '',
        },
        person_search : '',
        tgl_transaksi_label : '',
        cart: {
            'internal_used' : false,
            'member_uid' : '',
            'sales_uid' : '',
            'list_barang' : [],
            'tgl_transaksi' : '',
            'total_harga' : '',
        },
       
        member_bio : {
                        'alamat_dokter': "-",
                        'alamat_klinik_dokter': '-',
                        'email_dokter': "-",
                        'id': 1,
                        'ktp_dokter': "",
                        'nama_dokter': "Umum",
                        'nama_klinik_dokter': null,
                        'no_hp_dokter': "-",
                        'no_member': "JK-00000001",
                        'no_tlp_klinik_dokter': null,
                        'npwp_dokter': "",
                        'plafon_dokter': "0.00",
                        'point_dokter': 680,
                        'selected': true,
                        'text': "Umum",
                        'uid_dokter': "a21b29a1-fea8-4df5-bd30-bb23c4065aa5",
                     },
        sales_bio : {
                        'alamat_sales': "-",
                        'email_sales': "-",
                        'id': "59ed3358-8b22-4b9f-a2d9-7ced3def3041",
                        'no_hp_sales': "-",
                        'no_ktp_sales': "-",
                        'selected': true,
                        'text': "Kasir",
                        'tgl_bergabung_sales': "2001-01-01",
                    },
        master_bank : [],
        master_edc : [],
        options: {
            member: [],
            sales: [],
        },
        maxHeight: {
            type: String,
            default: "1000px"
        },
        selected_item: [],   
        inputBonus: { status: 0, fromBarang: null },     
    },
    methods : {
        get_list_barang(loadmore) {
            this.barang.isLoading = true;
            this.barang.current_page++;
            if(!loadmore){
                this.barang.current_page = 1;
            }
            axios({
                method: 'post',
                url: `${this.global_url.api.sales.pos.get_barang.replace(':page',this.barang.current_page)}&q=${this.barang.q}`,
                onUploadProgress: function(progressEvent){
                    let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                    global.method.vueBlockElement('#barang-wrap',percentCompleted+' %');
                },
            }).then(response => {
                this.barang.current_page = response.data.currentPage;
                this.barang.total_page = response.data.totalPage;
                if(loadmore){
                    if(this.barang.more_page){
                        let old_barang = this.barang.list_barang;
                        let new_barang = old_barang.concat(response.data.results);
                        this.barang.more_page = response.data.morePage;
                        this.barang.isLoading = false;
                        this.barang.list_barang = new_barang; 
                    }
                }else{
                    this.barang.more_page = response.data.morePage;
                    this.barang.list_barang = response.data.results;
                    this.barang.isLoading = false;
                }
                $('#barang-wrap').unblock();
            }).catch(xhr => {
                this.barang.isLoading = true;
                $('#barang-wrap').unblock();
            })
        },
        get_list_person() {
            axios.post(this.global_url.api.sales.pos.get_member)
            .then(response => {
                this.list_person = response.data;
            }).catch(xhr => {
            })
        },
        get_master_bank() {
            axios.post(this.global_url.api.master.bank.list_data)
            .then(response => {
                this.master_bank = response.data.list;
            }).catch(xhr => {
                console.log(xhr)
            })
        },
        get_master_edc() {
            axios.post(this.global_url.api.master.mesin_edc.list_data)
            .then(response => {
                this.master_edc = response.data.list;
            }).catch(xhr => {
                console.log(xhr)
            })
        },
        getLabel (item) {
            return item.name
        },
        getSearch (updatePerson) {
            this.selected_person.nama_member = updatePerson.nama_dokter;

            this.cart.member_uid = updatePerson.uid_dokter;
            this.member_bio = updatePerson;
        },
        getSearchSales (updateSales) {
            this.selected_person.nama_sales = updateSales.text;

            this.cart.sales_uid = updateSales.id;
            this.sales_bio = updateSales;
        },
        total_per_barang(i){
            let barang = this.cart.list_barang[i];
            let harga_persatu = barang.harga_penawaran ? barang.harga_penawaran : barang.harga_persatu;
            let total_h = barang.qty * harga_persatu;
            let diskon = (barang.disc / 100) * total_h;
            let tt = total_h - diskon;
            let diskon_plus = (barang.disc_plus / 100) * tt;
            let totalHarga = barang.harga_penawaran ? total_h - diskon_plus : total_h - diskon - diskon_plus;
            this.cart.list_barang[i].total_harga = totalHarga;
            console.log(totalHarga);
            return totalHarga;
        },
        addTomodal(i){
            let barang = this.barang.list_barang[i],
                uid = barang.uid_barang;

                let getBarangbyid = base_url+'api/master/barang/get_data?uid='+uid;

                $.getJSON(getBarangbyid, function(data, status) {
                    if (status === 'success') {
                        data = data.data;
                        if (data) {
                            $('#detail_kode').html(data.kode ? data.kode : '-');
                            $('#detail_nama').html(data.nama ? data.nama : '-');
                            $('#detail_deskripsi').html(data.deskripsi ? data.deskripsi : '-');
                            $('#detail_jenis').html(data.jenis ? data.jenis : '-');
                            $('#detail_pabrik').html(data.pabrik ? data.pabrik : '-');
                            $('#detail_satuan_pembelian').html(data.satuan_pembelian ? data.satuan_pembelian : '-');
                            $('#detail_harga_penjualan').html(data.harga_penjualan ? data.harga_penjualan : '-');
                            $('#detail_harga_minimum').html(data.harga_minimum ? data.harga_minimum : '-');
                            $('#detail_satuan_penggunaan').html(data.satuan_penggunaan ? data.satuan_penggunaan : '-');
                            $('#detail_isi_satuan_penggunaan').html(data.isi_satuan_penggunaan ? data.isi_satuan_penggunaan : '-');
                        }
                    }
                });

        },
        addTocart(i){
            let barang = this.barang.list_barang[i];
            if(this.is_selectedItem(barang.uid_barang) && this.inputBonus.status == 0){
                let found_item = this.cart.list_barang.findIndex((val)=>{
                    return val.uid == barang.uid_barang && val.is_bonus == 0;
                });

                if(found_item !== -1) {
                    Swal.fire({
                        type: 'warning',
                        title: 'Barang sudah dipilih.',
                    });
                    return;
                }
                //return this.removeCart(found_item);
            };
            if(barang.stock == 0){
                Swal.fire({
                    type: 'error',
                    title: 'Barang belum tersedia',
                });
                return false;
            }
            // barang.active = true;
            this.selected_item.push({uid: barang.uid_barang});
            // this.barang.list_barang[i].stock--; 
            let n_item = {
                'uid' : barang.uid_barang,
                'id' : barang.id_barang,
                'nama' : barang.nama,
                'harga_persatu' : barang.harga,
                'harga_minimum' : barang.harga_minimum,
                'stock' : parseInt(barang.stock),
                'qty' : 1,
                'disc' : 0,
                'disc_plus' : 0,
                'total_harga' : 0,
                'harga_penawaran' : 0,
                'i_b' : i,
                'is_bonus' : this.inputBonus.status,
                'bonus_from_barang' : this.inputBonus.fromBarang,
                'mustApprove' : 'without_approve',
            }

            /*if(this.inputBonus.status == 1) {
                n_item.bonus_from_barang = this.inputBonus.fromBarang;
                n_item.harga_persatu = 0;
            }*/
            if(this.inputBonus.status == 1) {
                let searchIndex = this.cart.list_barang.findIndex((val, index) => {
                    if(val.is_bonus == 0) {
                        return this.inputBonus.fromBarang.uid == val.uid;
                    }
                });
                this.cart.list_barang.splice(searchIndex + 1, 0, n_item);
            } else this.cart.list_barang.push(n_item);
        },
        removeCart(barang){
            let unselected_item = this.selected_item.findIndex((val, index)=>{
                return val.uid == barang.uid;
            });
            this.selected_item.splice(unselected_item,1);

            let searchIndex = this.cart.list_barang.findIndex((val, index) => {
                return val.uid == barang.uid;
            });
            this.cart.list_barang.splice(searchIndex,1);
        },
        removeMulti(i) {
            let barang = this.cart.list_barang[i];
            let dataArray = [barang];
            if(barang.is_bonus == 0) {
                this.cart.list_barang.findIndex((val, index) => {
                    if(val.is_bonus == 1) {
                        if(val.bonus_from_barang) {
                            if(barang.uid == val.bonus_from_barang.uid) {
                                dataArray.push(this.cart.list_barang[index]);
                            }
                        }
                    }
                });
            }
            for (var j = 0; j < dataArray.length; j++) {
                this.removeCart(dataArray[j]);
            }
        },
        addBonusCart(i){
            let barang = this.cart.list_barang[i];
            $('.done-bonus-btn').addClass('hide');
            $('.bonus-btn').addClass('hide');
            $('.label_bonus_barang').removeClass('hide');
            $('#nama_barang').html(barang.nama);

            $(`#done-bonus-btn_${i}`).removeClass('hide');
            $(`#bonus-btn_${i}`).addClass('hide');
            //$(`#clear-btn_${i}`).addClass('hide');
            $('.btn-block').prop('disabled', true);

            this.inputBonus = {
                status: 1,
                fromBarang: barang,
            };
        },

        doneBonusCart(i){
            let barang = this.cart.list_barang[i];
            $('.bonus-btn').removeClass('hide');
            $('.label_bonus_barang').addClass('hide');
            $('#nama_barang').html(barang.nama);

            $(`#done-bonus-btn_${i}`).addClass('hide');
            $(`#bonus-btn_${i}`).removeClass('hide');
            $('.btn-block').prop('disabled', false);
            /*
            this.cart.list_barang.findIndex((val, index) => {
                if(val.is_bonus == 1) {
                    if(val.bonus_from_barang) {
                        if(barang.uid == val.bonus_from_barang.uid) {
                            $(`#clear-btn_${i}`).addClass('hide');
                        }
                    }
                }else{
                    $(`#clear-btn_${i}`).removeClass('hide');
                }
            });*/

            this.inputBonus = {
                status: 0,
                fromBarang: null,
            };
        },
        is_selectedItem(uid){
            let selected_item = this.selected_item.find((val, index)=>{
                return val.uid == uid;
            });
            
            return selected_item ? true : false;
        },
        changeDiskonPerItem(i){
            let index = i;
            let param = {
                i: index,
                disc: this.cart.list_barang[i].disc,
            };
            this.modal.diskon.param = param;
            this.modal.diskon.open = true;
        },
        changeHarga(i){
            let index = i;
            let param = {
                i: index,
                harga: this.cart.list_barang[i].total_harga,
            };
            this.modal.harga.param = param;
            this.modal.harga.open = true;
        },
        updateDiskonPerItem(result) {
            this.cart.list_barang[result.i].disc = result.disc;
            this.cart.list_barang[result.i].mustApprove = result.mustApprove;
            this.modal.diskon.open = false;
        },
        updateHargaPerItem(result) {
            if(result.harga < this.cart.list_barang[result.i].harga_minimum){
                Swal.fire({
                    type: 'error',
                    title: 'Harga Penawaran terlalu kecil',
                });
                return false;
            }
            this.cart.list_barang[result.i].harga_penawaran = result.harga;
            this.cart.list_barang[result.i].mustApprove = result.mustApprove;
            this.modal.harga.open = false;
            console.log(this.cart.list_barang[result.i]);
        },
        qtyDecrease(i){
            if(this.cart.list_barang[i].qty > 1){
                this.cart.list_barang[i].qty--;
                this.getPromo(i);
            }
        },
        qtyIncrease(i){
            let item_cart = this.cart.list_barang[i];
            if(item_cart.stock > item_cart.qty){
                this.cart.list_barang[i].qty++;
                this.getPromo(i);
            }
        },
        getPromo(i){
            let item_cart = this.cart.list_barang[i];
            $.getJSON(base_url+'api/master/promo/get_data/'+item_cart.id, function(res, status) {
                if (status === 'success') {
                    data = res.data;
                    if(data){
                        if (item_cart.qty >= data.qty_promo) {
                            item_cart.old_harga_persatu = typeof item_cart.old_harga_persatu === 'undefined' ? item_cart.harga_persatu : item_cart.old_harga_persatu;
                            item_cart.harga_persatu = data.harga_jual;
                        } else {
                            item_cart.old_harga_persatu = typeof item_cart.old_harga_persatu === 'undefined' ? item_cart.harga_persatu : item_cart.old_harga_persatu;
                            item_cart.harga_persatu = item_cart.old_harga_persatu;
                        }
                    }
                }
            });
            this.cart.list_barang[i] = item_cart;
        },
        updateQty(evt,i){
            this.cart.list_barang[i].qty = evt.target.value.replace(/^0+(?=\d)/,'');
            let cart_item = this.cart.list_barang[i];
            let barang_selected = this.barang.list_barang[cart_item.i_b];
            if(cart_item.qty == '') this.cart.list_barang[i].qty = 0;
            if(parseInt(cart_item.qty) > parseInt(cart_item.stock)){
                this.cart.list_barang[i].qty = 1;
                Swal.fire({
                    type: 'error',
                    title: 'Jumlah order melebihi stock',
                })
            }

        },
        updateDiskonPlus(evt, i){
            let total_harga = this.cart.list_barang[i].total_harga,
                disc_plus = this.cart.list_barang[i].disc_plus;

            if(disc_plus > 100){
                Swal.fire({
                    type: 'error',
                    title: 'Maksimal 100 %',
                });
                this.cart.list_barang[i].disc_plus = "";
            }

            
        },
        btnPaycheck(){

        },
        saveToDraf(){
            let data = {
                'json_data' : this.cart,
            };
            let formData = new FormData();
            formData.append('json_data', JSON.stringify(this.cart));
            // let config = {
            //     onUploadProgress: function(progressEvent) {
            //         let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
            //         global.method.vueBlockUI(percentCompleted+' %');
            //     }
            // };
            // axios.post(this.global_url.api.sales.pos.save_temporary, formData, config)
            axios({
                method: 'post',
                url: this.global_url.api.sales.pos.save_temporary,
                data: formData,
                onUploadProgress: function(progressEvent){
                    let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                    global.method.vueBlockUI(percentCompleted+' %');
                },
            })
            .then(response => {
                Swal.fire({
                    type: 'success',
                    title: response.data.message,
                });
                $.unblockUI();
            }).catch(xhr => {
                $.unblockUI();
            })
        },
        stockTersedia(uid_barang){
            let current_stock = this.barang.list_barang.find((val) => {
                return val.uid_barang == uid_barang;
            });
            current_stock = current_stock.stock;
            
            /*let item_cart = this.cart.list_barang.find((val) => {
                return val.uid == uid_barang;
            });*/
    
            /*if(item_cart != undefined){
                return current_stock - item_cart.qty;
            }else{
                return current_stock;
            }*/

            let bundleStock = 0;
            this.cart.list_barang.find((val) => {
                if(val.uid == uid_barang) {
                    bundleStock += parseFloat(val.qty);
                }
            });
            return current_stock - bundleStock;
        },
        onPay(){
            let plafon = this.member_bio.plafon_dokter != undefined ? this.member_bio.plafon_dokter : '0';
            let total_belanjaan = this.cart.total_harga; 
            let member_bio = this.member_bio; 
            let sales_bio = this.sales_bio; 
            if(this.cart.list_barang.length == 0 && total_belanjaan == 0){
                Swal.fire({
                    type: 'error',
                    title: 'Belum memilih barang apapun.',
                });
                return false;
            }
            // if(!this.cart.member_uid && !this.cart.sales_uid){
            //     Swal.fire({
            //         type: 'error',
            //         title: 'Belum memilih member',
            //     });
            //     return false;
            // }
            console.log(member_bio);
            console.log(sales_bio);

            this.cart.member_uid = member_bio.uid_dokter;
            this.cart.sales_uid = sales_bio.id;
            let filter_cart = this.cart.list_barang.filter(function(item) {
                return item.qty >= 1;
            });
            this.cart.list_barang = filter_cart;
            if(this.cart.list_barang.length == 0){
                Swal.fire({
                    type: 'error',
                    title: 'Belum memilih barang apapun / Qty 0',
                });
                return false;
            }
            this.page = 2;
            VM_PAYMENT.getData_VM_POS();
        },
        nextStep(){
            this.page = 2;
        },
        backStep(){
            this.page = 1;
        },
        openDetail(){
            if(this.member_bio.length == 0) {
                Swal.fire({
                    type: 'error',
                    title: 'Member belum dipilih',
                });

                return false;
            }
            if(this.sales_bio.length == 0) {
                Swal.fire({
                    type: 'error',
                    title: 'Sales belum dipilih',
                });

                return false;
            }
            if(this.detail_member == false){
                this.detail_member = true;
            }else{
                this.detail_member = false;
            }
        },
        isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
                evt.preventDefault();
            } else {
                return true;
            }
        },
        onSearchMember(search, loading) {
            loading(true);
            this.searchMember(loading, search, this);
        },
        searchMember: _.debounce((loading, search, vm) => {
            // fetch(
                //   `https://api.github.com/search/repositories?q=${escape(search)}`
                // ).then(res => {
                    //   res.json().then(json => (vm.options = json.items));
                    //   loading(false);
                    // });
                    axios.get(vm.global_url.api.sales.pos.search_member.replace(':q',search))
                    .then(response => {
                        vm.options.member = response.data;
                        loading(false);
            }).catch(xhr => {
                loading(false);
            })
        }, 350),
        setSelectedMember(value) {
            this.getSearch(value);
        },
        sendData_VM_PAYMENT(){
            VM_PAYMENT.page = this.page;
            VM_PAYMENT.cart = this.cart;
            VM_PAYMENT.member_bio = this.member_bio;
            VM_PAYMENT.sales_bio = this.sales_bio;
            VM_PAYMENT.master_bank = this.master_bank;
            VM_PAYMENT.master_edc = this.master_edc;
        },
        onSearchBarang(){
            this.get_list_barang(false);
        }
    },
    computed : {
        total_belanja() {
            let total = 0;
            if(this.cart.list_barang.length > 0){
                for(item of this.cart.list_barang){
                    total += parseFloat(item.total_harga);
                }
            }            
            this.cart.total_harga = total;
            return total;
        },
    },
    created () {
        this.get_list_barang(false);
        this.get_list_person();
        this.get_master_bank();
        this.get_master_edc();
        this.tgl_transaksi_label = moment().format('MMMM Do YYYY | h:mm:ss');
        this.cart.tgl_transaksi = moment().format('YYYY-MM-DD HH:mm:ss'); 
        VM_PAGE_HEADER.title = '<i class="icon-med-i-billing"></i> Points Of Sales';
    }
});

var VM_PAYMENT = new Vue({
    el: '#payment_menu',
    data : {
        page : 1,
        cart: {
            'internal_used' : false,
            'member_uid' : '',
            'sales_uid' : '',
            'list_barang' : [],
            'tgl_transaksi' : '',
            'total_harga' : '',
        },
        member_bio : [],
        sales_bio : [],
        pembayaran : {
            total : '',
            type_disc: 'persen',
            disc  : 0,
            piutang : false,
            point_didapat : '',
            grand_total : '',
            metode_pembayarans : [{
                'cara_bayar' : '',
                'receipt_no' : '',
                'bank_id': '',
                'mesin_edc_id': '',
                'nominal': 0,
            }],
            is_kurir : false, 
            is_free : false,
            ekspedisi: '',
            ongkir: 0,
            total_pembayaran : 0,
            sisa_pembayaran : 0,
            mustApprove : '',
        },
        master_cara_bayar: [
            {id: 'kartu_kredit', text: 'KARTU KREDIT'},
            {id: 'kartu_debit', text: 'KARTU DEBIT'},
            {id: 'transfer', text: 'TRANSFER'},
            {id: 'tunai', text: 'TUNAI'},
        ],
        master_ekspedisi: [
            {id: 'JNE', text: 'JNE'},
            {id: 'POS Indonesia', text: 'POS Indonesia'},
            {id: 'J&T', text: 'J&T'},
            {id: 'SiCepat', text: 'SiCepat'},
            {id: 'Ninja Express', text: 'Ninja Express'},
            {id: 'TiKi', text: 'TiKi'},
            {id: 'First Logistics', text: 'First Logistics'},
            {id: 'Indah Logistik', text: 'Indah Logistik'},
            {id: 'Wahana Logistik', text: 'Wahana Logistik'},
            {id: 'Pandu Logistik', text: 'Pandu Logistik'},
            {id: 'RPX', text: 'RPX'},
            {id: 'Cahaya Logistik', text: 'Cahaya Logistik'},
            {id: 'GO-SEND', text: 'GO-SEND'},
            {id: 'Grab Express', text: 'Grab Express'},
        ],
        master_bank : [],
        master_edc : [],      
        sisa_pembayaran : '',  
        type_diskon: '',
        uang_diterima: 0,
        batas_ongkir: 0,
        batas_pembayaran : [],
    },
    methods: {
        onBatal(){
            this.page = 1;
            VM_POS.page = 1;
        },
        changeTypeDiskon(){
            let setDisc = new Promise((resolve, reject)=>{
                this.pembayaran.disc = 0;
                resolve('success');
            });
            //1 percent
            //0 nominal
            setDisc.then((data)=>{
                if(this.pembayaran.type_disc != "persen"){
                    this.pembayaran.type_disc = "persen";
                    this.type_diskon = this.setupDefault.percent;
                }else{
                    this.pembayaran.type_disc = "nominal";
                    this.type_diskon = this.setupDefault.rupiah;
                    this.type_diskon.maximumValue = this.pembayaran.grand_total;
                }
            });
        },
        getData_VM_POS(){
            //set pembayaran
            this.pembayaran.total = VM_POS.cart.total_harga;
            this.pembayaran.disc = 0;
            this.pembayaran.piutang = 0;

            this.page = VM_POS.page;
            this.cart = VM_POS.cart;
            this.member_bio = VM_POS.member_bio;
            this.sales_bio = VM_POS.sales_bio;
            this.master_bank = this.mapOption(VM_POS.master_bank);
            this.master_edc = this.mapOption(VM_POS.master_edc);
        },
        total_per_barang(i){
            let barang = this.cart.list_barang[i];
            let harga_persatu = barang.harga_penawaran ? barang.harga_penawaran : barang.harga_persatu;
            let total_h = barang.qty * harga_persatu;
            let diskon = (barang.disc / 100) * total_h;
            let tt = total_h - diskon;
            let diskon_plus = (barang.disc_plus / 100) * tt;
            let totalHarga = barang.harga_penawaran ? total_h - diskon_plus : total_h - diskon - diskon_plus;
            this.cart.list_barang[i].total_harga = totalHarga;
            console.log(totalHarga);
            return totalHarga;
        },
        addPembayaran(){
            let n_p = {
                'cara_bayar' :'',
                'receipt_no' :'',
                'bank_id': '',
                'mesin_edc_id': '',
                'nominal': 0,
            };

            let batas_baru = {
                currencySymbol: 'Rp. ',
                minimumValue: 0,
                emptyInputBehavior: 'min',
                currencySymbolPlacement: 'p',
                decimalCharacter: ',',
                decimalPlacesShownOnFocus: 2,
                digitGroupSeparator: '.',
                minimumValue: '0',
                maximumValue: this.sisapembayaran,
            };
            this.batas_pembayaran.push(batas_baru);
            this.pembayaran.metode_pembayarans.push(n_p);
        },
        mapOption(array){
            if(typeof array == 'object'){
                let change = array.map(function(item){
                    return {
                        id: item.id,
                        text: item.nama,
                    }
                });
                return change;
            }
        },
        removePembayaran(i){
            if(this.pembayaran.metode_pembayarans.length == 1)return false;
            this.pembayaran.metode_pembayarans.splice(i,1);
            this.batas_pembayaran.splice(i,1);
        },
        checkDiskon(){
            if(this.pembayaran.type_disc == "persen"){
                this.pembayaran.mustApprove = 'without_approve';
                /*if(this.pembayaran.disc >= 0 && this.pembayaran.disc < 25){
                    this.pembayaran.mustApprove = 'without_approve';
                }else if(this.pembayaran.disc == 25){
                //}else if(this.pembayaran.disc >= 21 && this.pembayaran.disc <= 25){
                    this.pembayaran.mustApprove = 'with_manager';
                }else if(this.pembayaran.disc > 25){
                    this.pembayaran.mustApprove = 'with_direktur';
                }*/
            }
        },
        validasiPembayaran(){
            let filter = this.pembayaran.metode_pembayarans.filter((item)=>{
                if(item.cara_bayar == 'tunai'){
                    if(item.cara_bayar == 'tunai' && item.nominal == 0){
                        return true;
                    }else{
                        return false;
                    } 
                // }else if(item.cara_bayar != 'tunai' || item.cara_bayar == ''){
                //     if(item.nominal == 0 || !item.receipt_no || !item.bank_id || !item.mesin_edc_id){
                //         return true;
                //     }else{
                //         return false;
                //     } 
                // }
                }else if(item.cara_bayar != 'tunai' || item.cara_bayar == ''){
                    if(item.nominal == 0 || !item.bank_id ){
                        return true;
                    }else{
                        return false;
                    } 
                }
            });
            if(this.pembayaran.grand_total == 0){
                return true;
            }
            if(!this.pembayaran.piutang && filter.length > 0){
                return false;
            }else{
                return true;
            }
        },
        validasiTotalPembayaran(){
            if(this.pembayaran.total_pembayaran > this.pembayaran.grand_total){
                return false;
            }
            return true;
        },
        validasiKurir(){
            if(this.pembayaran.is_kurir){
                if(!this.pembayaran.ekspedisi) return false;
            }
            return true;
        },
        onProcess(){
            let _process = new Promise((resolve, reject) => {
                this.checkDiskon();
                let _vtp = this.validasiTotalPembayaran();
                //let _vp = this.validasiPembayaran();
                let _vk = this.validasiKurir();
                

                if(!_vk){
                    Swal.fire({
                        type: 'error',
                        title: 'Ekspedisi belum dipilih',
                    });
                }
                
                /*if(!_vp){
                    Swal.fire({
                        type: 'error',
                        title: 'Metode pembayaran belum lengkap',
                    });
                }*/

                if(!_vtp){
                    Swal.fire({
                        type: 'error',
                        title: 'Pembayaran melebihi total harga yang dibeli',
                    });
                }

                if(_vtp /*&& _vp*/ && _vk) resolve({
                    'status': true,
                });

                if(_vtp /*|| _vp*/ || _vk) reject({
                    'status': false,
                });
            });
            
            _process.then(()=>{         
                Sweet.fire({
                    title: 'Transaksi akan dilanjut?',
                    text: "Pastikan informasi yang ada masukan sudah benar !",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '<b><i class="icon-checkmark4"></i></b> Ya',
                    cancelButtonText: '<b><i class="icon-cancel-square"></i></b> Tidak',
                    // reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        global.method.vueBlockUI();
                        let formData = new FormData();
                        formData.append('cart', JSON.stringify(this.cart));
                        formData.append('transaksi', JSON.stringify(this.pembayaran));
                        formData.append('member_bio', JSON.stringify(this.member_bio));
                        formData.append('sales_bio', JSON.stringify(this.sales_bio));
                        formData.append('status_pembayaran', '1');
                        axios.post(this.global_url.api.sales.pos.proccess_order, formData)
                        .then(response => {
                            $.unblockUI();
                            Swal.fire({
                                type: 'success',
                                title: response.data.message,
                            });
                            console.log(response);
                            setTimeout(()=>{
                                window.location.href = base_url+"sales/pos/detail_transaksi?uid="+response.data.uid;
                            },2000)
                        }).catch(xhr => {
                            Swal.fire({
                                type: 'error',
                                title: 'Terjadi kesalahan',
                            });
                            $.unblockUI();
                        })     
                    }
                });
            }).catch((err) => {
                return false;
            });
        },
        setKurir(){
            this.pembayaran.ongkir = 0;
        },
        setPiutang(){
            this.pembayaran.metode_pembayarans = [];
            this.batas_pembayaran = [];
            let batas_baru = {
                currencySymbol: 'Rp. ',
                minimumValue: 0,
                emptyInputBehavior: 'min',
                currencySymbolPlacement: 'p',
                decimalCharacter: ',',
                decimalPlacesShownOnFocus: 2,
                digitGroupSeparator: '.',
                minimumValue: '0',
                maximumValue: this.sisapembayaran,
            };
            this.batas_pembayaran.push(batas_baru);
            this.pembayaran.metode_pembayarans.push({
                'cara_bayar' : '',
                'receipt_no' : '',
                'bank_id': '',
                'mesin_edc_id': '',
                'nominal': 0,
            });
        },
    },
    computed: {
        total_harga_barang() {
            let total = 0;
            if(this.cart.list_barang.length > 0){
                for(item of this.cart.list_barang){
                    total += parseFloat(item.total_harga);
                }
            }      
            this.cart.total_harga = total;
            this.pembayaran.total = total;      
            return total;
        },
        grand_total(){
            let disc = 0;
            if(this.pembayaran.type_disc == "nominal"){
                disc = this.pembayaran.disc;
            }else{
                disc = (parseFloat(this.cart.total_harga) * (parseFloat(this.pembayaran.disc)/100));
            }
            let grand_total = (parseFloat(this.cart.total_harga) - parseFloat(disc)) + parseFloat(this.pembayaran.ongkir);

            this.pembayaran.grand_total = grand_total;
            if(grand_total > 0) {
                this.pembayaran.is_free = false;
                return grand_total;
            }    
            if(grand_total == 0) {
                this.pembayaran.is_free = true;
                return 0;
            }
        },
        total_pembayaran() {
            let total = 0;
            if(this.pembayaran.metode_pembayarans.length > 0){
                for(item of this.pembayaran.metode_pembayarans){
                    total += parseFloat(item.nominal);
                }
            }            
            this.pembayaran.total_pembayaran = total;
            return total;
        },
        bayar_tunai_only(){
            let total = 0;
            if(this.pembayaran.metode_pembayarans.length > 0){
                for(item of this.pembayaran.metode_pembayarans){
                    if(item.cara_bayar == "tunai")total += parseFloat(item.nominal);
                }
            } 
            return total;           
        },
        kembalian(){
            let total = this.bayar_tunai_only;
            if(this.uang_diterima && this.uang_diterima > total) return this.uang_diterima - total;
            if(this.uang_diterima && this.uang_diterima < total) return 'Uang diterima tidak cukup';
            if(!this.uang_diterima) return 0;
        },
        sisapembayaran(){
            let sisa_pembayaran = this.pembayaran.grand_total - this.pembayaran.total_pembayaran;
            this.pembayaran.sisa_pembayaran = sisa_pembayaran;
            // this.batas_pembayaran.maximumValue = sisa_pembayaran;
            return sisa_pembayaran;
        },
        pointDidapat(){
            let point;
            if(this.grand_total >= 10000){
                point = Math.floor(this.grand_total/10000)
            }
            if(point == undefined) point = 0;
            this.pembayaran.point_didapat = point;
            return point;
        },
        batasPembayaran : {
            // getter
            get: function () {
                if(!this.loop_batas_pembayaran){
                    return {
                        currencySymbol: 'Rp. ',
                        minimumValue: 0,
                        emptyInputBehavior: 'min',
                        currencySymbolPlacement: 'p',
                        decimalCharacter: ',',
                        decimalPlacesShownOnFocus: 2,
                        digitGroupSeparator: '.',
                        minimumValue: '0',
                        // maximumValue: newValue,
                    }; 
                }
                return this.loop_batas_pembayaran;
            },
            // setter
            set: function (newValue) {
                this.loop_batas_pembayaran = newValue;
            }
        },
    },
    watch: {
        'pembayaran.grand_total': function(newValue, oldValue){
            
            // let sisa_pembayaran = this.pembayaran.grand_total - this.total_pembayaran;

            if(newValue != oldValue){
                this.pembayaran.metode_pembayarans = [];
                this.batas_pembayaran = [];
                this.batas_pembayaran.push({
                    currencySymbol: 'Rp. ',
                    minimumValue: 0,
                    emptyInputBehavior: 'min',
                    currencySymbolPlacement: 'p',
                    decimalCharacter: ',',
                    decimalPlacesShownOnFocus: 2,
                    digitGroupSeparator: '.',
                    minimumValue: '0',
                    maximumValue: this.sisapembayaran,
                });
                this.pembayaran.metode_pembayarans.push({
                    'cara_bayar' : '',
                    'receipt_no' : '',
                    'bank_id': '',
                    'mesin_edc_id': '',
                    'nominal': 0,
                });
            }
        },
    },
    beforeCreate() {
        
    },
    created() {
        this.getData_VM_POS();
        this.type_diskon = this.setupDefault.percent;
        this.batas_ongkir = {
            currencySymbol: 'Rp. ',
            minimumValue: 0,
            emptyInputBehavior: 'min',
            currencySymbolPlacement: 'p',
            decimalCharacter: ',',
            decimalPlacesShownOnFocus: 2,
            digitGroupSeparator: '.',
            minimumValue: '0',
        };
        this.cart.tgl_transaksi = moment().format('YYYY-MM-DD HH:mm:ss');
    },
});

$(document).ready(function(){
    let getData = base_url+'api/master/promo/get_all';

    $.getJSON(getData, function(data, status) {
        if (status === 'success') {
            data = data.list;
            if (data.length > 0) {
                //bawah
                let appen = '';
                $.each(data, function (index, value) {
                    appen +='<li>'
                                +'<div class="form-group text-left">'
                                    +'<div class="panel">'
                                        +'<div class="panel-body">'
                                            +'<div class="col-md-1">'
                                                +'<img src="'+base_url+'uploads/barang/original/'+value.foto_barang+'" style="height:60px">'
                                            +'</div>'
                                            +'<div class="col-md-11">'
                                                +'<h6 class="text-bold" style="margin: 0px;padding: 0px;">'+value.nama_barang+'</h6>'
                                                +'<label class="text-bold text-size-mini">'+value.description+'</label>'
                                            +'</div>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</li>';
                });

                $('.content-slider').html(appen);

                slider = $("#content-slider").lightSlider({
                    loop:true,
                    auto:true,
                    pauseOnHover: true,
                    keyPress:true,
                    item:1,
                    pager:false
                });
            }else{//bawah
                let appen = '<li><h6>Tidak Ada Promo</h6></li>';
                $('.content-slider').html(appen);

                slider = $("#content-slider").lightSlider({
                    loop:true,
                    auto:true,
                    pauseOnHover: true,
                    keyPress:true,
                    item:1,
                    pager:false
                });
            }
        }
    });
});