var VM_LIST_TRANSAKSI = new Vue({
    el: '#list_transaksi',
    data: {
        datatable : '',
        filter_range :  [moment(),moment()],
        filter_status : {
            all_status : [
                {   
                    status : 'Approved',
                    btn_class : 'btn-success'
                },
                {   
                    status : 'Waiting',
                    btn_class : 'btn-info'
                },
                {   
                    status : 'Rejected',
                    btn_class : 'btn-danger'
                },
            ],
            selected_status : null,
        },
        filter_status_lunas : {
            all_status : [
                {   
                    status : 'Sudah',
                    btn_class : 'btn-success'
                },
                {   
                    status : 'Belum',
                    btn_class : 'btn-danger'
                },
            ],
            selected_status : null,
        },
    },
    methods: {
        selected_status(status){
            if(this.filter_status.selected_status == status){
                this.filter_status.selected_status = null;
            }else{
                this.filter_status.selected_status = status;
            }
            this.drawFilter();
        },
        selected_status_lunas(status){
            if(this.filter_status_lunas.selected_status == status){
                this.filter_status_lunas.selected_status = null;
            }else{
                this.filter_status_lunas.selected_status = status;
            }
            this.drawFilter();
        },
        resetFilter(){
            this.get_filter_range = [moment(),moment()];
            this.get_filter_status = null;
            this.get_filter_status_lunas = null;
            this.drawFilter();
        },  
        drawFilter(){
            this.datatable.draw();
        },
        popup_reject(type, uid, e){
                this.koordinat_terakhir = parseFloat($(e).closest('.row').offset().top) - 150;
                $('#desc_reject').val(null);
                $('#uid').val(uid);
                $('#type').val(type);
                $('#modal_reject').modal({backdrop: 'static', keyboard: false},'show');
        },
        reject_pos(){
            let uid = $('#uid').val();
            let type = $('#type').val();
            let keterangan = $('#desc_reject').val();
            if(!keterangan){
                alert('Harap keterangan isi dengan lengkap !!!');
                return;
            }
            $('#modal_reject').modal('hide');
            this.change_status(type, 'reject', uid, keterangan);
        },
        change_status(jenis_approval, jenis_aksi, uid, keterangan){
            // Deklarasi ulang keterangan
            keterangan = keterangan || '';
            if(jenis_aksi == 'reject'){
                switch (jenis_approval) {
                    case 'batal_transaksi': this.url_approval = this.global_url.api.sales.pos.reject_diskon_direktur; break;
                    default: break;
                }
            }

            console.log(this.url_approval)
            // Progress loader
            let cur_value = 1;
            let update = false;
            let progress = '';
            let timer = '';

            // Make a loader.
            let loader = new PNotify({
                title: 'loading',
                text: `<div class="progress progress-striped active" style="margin:0">\
                    <div class="progress-bar bg-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">\
                        <span class="sr-only">0%</span>\
                    </div>\
                </div>`,
                addclass: 'bg-slate',
                icon: 'icon-spinner4 spinner',
                hide: false,
                buttons: {
                    closer: true,
                    sticker: false
                },
                history: {
                    history: false
                },
                before_open: (PNotify) => {
                    progress = PNotify.get().find("div.progress-bar");
                    progress.css('width', cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");
                    console.log(cur_value);
                    // Pretend to do something.
                    timer = setInterval(() => {
                        if (cur_value >= 100) {
                            // Remove the interval.
                            window.clearInterval(timer);
                            
                            update = true;
                            loader.remove();

                            return;
                        }
                        cur_value += 5;
                        progress.css('width', cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");
                    }, 65);
                },
                after_close: (PNotify, timer_hide) => {
                
                    clearInterval(timer);
                    if (update) {
                        axios.get(`${this.url_approval}?uid=${uid}&desc=${keterangan}`)
                            .then(response => {
                                console.log(response);
                                Swal.fire({
                                    type: 'success',
                                    title: response.data.message,
                                });
                                VM_LIST_TRANSAKSI.drawFilter();
                                // switch (jenis_approval) {
                                //     case 'verifikasi_pos_manager': this.dt_approval_pos_manager.draw(); break;
                                //     case 'verifikasi_diskon_manager': $('#approval_diskon_manager').DataTable().draw(); break;
                                //     case 'verifikasi_diskon_direktur': $('#approval_diskon_direktur').DataTable().draw(); break;
                                //     default: break;
                                // }
                            }).catch(xhr => {
                                console.log(xhr);
                                Swal.fire({
                                    type: 'error',
                                    title: 'Terjadi kesalahan',
                            });
                        })         
                    }
                    update = false;
                }
            });
        },
        detailStatus(keterangan, detail){
            keterangan = JSON.parse(keterangan);
            detail = detail;
            let main_approval_template = `<tr r-show=":show_disc_manager">
                    <td style="width: 90%"><span class="label label-block label-warning mt-5">Approval Diskon Manager</span></td>
                    <td class="text-center">
                        <i class="fa fa-check text-success" accepted=":status_disc_manager"></i>
                        <i class="fa fa-close text-danger" not_accepted=":status_disc_manager"></i>
                    </td>
                </tr>
                <tr r-show=":show_disc_direktur">
                    <td style="width: 90%"><span class="label label-block label-danger mt-5">Approval Diskon Direktur</span></td>
                    <td class="text-center">
                        <i class="fa fa-check text-success" accepted=":status_disc_direktur"></i>
                        <i class="fa fa-close text-danger" not_accepted=":status_disc_direktur"></i>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90%"><span class="label label-block label-success mt-5">Approval Transaksi Manager</span></td>
                    <td class="text-center">
                        <i class="fa fa-check text-success" accepted=":status_transaksi_manager"></i>
                        <i class="fa fa-close text-danger" not_accepted=":status_transaksi_manager"></i>
                    </td>
                </tr>`;
            let detail_item_template = `<tr>
                    <td>:nama</td>
                    <td>:qty</td>
                    <td>:disc</td>
                    <td>:total</td>
                </tr>`;
            let seluruh_template = `<tr>
                    <td colspan="2">:keterangan</td>
                    <td>:disc</td>
                    <td>:total</td>
                </tr>`;
        
            let message_detail_item = '';
            let message_seluruh = '';
            let message_main_approval = '';

            let count_manager  = 0;
            let count_direktur = 0;

            let status_disc_manager = detail.verifikasi_diskon_manager;
            let status_disc_direktur = detail.verifikasi_diskon_direktur; 
            let status_transaksi_manager = detail.verifikasi_pos_manager;

            let show_disc_manager = 0;
            let show_disc_direktur = 0;
            
            if(detail.verifikasi_pos_manager){
                count_manager += 1;
            } 
            
            if(keterangan.length > 0){
                for(item of keterangan){
                    if(item.type == 'seluruh'){
                        let keterangan = detail.no_invoice+' - '+ detail.tanggal_transaksi;
                        let disc = item.type_disc == 'persen' ? item.disc+' %' : `Rp. ${numeral(item.disc).format('0.0,')}`;
                        let total = 'Rp. '+numeral(item.total).format('0.0,');
                        message_seluruh += seluruh_template.replace(':keterangan', keterangan)
                        .replace(':disc', disc)
                        .replace(':total', total);
                    }else if(item.type == 'per_item'){
                        let disc = item.type_disc == 'persen' ? item.disc+' %' : `Rp. ${numeral(item.disc).format('0.0,')}`;
                        let total = 'Rp. '+numeral(item.total).format('0.0,');
                        message_detail_item += detail_item_template.replace(':nama', item.nama_barang)
                        .replace(':qty', item.qty)
                        .replace(':disc', disc)
                        .replace(':total', total);
                    }

                    if(item.must_approve == 'with_manager'){
                        count_manager += 1;
                    }else if(item.must_approve == 'with_direktur'){
                        count_direktur += 1;
                    }
                }
            }else{
                show_disc_manager = 0;
                show_disc_direktur = 0; 
            }   
           
            
            if(count_manager > 0) show_disc_manager = 1;
            if(count_direktur > 0) show_disc_direktur = 1;
            message_main_approval = main_approval_template.replace(/:show_disc_direktur/g,show_disc_direktur)
            .replace(/:show_disc_manager/g,show_disc_manager)
            .replace(/:status_disc_manager/g,status_disc_manager)
            .replace(/:status_disc_direktur/g,status_disc_direktur)
            .replace(/:status_transaksi_manager/g,status_transaksi_manager);

            $('#main-approval').html(message_main_approval);
            if(message_detail_item != ''){
                $('#detail-per_item').html(message_detail_item);
            }else{
                let empty = `<tr class="empty_state_item">
                    <td colspan="4" class="text-center">TIDAK ADA DATA</td>
                </tr>`;
                $('#detail-per_item').html(empty)
            }
            if(message_seluruh != ''){
                $('#detail-semua').html(message_seluruh);
            }else{
                let empty = `<tr class="empty_state_item">
                    <td colspan="4" class="text-center">TIDAK ADA DATA</td>
                </tr>`;
                $('#detail-semua').html(empty)
            }
            $('#modal_status').modal({backdrop: 'static', keyboard: false},'show');
        },
        rejectedDescription(uid){
            axios({
                method: 'post',
                url: `${this.global_url.api.sales.pos.rejected_description}?uid=${uid}`,
            }).then(response => {
                let data = response.data;
                let html = '';
                for(item of data){
                    html += `<div class="alert alert-danger alert-styled-left alert-bordered">
                        <span id="rejected_description">${item.description}</span>
                    </div>`;
                }
                $('#rejected_desc_wrapper').html(html);
                $('#modal_rejected').modal({backdrop: 'static', keyboard: false},'show');
            }).catch(xhr => {
                
            })
        },
    },
    computed: {
        get_filter_range : {
            get: function () {
                let tgl_awal = moment(this.filter_range[0]).format('YYYY-MM-DD');
                let tgl_akhir = moment(this.filter_range[1]).format('YYYY-MM-DD');
                return [tgl_awal, tgl_akhir];
            },
            set: function (newValue) {
                if(Array.isArray(newValue) && newValue.length == 2 ){
                    this.filter_range = newValue;
                }else{
                    console.log('Value is not valid');
                }
            }
        },
        get_filter_status: {
            get: function () {
                return this.filter_status.selected_status;
            },
            set: function (newValue) {
                this.filter_status.selected_status = newValue;
            },
        },
        get_filter_status_lunas: {
            get: function () {
                return this.filter_status_lunas.selected_status;
            },
            set: function (newValue) {
                this.filter_status_lunas.selected_status = newValue;
            },
        },
    },
    created() {
        VM_PAGE_HEADER.title = '<i class="fa fa-reorder"></i> Daftar Transaksi';
    },
    mounted() {
        this.datatable = $('#myTable').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: this.global_url.api.sales.pos.list_transaksi_dt,
                type: "POST",
                data: (data) => {
                    data.filter_range = this.get_filter_range;
                    data.filter_status = this.get_filter_status;
                    data.filter_status_lunas = this.get_filter_status_lunas;
                    global.method.vueBlockElement('#myTable');
                }
            },
            searchDelay: 600,
            processing: true,
            columns: [
                {data: 'tanggal', name: 'tanggal', 'className': 'text-center', 'searchable': false, render: (data, type, row) => {
                    let proc = moment(data).format('DD/MM/YYYY HH:mm:ss');
                    return proc;
                }},
                {data: 'no_invoice', name: 'no_invoice', 'className': 'text-center', render: (data, type, row) => {
                    let html = '';
                    /*if(row.verifikasi_diskon_direktur && row.verifikasi_diskon_manager && row.verifikasi_pos_manager){*/
                        html = `<a href="${this.global_url.page.sales.pos.detail_transaksi}?uid=${row.uid}" target="_blank">${data}</a>`;
                    /*}else{
                        html = data;
                    }*/
                    return html + (row.flag == 2 ? `<label class="label label-warning">eCommerce</label>` : '');
                }},
                {data: 'nama_sales', name: 'nama_sales', 'className': 'text-center', render: (data, type, row) => {
                    return data;
                }},
                {data: 'grand_total', name: 'grand_total', 'className': 'text-center', render: (data, type, row) => {
                    return data;
                }},
                // {data: 'nama_dokter', name: 'nama_dokter', render: (data, type, row) => {
                //     return `<div class="info-detail">
                //         <span class="dokter_info">${data}</span>
                //         <span class="sales_info">Sales : ${row.nama_sales ? row.nama_sales : '&mdash;'}</span>
                //     </div>`;
                // }},
                // {data: 'status_lunas', name: 'status_lunas','orderable': false, 'searchable': false, 'className': 'text-center', render: (data, type, row) => {
                //     let html = '';
                //     if(data){
                //         html = '<span class="label label-success">SUDAH</span>';
                //     }else{
                //         html = '<span class="label label-danger">BELUM</span>';
                //     }
                //     return html;
                // },},
                {data: 'piutang', name: 'piutang','orderable': false, 'searchable': false, 'className': 'text-center', render: (data, type, row) => {
                    let html = '';
                    if(row.is_rejected == 1 && row.status_pembayaran == 1){
                        html = '<span class="label label-danger">Dibatalkan</span>';
                    }else if(data == 0 && row.status_pembayaran == 2){
                        html = '<span class="label label-success">LUNAS</span>';
                    }else{
                        html = '<span class="label label-danger">BELUM BAYAR</span>';
                    }
                    return html;
                },},
                // {data: 'json_keterangan', name: 'json_keterangan','orderable': false, 'searchable': false, className: 'text-center', render: (data, type, row) =>{
                //     data = JSON.stringify(data);
                //     n_row = JSON.stringify(row);
                //     let html = '';
                //     if(row.verifikasi_diskon_direktur && row.verifikasi_diskon_manager && row.verifikasi_pos_manager){
                //         html = `<span class='label label-success' onclick='VM_LIST_TRANSAKSI.detailStatus(${data},${n_row})'><i class='fa fa-check'></i> Approved</span>`;
                //     }else if(row.is_rejected){
                //         html = `<span class='label label-danger' onclick='VM_LIST_TRANSAKSI.rejectedDescription("${row.uid}")'><i class='fa fa-close'></i> Rejected</span>`;
                //         html += `<span class='label label-info ml-5' onclick='VM_LIST_TRANSAKSI.detailStatus(${data},${n_row})'><i class='fa fa-info'></i></span>`;
                //     }else{
                //         html = `<span class='label label-info' onclick='VM_LIST_TRANSAKSI.detailStatus(${data},${n_row})'><i class='fa fa-info'></i> Waiting Approval</span>`;
                //     }
                //     return html;
                // },},
                {data: 'status_pembayaran', name: 'batal_transaksi','orderable': false, 'searchable': false, className: 'text-center', render: (data, type, row) =>{
                    let html = '';
                    if (row.status_pembayaran == 2 ) {
                        html = '-';
                    }else if (row.is_rejected == 1) {
                        html = '-';
                    }else{
                        html = `<button class="btn text-danger batal_transaksi" onclick=VM_LIST_TRANSAKSI.popup_reject("batal_transaksi","${row.uid}",this)><i class="fa fa-close text-danger"></i></button>`;
                    }
                    return html;
                },},
            ],
            drawCallback: (settings) => {
                $('#myTable').unblock();
            },
            aaSorting: [],
        });
    },
});

$(() => {
    let event_name = 'list-transaksi-event';
    let uid = global.method.Base64_encode("t_pos");
    let url = `${provider.url.api.event.subscribe.stream}?uid=${uid}&event_name=${event_name}`;
    let callback = function(data) {
        VM_LIST_TRANSAKSI.datatable.draw();
    };
    global.method.listenEvent(url,event_name, callback);
});
