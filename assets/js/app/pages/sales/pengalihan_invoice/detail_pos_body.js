var VM_POS_BODY = new Vue({
    el: '#detail_transkasi',
    data : {
        page : 1,
        cart: {
            'internal_used' : false,
            'member_uid' : '',
            'sales_uid' : '',
            'list_barang' : [],
            'tgl_transaksi' : '',
            'total_harga' : '',
        },
        member_bio : [],
        pembayaran : {
            total : '',
            type_disc: 'persen',
            disc  : 0,
            piutang : false,
            point_didapat : '',
            grand_total : '',
            metode_pembayarans : [{
                'cara_bayar' : '',
                'receipt_no' : '',
                'bank_id': '',
                'mesin_edc_id': '',
                'nominal': 0,
            }],
            is_kurir : false, 
            is_free : false,
            ekspedisi: '',
            ongkir: 0,
            total_pembayaran : '',
            sisa_pembayaran : '',
            mustApprove : '',
        },
        master_cara_bayar: [
            {id: 'kartu_kredit', text: 'KARTU KREDIT'},
            {id: 'kartu_debit', text: 'KARTU DEBIT'},
            {id: 'transfer', text: 'TRANSFER'},
            {id: 'tunai', text: 'TUNAI'},
        ],
        master_ekspedisi: [
            {id: 'JNE', text: 'JNE'},
            {id: 'POS Indonesia', text: 'POS Indonesia'},
            {id: 'J&T', text: 'J&T'},
            {id: 'SiCepat', text: 'SiCepat'},
            {id: 'Ninja Express', text: 'Ninja Express'},
            {id: 'TiKi', text: 'TiKi'},
            {id: 'First Logistics', text: 'First Logistics'},
            {id: 'Indah Logistik', text: 'Indah Logistik'},
            {id: 'Wahana Logistik', text: 'Wahana Logistik'},
            {id: 'Pandu Logistik', text: 'Pandu Logistik'},
            {id: 'RPX', text: 'RPX'},
            {id: 'Cahaya Logistik', text: 'Cahaya Logistik'},
            {id: 'GO-SEND', text: 'GO-SEND'},
            {id: 'Grab Express', text: 'Grab Express'},
        ],
        master_bank : [],
        master_edc : [],      
        sisa_pembayaran : '',  
        type_diskon: '',
        uang_diterima: 0,
        // batas_pembayaran: 0,
        batas_ongkir: 0,
    },
    methods: {
        onBatal(){
            this.page = 1;
            VM_POS.page = 1;
        },
        changeTypeDiskon(){
            let setDisc = new Promise((resolve, reject)=>{
                this.pembayaran.disc = 0;
                resolve('success');
            });
            //1 percent
            //0 nominal
            setDisc.then((data)=>{
                if(this.pembayaran.type_disc != "persen"){
                    this.pembayaran.type_disc = "persen";
                    this.type_diskon = this.setupDefault.percent;
                }else{
                    this.pembayaran.type_disc = "nominal";
                    this.type_diskon = this.setupDefault.rupiah;
                    this.type_diskon.maximumValue = this.pembayaran.grand_total;
                    // console.log(this.setupDefault.rupiah);
                }
            });
        },
        getData_VM_POS(){
            //set pembayaran
            this.pembayaran.total = VM_POS.cart.total_harga;
            this.pembayaran.disc = 0;
            this.pembayaran.piutang = 0;

            this.page = VM_POS.page;
            this.cart = VM_POS.cart;
            this.member_bio = VM_POS.member_bio;
            this.master_bank = this.mapOption(VM_POS.master_bank);
            this.master_edc = this.mapOption(VM_POS.master_edc);
        },
        total_per_barang(i){
            let barang = this.cart.list_barang[i];
            let total_h = barang.qty * barang.harga_persatu;
            let diskon = (barang.disc / 100) * total_h;
            let tt = total_h - diskon;
            this.cart.list_barang[i].total_harga = tt;
            return tt;
        },
        addPembayaran(){
            let n_p = {
                'cara_bayar' :'',
                'receipt_no' :'',
                'bank_id': '',
                'mesin_edc_id': '',
                'nominal': 0,
            }
            this.pembayaran.metode_pembayarans.push(n_p);
        },
        mapOption(array){
            if(typeof array == 'object'){
                let change = array.map(function(item){
                    return {
                        id: item.id,
                        text: item.nama,
                    }
                });

                return change;
            }
        },
        removePembayaran(i){
            if(this.pembayaran.metode_pembayarans.length == 1)return false;
            this.pembayaran.metode_pembayarans.splice(i,1);
        },
        checkDiskon(){
            if(this.pembayaran.type_disc == "persen"){
                this.pembayaran.mustApprove = 'without_approve';
                /*if(this.pembayaran.disc >= 0 && this.pembayaran.disc < 25){
                    this.pembayaran.mustApprove = 'without_approve';
                }else if(this.pembayaran.disc == 25){
                    this.pembayaran.mustApprove = 'with_manager';
                }else if(this.pembayaran.disc > 25){
                    this.pembayaran.mustApprove = 'with_direktur';
                }*/
            }
        },
        validasiPembayaran(){
            let filter = this.pembayaran.metode_pembayarans.filter((item)=>{
                console.log('ini',item.nominal)
                if(item.cara_bayar == 'tunai'){
                    if(item.cara_bayar == 'tunai' && item.nominal == 0){
                        return true;
                    }else{
                        return false;
                    } 
                }else if(item.cara_bayar != 'tunai' || item.cara_bayar == null){
                    if(item.nominal == 0 || !item.receipt_no || !item.bank_id || !item.mesin_edc_id){
                        return true;
                    }else{
                        return false;
                    } 
                }
            });
            if(this.pembayaran.grand_total == 0){
                return true;
            }
            if(!this.pembayaran.piutang && filter.length > 0){
                return false;
            }else{
                return true;
            }
        },
        validasiTotalPembayaran(){
            if(this.pembayaran.total_pembayaran > this.pembayaran.grand_total){
                return false;
            }
            return true;
        },
        validasiKurir(){
            if(this.pembayaran.is_kurir){
                if(!this.pembayaran.ekspedisi) return false;
            }
            return true;
        },
        onProcess(){
            let _process = new Promise((resolve, reject) => {
                this.checkDiskon();
                let _vtp = this.validasiTotalPembayaran();
                let _vp = this.validasiPembayaran();
                let _vk = this.validasiKurir();
                

                if(!_vk){
                    Swal.fire({
                        type: 'error',
                        title: 'Ekspedisi belum dipilih',
                    });
                }
                
                if(!_vp){
                    Swal.fire({
                        type: 'error',
                        title: 'Metode pembayaran belum lengkap',
                    });
                }

                if(!_vtp){
                    Swal.fire({
                        type: 'error',
                        title: 'Pembayaran melebihi total harga yang dibeli',
                    });
                }

                if(_vtp && _vp && _vk) resolve({
                    'status': true,
                });

                if(_vtp || _vp || _vk) reject({
                    'status': false,
                });
            });
            
            _process.then(()=>{         
                Sweet.fire({
                    title: 'Transaksi akan dilanjut?',
                    text: "Pastikan informasi yang ada masukan sudah benar !",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '<b><i class="icon-checkmark4"></i></b> Ya',
                    cancelButtonText: '<b><i class="icon-cancel-square"></i></b> Tidak',
                    // reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        global.method.vueBlockUI();
                        let formData = new FormData();
                        formData.append('cart', JSON.stringify(this.cart));
                        formData.append('transaksi', JSON.stringify(this.pembayaran));
                        axios.post(this.global_url.api.sales.pos.proccess_order, formData)
                        .then(response => {
                            console.log(response);
                            $.unblockUI();
                            Swal.fire({
                                type: 'success',
                                title: response.data.message,
                            });
                            setTimeout(()=>{
                                window.location.href = this.global_url.page.sales.pos.index;
                            },2000)
                        }).catch(xhr => {
                            console.log(xhr);
                            Swal.fire({
                                type: 'error',
                                title: 'Terjadi kesalahan',
                            });
                            $.unblockUI();
                        })     
                    }
                });
            }).catch((err) => {
                console.log(err);
                return false;
            });
        },
        setKurir(){
            this.pembayaran.ongkir = 0;
        },
        setPiutang(){
            this.pembayaran.metode_pembayarans = [];
            this.pembayaran.metode_pembayarans.push({
                'cara_bayar' : '',
                'receipt_no' : '',
                'bank_id': '',
                'mesin_edc_id': '',
                'nominal': 0,
            });
        },
        getDetailPos(){
            console.log(this.$route);
            let urlParams = new URLSearchParams(window.location.search);
            let uid = urlParams.get('uid');
            console.log('uid',uid);

            axios.post(`${this.global_url.api.sales.pos.get_detail_pos}?uid=${uid}`, )
            .then(response => {
                console.log('ini data ',response.data);
                let data = response.data;
                let json_data = JSON.parse(data.pos.json_before_process);
                json_data.cart = JSON.parse(json_data.cart);
                json_data.transaksi = JSON.parse(json_data.transaksi);
                console.log(json_data);
            }).catch(xhr => {
               
            })     
        }
    },
    computed: {
        total_harga_barang() {
            let total = 0;
            if(this.cart.list_barang.length > 0){
                for(item of this.cart.list_barang){
                    total += parseFloat(item.total_harga);
                }
            }      
            this.cart.total_harga = total;
            this.pembayaran.total = total;      
            return total;
        },
        grand_total(){
            let disc = 0;
            if(this.pembayaran.type_disc == "nominal"){
                disc = this.pembayaran.disc;
            }else{
                disc = (parseFloat(this.cart.total_harga) * (parseFloat(this.pembayaran.disc)/100));
            }
            // console.log(disc)
            let grand_total = (parseFloat(this.cart.total_harga) - parseFloat(disc)) + parseFloat(this.pembayaran.ongkir);
            // grand_total = round(grand_total);
            this.pembayaran.grand_total = grand_total;

            console.log('test',grand_total)
            if(grand_total > 0) {
                this.pembayaran.is_free = false;
                return grand_total;
            }    
            if(grand_total == 0) {
                this.pembayaran.is_free = true;
                return 'FREE';
            }
        },
        total_pembayaran() {
            let total = 0;
            if(this.pembayaran.metode_pembayarans.length > 0){
                for(item of this.pembayaran.metode_pembayarans){
                    total += parseFloat(item.nominal);
                }
            }            
            this.pembayaran.total_pembayaran = total;
            return total;
        },
        bayar_tunai_only(){
            let total = 0;
            if(this.pembayaran.metode_pembayarans.length > 0){
                for(item of this.pembayaran.metode_pembayarans){
                    if(item.cara_bayar == "tunai")total += parseFloat(item.nominal);
                }
            } 
            return total;           
        },
        kembalian(){
            let total = this.bayar_tunai_only;
            if(this.uang_diterima && this.uang_diterima > total) return this.uang_diterima - total;
            if(this.uang_diterima && this.uang_diterima < total) return 'Uang diterima tidak cukup';
            if(!this.uang_diterima) return 0;
        },
        sisapembayaran(){
            let sisa_pembayaran = this.pembayaran.grand_total - this.pembayaran.total_pembayaran;
            this.pembayaran.sisa_pembayaran = sisa_pembayaran;
            // this.batas_pembayaran.maximumValue = sisa_pembayaran;
            return sisa_pembayaran;
        },
        pointDidapat(){
            let point;
            if(this.grand_total >= 10000){
                point = Math.floor(this.grand_total/10000)
            }
            if(point == undefined) point = 0;
            this.pembayaran.point_didapat = point;
            return point;
        },
        batasPembayaran(){
            return {
                currencySymbol: 'Rp. ',
                minimumValue: 0,
                emptyInputBehavior: 'min',
                currencySymbolPlacement: 'p',
                decimalCharacter: ',',
                decimalPlacesShownOnFocus: 2,
                digitGroupSeparator: '.',
                minimumValue: '0',
                maximumValue: this.grand_total,
            };
        },
        
    },
    beforeCreate() {
        
    },
    created() {
        this.getDetailPos();
    },
})