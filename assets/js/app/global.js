// window.onbeforeunload = function(){
//     return 'Are you sure you want to leave?';
// };

var global = new function(){
    this.baseUrl   = $('meta[name="base_url"]').attr('content');
    this.assetsUrl   = $('meta[name="assets_url"]').attr('content');
    this.template = {
        'option_null' : '<option value="" selected disabled>Pilih :name</option>',
        'message_error' : '<span class="validate_error">:message</span>',
        'alert_error' : '<div class="alert alert-danger alert-styled-left alert-bordered alert-validate-error"><span class="text-semibold">:message</span></div>',
    };
    this.el = {
        'alert_wrap' : '.alert_wrapper',
    };
    this.var = {
        'lazyLoadInstance' : '',
    }
    this.method = {

        _loop_option: {
            getTempatLahir(){
                let res = new Promise(function(resolve, reject){
                    provider.method.getAllKota().done(function(data){
                        let option = '';
    
                        option += global.template.option_null.replace(':name','Tempat Lahir');
                        $.each(data, function(i,v){
                            option += global.method._loop_option.render(i,v);
                        });
                        resolve(option);
                    }).fail(function(xhr){
                        reject(xhr)
                    })
                })
    
                return res;
            },
            render(id,val){
                return '<option value="'+id+'">'+val+'</option>';
            },
        },
        _validation: {

        },
        _throw_error: {
            form_error(error){
                if(error.status == 422){
                    let errors = error.responseJSON.message;
                    console.log(errors)
                    for(let key in errors){
                        console.log('ini',typeof errors[key])
                        if(typeof errors[key] == "object"){
                            for(let key2 in errors[key]){
                                if(typeof errors[key][key2] == "object"){
                                    for(let key3 in errors[key][key2]){
                                        console.log('.'+key+'.'+key2+'.'+key3)
                                        $('.'+key+'.'+key2+'.'+key3).before(global.template.alert_error.replace(':message',errors[key][key2][key3].toUpperCase()));
                                    }
                                }else{
                                    $('.'+key+'.'+key2).before(global.template.alert_error.replace(':message',errors[key][key2].toUpperCase()));
                                }
                                // console.log('.'+key+'.'+key2);
                            }
                        }else{
                            let message = global.template.message_error.replace(':message', errors[key]);
                            let el = $('[name="'+key+'"]');
                            if(el.hasClass('select2') || el.hasClass('select_2')){
                                el.next().after(message);
                            }else if(el.hasClass('styled')){
                                $('.parent_styled').append(message)
                            }else{
                                el.after(message);
                            }
                        }
                    }
                }else{
                    let status = error.status;
                    let message = "Terjadi kesalahan";
                    if(typeof error.responseJSON == "object") message = error.responseJSON.message;
                    if(typeof error.responseJSON != "object") message = error.responseJSON;
                    if(status == 404) message = "Url Not Found";
                    if(status == 500) error.responseJSON ? message = error.responseJSON.message : message = 'Internal Server Error';
                    errorMessage('Peringatan !',message);
                }
            }
        },
        clear_error(el){
            let FORM = $(el);
            FORM.find('.validate_error').remove();
            FORM.find('.alert-validate-error').remove();
        },
        getArrayForm(form_el) {
            let result = { };
            $.each($(form_el).serializeArray(), function(i,v) {
                result[v.name] = v.value;
            });
            // console.log(result);
            return result;
        },
        checkDate(e){
            console.log('test',e)
            let parent = $(e).parent();
            let value = $(e).val();
            
            let day = value.substr(0, 2);
            day = isNaN(day) ? 0 : day;
            let month = value.substr(3, 2);
            month = isNaN(month) ? 0 : month;
            let year = value.substr(6, 4);
            year = isNaN(year) ? 0 : year;
            
            parent.find('.span-exdate-error').remove();
            $('button[type="submit"]').prop('disabled', false);
            if(value != "") {
                if(parseInt(day) > 31 || parseInt(month) > 12 || year.length < 4) {
                    $('button[type="submit"]').prop('disabled', true);
                    if(parent.find('.span-exdate-error').length <= 0) {
                        parent.append('<span class="label label-block label-danger span-exdate-error">Format Tanggal Salah</span>');
                    }
                }
            }
        },
        blockUI(message){
            message = message || 'Sedang Di Proses...';
            $.blockUI({
                message: `<span><img src="${provider.baseUrl}assets/img/loading.gif" style="height: 40px;">&nbsp;${message}</span>`,
                baseZ: 10000,
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.7,
                    cursor: 'wait'
                },
                css: {
                    border: 2,
                    padding: '10px 15px',
                    color: '#fff',
                    '-webkit-border-radius': 10,
                    '-moz-border-radius': 10,
                    backgroundColor: '#333',
                    opacity: 0.8,
                    'z-index' : 10020,
                    
                } 
            });
        },
        vueBlockUI(message){
            message = message || 'Sedang Di Proses...';
            $.blockUI({
                message: `<span><img src="${provider.baseUrl}assets/img/loading.gif" style="height: 28px;">&nbsp;${message}</span>`,
                baseZ: 10000,
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.7,
                    cursor: 'wait'
                },
                css: {
                    border: 2,
                    padding: '10px 15px',
                    color: '#fff',
                    '-webkit-border-radius': 10,
                    '-moz-border-radius': 10,
                    backgroundColor: '#333',
                    opacity: 0.8,
                    'z-index' : 10020,
                    
                } 
            });
        },
        blockElement(element,message){
            message = message || 'Sedang Di Proses...';
            $(element).block({
                message: `<span><img src="${provider.baseUrl}assets/img/loading.gif" style="height: 40px;image-rendering: auto;">&nbsp;${message}</span>`,
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.9,
                    cursor: 'wait'
                },
                css: {
                    border: 2,
                    padding: '10px 15px',
                    color: '#fff',
                    '-webkit-border-radius': 10,
                    '-moz-border-radius': 10,
                    backgroundColor: '#333',
                    opacity: 0.8,
                    'z-index' : 10020,
                } 
            });
    
        },
        vueBlockElement(element,message){
            message = message || 'Sedang Di Proses...';
            $(element).block({
                message: `<span><img src="${provider.baseUrl}assets/img/loading.gif" style="height: 28px;">&nbsp;${message}</span>`,
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.9,
                    cursor: 'wait'
                },
                css: {
                    border: 2,
                    padding: '10px 15px',
                    color: '#fff',
                    '-webkit-border-radius': 10,
                    '-moz-border-radius': 10,
                    backgroundColor: '#333',
                    opacity: 0.8,
                    'z-index' : 10020,
                } 
            });
    
        },
        Base64_encode(str){
            return window.btoa(str);
        },
        Base64_decode(str){
            return window.atob(str);
        },
        listenEvent(url, event_name, callback){
            if(!url && !event_name && !callback) errorMessage('Oops!!', 'Event gagal dimuat');
            let sse = new EventSource(url);
            sse.addEventListener(event_name,function(e){
                let data = e.data;
                callback(data);
                //handle your data here
            },false);
        },
        toRupiah(value){
            let format = '0.0,';
            if (!value) return '0';
            if(typeof value == "string") return value;
            return numeral(value).format(format);
        }
    };
    this.onCreate = new function(){
        this.setup = () =>{
            // global.var.lazyLoadInstance = new LazyLoad({
            //     elements_selector: ".lazy"
            // });
            // if (global.var.lazyLoadInstance) {
            //     global.var.lazyLoadInstance.update();
            // }
            $('.styled').uniform({radioClass: 'choice'});
            // console.log('cek',$('select').select2())
        };
        this.initMask = () => {
            $('.date_mask').mask("00/00/0000", {placeholder: "DD/MM/YYYY"});
        };
        this.setProvinsi = (el) => {
            global.method._loop_option.getProvinsi().then(function(data){
                // console.log(data)
                $(el).empty();
                $(el).append(data);
                global.onCreate.initSelet2();
            }).catch(function(error){
                console.log(error)
            });
        };
        this.setFotoUpload = (el_preview,el_input) => {
            $(el_preview).tooltip({
                title: 'Click to uploads',
                position: 'bottom'
            });
            $(el_preview).on('click', () => {
                $("#foto").click();
            });
            $(el_input).on('change', () => {
                var input = this;
                console.log(input.files);
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $(el_preview).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            });
        };
        this.tambahan = function(){
            setTimeout(function(){
                $('.crud-biasa').slideUp(function(){
                    $(this).remove();
                });
            },8000);
        };
        this.productInput = function(){
            let parent = $('.product-input');
            let stock  = parseInt(parent.data('stock')); 
            parent.on('click', '.btn-plus', function(){
                let input  = $(this).parent().find('input.count-product'); 
                let value  = parseInt(input.val());
                if(value < stock){
                    input.val(value+=1);
                }
            })
            parent.on('click', '.btn-min', function(){
                let input  = $(this).parent().find('input.count-product'); 
                let value  = parseInt(input.val());
                if(value != 0){
                    input.val(value-=1);
                }
            })
        };
        this.initScrollBar = function(){
            $('.scrollbar-inner').scrollbar();
        };
        this.initCheckboc = function(){

        }
    };
    this.onCreated = function(){
        $('input[name="checkbox"]').uniform();
        global.onCreate.setup();
        global.onCreate.initScrollBar();
        global.onCreate.initMask();
    };
}

$(() => {
    global.onCreated();
})

//swal
const Sweet = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-sm btn-success btn-labeled mr-5',
      cancelButton: 'btn btn-sm btn-danger btn-labeled',
    },
    buttonsStyling: false,
});
  
//Numeral
const format = ',';
// load a language
numeral.language('id', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'ribu',
        million: 'juta',
        billion: 'milyar',
        trillion: 'triliun'
    },
    currency: {
        symbol: 'Rp.'
    }
});
numeral.language('id');


/**
 * @author Ari Ardiansyah
 * Vue Global Variables & Vue Component & Vue Filter
 * */ 

Vue.prototype.assetsUrl = global.assetsUrl;
Vue.prototype.setupDefault = {
    percent: {
        currencySymbol: ' %',
        minimumValue: 0,
        emptyInputBehavior: 'min',
        currencySymbolPlacement: 's',
        decimalCharacter: ',',
        decimalPlacesShownOnFocus: 2,
        digitGroupSeparator: '.',
        minimumValue: '0',
        maximumValue: '100',
    },
    rupiah: {
        currencySymbol: 'Rp. ',
        minimumValue: 0,
        emptyInputBehavior: 'min',
        currencySymbolPlacement: 'p',
        decimalCharacter: ',',
        decimalPlacesShownOnFocus: 2,
        digitGroupSeparator: '.',
        minimumValue: '0',
    },
    datepicker : {
        lang: {
            dow: 0, // Sunday is the first day of the week
            hourTip: 'Select Hour', // tip of select hour
            minuteTip: 'Select Minute', // tip of select minute
            secondTip: 'Select Second', // tip of select second
            yearSuffix: '', // suffix of head
            monthsHead: 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'), // months of head
            months: 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'), // months of panel
            weeks: 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'), // weeks
            cancelTip: 'Cancel',
            submitTip: 'Confirm'
        },
    },
};

// Directives
Vue.directive('mask', VueMask.VueMaskDirective);
// Use
Vue.use(VueRouter);
// Components
Vue.component('v-select', VueSelect.VueSelect);

Vue.component('content-loader', contentLoaders.ContentLoader);
Vue.component('facebook-loader', contentLoaders.FacebookLoader);
Vue.component('code-loader', contentLoaders.CodeLoader);
Vue.component('bullet-list-loader', contentLoaders.BulletListLoader);
Vue.component('instagram-loader', contentLoaders.InstagramLoader);
Vue.component('list-loader', contentLoaders.ListLoader);
// Vue.component('vue-autonumeric', VueAutonumeric);
// Vue.component('vue-datepicker', VueDatepickerLocal);
Vue.component('select2', {
    props: ['options', 'value', 'classname', 'lengthselected'],
    template: '#select2-template',
    methods: {
        selectedTemplate(data){
            if(this.lengthselected != null){
                if(this.lengthselected == 0) return data.text;
                if(this.lengthselected > 0){
                    if(data.text.length > 11) return data.text.substr(0,this.lengthselected)+'...';
                }
            }else{
                if(data.text.length > 11) return data.text.substr(0,8)+'...';
            }
            return data.text;
        }
    },
    mounted: function () {
        var vm = this
        $(this.$el)
        // init select2
        .select2({ data: this.options, templateSelection: this.selectedTemplate })
        .val(this.value)
        .trigger('change')
        // emit event on change.
        .on('change', function () {
            vm.$emit('input', this.value)
        })
    },
    watch: {
        value: function (value) {
            // update value
            $(this.$el)
                .val(value)
                .trigger('change')
        },
        options: function (options) {
            // update options
            $(this.$el).empty().select2({ data: options })
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
})

//Component Select2 Ajax
//Created April-May 2019
//Created by Ari Ardiansyah
Vue.component('select2-ajax', {
    props: {
        'options' : '', 
        'value' : '',
        'classname' : '',
        'placeholder' : '',
        'url': '',
        'selected_item' : '',
    },
    template: '#select2-template',
    methods: {
        templateSelection(data){
            return data.text;
        },
        templateResult(data){
            return data.text;
        }
    },
    mounted: function () {
        var vm = this;
        $(this.$el).select2({
            width: '100%',
            placeholder: this.placeholder,
            delay: 250,
            ajax: {
                url: this.url,
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1.
                    }
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
        
                    return {
                        'results': data.results, 
                        'pagination': {
                            'more': data.pagination.more
                        },
                    }
                },
                cache: true
            },
            templateResult: this.templateResult,
            templateSelection: this.templateSelection,
            // minimumInputLength: 3,
            escapeMarkup: function(m) { return m; }
        }).on('select2:select', function (e) {
            // console.log('Ini event', e);
            this.selected_item = e.params.data;
            vm.$emit('onselect', this.selected_item);
        }).on('change', function(){
            vm.$emit('input', this.value);
        });
        
        if(this.value){
            let option = new Option(this.value.nama, this.value.id, true, true);
            $(this.$el).append(option).trigger('change');

            // manually trigger the `select2:select` event
            $(this.$el).trigger({
                type: 'select2:select',
                params: {
                    data: this.value
                }
            });
        }
    },
    watch: {
        value: function (value) {
            // update value
            $(this.$el)
                .val(value)
                .trigger('change')
        },
        options: function (options) {
            // update options
            $(this.$el).empty().select2({ data: options })
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
});

Vue.component('select2-ajax2', {
    props: {
        'options' : '', 
        'value' : '',
        'classname' : '',
        'placeholder' : '',
        'url': '',
        'selected_item' : '',
        'template_result' : '',
        'template_selection' : '',
    },
    template: '#select2-template',
    methods: {
        templateSelection(data){
            return data.text;
        },
        templateResult(data){
            if (data.loading) return data.text;

            if(this.template_result){
                return this.template_result(data)
            }
        }
    },
    mounted: function () {
        var vm = this;
        $(this.$el).select2({
            width: '100%',
            placeholder: this.placeholder,
            delay: 250,
            ajax: {
                url: this.url,
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1.
                    }
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
        
                    return {
                        'results': data.results, 
                        'pagination': {
                            'more': data.pagination.more
                        },
                    }
                },
                cache: true
            },
            templateResult: this.templateResult,
            templateSelection: this.templateSelection,
            // minimumInputLength: 3,
            escapeMarkup: function(m) { return m; }
        }).on('select2:select', function (e) {
            // console.log('Ini event', e);
            this.selected_item = e.params.data;
            vm.$emit('onselect', this.selected_item);
        }).on('change', function(){
            vm.$emit('input', this.value);
        });
        
        if(this.value){
            let option = new Option(this.value.nama, this.value.id, true, true);
            $(this.$el).append(option).trigger('change');

            // manually trigger the `select2:select` event
            $(this.$el).trigger({
                type: 'select2:select',
                params: {
                    data: this.value
                }
            });
        }
    },
    watch: {
        value: function (value) {
            // update value
            $(this.$el)
                .val(value)
                .trigger('change')
        },
        options: function (options) {
            // update options
            $(this.$el).empty().select2({ data: options })
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
});
  

// Filters
Vue.filter('_mines', function (value) {
    console.log('mines', value);
    let _new = numeral(value)._value;
    if(_new < 0)return '<span style="color: red">('+value+')</span>';
    return value; 
})

Vue.filter('to_rupiah', function (value) {
    if (!value) return '0';
    if(typeof value == "string") return value;
    return numeral(value).format(format);
})

Vue.filter('bil_bulat', function (value) {
    // console.log(value)
    if (!value) return '0';
    return parseFloat(value).toFixed(); 
})

Vue.filter('percent', function (value) {
    // console.log(value)
    if (!value) return '0';
    return parseFloat(value).toFixed(2)+" %";
})

Vue.filter('d_m_Y', function (value) {
    if (!value) return '';
    return moment(value).format('DD-MM-YYYY');
})

Vue.filter('d_m_Y_H_m_s', function (value) {
    if (!value) return '';
    return moment(value).format('DD-MM-YYYY HH:mm:ss');
})

if(document.getElementById("vm_page_header")){
    var VM_PAGE_HEADER = new Vue({
        el: '#vm_page_header',
        data: {
            title: '',
            action: '',
        },
    });
}