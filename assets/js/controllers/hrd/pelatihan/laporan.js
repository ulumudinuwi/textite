$(function(){
    var the_table;
    $tahun        = $('#tahun');
    $unitusaha  = $('#unitusaha');
    $durasi  = $('#durasi');

    $tahun.change(function() {
        console.log('jalan');
        durasi      = $durasi.val();
        unitusaha   = $unitusaha.val();
        tahun       = $tahun.val();
        $('#disp_tahun').text(tahun);
        get_data(tahun,unitusaha,durasi);
    });

    $unitusaha.change(function(){
        durasi      = $durasi.val();
        unitusaha   = $unitusaha.val();
        tahun       = $tahun.val();
        $('#disp_tahun').text(tahun);
        get_data(tahun,unitusaha,durasi);
    });

    $durasi.change(function(){
        durasi      = $durasi.val();
        unitusaha   = $unitusaha.val();
        tahun       = $tahun.val();
        $('#disp_tahun').text(tahun);
        get_data(tahun,unitusaha,durasi);
    });
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}

function get_data(tahun,unitusaha,durasi){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable_lap_pelatihan").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+'/api/hrd/renbang/pelatihan/get_lap_pelatihan',
            "type": "POST",
            "dataType":"json",
            "data": {tahun:tahun,unitusaha:unitusaha,durasi:durasi}
        },
        "columns": [
            {
                "data": "no",
            },
            {
                "data": "nik",
                "render": function(data, type, row, meta) {
                    return '<span class="text-primary" style="cursor:pointer" href="'+base_url+'/hrd/renbang/laporan_pelatihan/view/' + row.uid + '" onClick="view_detail(this)">' + data + '</span>';
                }
            },
            {
                "data": "nama",
            },
            {
                "data": "jumlah_ikut",
                "searchable": false
            },
            {
                "data": "jumlah_durasi",
                "searchable": false
            },
        ],
        "initComplete":function(){
            $('#panel-laporan').removeClass('hidden');
        },
        "fnCreatedRow": function (row, data, index) {
            $('td', row).eq(0).html(index + 1);
        }
    });
}
