var page_url    = '/api/hrd/pegawai/';
$notif          = $('#notif');
$formPromosi     = $('#form-promosi');
$btnSubmitResign= $('#save-button');
$detailPegawai  = $('#detail-pegawai');

$.validator.addMethod("onlyNumber",
    function (value, element, options)
    {
        var req = /^\d+$/;

        return (value.match(req));
    },
    "Please enter a number."
);

$(function(){
    $('input.form-control').keypress(function (e) {
      if (e.which == 13) {
        return false;    //<---- Add this line
      }
    });

    $('.pickadate').pickadate({
        // options
        format: 'dd-mm-yyyy'
    });
    // $('input.nilai').autoNumeric('init', {aSep: '.', aDec:',', aPad: false});
    $formPromosi.validate({
        rules: {
            "tahun_awal": {
                required: "required",
                onlyNumber: true,
                maxlength: 10
            },
            "tahun_akhir": {
                required: "required",
                onlyNumber: true,
                maxlength: 10
            },
            "tahun_mulai": {
                required: "required",
                onlyNumber: true,
                maxlength: 10
            },
            "jenis_penilaian": {
                required: true,
            }
        }
    });

    $('input.nilai').change(function(){
        var total = parseInt(0);
        $.each($('input.nilai'),function(i,v){
            var item = $(v).val();
            total += parseInt(item);
        });
        $('input[name=total_nilai]').val(total);
    });
});

function convert_array_to_object(dataArray){
    var result = {};
    dataArray.forEach(function(item,index){
        result[item.name] = item.value;
    });
    return result;
}

var MyModel = {
    page_url : '/api/hrd/pegawai/',
    insert : function(page_url,data,callback){
        $.ajax({
            'url':base_url+page_url,
            'type':'POST',
            'dataType':'json',
            'data':data,
            'success': function(data){
                return callback(data);
            }
        });
    },
    delete : function(url,callback){
        $.ajax({
            'url':url,
            'type':'POST',
            'dataType':'json',
            'success': function(data){
                return callback(data);
            }
        });
    },
    get : function(tabel,columns,data){
        console.log('run');
        var page_url = this.page_url;
        if(typeof the_table != 'undefined'){
            the_table.destroy();
        }
        the_table = $("#"+tabel).DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url+page_url,
                "type": "POST",
                "dataType":"json",
                "data": data
            },
            "columns": columns
        });
    }
}
