var page_url    = '/api/hrd/pegawai/';
$notif          = $('#notif');
$formPengajuan  = $('#form-pengajuan');
$btnSubmitPengajuan= $('#save-button');
$detailPegawai  = $('#detail-pegawai');
$jenisPengajuan = $('select[name=jenis_pengajuan]');
$jabatanLama    = $('select[name=jabatan_lama]');
$jabatanBaru    = $('select[name=jabatan_baru]');
$unitusaha_id   = $('select[name=unitusaha_id]');

var rules_default = {
        "jabatan_lama": {
            required: true,
        },
        "jabatan_baru": {
            required: true,
        },
        "jenis_pengajuan": {
            required: true,
        },
        "unitusaha_id": {
            required: true,
        }
    };

$(function(){
    $('input.form-control').keypress(function (e) {
      if (e.which == 13) {
        return false;    //<---- Add this line
      }
    });

    $('.pickadate').pickadate({
        // options
        format: 'dd-mm-yyyy'
    });

    $jenisPengajuan.change(function(){
        var rules = angular.copy(rules_default);
        if($(this).val() == 'resign')
        {
            delete(rules.jabatan_baru);
            delete(rules.jabatan_lama);
            set_validation(rules);
            $jabatanLama.prop('disabled',true);
            $jabatanBaru.prop('disabled',true);
        }
        else{
            set_validation(rules);
            $jabatanLama.prop('disabled',false);
            $jabatanBaru.prop('disabled',false);
        }
        console.log('rules ', rules);
        console.log('default rules',rules_default);
    });

    var jenis_pengajuan = $jenisPengajuan.val();
    var rules = angular.copy(rules_default);
    if(jenis_pengajuan == 'resign')
    {
        delete(rules.jabatan_baru);
        delete(rules.jabatan_lama);
        $jabatanLama.prop('disabled',true);
        $jabatanBaru.prop('disabled',true);

        set_validation(rules);
    }
    else{
        $jabatanLama.prop('disabled',false);
        $jabatanBaru.prop('disabled',false);

        set_validation(rules);
    }

});

function set_validation(rules){
    console.log('set validation');
    if(typeof formValidator != 'undefined')
        formValidator.destroy();

    formValidator = $formPengajuan.validate({
        rules:rules
        ,
        errorPlacement: function (error, element) {
            error.insertAfter(element);
            element.focus();  // <- this line is causing your whole problem
        },
        submitHandler: function(form) {
            var id = $(document).find('#form-pengajuan input[name=id]').val();

            var target_url  = base_url+'/api/hrd/jabatan/pengajuan/update/'+id;
            var captionBtn  = $btnSubmitPengajuan.html();
            var data = new FormData($(form)[0]);
            console.log(target_url);
            console.log(data);
            $.ajax({
                url         : target_url,
                type        : "POST",
                data        : data,
                dataType    : 'json',
                mimeType    : "multipart/form-data",
                contentType : false,
                cache       : false,
                processData : false,
                beforeSend: function() {
                        $btnSubmitPengajuan.html('Please wait....');
                },
                success:function(data)
                {
                    console.log(data);
                    $btnSubmitPengajuan.html(captionBtn);
                    $notif.text(data.info);
                    $notif.fadeIn('slow').delay('3500').fadeOut('slow');
                }
            });
            return false;
        }
    });
    return false;
}

function convert_array_to_object(dataArray){
    var result = {};
    dataArray.forEach(function(item,index){
        result[item.name] = item.value;
    });
    return result;
}

var MyModel = {
    page_url : '/api/hrd/pegawai/',
    insert : function(page_url,data,callback){
        $.ajax({
            'url':base_url+page_url,
            'type':'POST',
            'dataType':'json',
            'data':data,
            'success': function(data){
                return callback(data);
            }
        });
    },
    delete : function(url,callback){
        $.ajax({
            'url':url,
            'type':'POST',
            'dataType':'json',
            'success': function(data){
                return callback(data);
            }
        });
    },
    get : function(tabel,columns,data){
        console.log('run');
        var page_url = this.page_url;
        if(typeof the_table != 'undefined'){
            the_table.destroy();
        }
        the_table = $("#"+tabel).DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url+page_url,
                "type": "POST",
                "dataType":"json",
                "data": data
            },
            "columns": columns
        });
    }
}
