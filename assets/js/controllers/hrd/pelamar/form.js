var pelamar_id  = $('input[name=id]').val();
var page_url    = '/api/hrd/rekrutment/pelamar/';
$notif          = $('#notif');
$formKontak     = $('#form-kontak');
$formPelamar    = $('#form-pelamar');
var rules       = [];
$.validator.addMethod("phoneNumber",
    function (value, element, options)
    {
        var phoneno = /^\d+$/;

        return (value.match(phoneno));
    },
    "Please enter a valid phone number."
);
rules['trigger-umum'] = {
            "no_ktp": {
                required: true
            },
            "nama": {
                required: true
            },
            "tempat_lahir": {
                required: true
            },
            "tgl_lahir": {
                required: true
            },
            "jenis_kelamin": {
                required: true
            },
            "agama": {
                required: true
            },
            "status_kawin": {
                required: true
            },
            "golongan_darah": {
                required: true
            }
        };

rules['trigger-alamat'] = {
            "alamat": {
                required: true
            },
            "id_provinsi": {
                required: true
            },
            "id_kabupaten": {
                required: true
            },
            "id_kecamatan": {
                required: true
            },
            "id_kelurahan": {
                required: true
            },
            "kode_pos": {
                required: true
            },
            "cur_alamat": {
                required: true
            },
            "cur_id_provinsi": {
                required: true
            },
            "cur_id_kabupaten": {
                required: true
            },
            "cur_id_kecamatan": {
                required: true
            },
            "cur_id_kelurahan": {
                required: true
            },
            "cur_kode_pos": {
                required: true
            },
            "telepon": {
                required: true
            }
        };

rules['trigger-pendidikan'] = {
            "pendidikan": {
                required: true
            },
            "thn_lulus": {
                required: true
            }
        };
function show_next(id,nextid){
    console.log('run next');
    if(typeof formValidator != 'undefined')
        formValidator.destroy();

    formValidator = $formPelamar.validate({
        rules:rules[id]
        ,
        errorPlacement: function (error, element) {
            error.insertAfter(element);
            element.focus();  // <- this line is causing your whole problem
        },
        submitHandler: function(form) {
            if(nextid == 'submit')
            {
                form.submit();
            }
            else
            {
                $('#'+nextid).tab('show');
            }
        }
    });
    return false;
}

function show_prev(previd){
    $('a#'+previd).tab('show');
    console.log('run prev');
    console.log(previd);
    return false;
}

$(function(){
    $('input.form-control').keypress(function (e) {
      if (e.which == 13) {
        return false;    //<---- Add this line
      }
    });
    show_next('trigger-umum','trigger-alamat');
    $formKontak.validate({
        rules: {
            "telepon_rumah": {
                required: "required",
                phoneNumber: true,
                minlength: 10,
                maxlength: 15
            },
            "hp": {
                required: "required",
                phoneNumber: true,
                minlength: 10,
                maxlength: 12
            }
        },
        submitHandler: function(form) {
            var dataForm    = $(form).serializeArray();
            var target_url  = page_url+'save_kontak/'+pelamar_id;
            var data        = convert_array_to_object(dataForm);
            console.log(data);
            // save_kontak(target_url,data);
            MyModel.insert(target_url,data,function(data){
                console.log(data);
                get_kontak();
                $('#add_kontak_modal').modal('hide');
                $notif.text(data.info);
                $notif.fadeIn('slow').delay('3500').fadeOut('slow');
                $formKontak[0].reset();
            });
            return false;
        }
    });
});

function get_kontak() {
    var page_url    = '/api/hrd/rekrutment/pelamar/get_kontak/'+pelamar_id;
    var columns     = [
        {
            "data": "nama",
        },
        {
            "data": "hubungan",
        },
        {
            "data": "telepon_rumah",
        },
        {
            "data": "hp",
        },
        {
            "data": "action",
            "render": function(data, type, row, meta) {
                return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/rekrutment/pelamar/hapus_kontak/' + row.uid + '" onClick="delete_kontak(this)"><i class="fa fa-trash"></i></span>';
            },
            "sortable":false
        }
    ];
    MyModel.page_url = page_url;
    MyModel.get('tabel-kontak',columns);
}

function delete_kontak(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    MyModel.delete(url,function(data){
        console.log(data);
        get_kontak();
        $notif.text(data.info);
        $notif.fadeIn('slow').delay('3500').fadeOut('slow');
    });
    return false;
}

function tambah_kontak(){
    $('#add_kontak_modal').modal('show');
}

function convert_array_to_object(dataArray){
    var result = {};
    dataArray.forEach(function(item,index){
        result[item.name] = item.value;
    });
    return result;
}

var MyModel = {
    page_url : '/api/hrd/rekrutment/pelamar/',
    insert : function(page_url,data,callback){
        $.ajax({
            'url':base_url+page_url,
            'type':'POST',
            'dataType':'json',
            'data':data,
            'success': function(data){
                return callback(data);
            }
        });
    },
    delete : function(url,callback){
        $.ajax({
            'url':url,
            'type':'POST',
            'dataType':'json',
            'success': function(data){
                return callback(data);
            }
        });
    },
    get : function(tabel,columns,data){
        console.log('run');
        var page_url = this.page_url;
        if(typeof the_table != 'undefined'){
            the_table.destroy();
        }
        the_table = $("#"+tabel).DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url+page_url,
                "type": "POST",
                "dataType":"json",
                "data": data
            },
            "columns": columns
        });
    }
}
