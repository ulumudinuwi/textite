$formLisensi         = $('#form-lisensi');
$notifKualifikasi   = $('#notif-kualifikasi');
$modalLisensi        = $('#add_lisensi_modal');

$(function(){
    $formLisensi.validate({
        submitHandler: function(form) {
            var dataForm    = $(form).serializeArray();
            var target_url  = page_url+'save_lisensi/'+pelamar_id;
            var data        = convert_array_to_object(dataForm);
            console.log(data);
            // save_lisensi(target_url,data);
            MyModel.insert(target_url,data,function(data){
                console.log(data);
                Lisensi.getLisensi();
                $modalLisensi.modal('hide');
                $notifKualifikasi.text(data.info);
                $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
                $formLisensi[0].reset();
            });
            return false;
        }
    });
});

var Lisensi = {
    getLisensi: function() {
        var page_url    = '/api/hrd/rekrutment/pelamar/get_lisensi/'+pelamar_id;
        var columns     = [
            {
                "data": "lisensi_nama",
            },
            {
                "data": "deskripsi",
            },
            {
                "data": "action",
                "render": function(data, type, row, meta) {
                    return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/rekrutment/pelamar/hapus_lisensi/' + row.uid + '" onClick="Lisensi.deleteLisensi(this)"><i class="fa fa-trash"></i></span>';
                },
                "sortable":false
            }
        ];
        MyModel.page_url = page_url;
        MyModel.get('tabel-lisensi',columns);
    },
    deleteLisensi: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.delete(url,function(data){
            console.log(data);
            Lisensi.getLisensi();
            $notifKualifikasi.text(data.info);
            $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahLisensi: function(){
        $modalLisensi.modal('show');
    }
}
