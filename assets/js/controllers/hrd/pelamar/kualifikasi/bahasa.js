$formBahasa         = $('#form-bahasa');
$notifKualifikasi   = $('#notif-kualifikasi');
$modalBahasa        = $('#add_bahasa_modal');

$(function(){
    $formBahasa.validate({
        submitHandler: function(form) {
            var dataForm    = $(form).serializeArray();
            var target_url  = page_url+'save_bahasa/'+pelamar_id;
            var data        = convert_array_to_object(dataForm);
            console.log(data);
            // save_bahasa(target_url,data);
            MyModel.insert(target_url,data,function(data){
                console.log(data);
                Bahasa.getBahasa();
                $modalBahasa.modal('hide');
                $notifKualifikasi.text(data.info);
                $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
                $formBahasa[0].reset();
            });
            return false;
        }
    });
});

var Bahasa = {
    getBahasa: function() {
        var page_url    = '/api/hrd/rekrutment/pelamar/get_bahasa/'+pelamar_id;
        var columns     = [
            {
                "data": "bahasa",
            },
            {
                "data": "membaca_level",
            },
            {
                "data": "menulis_level",
            },
            {
                "data": "berbicara_level",
            },
            {
                "data": "action",
                "render": function(data, type, row, meta) {
                    return '<span style="cursor:pointer" class="text-danger text-center" href="'+base_url+'/api/hrd/rekrutment/pelamar/hapus_bahasa/' + row.uid + '" onClick="Bahasa.deleteBahasa(this)"><i class="fa fa-trash"></i></span>';
                },
                "sortable":false
            }
        ];
        MyModel.page_url = page_url;
        MyModel.get('tabel-bahasa',columns);
    },
    deleteBahasa: function(element){
        console.log($(element).attr('href'));
        var url = $(element).attr('href');
        MyModel.delete(url,function(data){
            console.log(data);
            Bahasa.getBahasa();
            $notifKualifikasi.text(data.info);
            $notifKualifikasi.fadeIn('slow').delay('3500').fadeOut('slow');
        });
        return false;
    },
    tambahBahasa: function(){
        $modalBahasa.modal('show');
    }
}
