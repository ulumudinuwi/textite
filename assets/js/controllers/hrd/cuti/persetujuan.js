var the_table;
$unitusaha   = $('#unitusaha');
$statuscuti  = $('#status_cuti');
$jeniscuti   = $('#jenis_cuti');
var page_url = '/api/hrd/cuti';
$(function(){

    $unitusaha.change(function(){
        unitusaha   = $unitusaha.val();
        statuscuti  = $statuscuti.val();
        jeniscuti   = $jeniscuti.val();
        get_data(statuscuti,jeniscuti,unitusaha);
    });

    $statuscuti.change(function(){
        unitusaha   = $unitusaha.val();
        statuscuti  = $statuscuti.val();
        jeniscuti   = $jeniscuti.val();
        console.log('status ',statuscuti);
        get_data(statuscuti,jeniscuti,unitusaha);
    });

    $jeniscuti.change(function(){
        unitusaha   = $unitusaha.val();
        statuscuti  = $statuscuti.val();
        jeniscuti   = $jeniscuti.val();
        get_data(statuscuti,jeniscuti,unitusaha);
    });

    get_data('pengajuan');
    $statuscuti.val('pengajuan');

});

function notif(data){
    $notif = $(document).find('#notif');
    console.log($notif.text());
    console.log(data);
    if(!$notif.hasClass(data.info))
    {
        if(data.class_notif == 'alert-info')
            $notif.removeClass('alert-danger');
        else if(data.class_notif == 'alert-danger')
            $notif.removeClass('alert-info');
    }
    $notif.find('b').text(data.info);
    $notif.addClass(data.class_notif);

    $notif.fadeIn('slow');
    setTimeout(function(){
        $notif.fadeOut('slow');
    },1500);
}

function set_status_cuti(res){
    $status = $(document).find('#status-cuti');
    data = res.data;
    $status.find('span').removeClass('label-warning');
    $status.find('span').removeClass('label-info');
    $status.find('span').removeClass('label-danger');
    if(data.status == 'sedang cuti')
    {
        $status.find('span').addClass('label-success');
    }
    else if(data.status == 'pengajuan')
    {
        $status.find('span').addClass('label-info');
    }
    else
    {
        $status.find('span').addClass('label-danger');
    }
    $status.find('span').text(data.status);
}

function view_detail(element){
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}

function accept(element,status_cuti){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    caption = $(element).text();
    $(element).text('Loading...');
    $(element).attr('disabled',true);

    $.ajax({
        'url':url,
        'type':'POST',
        'data':{status_cuti:status_cuti},
        'dataType':'json',
        'success': function(data){
            $(element).text(caption);
            $(element).attr('disabled',false);
            console.log(data);
            //notif muncul
            notif(data);
            //rubah status
            set_status_cuti(data);
            unitusaha   = $unitusaha.val();
            statuscuti  = $statuscuti.val();
            jeniscuti   = $jeniscuti.val();
            console.log('status',statuscuti)
            get_data(statuscuti,jeniscuti,unitusaha);
        }
    });
    return false;
}

function get_data(statuscuti,jeniscuti,unitusaha){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable_lowongan").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+page_url,
            "type": "POST",
            "dataType":"json",
            "data": {status_cuti:statuscuti,jenis_cuti:jeniscuti,unit_usaha:unitusaha}
        },
        "columns": [
            {
                "data": "no",
                "searchable": false
            },
            {
                "data": "nik"
            },
            {
                "data": "nama",
                "render": function(data, type, row, meta) {
                    return '<span class="text-primary" style="cursor:pointer" href="'+base_url+'/hrd/persetujuan_cuti/view/' + row.uid + '" onClick="view_detail(this)">' + data + '</span>';
                }
            },
            {
                "data": "unit_usaha"
            },
            {
                "data": "jenis_cuti"
            },
            {
                "data": "tgl_mulai_cuti",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "tgl_selesai_cuti",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "hari",
                "render": function(data){
                    return data+' Hari';
                }
            },
            {
                "data": "status_cuti",
                "render": function(data){
                    if(data == 'pengajuan')
                        label = 'label-info';
                    else if(data == 'sedang cuti')
                        label = 'label-success';
                    else
                        label = 'label-danger';
                    return '<span class="label '+label+'">'+data+'</span>';
                }
            },
            {
                "data": "hari_sisa_cuti",
                "render": function(data){
                    return data+' Hari';
                }
            },

            {
                "data": "masa_aktif"
            }

        ],
        "fnCreatedRow": function (row, data, index) {
            $('td', row).eq(0).html(index + 1);
        }
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {
        the_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();*/
}
