var the_table;
$unitusaha   = $('#unitusaha');
$statuscuti  = $('#status_cuti');
$jeniscuti   = $('#jenis_cuti');
var page_url = '/api/hrd/cuti';
$(function(){

    $unitusaha.change(function(){
        unitusaha   = $unitusaha.val();
        statuscuti  = $statuscuti.val();
        jeniscuti   = $jeniscuti.val();
        get_data(statuscuti,jeniscuti,unitusaha);
    });

    $statuscuti.change(function(){
        unitusaha   = $unitusaha.val();
        statuscuti  = $statuscuti.val();
        jeniscuti   = $jeniscuti.val();
        get_data(statuscuti,jeniscuti,unitusaha);
    });

    $jeniscuti.change(function(){
        unitusaha   = $unitusaha.val();
        statuscuti  = $statuscuti.val();
        jeniscuti   = $jeniscuti.val();
        get_data(statuscuti,jeniscuti,unitusaha);
    });

    get_data();
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}

function get_data(statuscuti,jeniscuti,unitusaha){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable_lowongan").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+page_url,
            "type": "POST",
            "dataType":"json",
            "data": {status_cuti:statuscuti,jenis_cuti:jeniscuti,unit_usaha:unitusaha}
        },
        "columns": [
            {
                "data": "no",
                "searchable": false
            },
            {
                "data": "nik"
            },
            {
                "data": "nama",
                "render": function(data, type, row, meta) {
                    return '<span class="text-primary" style="cursor:pointer" href="'+base_url+'/hrd/cuti/view/' + row.uid + '" onClick="view_detail(this)">' + data + '</span>';
                }
            },
            {
                "data": "unit_usaha"
            },
            {
                "data": "jenis_cuti"
            },
            {
                "data": "tgl_mulai_cuti",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "tgl_selesai_cuti",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "hari",
                "render": function(data){
                    return data+' Hari';
                }
            },
            {
                "data": "status_cuti",
                "render": function(data){
                    if(data == 'pengajuan')
                        label = 'label-info';
                    else if(data == 'sedang cuti')
                        label = 'label-success';
                    else
                        label = 'label-danger';
                    return '<span class="label '+label+'">'+data+'</span>';
                }
            },
            {
                "data": "hari_sisa_cuti",
                "render": function(data){
                    return data+' Hari';
                }
            },

            {
                "data": "masa_aktif"
            }

        ],
        "fnCreatedRow": function (row, data, index) {
            $('td', row).eq(0).html(index + 1);
        }
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {
        the_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();*/
}
