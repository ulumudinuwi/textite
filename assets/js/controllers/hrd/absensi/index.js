var the_table;
$ringkasan1   = $('#ringkasan1');
$ringkasan2   = $('#ringkasan2');

$unitusaha   = $('#unitusaha');
$statuscuti  = $('#status_cuti');
$jeniscuti   = $('#jenis_cuti');
$inputtgl    = $('#tabel-absensi').find('input[name=tgl]');
$ketTgl      = $('.ket-tgl'); //<span>
var page_url = '/api/hrd/absensi';
base_file           = base_url.replace('/index.php','');
form_status_kehadiran = '';
form_type_lembur = '';
$(function(){
    $('.pickadate').pickadate({
        // options
        format: 'dd-mm-yyyy'
    });
    bulan = [];
    bulan['01'] = 'Januari';
    bulan['02'] = 'Februari';
    bulan['03'] = 'Maret';
    bulan['04'] = 'April';
    bulan['05'] = 'Mei';
    bulan['06'] = 'Juni';
    bulan['07'] = 'Juli';
    bulan['08'] = 'Agustus';
    bulan['09'] = 'September';
    bulan['10'] = 'Oktober';
    bulan['11'] = 'November';
    bulan['12'] = 'Desember';

    $('#tgl').change(function(){
        var t = $(this).val().split('-');
        var tgl = t[2]+'-'+t[1]+'-'+t[0];
        $inputtgl.val(tgl);
        console.log(tgl);
        $ketTgl.html(t[0]+' '+bulan[t[1]]+' '+t[2]);
        get_data(tgl);
        get_ringkasan_kehadiran(tgl);
    });

    $.ajax({
        url: base_url+page_url+'/get_form_status_kehadiran',
        dataType: "HTML",
        success: function(data){
            form_status_kehadiran = data;
        }
    });

    $.ajax({
        url: base_url+page_url+'/get_form_type_lembur',
        dataType: "HTML",
        success: function(data){
            form_type_lembur = data;
        }
    });

    $unitusaha.change(function(){
        unitusaha   = $unitusaha.val();
        statuscuti  = $statuscuti.val();
        jeniscuti   = $jeniscuti.val();
        get_data(statuscuti,jeniscuti,unitusaha);
    });

    $statuscuti.change(function(){
        unitusaha   = $unitusaha.val();
        statuscuti  = $statuscuti.val();
        jeniscuti   = $jeniscuti.val();
        get_data(statuscuti,jeniscuti,unitusaha);
    });

    $jeniscuti.change(function(){
        unitusaha   = $unitusaha.val();
        statuscuti  = $statuscuti.val();
        jeniscuti   = $jeniscuti.val();
        get_data(statuscuti,jeniscuti,unitusaha);
    });

    var tgl = $('#tabel-absensi').find('input[name=tgl]').val();
    get_data(tgl);

    // Basic initialization
    $(".jam_masuk").AnyTime_picker({
        format: "%H:%i",
        firstDOW: 1
    });

    $(".jam_keluar").AnyTime_picker({
        format: "%H:%i",
        firstDOW: 1
    });
});

function get_data(tgl){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#data_pegawai").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+page_url,
            "type": "POST",
            "dataType":"json",
            "data": {tgl:tgl}
        },
        "columns": [
            {
                "data": "nama",
                "render": function(data, type, row, meta) {
                    if(row.foto)
                        src = base_file+row.foto;
                    else
                        src = assets_url+'/img/user.jpg';
                    foto = '<img src="'+src+'" class="img-responsive">';
                    result = '<div class="row">'+
                    '<div class="col-sm-4">'+
                    foto+
                    '</div>'+
                    '<div class="col-sm-8">'+
                    '<div style="padding-top:20px"><a href="'+base_url+'/hrd/kehadiran/absensi/detail_absensi/' + row.uid_pegawai + '/' + tgl +'">'+ data + '</a></div>'+
                    '</div>'+
                    '</div>'+
                    '<input type="hidden" name="pegawai_id" value="'+row.pegawai+'">';
                    return result;
                }
            },
            {
                "data": "status_kehadiran",
                "render": function(data, type, row, meta){
                    return form_status_kehadiran;
                }
            },
            {
                "data": "jam_masuk",
                "render": function(data, type, row, meta){

                    return '<input name="jam_masuk" onkeyup="simpan_absensi(this)" type="text" class="form-control jam_masuk anytime-time">';
                }
            },
            {
                "data": "jam_keluar",
                "render": function(data, type, row, meta){

                    return '<input name="jam_keluar" onkeyup="simpan_absensi(this)" type="text" class="form-control jam_masuk anytime-time">';
                }
            },
            {
                "data": "jam_lembur",
                "render": function(data, type, row, meta){

                    return '<input name="jam_lembur" onkeyup="simpan_absensi(this)" onchange="simpan_absensi(this) type="number" class="form-control jam_masuk anytime-time">';
                }
            },
            {
                "data": "type_lembur",
                "render": function(data, type, row, meta){
                    return form_type_lembur;
                }
            },
            {
                "data": "terlambat",
                "render": function(data, type, row, meta){
                    return '<div class="input-group"><input name="terlambat" onkeyup="simpan_absensi(this)" onchange="simpan_absensi(this)" type="number" class="form-control jam_masuk anytime-time"><span class="input-group-addon">menit</span></div>';
                }
            },

        ],
        "fnCreatedRow": function (row, data, index) {
            console.log(data);
            $('td', row).find('select[name=status_kehadiran]').val(data.status_kehadiran);
            $('td', row).find('select[name=type_lembur]').val(data.type_lembur);
            $('td', row).find('input[name=jam_masuk]').val(data.jam_masuk);
            $('td', row).find('input[name=jam_keluar]').val(data.jam_keluar);
            $('td', row).find('input[name=jam_lembur]').val(data.jam_lembur);
            $('td', row).find('input[name=terlambat]').val(data.terlambat);
        }
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {
        the_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();*/
}

function simpan_absensi(element){
    var data = {};
    name = $(element).prop('name');
    if(name == 'terlambat')
        $tr = $(element).parent().parent().parent();
    else
        $tr = $(element).parent().parent();

    nama = $tr.find('a').html();
    console.log(nama);
    var tgl                 = $('#tabel-absensi').find('input[name=tgl]').val();
    var status_kehadiran    = $tr.find('select[name=status_kehadiran]').val();
    var jam_masuk           = $tr.find('input[name=jam_masuk]').val();
    var jam_keluar          = $tr.find('input[name=jam_keluar]').val();
    var jam_lembur          = $tr.find('input[name=jam_lembur]').val();
    var type_lembur         = $tr.find('select[name=type_lembur]').val();
    var terlambat           = $tr.find('input[name=terlambat]').val();
    var pegawai_id          = $tr.find('input[name=pegawai_id]').val();
    console.log(data);

    $.ajax({
        url: base_url+page_url+'/simpan_absensi',
        type: 'POST',
        data: {
            tgl:tgl,
            status_kehadiran:status_kehadiran,
            jam_masuk:jam_masuk,
            jam_keluar:jam_keluar,
            jam_lembur:jam_lembur,
            type_lembur:type_lembur,
            terlambat:terlambat,
            pegawai_id:pegawai_id
        },
        dataType: 'JSON',
        success : function(data){
            console.log(data);
        }
    });
}

function get_ringkasan_kehadiran(tgl)
{
    $.ajax({
        url: base_url+page_url+'/get_ringkasan_kehadiran',
        type: 'POST',
        data: {
            tgl:tgl
        },
        dataType: 'JSON',
        success : function(data){
            console.log(data);
            ringkasan1 = '';
            index = 0;
            for (i = index; i <=5;i++) {
                row1 = '<tr>'+
                '<td>'+data[i].nama+'</td>'+
                '<td class="text-center">'+data[i].jml+'</td>'+
                '<td class="text-center">'+data[i].persen+'</td>'+
                '</tr>';
                ringkasan1 += row1;
            }
            $ringkasan1.find('tbody').html(ringkasan1);

            ringkasan2 = '';
            for (i = 6; i <=(data.length-1);i++) {
                row2 = '<tr>'+
                '<td>'+data[i].nama+'</td>'+
                '<td class="text-center">'+data[i].jml+'</td>'+
                '<td class="text-center">'+data[i].persen+'</td>'+
                '</tr>';
                ringkasan2 += row2;
            }
            $ringkasan2.find('tbody').html(ringkasan2);
        }
    });
}

