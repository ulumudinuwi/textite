var change_tgl = false;
$(function(){
    var the_table;
    $tgl        = $('#tgl_penyelenggaraan');
    $unitusaha  = $('#unitusaha');

    $tgl.daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        locale: {
          format: 'DD/MM/YYYY'
        },
        startDate: {
            value: moment().subtract(29, 'days')
        },
        endDate: {
            value: moment()
        }
    });

    $tgl.on('apply.daterangepicker', function(ev, picker) {
        console.log('jalan');
        change_tgl = true;
        tgl_mulai = picker.startDate.format('YYYY-MM-DD');
        tgl_akhir = picker.endDate.format('YYYY-MM-DD');
        unitusaha = $unitusaha.val();
        get_data(tgl_mulai,tgl_akhir,unitusaha);
    });

    $unitusaha.change(function(){
        unitusaha = $unitusaha.val();
        if(change_tgl)
        {
            tgl_mulai = $tgl.data('daterangepicker').startDate.format('YYYY-MM-DD');
            tgl_akhir = $tgl.data('daterangepicker').endDate.format('YYYY-MM-DD');
        }
        else
        {
            tgl_mulai = '';
            tgl_akhir = '';
        }
        console.log('mulai ',tgl_mulai);
        console.log('akhir ',tgl_akhir);
        get_data(tgl_mulai,tgl_akhir,unitusaha);
    });

    get_data();
});

function view_detail(element){
    console.log($(element).attr('href'));
    var url = $(element).attr('href');
    $.ajax({
        'url':url,
        'type':'POST',
        'dataType':'html',
        'success': function(data){
            console.log(data);
            $('#detail_modal .modal-dialog').html(data);
            $('#detail_modal').modal('show');
        }
    });
    return false;
}
function get_data(tgl_mulai,tgl_akhir,unitusaha){
    console.log('run');
    if(typeof the_table != 'undefined'){
        the_table.destroy();
    }
    the_table = $("#dataTable_lowongan").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+'/api/hrd/rekrutment/lowongan',
            "type": "POST",
            "dataType":"json",
            "data": {tgl_mulai:tgl_mulai,tgl_akhir:tgl_akhir,unitusaha:unitusaha}
        },
        "columns": [
            {
                "data": "no",
                "searchable": false
            },
            {
                "data": "created_at",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "nomor_lowongan",
                "render": function(data, type, row, meta) {
                    return '<span class="text-primary" style="cursor:pointer" href="'+base_url+'/hrd/rekrutment/lowongan/view/' + row.uid + '" onClick="view_detail(this)">' + data + '</span>';
                }
            },
            {
                "data": "tanggal_mulai",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "tanggal_akhir",
                "render": function(data, type, row, meta) {
                    if(data)
                    return moment(data).format('DD/MM/YYYY');
                    else
                    return '';
                }
            },
            {
                "data": "jabatan"
            },
            {
                "data": "spesifikasi_pekerjaan"
            },
            {
                "data": "unit_usaha"
            },
            {
                "data": "unit_kerja"
            },
            /*{
                "data": "status_pegawai"
            },
            {
                "data": "requirement"
            },*/

        ],
        "fnCreatedRow": function (row, data, index) {
            $('td', row).eq(0).html(index + 1);
        }
    });
    /*the_table.on( 'order.dt search.dt stateLoaded.dt', function () {
        the_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();*/
}
