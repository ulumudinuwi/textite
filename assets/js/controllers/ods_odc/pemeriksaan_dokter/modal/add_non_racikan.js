/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddNonRacikanController', modalAddNonRacikanController);

/**
 * Modal Add Non Racikan
 * @type {string[]}
 */
modalAddNonRacikanController.$inject = ['$scope', 'api', '$http', '$uibModalInstance', '$log', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'items'];
function modalAddNonRacikanController($scope, api, $http, $uibModalInstance, $log, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, items) {

    var url = base_url + "/api/master/obat/non_racikan/" + items.kelompok_pasien_id;
    var _tarif = [];
    $rootScope.searchObat = base_url + '/api/master/obat/non_racikan?kelompok_pasien='+items.pasien.kelompok_pasien_id+'&s=';

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            // Either you specify the AjaxDataProp here
            // dataSrc: 'data',
            url: url,
            type: 'POST'
        })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('createdRow', function (row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('serverSide', true)
        .withPaginationType('full_numbers');
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('kode_obat').withClass('row-5 text-center').withTitle('Pilih').notSortable().renderWith(function (data, type, full, meta) {
            return '<input type="checkbox"  ng-model="tarif' + full.tarif_id + '" ng-click="selectTarif(tarif' + full.tarif_id + ',' + full.tarif_id + ')"/>';
        }),
        DTColumnBuilder.newColumn('kode_obat').withTitle('Kode').withClass('row-10'),
        DTColumnBuilder.newColumn('nama_obat').withTitle('Nama'),
        DTColumnBuilder.newColumn('harga').withTitle('Biaya').withClass('row-10')
    ];

    $scope.selectTarif = function (data, id) {
        if (data) {
            $http.get(base_url + '/api/master/obat/tarif_by', {
                params: {
                    id: id
                }
            }).then(function (tarif) {
                _tarif.push(tarif.data);
            })
        } else {
            _.remove(_tarif, function (data) {
                return data.id == id;
            });
        }
    };

    $scope.close = function () {
        console.log("data : ", angular.copy(_tarif));
        $uibModalInstance.close(_tarif);
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

}

