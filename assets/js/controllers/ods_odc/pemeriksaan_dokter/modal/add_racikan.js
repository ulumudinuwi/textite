/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddRacikanController', modalAddRacikanController);

/**
 * Modal Add Racikan
 * @type {string[]}
 */
modalAddRacikanController.$inject = ['$scope', 'api', '$http', '$uibModalInstance', '$log', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'items', '$filter'];
function modalAddRacikanController($scope, api, $http, $uibModalInstance, $log, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, items, $filter) {

    var url = base_url + "/api/master/obat/non_racikan/" + items.data.kelompok_pasien_id;
    var _tarif = [];

    $rootScope.searchObat = base_url + '/api/master/obat/non_racikan?kelompok_pasien='+items.data.kelompok_pasien_id+'&s=';


    $scope.qty = [];
    $scope.harga = [];
    $scope.diskon = [];
    $scope.dataObat = [];

    $scope.isTindakanEdit = false;


    $scope.selectTarif = selectTarif;
    $scope.onChangeQty = onChangeQty;
    $scope.onChangeDiskon = onChangeDiskon;
    $scope.onCheckObat = onCheckObat;
    $scope.obatQtyChange = obatQtyChange;
    $scope.obatDiskonChange = obatDiskonChange;

    $scope.close = close;
    $scope.dismiss = dismiss;

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            // Either you specify the AjaxDataProp here
            // dataSrc: 'data',
            url: url,
            type: 'POST'
        })
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('createdRow', function (row, data, dataIndex) {
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('serverSide', true)
        .withPaginationType('full_numbers');
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('kode_obat').withClass('row-5 text-center').withTitle('Pilih').notSortable().renderWith(selectRender),
        DTColumnBuilder.newColumn('kode_obat').withTitle('Kode').withClass('row-10'),
        DTColumnBuilder.newColumn('nama_obat').withTitle('Nama'),
        DTColumnBuilder.newColumn('nama_obat').withTitle('Qty').withClass('row-10').notSortable().renderWith(qtyRender),
        DTColumnBuilder.newColumn('nama_obat').withTitle('Diskon %').withClass('row-10').notSortable().renderWith(diskonRender),
        DTColumnBuilder.newColumn('harga').withTitle('Biaya').withClass('row-10').renderWith(biayaRender)
    ];
    $scope.dtInstance = {};

    function onChangeDiskon(data, diskon) {

    }
    function onChangeQty(id, qty) {
        var totalHarga = qty[id] * $scope.harga[id];
        $scope.harga[id] = totalHarga;


    }
    function onCheckObat(obat){
        obat.edit = true;
        console.log(obat);
        _tarif.push(obat);
    }

    function obatQtyChange(idx, qty) {
        var total = $scope.dataObat[idx].harga * qty
        $scope.dataObat[idx].qty = qty;
        $scope.dataObat[idx].total = total;

        _.find(_tarif, {qty: qty, total: total}, {})

    }

    function obatDiskonChange(idx, diskon) {
        var diskon = diskon / 100;
        var diskonTotal = ($scope.dataObat[idx].harga * $scope.dataObat[idx].qty) * diskon;
        var total = ($scope.dataObat[idx].harga * $scope.dataObat[idx].qty) - diskonTotal;
        $scope.dataObat[idx].total = total;

    }

    function selectRender(data, type, full, meta) {
        return '<input type="checkbox"  ng-model="tarif' + full.tarif_id + '" ng-click="selectTarif(tarif' + full.tarif_id + ',' + full.tarif_id + ')"/>';
    }
    function biayaRender(data, type, full, meta) {
        $scope.harga[full.tarif_id] = full.harga;
        return '<input type="text" ng-disabled="true" ng-model="harga['+full.tarif_id+']" class="form-control" />';
    }
    function qtyRender(data, type, full, meta) {
        return '<input type="number" ng-disabled="!tarif' + full.tarif_id + '" ng-model="qty[' + full.tarif_id + ']" class="form-control" ng-change="onChangeQty(' + full.tarif_id + ',qty)">';
    }
    function diskonRender(data, type, full, meta) {
        return '<input type="number" ng-disabled="!tarif' + full.tarif_id + '" ng-model="diskon[' + full.tarif_id + ']" class="form-control" ng-change="onChangeDiskon(' + full.tarif_id + ',diskon)">';
    }
    function selectTarif(data, id) {
        if (data) {
            $http.get(base_url + '/api/master/obat/tarif_by', {
                params: {
                    id: id
                }
            }).then(function (tarif) {
                _tarif.push(tarif.data);
                console.log({
                    qty: $scope.qty[id],
                    diskon: $scope.diskon[id]
                });
            })
        } else {
            _.remove(_tarif, function (data) {
                return data.id == id;
            });
        }
    }

    function successObat(result){
        $scope.dataObat = _.each(result.data.data, function(data){
            data.qty = 1;
            data.diskon = 0;
            data.total = parseFloat(data.harga);
            data.edit = false;
            return data;
        });

    }

    function errorObat(err){
        console.log(err);
    }

    function close() {
        console.log("data : ", angular.copy(_tarif));
        $uibModalInstance.close(_tarif);
    }
    function dismiss() {
        $uibModalInstance.dismiss();
    }


    $http.get(url).then(successObat, errorObat);

}

