/**
 * Created by agungrizkyana on 8/20/16.
 */
app.controller('MasterDokterPoliInsertController', ['$rootScope', '$scope', 'api', '$http', 'CONFIG', 'toastr',
    function ($rootScope, $scope, api, $http, CONFIG, toastr) {

        $scope.data = {};

        $scope.selectedPegawai = function (item) {
            console.log(item);
            if (item) {
                $scope.data.pegawai = angular.copy(item.originalObject);

            }
        };

        $scope.selectedLayanan = function (item) {
            console.log("layanan");
            console.log(item);
            if (item) {
                $scope.data.layanan = angular.copy(item.originalObject);

            }
        };

        $scope.save = function () {
            console.log(angular.copy($scope.data));

            var req = {
                method: 'POST',
                url: base_url + '/api/master/dokter_poli/save',
                data: 'data=' + JSON.stringify(angular.copy($scope.data)),
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                }
            };
            $http(req).then(function (result) {
                console.log("result save, ", result);
                toastr.success('Simpan Dokter Poli Jalan Berhasil !', 'Sukses');
            }, function (err) {
                console.log("err", err);
                toastr.error(err.data, 'Error');
            });

        };

        $scope.clear = function () {
            $scope.data = {};
        }

    }
]);