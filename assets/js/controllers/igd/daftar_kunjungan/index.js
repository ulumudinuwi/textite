
app.controller('DaftarKunjunganIndexController', daftarKunjunganIndexController);
daftarKunjunganIndexController.$inject = ['$scope', 'api', '$http', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'toastr', '$uibModal', '$timeout', '$q'];
function daftarKunjunganIndexController($scope, api, $http, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, toastr, $uibModal, $timeout, $q) {

    $scope.data = {};
    $scope.filter = {};

    $scope.data = {
        tanggalDari: moment().format('DD/MM/YYYY'),
        tanggalSampai: moment().format('DD/MM/YYYY')
    };


    $scope.save = save;
    $scope.selected = selected;

    $scope.onChangeRangeWaktu = onChangeRangeWaktu;
    $scope.onChangeKelompokPasien = onChangeKelompokPasien;
    $scope.onChangeLayanan = onChangeLayanan;
    $scope.onChangeDokter = onChangeDokter;

    $scope.cetakNota = cetakNota;
    $scope.cetakTracer = cetakTracer;
    $scope.cetakStatus = cetakStatus;
    $scope.cetakFJP = cetakFJP;

    $scope.cancelKunjungan = cancelKunjungan;

    $http.get(base_url + "/api/master/pegawai").then(successPegawai, errorPegawai);
    $http.get(base_url + "/api/master/kelompok_pasien").then(successKelompokPasien, errorKelompokPasien);
    $http.get(base_url + "/api/master/layanan").then(successLayanan, errorLayanan);

    function cetakNota(id) {
        console.log(id);

        // window.open(base_url + '/api/rawat_jalan/pendaftaran/cetak_nota/' + pendaftaran.pelayanan.id + '/' + pendaftaran.nomor_antrian, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function cetakTracer(id) {

        // window.open(base_url + '/api/rawat_jalan/pendaftaran/cetak_tracer/' + pendaftaran.pelayanan.id + '/' + pendaftaran.nomor_antrian, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function cetakStatus(id) {

        // window.open(base_url + '/api/rawat_jalan/pendaftaran/cetak_status?no_rekam_medis=' + '02.00.000005', '_blank', 'location=yes,height=720,width=920,scrollbars=yes,status=yes');
    }

    function cetakFJP(id) {

        // window.open(base_url + '/api/rawat_jalan/pendaftaran/cetak_fjp/umum/11?penanggung=1', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function save() {
        console.log(angular.copy($scope.data));
    }
    function selected(item) {
        console.log("masuk selected");
        if (item) {
            var obj = angular.copy(item.originalObject);
            $scope.data = obj;
            $scope.data.tanggal = new Date($scope.data.tanggal);
            $scope.$emit('selectedPasien', obj);
            console.log(obj);
            // $scope.reload();
            buildTable({
                noRekamMedis: item.originalObject.no_rekam_medis,
            });
        }

    }
    function successPegawai(result) {
        //console.log("provinsi");

        $scope.dataDokter = _.map(angular.copy(result.data), function (data) {
            return {
                id: data.id,
                nama: data.nama
            }
        });
    }
    function errorPegawai(err){
        console.log(err);
    }
    function successKelompokPasien(result) {
        $scope.dataKelompokPasien = _.map(angular.copy(result.data), function (data) {
            return {
                id: data.id,
                nama: data.nama
            }
        });
    }
    function errorKelompokPasien(err){
        console.log(err);
    }
    function successLayanan(result) {
        $scope.dataLayanan = _.map(angular.copy(result.data), function (data) {
            return {
                id: data.id,
                nama: data.nama
            }
        });
    }
    function errorLayanan(err){
        console.log(err);
    }
    function onChangeRangeWaktu(range) {
        var tanggalDari = moment(range.substr(0, 10)).format('YYYY-MM-DD'),
            tanggalSampai = moment(range.substr(13, 10)).format('YYYY-MM-DD');

        // $scope.dtInstance.reload();
        // buildTable({
        //     tanggalDari: tanggalDari,
        //     tanggalSampai: tanggalSampai
        // });

        // setTimeout(function(){
        //     $scope.filter = {
        //         tanggalDari: tanggalDari,
        //         tanggalSampai : tanggalSampai
        //     };
        //     $scope.dtInstance.reloadData();
        // }, 300);
    }
    function onChangeKelompokPasien(kelompok_pasien){

        // buildTable({
        //     kelompok_pasien : kelompok_pasien
        // });

        $scope.filter = {
            kelompok_pasien : kelompok_pasien
        };


    }
    function onChangeLayanan(layanan){
        // buildTable({
        //     layanan : layanan.id
        // });

        $scope.filter = {
            layanan : layanan.id
        };
        $scope.dtInstance.reloadData();
    }
    function onChangeDokter(dokter){
        // buildTable({
        //     dokter : dokter.id
        // });

        $scope.filter = {
            dokter : dokter.id
        };
        $scope.dtInstance.reloadData();

    }
    function cancelKunjungan(id) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalCancelKunjungan.html',
            controller: 'ModalCancelKunjunganController',
            size: 'sm',
            resolve : {
                items : function(){
                    return id
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            console.log($scope.dtInstance);
            $scope.dtInstance.reloadData();
        }, function () {

        });
    }


    var url = base_url + '/api/rawat_jalan/daftar_kunjungan/data_daftar_kunjungan';
    var filter = $scope.filter;
    if (filter) {

        if (filter.noRekamMedis) {
            url += '?no_rm=' + filter.noRekamMedis;
        }
        if (filter.tanggalDari) {
            url += "?tanggalDari=" + filter.tanggalDari + "&tanggalSampai=" + filter.tanggalSampai;
        }
        if (filter.kelompok_pasien) {
            url += "?kelompok_pasien=" + filter.kelompok_pasien;
        }
        if (filter.layanan) {
            url += "?layanan=" + filter.layanan;
        }
        if (filter.dokter) {
            url += '?dokter=' + filter.dokter;
        }



    }
    $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
        var defer = $q.defer();
        $http.post(url).then(function(result) {
            defer.resolve(result.data);
        });
        return defer.promise;
    })
        // or here
        .withDataProp('data')
        .withOption('processing', true)
        // .withOption('serverSide', true)
        .withOption('order', [[0, 'desc']])
        .withOption('createdRow', function (row) {
            // Recompiling so we can bind Angular directive to the DT
            $compile(angular.element(row).contents())($scope);
        })
        .withPaginationType('full_numbers');


    $scope.dtColumns = [
        DTColumnBuilder.newColumn('tanggal').withTitle('Tanggal').renderWith(function (data, type, row, meta) {

            return '<a href="pendaftaran/edit/'+row.id+'">'+moment(row.tanggal).format('DD-MMM-YYYY H:mm:s')+'</a>';
        }).withOption('searchable', true),
        DTColumnBuilder.newColumn('no_rekam_medis').withTitle('No MR'),
        DTColumnBuilder.newColumn('no_register').withTitle('No Register'),
        DTColumnBuilder.newColumn('nama_pasien').withTitle('Nama Pasien'),
        DTColumnBuilder.newColumn('nama_layanan').withTitle('Nama Layanan'),
        DTColumnBuilder.newColumn('nama_dokter').withTitle('Nama Dokter').withOption('searchable', true),
        DTColumnBuilder.newColumn('id').withTitle('Action').withClass('text-center').renderWith(function (row, type, data, meta) {

            var link = '<ul class="icons-list">' +
                '<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' +
                '<i class="icon-menu9"></i>' +
                '</a>' +

                '<ul class="dropdown-menu dropdown-menu-right">' +
                '<li><a ng-click="cetakNota('+row+')"><i class="icon-file-excel"></i> Nota</a></li>' +
                '<li><a ng-click="cetakTracer()"><i class="icon-file-word"></i> Tracer</a></li>' +
                '<li><a ng-click="cetakStatus()"><i class="icon-file-word"></i> Status</a></li>' +
                '<li><a ng-click="cetakSJP()"><i class="icon-file-word"></i> SJP</a></li>' +
                '</ul>' +
                '</li>' +
                '</ul>';
            // return '<a ng-click="cancelKunjungan(\'' + row.id + '\')" class="btn btn-danger">Batalkan <i class="fa fa-trash-o"></i></a>';
            return link;
        }).withOption('searchable', false),
        DTColumnDefBuilder.newColumnDef(6).withTitle('Batalkan Kunjungan').withClass('text-center').renderWith(function (data, type, row, meta) {
            return '<a ng-click="cancelKunjungan(\'' + row.id + '\')" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>';

        }).withOption('searchable', false)
    ];
    $scope.dtInstance = {};



}
