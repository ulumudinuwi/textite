/**
 * Created by agungrizkyana on 6/12/17.
 */
var formFilter,
    periode;

$(document).ready(function(){
    formFilter = $("#form-filter");
    periode = $("#periode");

    periode.datepicker({
        autoclose: true,
        format: 'yyyy-mm',
        viewMode: 'months',
        minViewMode: 'months'
    });


    formFilter.submit(function(e){
        e.preventDefault();
        window.location.href = base_url + '/igd/laporan/tindakan/data?' + formFilter.serialize();
        return;
    })
});

function logError(err){
    console.log(err);
}