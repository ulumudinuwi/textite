app.controller('PendaftaranController', pendaftaranController);
pendaftaranController.$inject = ['$rootScope', '$scope', 'api', '$http', 'toastr', '$uibModal', '$q', 'select2ChainedData', 'EVENT', 'CONFIG', '$log', '$timeout', '$compile'];
function pendaftaranController($rootScope, $scope, api, $http, toastr, $uibModal, $q, select2ChainedData, EVENT, CONFIG, $log, $timeout, $compile) {


    $scope.data = {};
    $scope.data.jenisKelamin = 1;
    $scope.data.kewarganegaraan = 1;
    $scope.data.tanggalregister = moment().format('DD/MM/YYYY');
    $scope.data.tgl_reg = moment().format('DD-MM-YYYY');
    $scope.data.tgl_lahir = '';
    $scope.data.tgl_lahir_form = '';
    $scope.data.isOldPasien = 0;
    $scope.data.tarifklaim = 0;

    $scope.data.tindakan = [];

   // $scope.data.nosjp = 'RJ-011-1706-001-00019';
    $scope.paketJaminanWS = {};


    /**
     * Penunjang Medis
     */

    //rootscope var
    $rootScope.rujukan = [];

    // initial vars
    $scope.data.rujukan = {
        rujuk_ok: 0,
        rujuk_rawatinap: 0,
        rujuk_audiometri: 0,
        rujuk_catchlab: 0,
        rujuk_ctscan: 0,
        rujuk_diagnostik_fungsional: 0,
        rujuk_fisioterapi: 0,
        rujuk_laboratorium: 0,
        rujuk_ods_odc: 0,
        rujuk_radiologi: 0
    };
    $scope.data.rujuk = [];

    $scope.openModalAddLaboratorium = addLaboratorium;
    $scope.openModalAddRadiologi = addRadiologi;
    $scope.openModalAddCtScan = addCtScan;
    $scope.openModalAddFisioterapi = addFisioterapi;
    $scope.openModalAddCatchLab = addCatchLab;
    $scope.openModalAddAudiometri = addAudiometri;
    $scope.openModalAddDiagnostikFungsional = addDiagnostikFungsional;


    $scope.isTambah = true;
    $scope.isTambahRumahSakit = false;
    $scope.isTambahPerusahaan = false;
    $scope.isBiodataDisabled = false;
    $scope.isTambahRumahSakit = false;
    $scope.isSubmit = false;
    $scope.isTambahRumahSakit = false;
    $scope.isEditTarifTindakan = false;
    $scope.isTindakanEdit = false;
    $scope.wrongDate = false;
    $scope.isRequiredDokter = true;
    $scope.isNotaPiutang = true;


    $scope.tambahRumahSakit = tambahRumahSakit;
    $scope.cetakNota = cetakNota;
    $scope.cetakTracer = cetakTracer;
    $scope.cetakStatus = cetakStatus;
    $scope.cetakFJP = cetakFJP;
    $scope.cetakGeneralConsent = cetakGeneralConsent;
    $scope.cetakNotaPiutang = cetakNotaPiutang;

    $scope.cetakIdCard = cetakIdCard;
    $scope.cetakKiup = cetakKiup;

    $scope.tambahDataPerusahaanRujukan = tambahDataPerusahaanRujukan;
    $scope.onChangeTitle = onChangeTitle;
    $scope.selectedPegawai = selectedPegawai;
    $scope.selected = selected;
    $scope.save = save;
    $scope.tambahData = tambahData;
    $scope.onChangeProvinsi = onChangeProvinsi;
    $scope.onChangeKota = onChangeKota;
    $scope.onChangeKecamatan = onChangeKecamatan;
    $scope.onChangeDesa = onChangeDesa;
    $scope.onChangeKelompokPasien = onChangeKelompokPasien;
    $scope.onChangeTanggalLahir = onChangeTanggalLahir;
    $scope.onChangeUmur = onChangeUmur;
    $scope.onChangeLayanan = onChangeLayanan;
    $scope.onChangeTarifTindakanQty = onChangeTarifTindakanQty;
    $scope.onChangeTarifTindakanDiskon = onChangeTarifTindakanDiskon;
    $scope.onChangeDokter = onChangeDokter;
    $scope.onChangeTempatLahir = onChangeTempatLahir;
    $scope.onChangePaketJaminan = onChangePaketJaminan;
    $scope.onChangeKewarganegaraan = onChangeKewarganegaraan;

    $scope.onChangePasien = onChangePasien;
    $scope.onChangePenanggung = onChangePenanggung;
    $scope.onChangeAsuransi = onChangeAsuransi;
    $scope.onChangePerusahaan = onChangePerusahaan;

    $scope.tindakanQtyChange = tindakanQtyChange;
    $scope.tindakanDiskonChange = tindakanDiskonChange;
    $scope.verifikasiSep = verifikasiSep;
    $scope.openModalPendaftaranTindakan = openModalPendaftaranTindakan;
    $scope.toggleAnimation = toggleAnimation;
    $scope.deleteTindakan = deleteTindakan;
    $scope.cetakLabel = cetakLabel;
    $scope.cetakSampulRM = cetakSampulRM;
    $scope.openModalPrintSbpk = openModalPrintSbpk;

    $scope.cekMCS = cekMCS;
    $scope.cekPegawai = cekPegawai;

    function onChangePaketJaminan(paket) {
        var paket = (paket == '2') ? 1 : 2;

        var rujukan = $scope.data.rujukanId.nama;

        var url = '/api/master/layanan/';
        if (rujukan == 'FKTP' || rujukan == 'FKRTL') {
            url += 'rawat_jalan';
        } else {
            url += 'mcs?paket=' + paket;
        }



    }

    function cekPegawai(nik){
        $http.post(base_url + '/api/master/pegawai/cek_pegawai', {nik: nik})
            .then(function(result){
                toastr.success('Pegawai '+result.data.nama+' Berhasil Ditemukan', 'Berhasil!');
            }, function(err){
                toastr.error('Terjadi Kesalahan Sistem', 'Gagal!');
            });
    }

    function cekMCS(sjp) {
        $scope.whenLoadingSJP = true;
        $http.post(base_url + '/api/rawat_jalan/pendaftaran/cek_mcs', {sjp: sjp}).then(function (result) {
            if(!result.data.status){
                toastr.error(result.data.message, 'Gagal!');
                $scope.whenLoadingSJP = false;
                return;
            }

            var peserta = result.data.result[0];

            console.log('peserta', result);

            console.log("paket jaminan", $scope.dataPaketJaminan);
            $scope.data.namapesertamcs = peserta.peserta_nama;
            $scope.paketJaminanWS = {
                id: peserta.peserta_kategori_kartu_id,
                nama: peserta.peserta_kategori_kartu
            };

            // onChangePaketJaminan(peserta.peserta_kategori_kartu);
          //  onChangePaketJaminan(peserta.peserta_kategori_kartu_id);

            $scope.isMcsValid = true;
            toastr.success('Peserta MCS Berhasil Ditemukan', 'Berhasil!');
            $scope.whenLoadingSJP = false;
        }, function (err) {
            $scope.isMcsValid = false;
            toastr.error('Terjadi Kesalahan Sistem', 'Gagal!');
        });
    }


    function onChangePenanggung(penanggung) {
        console.log("data", $scope.data);
        if (penanggung == 1) {

            $scope.data.namapenanggungjawab = $scope.data.nama || $scope.data.namaPasien;
            $scope.data.alamatPenanggungJawab = $scope.data.alamat;
            $scope.data.teleponpenanggungjawab = $scope.data.telepon1;
            $scope.data.pNoIdentitas = $scope.data.no_identitas || $scope.data.noIdentitas;

        } else {
            $scope.data.namapenanggungjawab = "";
            $scope.data.alamatPenanggungJawab = "";
            $scope.data.teleponpenanggungjawab = "";
            $scope.data.pNoIdentitas = "";
        }
    }

    function onChangeTempatLahir(id) {
        $http.get(base_url + '/api/master/wilayah/wilayah_by_id/' + id).then(function (result) {
            $scope.data.tempatlahir = result.data.nama.toString().trim();
        });
    }

    /**
     * Function Penunjang Medis
     * @param size
     */
    function addLaboratorium(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddLaboratorium.html',
            controller: 'ModalAddLaboratoriumController',
            size: size,
            resolve: {
                items: function () {
                    $scope.data.pasien_id = $scope.data.id;
                    $scope.data.kelompokpasien = $scope.data.kelompokpasienId.nama;
                    $scope.data.layanan_id = $scope.data.layananId;
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.data.rujukan.rujuk_laboratorium = 1;
            console.log("data tindakan sebelum add lab,  ", $scope.dataTindakan);


            console.log("data tindakan sekarang, ", $scope.dataTindakan);
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addRadiologi(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddRadiologi.html',
            controller: 'ModalAddRadiologiController',
            size: size,
            resolve: {
                items: function () {
                    $scope.data.pasien_id = $scope.data.id;
                    $scope.data.kelompokpasien = $scope.data.kelompokpasienId.nama;
                    $scope.data.layanan_id = $scope.data.layananId;

                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_radiologi = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addCtScan(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddCtScan.html',
            controller: 'ModalAddCtScanController',
            size: size,
            resolve: {
                items: function () {
                    $scope.data.pasien_id = $scope.data.id;
                    $scope.data.kelompokpasien = $scope.data.kelompokpasienId.nama;
                    $scope.data.layanan_id = $scope.data.layananId;
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_ctscan = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addFisioterapi(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddFisioterapi.html',
            controller: 'ModalAddFisioterapiController',
            size: size,
            resolve: {
                items: function () {
                    $scope.data.pasien_id = $scope.data.id;
                    $scope.data.kelompokpasien = $scope.data.kelompokpasienId.nama;
                    $scope.data.layanan_id = $scope.data.layananId;
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_fisioterapi = 1;

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addCatchLab(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddCatchLab.html',
            controller: 'ModalAddCatchLabController',
            size: size,
            resolve: {
                items: function () {
                    $scope.data.pasien_id = $scope.data.id;
                    $scope.data.kelompokpasien = $scope.data.kelompokpasienId.nama;
                    $scope.data.layanan_id = $scope.data.layananId;
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_catchlab = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addAudiometri(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddAudiometri.html',
            controller: 'ModalAddAudiometriController',
            size: size,
            resolve: {
                items: function () {
                    $scope.data.pasien_id = $scope.data.id;
                    $scope.data.kelompokpasien = $scope.data.kelompokpasienId.nama;
                    $scope.data.layanan_id = $scope.data.layananId;
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_audiometri = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function addDiagnostikFungsional(size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalAddDiagnostikFungsional.html',
            controller: 'ModalAddDiagnostikFungsionalController',
            size: size,
            resolve: {
                items: function () {
                    $scope.data.pasien_id = $scope.data.id;
                    $scope.data.kelompokpasien = $scope.data.kelompokpasienId.nama;
                    $scope.data.layanan_id = $scope.data.layananId;
                    return {
                        pasien: $scope.data,
                        tindakan: $scope.dataTindakan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.data.rujukan.rujuk_diagnostik_fungsional = 1;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

    function cetakLabel() {
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/igd/pendaftaran/cetak_label_pasien/' + pendaftaran.pelayanan.id, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function cetakSampulRM(){
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/rawat_jalan/pendaftaran/print_sampul_rm/' + pendaftaran.pelayanan.id, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function cetakIdCard() {
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/api/igd/pendaftaran/cetak_idcard/' + pendaftaran.pelayanan.id, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function cetakKiup() {
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/api/igd/pendaftaran/cetak_kiup/' + pendaftaran.pelayanan.id, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function tambahRumahSakit() {
        $scope.isTambahRumahSakit = true;
    }

    function cetakNota() {

        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/api/igd/pendaftaran/cetak_nota/' + pendaftaran.pelayanan.id + '/' + pendaftaran.nomor_antrian, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function cetakTracer() {
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/api/igd/pendaftaran/cetak_tracer/' + pendaftaran.pelayanan.id + '/' + pendaftaran.nomor_antrian, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function cetakStatus() {
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/api/igd/pendaftaran/cetak_status?pelayanan_id=' + pendaftaran.pelayanan.id, '_blank', 'location=yes,height=720,width=920,scrollbars=yes,status=yes');
    }

    function cetakGeneralConsent() {
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/api/igd/pendaftaran/cetak_general_consent?no_rekam_medis=' + pendaftaran.pasien.no_rekam_medis, '_blank', 'location=yes,height=720,width=920,scrollbars=yes,status=yes');
    }

    function cetakFJP() {
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/api/igd/pendaftaran/cetak_fjp/umum/11?penanggung=1', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function cetakNotaPiutang() {
        var pendaftaran = angular.copy($scope.pendaftaran);
        window.open(base_url + '/api/igd/pendaftaran/cetak_nota_piutang/' + pendaftaran.pelayanan.id + '/' + pendaftaran.nomor_antrian, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
    }

    function tambahDataPerusahaanRujukan(fkrtl) {
        var last = $scope.dataPerusahaanRujukan.length;
        var obj = {
            id: last + 1,
            nama: fkrtl.namaRumahSakit
        };
        _dataPerusahaan.push(obj);
        $scope.dataPerusahaanRujukan = _.orderBy(_dataPerusahaan, ['nama'], ['asc']);
        $scope.isTambahRumahSakit = false;
    }

    function resetKelompokPasien() {
        $scope.isUmum = false;
        $scope.isBpjs = true;
        $scope.isMcs = false;
        $scope.isMcsValid = false;
        $scope.isPerusahaan = false;
        $scope.isAsuransi = false;
        $scope.isInternal = false;
    }

    function onChangePasien(pasien) {
        console.log(pasien);
    }

    function onChangeAsuransi(perusahaan) {
        $http.get(base_url + "/api/master/asuransi/tarif/" + perusahaan).then(function success(result){
            console.log(result);
            $scope.perusahaan = result.data;
        }, function error(err){
            toastr.error(err.getMessage().toString(), 'Terjadi Kesalahan Sistem!');
        })
    }

    function onChangePerusahaan(perusahaan) {
        $http.get(base_url + "/api/master/perusahaan/tarif/" + perusahaan).then(function success(result){
            console.log(result);
            $scope.perusahaan = result.data;
        }, function error(err){
            toastr.error(err.getMessage().toString(), 'Terjadi Kesalahan Sistem!');
        })
    }

    function onChangeTitle(title) {

        switch (title.id) {
            case '1':

                $scope.data.jenisKelamin = 1;
                break;
            case '2':
            case '3':
                $scope.data.jenisKelamin = 0;
                break;
            default:

                $scope.data.jenisKelamin = 3;
                break;
        }
    }

    function selectedPegawai(pegawai) {
        if (pegawai) {
            console.log(pegawai);
            $scope.data.pegawaiId = pegawai.originalObject;
        }

    }

    function selected(item) {

        if (item) {


            $http.get(base_url + '/api/master/pasien/get_by_id/' + item).then(function (result) {
                console.log("item peserta, ", result.data);

                resetKelompokPasien();
                var obj = result.data;
                var deferred = $q.defer();
                $timeout(function () {
                    console.log("load provinsi dari selected");
                    $('#select-provinsi').select2().val(obj.provinsi_id).trigger('change');
                }, 1500);

                $timeout(function () {
                    console.log("load kota dari selected");
                    $('#select-kota').select2().val(obj.kabupaten_id).trigger('change');
                }, 2000);

                $timeout(function () {
                    console.log("load kecamatan dari selected");
                    $('#select-kecamatan').select2().val(obj.kecamatan_id).trigger('change');
                }, 2500);

                $timeout(function () {
                    console.log("load kelurahan dari selected");
                    $('#select-desa').select2().val(obj.kelurahan_id).trigger('change');
                }, 3000);

                $timeout(function () {
                    console.log("load kelurahan dari selected");

                    $('#select-tempat-lahir').select2().val(obj.tempat_lahir).trigger('change');
                }, 3500);

                $timeout(function () {
                    console.log("load kelurahan dari selected");

                    $('#select-suku').select2().val(obj.suku).trigger('change');
                }, 4000);


                var selectedProvinsi = angular.copy($scope.dataProvinsi)[_.findIndex(angular.copy($scope.dataProvinsi), {id: obj.provinsi_id})];
                var selectedKota = angular.copy($scope.dataKota)[_.findIndex(angular.copy($scope.dataKota), {id: obj.kabupaten_id})];

                console.log(obj);

                $scope.$emit(EVENT.OLD_PATIENT);
                $scope.isBiodataDisabled = true;

                $scope.data = angular.copy(obj);
                $scope.data.tanggal = moment().format('YYYY-MM-DD');
                $scope.data.tanggalregister = moment().format('DD/MM/YYYY');
                $scope.data.tgl_reg = moment().format('DD-MM-YYYY');

                var getUrl = window.location;
                var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
                $scope.data.foto_preview = baseUrl+obj.foto;

                $scope.data.namaPasien = obj.nama;

                $scope.data.provinsi = obj.provinsi_id;
                $scope.data.desa = obj.kelurahan_id;
                $scope.data.kota = obj.kabupaten_id;
                $scope.data.kecamatan = obj.kecamatan_id;

                $scope.data.nosjp = '';


                $scope.data.identitasId = angular.copy($scope.dataIdentitas)[_.findIndex(angular.copy($scope.dataIdentitas), {id: obj.identitas_id})];
                $scope.data.noIdentitas = obj.no_identitas;
                $scope.data.telepon1 = obj.no_telepon_1;
                $scope.data.tempatlahir = obj.tempat_lahir;
                $scope.data.tanggallahir = moment(obj.tgl_lahir, "DD-MM-YYYY").format("DD/MM/YYYY");
                $scope.data.tgl_lahir = moment(obj.tgl_lahir, "DD-MM-YYYY").format("YYYY-MM-DD");
                $scope.data.tgl_lahir_form = moment(obj.tgl_lahir, "DD-MM-YYYY").format("YYYY-MM-DD");
                $scope.data.umur = moment().diff(moment(obj.tgl_lahir, "DD-MM-YYYY").format(), 'years');
                $scope.data.golonganDarah = obj.gol_darah_id;
                $scope.data.agamaId = angular.copy($scope.dataAgama)[_.findIndex(angular.copy($scope.dataAgama), {id: obj.agama_id})];
                $scope.data.statusKawin = angular.copy($scope.dataStatusKawin)[_.findIndex(angular.copy($scope.dataStatusKawin), {id: obj.status_kawin_id})];
                $scope.data.pendidikanId = angular.copy($scope.dataPendidikan)[_.findIndex(angular.copy($scope.dataPendidikan), {id: obj.pendidikan_id})];
                $scope.data.pekerjaanId = angular.copy($scope.dataPekerjaan)[_.findIndex(angular.copy($scope.dataPekerjaan), {id: obj.pekerjaan_id})];

                $scope.data.titleId = angular.copy($scope.dataTitle)[_.findIndex(angular.copy($scope.dataTitle), {id: obj.title_id})];
                $scope.data.kelompokpasienId = angular.copy($scope.dataKelompokPasien)[_.findIndex(angular.copy($scope.dataKelompokPasien), {id: "2"})]; //default id kepulauan bangka belitun
                $scope.data.jenisKelamin = obj.jenis_kelamin;
                $scope.data.kewarganegaraan = obj.kewarganegaraan;
                $scope.data.golonganDarah = obj.gol_darah_id;
                $scope.data.isOldPasien = 1;

                $scope.data.rujukanId = angular.copy($scope.dataRujukan)[2];

                $scope.data.rujukan = {
                    rujuk_ok: 0,
                    rujuk_rawatinap: 0,
                    rujuk_audiometri: 0,
                    rujuk_catchlab: 0,
                    rujuk_ctscan: 0,
                    rujuk_diagnostik_fungsional: 0,
                    rujuk_fisioterapi: 0,
                    rujuk_laboratorium: 0,
                    rujuk_ods_odc: 0,
                    rujuk_radiologi: 0
                };
                $scope.data.rujuk = [];

                $http.get(base_url + "/api/master/tindakan/init", {
                    params: {
                        status_pasien: 'lama',
                        jenis_kel: '1' // tambahan tri

                    }
                }).then(function (result) {

                    $scope.dataTindakan = _.map(result.data.tarif, function (data) {
                        data.qty = 1;
                        data.total = data.biaya;
                        return data;
                    });
                    $scope.tarifTotal = result.data.total;

                });
            });

            // $selectTempatLahir.val();

        } else {
            $scope.isBiodataDisabled = false;
        }
    }

    function deleteTindakan(idx) {

        $scope.dataTindakan.splice(idx, 1);
        $scope.tarifTotal = 0;
        $scope.tarifTotal = _.sum(_.map($scope.dataTindakan, function (data) {
            return parseFloat(data.total);
        }));

    }

    function initData() {
        $q.all([
            $http.get(base_url + "/api/master/wilayah/provinsi"),
            $http.get(base_url + "/api/master/wilayah/kota/", {
                params: {
                    'provinsiId': DEFAULT_PROVINSI_ID // default kepulauan bangka belitung
                }
            }),
            $http.get(base_url + "/api/master/wilayah/kecamatan/", {
                params: {
                    'kotaId': "179" // default kepulauan bangka belitung
                }
            }),
            $http.get(base_url + "/api/master/wilayah/kelurahan/", {
                params: {
                    'kecamatanId': "2344" // default kepulauan bangka belitung
                }
            }),
            $http.get(base_url + "/api/master/identitas"),
            $http.get(base_url + "/api/master/status_kawin"),
            $http.get(base_url + "/api/master/agama"),
            $http.get(base_url + "/api/master/pendidikan"),
            $http.get(base_url + "/api/master/pekerjaan"),
            $http.get(base_url + "/api/master/rujukan"),
            $http.get(base_url + "/api/master/kelompok_pasien"),
            $http.get(base_url + "/api/master/paket_jaminan"),
            $http.get(base_url + "/api/master/layanan/rawat_jalan"),
            $http.get(base_url + "/api/master/hubungan_pasien"),
            $http.get(base_url + "/api/master/dokter_poli", {
                params: {
                    jenis_dokter: '1'
                }
            }),
            $http.get(base_url + "/api/master/title"),
            $http.get(base_url + "/api/master/tindakan/init", {
                params: {
                    status_pasien: 'baru',
                    jenis_kel: '1',
                    jenis_dokter: '1' // default bukan mitra
                }
            }),
            $http.get(base_url + "/api/master/wilayah/kotaAll"),
            $http.get(base_url + "/api/master/wilayah/suku")

        ]).then(function (results) {


            $scope.dataProvinsi = angular.copy(results[0].data);
            $scope.dataKota = angular.copy(results[1].data);
            $scope.dataKecamatan = angular.copy(results[2].data);
            $scope.dataDesa = angular.copy(results[3].data);


            $scope.dataIdentitas = angular.copy(results[4].data);
            $scope.data.identitasId = angular.copy($scope.dataIdentitas)[_.findIndex(angular.copy($scope.dataIdentitas), {id: "1"})]; //default id kepulauan bangka belitun

            $scope.dataStatusKawin = angular.copy(results[5].data);


            $scope.data.provinsi = angular.copy($scope.dataProvinsi)[_.findIndex(angular.copy($scope.dataProvinsi), {id: DEFAULT_PROVINSI_ID.toString()})]; //default id kepulauan bangka belitung


            $timeout(function () {
                $('#select-provinsi').select2().val($scope.data.provinsi.id).trigger('change');

            }, 1200);


            $scope.dataAgama = angular.copy(results[6].data);
            $scope.dataPendidikan = angular.copy(results[7].data);
            $scope.dataPekerjaan = angular.copy(results[8].data);
            $scope.dataRujukan = angular.copy(results[9].data);


            //default rujukan : pasien data sendiri
            $scope.data.rujukanId = angular.copy($scope.dataRujukan)[_.findIndex(angular.copy($scope.dataRujukan), {id: "3"})]; //default id kepulauan bangka belitun;

            $scope.dataKelompokPasien = angular.copy(results[10].data);
            $scope.data.kelompokpasienId = angular.copy($scope.dataKelompokPasien)[_.findIndex(angular.copy($scope.dataKelompokPasien), {id: "2"})]; //default id kepulauan bangka belitun
            $scope.dataPaketJaminan = _.map(angular.copy(results[11].data), function (o) {
                return {
                    id: o.id,
                    nama: o.nama.toUpperCase()
                }
            });
            $scope.dataLayanan = angular.copy(results[12].data);
            $scope.dataHubunganPasien = angular.copy(results[13].data);

            $scope.dataPegawai = [];
            $scope.dataTitle = angular.copy(results[15].data);

            _.remove(results[16].data.tarif, {
                kode: '3.02.01.03.03.00.02'
            });


            $scope.dataTindakan = _.map(angular.copy(results[16].data.tarif), function (data, i) {
                data.total = data.biaya;
                data.qty = 1;
                return data;
            });


            $scope.tarifTotal = results[16].data.total;


            $scope.data.tanggalregister = moment().format('DD/MM/YYYY');
            $scope.dataTempatLahir = results[17].data.items;
            $scope.data.tempatLahir = angular.copy($scope.dataTempatLahir)[_.findIndex(angular.copy($scope.dataTempatLahir), {id: "179"})]; //default id kota pangkal pinang

            $timeout(function () {
                $('#select-tempat-lahir').select2().val($scope.data.tempatLahir.id).trigger('change');

            }, 1200);
            $scope.dataSuku = results[18].data;
        }).catch(function (err) {
            console.log(err);
        });
    }
    function initTempatLahir() {
        $q.all([
            $http.get(base_url + "/api/master/wilayah/kotaAll")

        ]).then(function (results) {
            $scope.dataTempatLahir = results[0].data.items;
        }).catch(function (err) {
            console.log(err);
        });
    }

    function save() {

        if ($scope.data.pegawaiId =="" || $scope.data.pegawaiId == null){
            if ($scope.data.jenis_layanan !="4"){
                warningMessage('Peringatan', 'Dokter Belum dipilih.');
                return;
            }
        }
        /**
         * Untuk Jenis Pasien MCS SJP Harus diisi.
         */
        if ($scope.isMcs) {
            // Jika No. SJP belum diisi
            if ($scope.data.nosjp == "" || $scope.data.nosjp == null) {
                warningMessage('Peringatan', 'No. SJP Belum diisi.');
                return;
            }

            // Jika No. SJP belum diverifikasi
            if (! $scope.isMcsValid) {
                warningMessage('Peringatan', 'No. SJP Belum diverifikasi.');
                return;
            }
        }

        $rootScope.isProcessing = true;
        $rootScope.$emit('LOADING:EVENT:PROCESSING');


        api.Nomor.getNomor().$promise.then(function (result) {
            $scope.data.nomorAntrian = result[1].no_reg;
            if ($scope.data.isOldPasien == 0) {
                $scope.data.no_rekam_medis = result[0];
            }

            $scope.data.tindakan = $scope.dataTindakan;
            $scope.data.paketJaminan = angular.copy($scope.paketJaminanWS);
            var _data = angular.copy($scope.data);

            console.log("data send", _data);

            _data.provinsi = _data.provinsi.id;
            _data.kota = _data.kota.id;
            _data.kecamatan = _data.kecamatan.id;
            _data.desa = _data.desa.id;


            var req = {
                method: 'POST',
                url: base_url + '/api/igd/pendaftaran/save',
                data: $.param(_data),
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                }
            };

            if ($("#upload-foto").val()) {
                console.log("foto", $("#upload-foto")[0].files[0]);
                var fd = new FormData();
                fd.append('image', $("#upload-foto")[0].files[0]);

                $.ajax({
                    url: base_url + '/cdn/upload_image',
                    data: fd,
                    contentType: false,
                    processData: false,
                    method: 'post'
                }).then(function (result) {
                    console.log("result upload", result);

                    var parse = JSON.parse(result);

                    if (parse.upload_data) {
                        _data.foto = '/uploads/img/pasien/' + parse.upload_data.file_name;

                        req = {
                            method: 'POST',
                            url: base_url + '/api/igd/pendaftaran/save',
                            data: $.param(_data),
                            headers: {
                                'Content-type': 'application/x-www-form-urlencoded'
                            }
                        };


                        $http(req).then(function (response) {
                            // console.log("result upload", result[1]);
                            console.log("result save pendaftaran", result);
                            console.log(response);
                            toastr.success('Simpan IGD Berhasil !', 'Sukses');
                            $rootScope.isProcessing = false;
                            $scope.isTambah = false;
                            $scope.isSubmit = true;
                            $scope.pendaftaran = response.data;
                            $rootScope.$emit('LOADING:EVENT:FINISH');
                        }).catch(function (err) {
                            toastr.error('Terjadi Kesalahan Sistem', 'Gagal Daftar');
                            $rootScope.$emit('LOADING:EVENT:FINISH');
                        })
                    }
                });
            } else {

                $http(req).then(function(response){
                    // console.log("result upload", result[1]);
                    console.log("result save pendaftaran", result);
                    console.log(response);
                    toastr.success('Simpan IGD Berhasil !', 'Sukses');
                    $rootScope.isProcessing = false;
                    $scope.isTambah = false;
                    $scope.isSubmit = true;
                    $scope.pendaftaran = response.data;
                    $rootScope.$emit('LOADING:EVENT:FINISH');
                }).catch(function(err){
                    toastr.error('Terjadi Kesalahan Sistem', 'Gagal Daftar');
                    $rootScope.$emit('LOADING:EVENT:FINISH');
                })
            }
        }).catch(function (err) {
            toastr.error('Terjadi Kesalahan Sistem', 'Gagal Daftar');
            $rootScope.$emit('LOADING:EVENT:FINISH');
        });


    }

    function tambahData() {
        window.location.href = base_url + "/igd/pendaftaran";
    }

    function onChangeProvinsi(provinsi) {

        console.log("masuk on change provinsi", provinsi);

        if (provinsi) {
            var deferred = $q.defer();
            $http.get(base_url + "/api/master/wilayah/kota/", {
                params: {
                    'provinsiId': provinsi.id
                }
            }).then(
                function success(result) {
                    console.log(result);
                    deferred.resolve(result);
                    $scope.dataKota = _.map(result.data, function (data) {
                        return {
                            id: data.id,
                            nama: data.nama
                        }
                    });

                    // $scope.data.kota = angular.copy($scope.dataKota)[_.findIndex(angular.copy($scope.dataKota), {id: "179"})];
                    // $('#select-kota').select2().val($scope.data.kota.id).trigger('change');

                }, function error(err) {
                    deferred.reject(err);
                    //console.log(err);
                })
        }

    }

    function onChangeKota(kota) {
        console.log("masuk on change kota", kota);
        var defer = $q.defer();

        if (kota) {
            $http.get(base_url + "/api/master/wilayah/kecamatan/", {
                params: {
                    'kotaId': kota.id
                }
            }).then(
                function success(result) {
                    $scope.dataKecamatan = _.map(result.data, function (data) {
                        return {
                            id: data.id,
                            nama: data.nama
                        }
                    });
                }, function error(err) {
                    defer.reject(err);
                });

        }


    }

    function onChangeKecamatan(kecamatan) {
        var defer = $q.defer();
        if (kecamatan) {


            $http.get(base_url + "/api/master/wilayah/kelurahan/", {
                params: {
                    'kecamatanId': kecamatan.id
                }
            }).then(
                function success(result) {
                    //console.log("desa");
                    //console.log(result);
                    $scope.dataDesa = _.map(result.data, function (data) {
                        return {
                            id: data.id,
                            nama: data.nama
                        }
                    });
                }, function error(err) {
                    //console.log(err);
                    defer.reject(err);
                })
        }
    }

    function onChangeDesa(desa) {

        var defer = $q.defer();
        if (desa) {
            $http.get(base_url + "/api/master/wilayah/search/", {
                params: {
                    'id': desa.id
                }
            }).then(
                function success(result) {
                    //console.log("desa");
                    console.log(result);
                    $scope.data.kodepos = angular.copy(result.data[0].kodepos);
                    // $scope.dataDesa = angular.copy(result.data);
                }, function error(err) {
                    //console.log(err);
                    defer.reject(err);
                })
        }
    }

    function onChangeKelompokPasien(kelompokPasien) {
        switch (kelompokPasien.nama) {
            case 'Asuransi':
                $scope.isAsuransi = true;
                $scope.isUmum = false;
                $scope.isBpjs = false;
                $scope.isMcs = false;
                $scope.isMcsValid = false;
                $scope.isPerusahaan = false;
                $scope.isInternal = false;
                $scope.isNotaPiutang = true;
                break;
            case 'BPJS':
                $scope.isBpjs = true;
                $scope.isUmum = false;
                $scope.isMcs = false;
                $scope.isMcsValid = false;
                $scope.isPerusahaan = false;
                $scope.isAsuransi = false;
                $scope.isInternal = false;
                $scope.isNotaPiutang = true;
                break;
            case 'Internal':
                $scope.isInternal = true;
                $scope.isUmum = false;
                $scope.isBpjs = false;
                $scope.isMcs = false;
                $scope.isMcsValid = false;
                $scope.isPerusahaan = false;
                $scope.isAsuransi = false;
                $scope.isNotaPiutang = true;
                break;
            case 'MCS':
                $scope.isMcs = true;
                $scope.isMcsValid = false;
                $scope.isUmum = false;
                $scope.isBpjs = false;
                $scope.isPerusahaan = false;
                $scope.isAsuransi = false;
                $scope.isInternal = false;
                $scope.isNotaPiutang = true;
                break;
            case 'Perusahaan':
                $scope.isPerusahaan = true;
                $scope.isUmum = false;
                $scope.isBpjs = false;
                $scope.isMcs = false;
                $scope.isMcsValid = false;
                $scope.isAsuransi = false;
                $scope.isInternal = false;
                $scope.isNotaPiutang = true;
                break;
            case 'Umum':
                $scope.isPerusahaan = false;
                $scope.isUmum = true;
                $scope.isBpjs = false;
                $scope.isMcs = false;
                $scope.isMcsValid = false;
                $scope.isAsuransi = false;
                $scope.isInternal = false;
                $scope.isNotaPiutang = false;
                break;
            default:
                resetKelompokPasien();
                break;
        }
    }

    function onChangeTanggalLahir(tanggalLahir) {
        console.log("tanggal lahir", tanggalLahir);
        if (tanggalLahir) {
            var tanggal = parseInt(tanggalLahir.toString().substr(0, 2));
            var bulan = parseInt(tanggalLahir.toString().substr(2, 2));
            var tahun = parseInt(tanggalLahir.toString().substr(4, 4));

            if (tanggal > 31 || bulan > 12) {
                $scope.wrongDate = true;
            } else {
                $scope.wrongDate = false;
                var mTanggalLahir = moment(tanggalLahir, "DDMMYYYY");

                var umur = {
                    tahun: moment().diff(mTanggalLahir, 'years')
                };

                console.log($scope.formAngular.tanggalLahir);

                $scope.data.umur = umur.tahun;
                $scope.data.tgl_lahir = mTanggalLahir.format("DD-MM-YYYY");
                $scope.data.tgl_lahir_form = mTanggalLahir.format("DD-MM-YYYY");



            }
        }

    }
    function onChangeKewarganegaraan(id) {
        console.log("masuk on change");
        if(id==1){
            initTempatLahir();
        }else {
            $scope.data.tempat_lahir= true;
        }
    }

    function onChangeUmur(umur) {
        var age = parseInt(umur);
        var yearNow = moment().year(),
            yearAgo = ((yearNow - age) > 0) ? yearNow - umur : 0;

        var dob = moment([yearAgo, moment().month(), moment().date()]).format();
        $scope.data.tgl_lahir = moment(new Date(dob)).format("DD-MM-YYYY");
        $scope.data.tanggallahir = moment(new Date(dob)).format("DD/MM/YYYY");

        $scope.data.tgl_lahir_form = moment(new Date(dob)).format("DD-MM-YYYY");

        console.log({
            tgl_lahir : $scope.data.tgl_lahir,
            tanggallahir: $scope.data.tanggallahir
        });
    }

    function onChangeLayanan(layanan) {

        $http.get(base_url + '/api/rawat_jalan/pendaftaran/cek_antrian/' + $scope.data.id + '/' + layanan).then(function (result) {
            if (result.data.response) {

                console.log("response layanan, ", result.data.response);

                $scope.data.layanan_id = result.data.response.layanan.id;
                $scope.data.nama_layanan = result.data.response.layanan.nama;
                $scope.data.tarip_layanan_id = result.data.response.layanan.tarip_layanan_id;
                $scope.data.jenis_layanan = result.data.response.layanan.jenis;
                $scope.dataTindakan = _.each($scope.dataTindakan, function (data, i) {
                    if (i > 0) {
                        $scope.dataTindakan.splice(1, i);
                    }
                    return data;
                });
            } else {
                toastr.error(result.data, 'Terjadi Kesalahan Sistem!');
            }

        }).catch(function (err) {
            console.log(err);
        });


    }

    function onChangeTarifTindakanQty(tindakan) {


        if (tindakan.qty <= 0) {
            // tindakan.qty = 1;
        } else {

            tindakan.biaya *= tindakan.qty;
        }

    }

    function onChangeDokter(pegawai) {


        if (pegawai) {
            $http.get(base_url + '/api/master/tindakan/tarif', {
                params: {
                    layanan: $scope.data.layananId,
                    jenis_dokter: 1,
                    kelas: 0
                }
            }).then(function (result) {
                var tarif = _.map(angular.copy(result.data.tarif), function (data, i) {
                    data.total = data.biaya;
                    data.qty = 1;
                    return data;
                });

                if (tarif[0]) {

                    var temp = [];
                    temp[0] = $scope.dataTindakan[0]; // get top of array
                    $scope.dataTindakan = [];
                    temp.push(tarif[0]);
                    $scope.dataTindakan = temp;
                    $scope.$emit('add:tindakan');


                }
            });
        }
    }

    function onChangeTarifTindakanDiskon(tindakan) {

        if (tindakan.qty && tindakan.diskon) {
            tindakan.biaya -= ((tindakan.qty * tindakan.biaya) * (tindakan.diskon / 100))
        } else {
            tindakan.biaya = tindakan.biaya;
        }
        $scope.tarifTotal += parseFloat(tindakan.biaya);
    }

    function tindakanQtyChange(idx, qty) {
        $scope.dataTindakan[idx].qty = qty;
        $scope.dataTindakan[idx].total = $scope.dataTindakan[idx].biaya * qty;
        $scope.dataTindakan[idx].anthesi = $scope.dataTindakan[idx].anthesi * qty;
        $scope.dataTindakan[idx].dokter = $scope.dataTindakan[idx].dokter * qty;
        $scope.dataTindakan[idx].dokter_operator = $scope.dataTindakan[idx].dokter_operator * qty;
        $scope.dataTindakan[idx].dokter_pendamping = $scope.dataTindakan[idx].dokter_pendamping * qty;
        $scope.dataTindakan[idx].jasa_sarana = $scope.dataTindakan[idx].jasa_sarana * qty;
        $scope.dataTindakan[idx].rumah_sakit = $scope.dataTindakan[idx].rumah_sakit * qty;
        $scope.dataTindakan[idx].sewa_ok = $scope.dataTindakan[idx].sewa_ok * qty;
        $scope.dataTindakan[idx].tenaga_ahli = $scope.dataTindakan[idx].tenaga_ahli * qty;
        $scope.tarifTotal = 0;
        $scope.tarifTotal += parseFloat(_.sum(_.map($scope.dataTindakan, function (data) {
            return parseFloat(data.total);
        })));
    }

    function tindakanDiskonChange(idx, diskon) {

        var diskon = diskon / 100;
        var diskonTotal = ($scope.dataTindakan[idx].biaya * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalAnthesi = ($scope.dataTindakan[idx].anthesi * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalDokter = ($scope.dataTindakan[idx].dokter * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalDokterOperator = ($scope.dataTindakan[idx].dokter_operator * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalDokterPendamping = ($scope.dataTindakan[idx].dokter_pendamping * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalJasaSarana = ($scope.dataTindakan[idx].jasa_sarana * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalRumahSakit = ($scope.dataTindakan[idx].rumah_sakit * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalSewaOk = ($scope.dataTindakan[idx].sewa_ok * $scope.dataTindakan[idx].qty) * diskon;
        var diskonTotalTenagaAhli = ($scope.dataTindakan[idx].tenaga_ahli * $scope.dataTindakan[idx].qty) * diskon;

        var total = ($scope.dataTindakan[idx].biaya * $scope.dataTindakan[idx].qty) - diskonTotal;
        var anthesi = ($scope.dataTindakan[idx].anthesi * $scope.dataTindakan[idx].qty) - diskonTotalAnthesi;
        var dokter = ($scope.dataTindakan[idx].dokter * $scope.dataTindakan[idx].qty) - diskonTotalDokter;
        var dokterOperator = ($scope.dataTindakan[idx].dokter_operator * $scope.dataTindakan[idx].qty) - diskonTotalDokterOperator;
        var dokterPendamping = ($scope.dataTindakan[idx].dokter_pendamping * $scope.dataTindakan[idx].qty) - diskonTotalDokterPendamping;
        var jasaSarana = ($scope.dataTindakan[idx].jasa_sarana * $scope.dataTindakan[idx].qty) - diskonTotalJasaSarana;
        var rumahSakit = ($scope.dataTindakan[idx].rumah_sakit * $scope.dataTindakan[idx].qty) - diskonTotalRumahSakit;
        var sewaOk = ($scope.dataTindakan[idx].sewa_ok * $scope.dataTindakan[idx].qty) - diskonTotalSewaOk;
        var tenagaAhli = ($scope.dataTindakan[idx].tenaga_ahli * $scope.dataTindakan[idx].qty) - diskonTotalTenagaAhli;

        $scope.dataTindakan[idx].total = total;
        $scope.dataTindakan[idx].anthesi = anthesi;
        $scope.dataTindakan[idx].dokter = dokter;
        $scope.dataTindakan[idx].dokter_operator = dokterOperator;
        $scope.dataTindakan[idx].dokter_pendamping = dokterPendamping;
        $scope.dataTindakan[idx].jasa_sarana = jasaSarana;
        $scope.dataTindakan[idx].rumah_sakit = rumahSakit;
        $scope.dataTindakan[idx].sewa_ok = sewaOk;
        $scope.dataTindakan[idx].tenaga_ahli = tenagaAhli;


        $scope.tarifTotal = 0;
        $scope.tarifTotal += parseFloat(_.sum(_.map($scope.dataTindakan, function (data) {
            return parseFloat(data.total);
        })));
        console.log($scope.dataTindakan[idx]);
    }

    function verifikasiSep(nosep) {
        // SCRIPT BPJS YANG DULU
        // $http.get(CONFIG.VERIFIKASI_SEP, {
        //     params: {
        //         no_sep: nosep
        //     }
        // }).then(function (result) {
        //     console.log(result);
        //     if (!result.data.response) {
        //         toastr.error('NO SEP belum di inputkan', 'error');
        //         return;
        //     }
        //     if (result.data.response.metadata.code == 200) {
        //         $scope.data.namapeserta = result.data.response.response.peserta.nama;
        //         $scope.data.bpjs = result.data.response.response;
        //         toastr.success(result.data.response.metadata.message, 'sukses');
        //     } else {
        //         toastr.error(result.data.response.metadata.message, 'error');
        //     }

        //     console.log("result verifikasi, ", result);
        // });

        $q.all([
            // $http.get(CONFIG.VERIFIKASI_SEP, {
            //     params: {
            //         no_sep: nosep
            //     }
            // }),
            $http.post(CONFIG.INACBG.GET_CLAIM_DATA, {sep: nosep}),
        ]).then(function (results) {
            // BPJS SEP
            console.log(results);
            // var result = results[0];
            // var inacbg = results[1].data;
            // var result = results.shift();
            var inacbg = results.shift();


            // if (!result.data.response) {
            //     toastr.error('NO SEP belum di inputkan', 'error');
            //     return;
            // }
            // if (result.data.response.metadata.code == 200) {
            //     $scope.data.namapeserta = result.data.response.response.peserta.nama;
            //     $scope.data.bpjs = result.data.response.response;
            //     toastr.success(result.data.response.metadata.message, 'Data Peserta BPJS');
            // } else {
            //     toastr.error(result.data.response.metadata.message, 'Error Data Peserta BPJS');
            // }

            // cek inacbg
            console.log(inacbg);
            if (inacbg.status == 200) {
                $scope.data.inacbg = inacbg.data.response.data;

                if ($scope.data.inacbg.grouper) {
                    var grouper = $scope.data.inacbg.grouper;
                    $scope.data.tarifklaim = grouper.response.cbg.tariff;
                }

                // Nama Peserta
                $scope.data.namapeserta = $scope.data.inacbg.nama_pasien;

                toastr.success(inacbg.data.metadata.message, 'Get Claim Data - INACBG');
            } else {
                toastr.error(inacbg.data.metadata.message, 'Error Get Claim Data - INACBG');
            }


        });
    }

    function openModalPendaftaranTindakan(size) {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalPendaftaranTindakan.html',
            controller: 'ModalPendaftaranTindakanController',
            size: size,
            resolve: {
                items: function () {
                    return {
                        layanan: $scope.data.layananId,
                        jenis_layanan: $scope.data.jenis_layanan,
                        dokter: $scope.data.pegawaiId,
                        kelompokPasien: $scope.data.kelompokpasienId.id,
                        perusahaan: $scope.perusahaan
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {

            _.each(selectedItem, function (data) {
                // var total = parseFloat(angular.copy($scope.tarifTotal));
                // total += parseFloat(data.biaya);
                // console.log("nilai total tindakan", total);
                // $scope.tarifTotal = total;
                data.total = data.biaya;
                data.qty = 1;
                $scope.dataTindakan.push(data);
                $scope.$emit('add:tindakan');
            });
        }, function () {

        });
    }

    function openModalPrintSbpk() {

        if ($scope.data.bpjs) {
            $scope.data.bpjs.pendaftaran = $scope.pendaftaran.pelayanan.id;
        }

        var items = {
            pendaftaran: angular.copy($scope.pendaftaran),
            bpjs: angular.copy($scope.data.bpjs),
            tindakan: angular.copy($scope.dataTindakan)
        };

        if (items.bpjs) {
            delete items.pendaftaran;
            $scope.url = base_url + '/rawat_jalan/pendaftaran/print_sbpk?param=' + JSON.stringify(items);
        } else {
            $scope.url = base_url + '/rawat_jalan/pendaftaran/print_sbpk_non_bpjs/' + items.pendaftaran.pelayanan.id;
        }

        window.open($scope.url, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');

    }


    function toggleAnimation() {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    }

    function reloadSelectProvinsi(mapProvinsi) {
        var data = [{'id': DEFAULT_PROVINSI_ID, 'text': DEFAULT_PROVINSI}];
        if (mapProvinsi) {
            console.log("map provinsi ada");
            data = [mapProvinsi];
        }

        $selectProvinsi.empty();
        $selectProvinsi.select2({
            data: data,
            placeholder: 'PILIH PROVINSI',
            ajax: {
                url: base_url + '/api/master/wilayah/provinsi',
                dataType: 'json',
                data: function (param) {
                    return {
                        delay: 0.3,
                        q: param.term
                    }
                },
                processResults: function (data) {
                    return {
                        results: _.map(data.items || data, function (obj) {
                            return {
                                id: obj.id,
                                text: obj.text || obj.nama,
                            }
                        })
                    }
                },
                cache: false,
                minimumInputLength: 3,
            }
        });
    }

    initData();
    resetKelompokPasien();


    $rootScope.$on(EVENT.SWITCH_SHIFT_CLOSE, function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'modalDaftarKunjungan.html',
            controller: 'ModalDaftarKunjunganController',
            size: 'lg',
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            // $scope.selected = selectedItem;
            $scope.dataTindakan.push(selectedItem);
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    });


    $scope.$on('add:tindakan', function () {
        console.log("data tindakan", $scope.dataTindakan);
        $scope.tarifTotal = parseFloat(_.sum(_.map($scope.dataTindakan, function (data) {
            return parseFloat(data.biaya);
        })));
        console.log($scope.tarifTotal);
    });


    // uid
    var uid = $('#uid');
    if (uid.val()) {

        $http.get(base_url + '/api/rawat_jalan/pendaftaran/by_uid?uid=' + uid.val()).then(function (result) {
            console.log("item peserta, ", result.data);

            resetKelompokPasien();
            var obj = result.data;

            var selectedProvinsi = angular.copy($scope.dataProvinsi)[_.findIndex(angular.copy($scope.dataProvinsi), {id: obj.provinsi_id})];
            var selectedKota = angular.copy($scope.dataKota)[_.findIndex(angular.copy($scope.dataKota), {id: obj.kabupaten_id})];

            console.log(obj);

            $scope.$emit(EVENT.OLD_PATIENT);
            $scope.isBiodataDisabled = true;

            $scope.data = angular.copy(obj);
            $scope.data.tanggal = moment().format('YYYY-MM-DD');
            $scope.data.tanggalregister = moment().format('DD/MM/YYYY');
            $scope.data.tgl_reg = moment().format('DD-MM-YYYY');


            $scope.data.namaPasien = obj.nama;

            $scope.data.provinsi = obj.provinsi_id;
            $scope.data.desa = obj.kelurahan_id;
            $scope.data.kota = obj.kabupaten_id;
            $scope.data.kecamatan = obj.kecamatan_id;

            console.log("data provinsi dan kota, ", {
                provinsi: $scope.data.provinsi,
                kota: $scope.data.kota,
                dataKota: $scope.dataKota,
                kabupatenId: obj.kabupaten_id
            });

            $scope.data.identitasId = angular.copy($scope.dataIdentitas)[_.findIndex(angular.copy($scope.dataIdentitas), {id: obj.identitas_id})];
            $scope.data.noIdentitas = obj.no_identitas;
            $scope.data.telepon1 = obj.no_telepon_1;
            $scope.data.tempatlahir = obj.tempat_lahir;
            $scope.data.tanggallahir = moment(obj.tgl_lahir, "DD-MM-YYYY").format("DD/MM/YYYY");
            $scope.data.tgl_lahir = moment(obj.tgl_lahir, "DD-MM-YYYY").format("YYYY-MM-DD");
            $scope.data.tgl_lahir_form = moment(obj.tgl_lahir, "DD-MM-YYYY").format("YYYY-MM-DD");
            $scope.data.umur = moment().diff(moment(obj.tgl_lahir, "DD-MM-YYYY").format(), 'years');
            $scope.data.golonganDarah = obj.gol_darah_id;
            $scope.data.agamaId = angular.copy($scope.dataAgama)[_.findIndex(angular.copy($scope.dataAgama), {id: obj.agama_id})];
            $scope.data.statusKawin = angular.copy($scope.dataStatusKawin)[_.findIndex(angular.copy($scope.dataStatusKawin), {id: obj.status_kawin_id})];
            $scope.data.pendidikanId = angular.copy($scope.dataPendidikan)[_.findIndex(angular.copy($scope.dataPendidikan), {id: obj.pendidikan_id})];
            $scope.data.pekerjaanId = angular.copy($scope.dataPekerjaan)[_.findIndex(angular.copy($scope.dataPekerjaan), {id: obj.pekerjaan_id})];

            $scope.data.titleId = angular.copy($scope.dataTitle)[_.findIndex(angular.copy($scope.dataTitle), {id: obj.title_id})];
            $scope.data.kelompokpasienId = angular.copy($scope.dataKelompokPasien)[_.findIndex(angular.copy($scope.dataKelompokPasien), {id: "2"})]; //default id kepulauan bangka belitun
            $scope.data.jenisKelamin = obj.jenis_kelamin;
            $scope.data.kewarganegaraan = obj.kewarganegaraan;
            $scope.data.golonganDarah = obj.gol_darah_id;
            $scope.data.isOldPasien = 1;

            $scope.data.rujukanId = angular.copy($scope.dataRujukan)[2];

            $scope.data.rujukan = {
                rujuk_ok: 0,
                rujuk_rawatinap: 0,
                rujuk_audiometri: 0,
                rujuk_catchlab: 0,
                rujuk_ctscan: 0,
                rujuk_diagnostik_fungsional: 0,
                rujuk_fisioterapi: 0,
                rujuk_laboratorium: 0,
                rujuk_ods_odc: 0,
                rujuk_radiologi: 0
            };
            $scope.data.rujuk = [];

            $q.all([
                $http.get(base_url + "/api/master/wilayah/search/", {
                    params: {
                        'id': obj.kabupaten_id
                    }
                }),
                $http.get(base_url + "/api/master/wilayah/search/", {
                    params: {
                        'id': obj.kecamatan_id
                    }
                }),
                $http.get(base_url + "/api/master/wilayah/search/", {
                    params: {
                        'id': obj.kelurahan_id
                    }
                }),
                $http.get(base_url + "/api/master/tindakan/init", {
                    params: {
                        status_pasien: 'lama',
                        jenis_kel: '1' // tambahan tri

                    }
                })
            ]).then(function (results) {
                console.log("selected wilayah untuk pasien lama, ", results);


                $scope.dataTindakan = _.map(results[3].data.tarif, function (data) {
                    data.qty = 1;
                    data.total = data.biaya;
                    return data;
                });
                $scope.tarifTotal = results[3].data.total;

            });
        })
    }

    // jquery
    $(document).ready(function () {
        var uploadFoto = $("#upload-foto");

        uploadFoto.change(function (e) {

            var reader = new FileReader();
            reader.onload = function () {
                var output = document.getElementById('preview');
                output.src = reader.result;
            };
            console.log(e.target.files[0]);
            reader.readAsDataURL(e.target.files[0]);
        });


        // DataTable Search Pasien
        $("#btn-advance-search").click(function () {
            $("#modal_advance_search").modal('show');
        });

        var tablePasienTimer = null;
        var tablePasienDt = $("#table_pasien").dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": url_load_pasien,
            "columns": [
                {
                    "data": "no_rekam_medis",
                    "render": function (data, type, row, meta) {
                        return '<a href="#" class="click" ng-click="selected(' + row.id + ')">' + data + '</a>';
                    },
                    "className": "text-center"
                },
                {
                    "data": "nama",
                    "render": function (data, type, row, meta) {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "alamat",
                    "render": function (data, type, row, meta) {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "no_identitas",
                    "render": function (data, type, row, meta) {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "no_telepon_1",
                    "render": function (data, type, row, meta) {
                        return data;
                    },
                    "className": "text-left"
                },
                {
                    "data": "tgl_lahir",
                    "render": function (data, type, row, meta) {
                        return moment(data).format('DD/MM/YYYY');
                    },
                    "className": "text-center"
                }
            ],
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (tablePasienTimer) clearTimeout(tablePasienTimer);
                tablePasienTimer = setTimeout(function () {
                    // blockElement("#table_pasien");
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "createdRow": function(row, data, dataIndex) {
                console.log("COMPILE DATATABLE ANGULAR")
                $compile(angular.element(row).contents())($scope);
            },
            "fnDrawCallback": function (oSettings) {
              var n = oSettings._iRecordsTotal;
              
              $("#table_pasien").unblock();

              $("#table_pasien").find('[data-popup=tooltip]').tooltip();

              $.unblockUI();
            }
        });

        $("#table_pasien").on('click', '.click', function () {
            $("#modal_advance_search").modal('hide');
        });

        $("#search_no_rekam_medis, #search_nama, #search_alamat, #search_no_identitas, #search_no_telepon, #search_tgl_lahir").on('keyup', function (e) {
            if(e.which == 13) {
                $("#btn-cari_pasien").click();
                return;
            }
        });

        $("#btn-cari_pasien").click(function () {
            blockPage();
            tablePasienDt.fnFilter($("#search_no_rekam_medis").val(), 0);
            tablePasienDt.fnFilter($("#search_nama").val(), 1);
            tablePasienDt.fnFilter($("#search_alamat").val(), 2);
            tablePasienDt.fnFilter($("#search_no_identitas").val(), 3);
            tablePasienDt.fnFilter($("#search_no_telepon").val(), 4);
            tablePasienDt.fnFilter($("#search_tgl_lahir").val(), 5);
        });
    });
}



