angular.module('rsbt').controller('BpjsRujukanPcareController', bpjsRujukanPcareController);

bpjsRujukanPcareController.$inject = ['$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource'];

function bpjsRujukanPcareController($rootScope, $http, CONFIG, EVENT, toastr, $resource) {
    var vm = this;
    vm.peserta = {};

    vm.cekNoKartu = function () {
        $resource(base_url + '/api/bpjs/rujukan/no_kartu_pcare/').get({
            no_kartu : vm.peserta.noKartu
        }).$promise.then(function (result) {
            var result = JSON.parse(result.data);
            console.log(result);
            vm.message = result.metadata.message;

            if(result.response.peserta){
                vm.peserta = result.response.peserta;
            }

        }).catch(function (err) {
            console.log(err);
        });
    };

    vm.cekNoRujukan = function () {
        $resource(base_url + '/api/bpjs/rujukan/no_rujukan_pcare/').get({
            no_rujukan : vm.peserta.noRujukan
        }).$promise.then(function (result) {
            console.log(result.data);
            var result = JSON.parse(result.data);
            console.log(result);
            vm.messageNik = result.metadata.message;

            if(result.response.peserta){
                vm.peserta = result.response.peserta;
            }

        }).catch(function (err) {
            console.log(err);
        });
    };

    console.log("controller bpjs peserta");
}