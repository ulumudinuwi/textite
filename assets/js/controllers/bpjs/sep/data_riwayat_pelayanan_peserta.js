/**
 * Created by agungrizkyana on 9/13/16.
 */
angular.module('rsbt').controller('BpjsDataRiwayatPelayananPesertaController', bpjsDataRiwayatPelayananPesertaController);

bpjsDataRiwayatPelayananPesertaController.$inject = ['$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource'];

function bpjsDataRiwayatPelayananPesertaController($rootScope, $http, CONFIG, EVENT, toastr, $resource) {
    var vm = this;
    vm.sep = {};

    vm.getRiwayat = function(){
        var data = angular.copy(vm.sep);
        $.ajax({
            url : base_url + '/api/bpjs/sep/riwayat_pelayanan',
            type: 'GET',
            data : "no_kartu=" + data.noKartu,

            success : function(result){
                var _result = JSON.parse(result);
                console.log(_result);
                if (!_result) {
                    toastr.error('Sistem Error', '');
                    return;
                }

                if (_result.response.metadata.code != 200) {
                    toastr.warning(_result.response.metadata.message, _result.response.metadata.code);
                }
            },
            error : function(err){
                toastr.error(err);
            }
        });
    };

    console.log("controller bpjs peserta");
}