/**
 * Created by agungrizkyana on 9/13/16.
 */
/**
 * Created by agungrizkyana on 9/13/16.
 */
angular.module('rsbt').controller('BpjsSepUpdateTanggalPulangController', bpjsSepUpdateTanggalPulangController);

bpjsSepUpdateTanggalPulangController.$inject = ['$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource', '$scope'];

function bpjsSepUpdateTanggalPulangController($rootScope, $http, CONFIG, EVENT, toastr, $resource, $scope) {
    var vm = this;
    vm.sep = {
        ppkPelayanan: '0110R005'
    };

    $scope.onChangeNoSep = function(noSep){
        $http.get(base_url + '/api/bpjs/sep/detail', {
            params: {
                no_sep: noSep
            }
        }).then(success);
        function success(result) {
            console.log(result.data.response);

            if (result.data.response) {
                var metadata = result.data.response.metadata;
                if (metadata.code != 200) {
                    toastr.warning(result.data.response.metadata.message, result.data.response.metadata.code);
                } else {
                    toastr.success('SEP di Temukan', 'Sukses !');
                    // vm.sep = result.data.response.response;
                }
            } else {
                toastr.error('Tidak Ada Response dari BPJS', 'Sistem Error');
            }



        }
    };

    vm.update = function () {
        vm.sep.tglPlg = moment(vm.sep.tglPlg).format('YYYY-MM-DD hh:mm:ss');
        var _data = {
            "request": {
                "t_sep": angular.copy(vm.sep)
            }
        };

        $.ajax({
            url: base_url + '/api/bpjs/sep/update_tanggal_pulang',
            type: 'POST',
            data: _data,
            success: function (result) {
                var _result = JSON.parse(result);
                console.log(_result);
                if (!_result) {
                    toastr.error('Sistem Error', '');
                    return;
                }

                if (_result.metadata.code != 200) {
                    toastr.warning(_result.metadata.message, _result.metadata.code);
                } else {
                    toastr.success(_result.metadata.message, _result.metadata.code);
                }
            },
            error: function (err) {
                console.log(err);
            }
        });



    };

    console.log("controller bpjs peserta");
}