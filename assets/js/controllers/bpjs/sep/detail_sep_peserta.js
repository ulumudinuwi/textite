/**
 * Created by agungrizkyana on 9/13/16.
 */
angular.module('rsbt').controller('BpjsSepDetailController', bpjsSepDetailController);

bpjsSepDetailController.$inject = ['$rootScope', '$http', 'CONFIG', 'EVENT', 'toastr', '$resource'];

function bpjsSepDetailController($rootScope, $http, CONFIG, EVENT, toastr, $resource) {
    var vm = this;
    vm.sep = {noSep: ''};


    vm.getDetailSep = function () {

        var data = angular.copy(vm.sep);
        $http.get(base_url + '/api/bpjs/sep/detail', {
            params: {
                no_sep: data.noSep
            }
        }).then(success);
        function success(result) {
            console.log(result.data.response);

            if (result.data.response) {
                var metadata = result.data.response.metadata;
                if (metadata.code != 200) {
                    toastr.warning(result.data.response.metadata.message, result.data.response.metadata.code);
                } else {
                    toastr.success('SEP di Temukan', 'Sukses !');
                    vm.sep = result.data.response.response;
                    var btnUpdateSEP = $(".update_sep");
                    var btnDeleteSEP = $(".delete_sep");
                    btnUpdateSEP.removeAttr('disabled');
                    btnDeleteSEP.removeAttr('disabled');
                }
            } else {
                toastr.error('Tidak Ada Response dari BPJS', 'Sistem Error');
            }



        }


    };

    console.log("controller bpjs peserta");
}