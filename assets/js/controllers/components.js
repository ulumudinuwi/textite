/**
 * Created by agungrizkyana on 10/11/16.
 */
var app = angular.module('rsbt.components', ['ngResource']);

app.directive('elementWithLoad', ['$rootScope', function ($rootScope) {

    function link(scope, elem, attr, controller) {
        $(elem).block({
            message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-relative fa-2x"></i>&nbsp;Loading...</span>',
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.6,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: '10px 15px',
                color: '#fff',
                '-webkit-border-radius': 2,
                '-moz-border-radius': 2,
                backgroundColor: '#333',
                'z-index': 2000
            }
        });

        $rootScope.$on('ajax:progress', function () {
            $(elem).block({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-relative fa-2x"></i>&nbsp;Loading...</span>',
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: 0.6,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333',
                    'z-index': 2000
                }
            });
        });

        $rootScope.$on('ajax:stop', function () {
            $(elem).unblock();
        });

        $rootScope.$on('loading:finish', function () {
            setTimeout(function () {
                $(elem).unblock();
            }, 300);
        });


        $(elem).load(function () {
            $('#load-iframe').addClass('hide');
        });


    }

    return {
        scope: {
            withIframe: '@'
        },
        link: link
    };
}]);

app.directive('select2', ['$rootScope', function ($rootScope) {

    function link(scope, elem, attr, controller) {
        if (scope.remoteUrl) {
            elem.select2({
                data: scope.initData,
                placeholder: scope.selectPlaceholder || 'Pilih Data',
                ajax: {
                    url: scope.remoteUrl,
                    dataType: 'json',
                    data: function (param) {
                        return {
                            delay: 0.3,
                            q: param.term
                        }
                    },
                    processResults: function (data) {
                        return {
                            results: _.map(data.items || data, function (obj) {
                                return {
                                    id: obj.id,
                                    text: obj.text || obj.nama,
                                }
                            })
                        }
                    },
                    cache: false,
                    minimumInputLength: 3,
                    minimumResultsForSearch: 10
                }
            });
            $rootScope.$on('change::select', function () {

            })
        } else {
            elem.select2({
                data: scope.initData,
                placeholder: scope.selectPlaceholder || 'Pilih Data'
            });

        }

    }

    return {
        restrict: 'A',
        scope: {
            selectPlaceholder: '=',
            remoteUrl: '=',
            dataModel: '@',
            initData: '='

        },
        link: link
    }
}]);
app.directive('select2Wilayah', ['$rootScope','$timeout', function ($rootScope, $timeout) {

    function link(scope, elem, attr, controller) {
        var url = scope.url;
        var placeholder = scope.selectPlaceholder;
        scope.$watch('initData', function (newVal, oldVal, scope) {

            $timeout(function(){

                if(newVal){
                    console.log('masuk update', newVal);
                    $(elem).select2("trigger", "select", {
                        data: newVal
                    });
                    // $(elem).select2({
                    //     data: [scope.initData],
                    //     placeholder: placeholder || 'Pilih Data',
                    //     ajax: {
                    //         url: base_url + url,
                    //         dataType: 'json',
                    //         data: function (param) {
                    //             console.log("param", param);
                    //             return {
                    //                 delay: 0.3,
                    //                 q: param.term
                    //             }
                    //         },
                    //         processResults: function (data) {
                    //             return {
                    //                 results: _.map(data.items || data, function (obj) {
                    //                     return {
                    //                         id: obj.id,
                    //                         text: obj.text || obj.nama,
                    //                     }
                    //                 })
                    //             }
                    //         },
                    //         cache: false,
                    //         minimumInputLength: 3,
                    //         minimumResultsForSearch: 10
                    //     }
                    // });
                }

            }, 1000);

            // elem.select2({
            //     data: [scope.initData],
            //     placeholder: scope.selectPlaceholder || 'Pilih Data',
            //     ajax: {
            //         url: base_url + scope.url,
            //         dataType: 'json',
            //         data: function (param) {
            //             return {
            //                 delay: 0.3,
            //                 q: param.term
            //             }
            //         },
            //         processResults: function (data) {
            //             return {
            //                 results: _.map(data.items || data, function (obj) {
            //                     return {
            //                         id: obj.id,
            //                         text: obj.text || obj.nama,
            //                     }
            //                 })
            //             }
            //         },
            //         cache: false,
            //         minimumInputLength: 3,
            //         minimumResultsForSearch: 10
            //     }
            // });
        });

        if (scope.url) {

            $(elem).select2({
                data: [scope.initData],
                placeholder: scope.selectPlaceholder || 'Pilih Data',
                ajax: {
                    url: base_url + scope.url,
                    dataType: 'json',
                    data: function (param) {
                        console.log("param", param);
                        return {
                            delay: 0.3,
                            q: param.term
                        }
                    },
                    processResults: function (data) {
                        return {
                            results: _.map(data.items || data, function (obj) {
                                return {
                                    id: obj.id,
                                    text: obj.text || obj.nama,
                                }
                            })
                        }
                    },
                    cache: false,
                    minimumInputLength: 3,
                    minimumResultsForSearch: 10
                }
            });
            $rootScope.$on('change::select', function () {

            })
        } else {
            $(elem).select2({
                data: scope.initData,
                placeholder: scope.selectPlaceholder || 'Pilih Data'
            });

        }

    }

    return {
        restrict: 'A',
        scope: {
            selectPlaceholder: '=',
            url: '@',
            dataModel: '@',
            initData: '='

        },
        link: link
    }
}]);
app.directive('datePicker', ['$http', function ($http) {
    return {
        restrict: 'A',
        scope: {
            ngModel: '=',
            picker: '@'
        },
        link: function (scope, elem, attr, ctrl) {
            if (scope.picker) {
                switch (scope.picker) {
                    case 'single':
                        elem.datepicker();
                        break;
                    case 'range':
                        elem.daterangepicker();
                        break;
                }
            } else {
                elem.daterangepicker();
            }
            return;
        }
    }
}]);

app.directive('inputMask', ['$timeout', '$compile', function ($timeout, $compile) {
    return {
        restrict: 'A',
        link: function (scope, elem, attr, ctrl) {
            $(elem).inputmask('hh:mm', {"placeholder": "00:00"});
        }
    }
}]);

app.directive('select2Ajax', ['$http', '$timeout', '$compile', function ($http, $timeout, $compile) {
    return {
        restrict: 'A',
        scope: {
            url: '=',
            initData: '@',
            select2For: '@'
        },
        link: function (scope, elem, attr, ctrl) {



            $(elem).select2({
                data: [scope.initData],
                ajax: {
                    url: base_url + scope.url,
                    dataType: 'json',
                    method: 'post',
                    data: function (param) {
                        console.log("param", param);
                        return {
                            delay: 0.3,
                            q: param.term
                        }
                    },
                    processResults: function (data) {
                        var items = data.items ? data.items : data.results;
                        return {
                            results: _.map(items || data, function (obj) {
                                return {
                                    id: obj.id,
                                    text: obj.text || obj.nama,
                                }
                            })
                        }
                    },
                    cache: false,
                    minimumInputLength: 3,
                },

            });
        }
    }
}]);

// select provinsi


app.directive('fancytreeRadiologi', ['$http', '$timeout', '$compile', function ($http, $timeout, $compile) {
    return {
        restrict: 'A',
        scope: {
            selectedNodes: '=',
            unselectedNodes: '=',
            urlResource: '@',
            rujukanId: '@'
        },
        link: function (scope, elem, attr, ctrl) {

            // blockUI
            $.blockUI({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left fa-2x"></i>&nbsp;Loading...</span>',
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: 0.6,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333',
                    'z-index': 5000
                }
            });
            $timeout(function () {
                console.log(scope.rujukanId);

                var ukurans = [{id: 1, nama: "18X20"}, {id: 2, nama: "20X28"}, {id: 3, nama: "32X32"}];
                $("#search_tindakan").on("keyup", function () {
                    var match = $(this).val();
                    var filterFunc = treeTindakan.filterNodes;

                    var n = filterFunc.call(treeTindakan, match, {autoExpand: true, highlight: true});
                });

                function loadUkuran() {
                    $.getJSON(base_url + '/api/radiologi_pendaftaran/get_ukuran', function (data, status) {
                        if (status === 'success') {
                            console.log(data.data);
                            // ukurans = data.data;
                        }
                    });
                }

                // loadUkuran();


                var treeTindakan;
                var URL_LOAD_TINDAKAN = scope.urlResource;
                // Tree Tindakan


                $(elem).fancytree({
                    extensions: ["table", "filter"],
                    quicksearch: true,
                    filter: {
                        autoApply: true,   // Re-apply last filter if lazy data is loaded
                        autoExpand: true, // Expand all branches that contain matches while filtered
                        counter: false,     // Show a badge with number of matching child nodes near parent icons
                        fuzzy: true,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                        hideExpandedCounter: true,  // Hide counter badge if parent is expanded
                        hideExpanders: false,       // Hide expanders if all child nodes are hidden by filter
                        highlight: true,   // Highlight matches by wrapping inside <mark> tags
                        leavesOnly: false, // Match end nodes only
                        nodata: true,      // Display a 'no data' status node if result is empty
                        mode: "hide"       // Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                    },
                    checkbox: true,
                    table: {
                        indentation: 20,      // indent 20px per node level
                        nodeColumnIdx: 1,     // render the node title into the 2nd column
                        checkboxColumnIdx: 0  // render the checkboxes into the 1st column
                    },
                    source: {
                        url: URL_LOAD_TINDAKAN,
                        complete: function () {
                            var rootNode = $(elem).fancytree("getRootNode");
                            rootNode.sortChildren(function (a, b) {
                                var x = (a.isFolder() ? "0" : "1") + a.title.toLowerCase(),
                                    y = (b.isFolder() ? "0" : "1") + b.title.toLowerCase();
                                return x === y ? 0 : x > y ? 1 : -1;
                            }, true);

                            var tree = $(elem).fancytree("getTree");
                            var selectedNodes = tree.getSelectedNodes();

                            var d;
                            var tr;
                            for (var i = 0; i < selectedNodes.length; i++) {
                                selectedNodes[i].parent.setExpanded(true);
                                d = selectedNodes[i].data;
                                tr = $(selectedNodes[i].tr);

                                /*var inputId = $("<input/>")
                                 .prop("type", "hidden")
                                 .prop("name", "detail_id[]")
                                 .val(d.details.id);*/
                                var inputTarifId = $("<input/>")
                                    .prop("type", "hidden")
                                    .prop("name", "detail_tarif_pelayanan_id[]")
                                    .val(d.details.tarif_pelayanan_id);
                                var inputUkuran = $("<select/>")
                                    .prop("name", "detail_ukuran[]")
                                    .addClass("form-control");
                                for (var j = 0; j < ukurans.length; j++) {
                                    inputUkuran.append('<option value="' + ukurans[j].id + '">' + ukurans[j].nama + '</option>');
                                }
                                inputUkuran.val(d.details.radiologi_ukuran_id);

                                tr.find("td:eq(2)")/*.append(inputId)*/.append(inputTarifId).append(inputUkuran);
                                $compile(tr)(scope);
                            }
                        }
                    },
                    lazyLoad: function (event, data) {
                        data.result = {url: "ajax-sub2.json"}
                    },
                    beforeSelect: function (event, data) {
                        var node = data.node;
                        var d = node.data;
                        var tr = $(node.tr);

                        if (!node.selected) {

                            var inputTarifId = $("<input/>")
                                .prop("type", "hidden")
                                .prop("name", "detail_tarif_pelayanan_id[]")
                                .val(d.id);

                            var inputUkuran = $("<select/>")
                                .prop("name", "detail_ukuran[]")
                                .addClass("form-control");
                            for (var i = 0; i < ukurans.length; i++) {
                                inputUkuran.append('<option value="' + ukurans[i].id + '">' + ukurans[i].nama + '</option>');
                            }

                            tr.find("td:eq(2)")/*.append(inputId)*/.append(inputTarifId).append(inputUkuran);

                            inputUkuran.focus();
                        } else {
                            tr.find("td:eq(2)").empty();
                        }
                    },
                    select: function (event, data) {
                        console.log("select data", data);

                        data.node.visit(function (child) {
                            console.log("visiting");
                        });
                        var node = {
                            root: {},
                            children: []
                        };

                        if (data.node.isSelected()) {
                            // is have children?
                            if (data.node.children) {

                                node.root = data.node.data;
                                node.children = _.map(data.node.children, function (child) {
                                    return child.data;
                                });
                            } else {

                                node.root = data.node.parent.data;
                                node.children.push(data.node.data);
                            }

                            scope.selectedNodes(node);
                        }else{
                            scope.unselectedNodes(data.node.data);
                        }

                    },
                    renderColumns: function (event, data) {
                        var node = data.node;
                        var d = node.data;
                        $tdList = $(node.tr).find(">td");

                        // (index #0 is rendered by fancytree)

                        if (node.folder) {
                            $tdList.eq(1).prop("colspan", 4).css('text-align', 'left');
                            $tdList.eq(0).remove();
                            $tdList.eq(2).remove();
                            $tdList.eq(3).remove();
                        }
                    }
                });

                treeTindakan = $(elem).fancytree("getTree");
                if (scope.rujukanId != -1) {

                    $timeout(function () {

                        treeTindakan.options.source.url = scope.urlResource + "?id=" + scope.rujukanId;
                        treeTindakan.reload();
                    }, 3100);
                }
                $.unblockUI();
            }, 3000);


        }
    }
}]);
app.directive('fancytreeLab', ['$http', '$timeout', '$compile', function ($http, $timeout, $compile) {
    var element = null;

    return {
        restrict: 'A',
        scope: {
            selectedNodes: '=',
            unselectedNodes: '=',
            urlResource: '@',
            rujukanId: '@'
        },
        link: function (scope, elem, attr, ctrl) {
            function getSource(url) {
                return {
                    url: url,
                    complete: function () {
                        var rootNode = $(elem).fancytree("getRootNode");
                        rootNode.sortChildren(function (a, b) {
                            var x = (a.isFolder() ? "0" : "1") + a.title.toLowerCase(),
                                y = (b.isFolder() ? "0" : "1") + b.title.toLowerCase();
                            return x === y ? 0 : x > y ? 1 : -1;
                        }, true);

                        var tree = $(elem).fancytree("getTree");
                        var selectedNodes = tree.getSelectedNodes();

                        var d;
                        var tr;
                        for (var i = 0; i < selectedNodes.length; i++) {
                            selectedNodes[i].parent.setExpanded(true);
                            d = selectedNodes[i].data;
                            tr = $(selectedNodes[i].tr);

                            $compile(tr)(scope);
                        }
                    }
                };
            };

            element = elem;
            attr.$observe('urlResource', function (newUrl) {
                console.log("NEW URL", newUrl);
                scope.urlResource = newUrl;

                if (element) {
                    var tree = $(element).fancytree('getTree');
                    if (tree) {
                        tree.reload(getSource(newUrl));
                    }
                }
            });

            // blockUI
            $.blockUI({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left fa-2x"></i>&nbsp;Loading...</span>',
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: 0.6,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333',
                    'z-index': 5000
                }
            });
            $timeout(function () {
                console.log(scope.rujukanId);

                var ukurans = [{id: 1, nama: "18X20"}, {id: 2, nama: "20X28"}, {id: 3, nama: "32X32"}];
                $("#search_tindakan").on("keyup", function () {
                    var match = $(this).val();
                    var filterFunc = treeTindakan.filterNodes;

                    var n = filterFunc.call(treeTindakan, match, {autoExpand: true, highlight: true});
                });

                function loadUkuran() {
                    $.getJSON(base_url + '/api/radiologi_pendaftaran/get_ukuran', function (data, status) {
                        if (status === 'success') {
                            console.log(data.data);
                            // ukurans = data.data;
                        }
                    });
                }

                // loadUkuran();


                var treeTindakan;
                var URL_LOAD_TINDAKAN = scope.urlResource;
                // Tree Tindakan


                $(elem).fancytree({
                    extensions: ["table", "filter"],
                    quicksearch: true,
                    filter: {
                        autoApply: true,   // Re-apply last filter if lazy data is loaded
                        autoExpand: true, // Expand all branches that contain matches while filtered
                        counter: false,     // Show a badge with number of matching child nodes near parent icons
                        fuzzy: true,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                        hideExpandedCounter: true,  // Hide counter badge if parent is expanded
                        hideExpanders: false,       // Hide expanders if all child nodes are hidden by filter
                        highlight: true,   // Highlight matches by wrapping inside <mark> tags
                        leavesOnly: false, // Match end nodes only
                        nodata: true,      // Display a 'no data' status node if result is empty
                        mode: "hide"       // Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                    },
                    checkbox: true,
                    table: {
                        indentation: 20,      // indent 20px per node level
                        nodeColumnIdx: 1,     // render the node title into the 2nd column
                        checkboxColumnIdx: 0  // render the checkboxes into the 1st column
                    },
                    source: getSource(URL_LOAD_TINDAKAN),
                    lazyLoad: function (event, data) {
                        data.result = {url: "ajax-sub2.json"}
                    },
                    beforeSelect: function (event, data) {
                        var node = data.node;
                        var d = node.data;
                        var tr = $(node.tr);

                        if (!node.selected) {

                            var inputTarifId = $("<input/>")
                                .prop("type", "hidden")
                                .prop("name", "detail_tarif_pelayanan_id[]")
                                .val(d.id);

                            var inputUkuran = $("<select/>")
                                .prop("name", "detail_ukuran[]")
                                .addClass("form-control");
                            for (var i = 0; i < ukurans.length; i++) {
                                inputUkuran.append('<option value="' + ukurans[i].id + '">' + ukurans[i].nama + '</option>');
                            }

                            tr.find("td:eq(2)")/*.append(inputId)*/.append(inputTarifId).append(inputUkuran);

                            inputUkuran.focus();
                        } else {
                            tr.find("td:eq(2)").empty();
                        }
                    },
                    select: function (event, data) {
                        console.log("select data", data);

                        data.node.visit(function (child) {
                            console.log("visiting");
                        });
                        var node = {
                            root: {},
                            children: []
                        };

                        if (data.node.isSelected()) {
                            // is have children?
                            if (data.node.children) {

                                node.root = data.node.data;
                                node.children = _.map(data.node.children, function (child) {
                                    return child.data;
                                });
                            } else {

                                node.root = data.node.parent.data;
                                node.children.push(data.node.data);
                            }

                            scope.selectedNodes(node);
                        }else{
                            scope.unselectedNodes(data.node.data);
                        }

                    },
                    renderColumns: function (event, data) {
                        var node = data.node;
                        var d = node.data;
                        $tdList = $(node.tr).find(">td");

                        // (index #0 is rendered by fancytree)

                        if (node.folder) {
                            $tdList.eq(1).prop("colspan", 4).css('text-align', 'left');
                            $tdList.eq(0).remove();
                            $tdList.eq(2).remove();
                            $tdList.eq(3).remove();
                        }
                    }
                });

                treeTindakan = $(elem).fancytree("getTree");
                if (scope.rujukanId != -1) {

                    $timeout(function () {
                        console.log("url reload", scope.urlResource + "?id=" + scope.rujukanId);
                        treeTindakan.options.source.url = scope.urlResource + "?id=" + scope.rujukanId;
                        treeTindakan.reload();
                    }, 3100);
                }
                $.unblockUI();
            }, 3000);


        }
    }
}]);
app.directive('fancytreeFisioterapi', ['$http', '$timeout', '$compile', function ($http, $timeout, $compile) {
    return {
        restrict: 'A',
        scope: {
            selectedNodes: '=',
            unselectedNodes: '=',
            urlResource: '@',
            rujukanId: '@'
        },
        link: function (scope, elem, attr, ctrl) {

            // blockUI
            $.blockUI({
                message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left fa-2x"></i>&nbsp;Loading...</span>',
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: 0.6,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    '-webkit-border-radius': 2,
                    '-moz-border-radius': 2,
                    backgroundColor: '#333',
                    'z-index': 5000
                }
            });
            $timeout(function () {
                console.log(scope.rujukanId);

                var ukurans = [{id: 1, nama: "18X20"}, {id: 2, nama: "20X28"}, {id: 3, nama: "32X32"}];
                $("#search_tindakan").on("keyup", function () {
                    var match = $(this).val();
                    var filterFunc = treeTindakan.filterNodes;

                    var n = filterFunc.call(treeTindakan, match, {autoExpand: true, highlight: true});
                });

                function loadUkuran() {
                    $.getJSON(base_url + '/api/radiologi_pendaftaran/get_ukuran', function (data, status) {
                        if (status === 'success') {
                            console.log(data.data);
                            // ukurans = data.data;
                        }
                    });
                }

                // loadUkuran();


                var treeTindakan;
                var URL_LOAD_TINDAKAN = scope.urlResource;
                // Tree Tindakan


                $(elem).fancytree({
                    extensions: ["table", "filter"],
                    quicksearch: true,
                    filter: {
                        autoApply: true,   // Re-apply last filter if lazy data is loaded
                        autoExpand: true, // Expand all branches that contain matches while filtered
                        counter: false,     // Show a badge with number of matching child nodes near parent icons
                        fuzzy: true,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                        hideExpandedCounter: true,  // Hide counter badge if parent is expanded
                        hideExpanders: false,       // Hide expanders if all child nodes are hidden by filter
                        highlight: true,   // Highlight matches by wrapping inside <mark> tags
                        leavesOnly: false, // Match end nodes only
                        nodata: true,      // Display a 'no data' status node if result is empty
                        mode: "hide"       // Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
                    },
                    checkbox: true,
                    table: {
                        indentation: 20,      // indent 20px per node level
                        nodeColumnIdx: 1,     // render the node title into the 2nd column
                        checkboxColumnIdx: 0  // render the checkboxes into the 1st column
                    },
                    source: {
                        url: URL_LOAD_TINDAKAN,
                        complete: function () {
                            var rootNode = $(elem).fancytree("getRootNode");
                            rootNode.sortChildren(function (a, b) {
                                var x = (a.isFolder() ? "0" : "1") + a.title.toLowerCase(),
                                    y = (b.isFolder() ? "0" : "1") + b.title.toLowerCase();
                                return x === y ? 0 : x > y ? 1 : -1;
                            }, true);

                            var tree = $(elem).fancytree("getTree");
                            var selectedNodes = tree.getSelectedNodes();

                            var d;
                            var tr;
                            for (var i = 0; i < selectedNodes.length; i++) {
                                selectedNodes[i].parent.setExpanded(true);
                                d = selectedNodes[i].data;
                                tr = $(selectedNodes[i].tr);

                                $compile(tr)(scope);
                            }
                        }
                    },
                    lazyLoad: function (event, data) {
                        data.result = {url: "ajax-sub2.json"}
                    },
                    beforeSelect: function (event, data) {
                        var node = data.node;
                        var d = node.data;
                        var tr = $(node.tr);

                        if (!node.selected) {

                            var inputTarifId = $("<input/>")
                                .prop("type", "hidden")
                                .prop("name", "detail_tarif_pelayanan_id[]")
                                .val(d.id);

                            var inputUkuran = $("<select/>")
                                .prop("name", "detail_ukuran[]")
                                .addClass("form-control");
                            for (var i = 0; i < ukurans.length; i++) {
                                inputUkuran.append('<option value="' + ukurans[i].id + '">' + ukurans[i].nama + '</option>');
                            }

                            tr.find("td:eq(2)")/*.append(inputId)*/.append(inputTarifId).append(inputUkuran);

                            inputUkuran.focus();
                        } else {
                            tr.find("td:eq(2)").empty();
                        }
                    },
                    select: function (event, data) {
                        console.log("select data", data);

                        data.node.visit(function (child) {
                            console.log("visiting");
                        });
                        var node = {
                            root: {},
                            children: []
                        };

                        if (data.node.isSelected()) {
                            // is have children?
                            if (data.node.children) {

                                node.root = data.node.data;
                                node.children = _.map(data.node.children, function (child) {
                                    return child.data;
                                });
                            } else {

                                node.root = data.node.parent.data;
                                node.children.push(data.node.data);
                            }

                            scope.selectedNodes(node);
                        }else{
                            scope.unselectedNodes(data.node.data);
                        }

                    },
                    renderColumns: function (event, data) {
                        var node = data.node;
                        var d = node.data;
                        $tdList = $(node.tr).find(">td");

                        // (index #0 is rendered by fancytree)

                        if (node.folder) {
                            $tdList.eq(1).prop("colspan", 4).css('text-align', 'left');
                            $tdList.eq(0).remove();
                            $tdList.eq(2).remove();
                            $tdList.eq(3).remove();
                        }
                    }
                });

                treeTindakan = $(elem).fancytree("getTree");
                if (scope.rujukanId != -1) {

                    $timeout(function () {
                        console.log("url reload", scope.urlResource + "?id=" + scope.rujukanId);
                        treeTindakan.options.source.url = scope.urlResource + "?id=" + scope.rujukanId;
                        treeTindakan.reload();
                    }, 3100);
                }
                $.unblockUI();
            }, 3000);


        }
    }
}]);

app.directive('blockUi', ['$http', '$rootScope', function ($http, $rootScope) {
    return {
        restrict: 'E',
        scope: {
            isShow: '='
        },
        link: function (scope, elem, attr, ctrl) {

            $rootScope.$on('LOADING:EVENT:PROCESSING', function () {

                $.blockUI({
                    message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left fa-2x"></i>&nbsp;Loading...</span>',
                    overlayCSS: {
                        backgroundColor: '#000',
                        opacity: 0.6,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: '10px 15px',
                        color: '#fff',
                        '-webkit-border-radius': 2,
                        '-moz-border-radius': 2,
                        backgroundColor: '#333',
                        'z-index': 2000
                    }
                });

            });
            $rootScope.$on('LOADING:EVENT:FINISH', function () {
                $.unblockUI();
            });

        }
    }
}]);
app.directive('statusLokalis', [
    '$http', '$rootScope',
    function ($http, $rootScope) {
        function link(scope, elem, attr, controller) {
            var $container = $("#container");
            $container.css('cursor', 'pointer');
            $container.click(function (e) {
                e.preventDefault();

                var x = e.pageX - this.offsetLeft;
                var y = e.pageY - this.offsetTop;
                var img = $('<img>');
                img.css('top', y);
                img.css('left', x);
                img.attr('src', 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/map-marker-icon.png');
                img.css({
                    'width': '20px',
                    'height': '20px',
                    'z-index': 1000
                });
                console.log({
                    x: x,
                    y: y,
                    img: img
                });
                img.append('#container');
            });
        }

        return {
            restrict: 'E',
            replace: true,
            scope: {},
            template: '<div id="container"></div>',
            link: link
        }
    }
]);
app.directive('fileUpload', ['$rootScope', function ($rootScope) {
    function link(scope, elem, attr, controller) {
        console.log('use directive file upload');
        var inputForm = $(elem),
            inputName = attr.name,
            inputFile = {},
            inputFileName = '';

        var formData = new FormData();

        inputForm.on('change', function (e) {
            console.log(inputForm[0].files[0]);
            inputFile = inputForm[0].files[0];
            inputFileName = inputFile.name;
            formData.append('file', inputFile, inputFileName);
            setFileCallback(formData);
        });

        function setFileCallback(formData) {
            if (typeof scope.fileCallback === 'function') {
                scope.fileCallback(formData);
            } else {
                scope.fileCallback = formData;
            }
        }
    }

    return {
        restrict: 'A',
        scope: {
            fileUploadType: '@',
            fileCallback: '='
        },
        link: link
    }
}]);

app.directive('multiselect', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
    function link(scope, elem, attr, controller) {

        console.log("init data to multiselect", scope.initData);
        $(elem).select2({
            data: [{id: 0, text: 'tes'}],
            ajax: {
                url: base_url + '/api/master/signa/search?kelompok=' + $(elem).data('signa'),
                dataType: 'json',
                type: 'post',
                data: function (param) {
                    return {
                        delay: 0.3,
                        q: param.term
                    }
                },
                processResults: function (data) {
                    return {
                        results: _.map(data.items || data, function (obj) {
                            return {
                                id: obj.id,
                                text: obj.text || obj.nama || obj.label,
                            }
                        })
                    }
                },
                cache: false,
                minimumInputLength: 3,
            }
        });

        $(elem).on('select2:select', function () {
            scope.selectCallback($(elem).val());
        });
    }

    return {
        restrict: 'A',
        scope: {
            selectCallback: '=',
            initData: '='
        },
        transclude: true,

        link: link
    }
}]);