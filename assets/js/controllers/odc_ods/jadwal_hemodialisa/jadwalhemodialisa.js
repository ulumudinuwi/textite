var the_table;
var urlReq = base_url+ "/odc_ods/jadwal_hemodialisa/list_jadwal_odc";
var def = false;
var pdokter= false
var ppoli=false;
var save_method; //for save method string
var table;
var visible=true;


$(window).load(function () {
    //Bagian Pasien

    $('#btn-reset').click(function(){
        if(typeof the_table != 'undefined'){
            the_table.destroy();
        }
        console.log("masuk");

        pdokter=false;
        ppoli=false;
        create_table();
    });

    $('#poli').change(function () {
        if($('#poli').val()!="") {
            console.log($(this).val());
            if (typeof the_table != 'undefined') {
                the_table.destroy();
            }
            ppoli = true;
            create_table();
        }
    });
    $('#dokter').change(function () {
        if($('#dokter').val()!="") {
            console.log($(this).val());
            if (typeof the_table != 'undefined') {
                the_table.destroy();
            }
            pdokter = true;
            create_table();
        }
    });

    $('.daterange-basic').daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        startDate: {
            format: 'dd/mm/yyyy',
            value: '<?php echo $date_now; ?>'
        },
        endDate: {
            format: 'dd/mm/yyyy',
            value: '<?php echo $date_now; ?>'
        }
    });
    pdokter=false;
    ppoli=false;
    create_table();

});

function create_table() {

    var dokter = $('#dokter').val();
    var poli = $('#poli').val();
    console.log(dokter);
    console.log(poli);

    the_table = $("#dataTable_user").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": urlReq,
            "type": "POST",

            "data": {
                dokter: dokter,
                poli: poli,
                pdokter:pdokter,
                ppoli:ppoli
            },
        },
        "order" :[[ 5, 'asc' ], [ 2, 'asc' ]],
        "columns": [
            {
                "data": "no_rekam_medis"
            },
            {
                "data": "nama"
            },
            {
                "data": "layanan"
            },
            {
                "searchable":false,
                "render": function (data, type, row, meta) {
                    return "<strong>"+ row.kode+"</strong>" +" - "+row.bed;
                }
            },
            {
                "data": "tgl_text"
            },
            {
                "searchable":false,
                "render": function (data, type, row, meta) {
                    return row.jam_mulai +" - "+row.jam_akhir;
                }
            },


            {
                "data": "action",
                "orderable": false,
                "render": function (data, type, row, meta) {
                    return '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('+"'"+row.id+"'"+')"><i class="glyphicon glyphicon-pencil"></i></a>  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('+"'"+row.id+"'"+')"><i class="glyphicon glyphicon-trash"></i> </a>';
                }
            }
        ]
    });
}


$(document).ready(function() {

    $("#jam_mulai").inputmask("h:s",{ "placeholder": "hh/mm" });
    $("#jam_akhir").inputmask("h:s",{ "placeholder": "hh/mm" });


    //set input/textarea/select event when change value, remove class error and remove text help block
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select2").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    $("#tgl2").daterangepicker({
        singleDatePicker: true,
        locale: {
            format: "DD/MM/YYYY"
        }
    });

    $("#tgl2").on('apply.daterangepicker', function (ev, picker) {
        $("#tanggal").val(picker.startDate.format('YYYY-MM-DD'));
    });

    $("#btn_tanggal").click(function () {
        $("#tgl2").data("daterangepicker").toggle();
    });

    $("#poli").select2();
    $("#dokter").select2();
    $("#poli_add").select2();
    $("#pasien_add").select2();
    $("#bed_add").select2();
    $('#tambah').attr('enable',true);
    fillDepo( "#poli", "#loading_search_depo");
    fillDokter( "#dokter", "#loading_search_depo");
    fillDepo( "#poli_add", "#loading_search_depo");
    fillBed( "#bed_add", "#loading_search_depo");
    fillPasien( "#pasien_add", "#loading_search_depo");


});

function fillDepo( element, loading) {
    // $(loading).show();
    $.getJSON( base_url+"/api/layanan/get_layanan_odc_ods", function(data, status) {
        if (status === "success") {
            var option = '';
            option += '<option value="" selected="selected">-Pilih Layanan- </option>';
            for (var i = 0; i < data.depo_list.length; i++) {
                    option += '<option value="' + data.depo_list[i].id + '">' + data.depo_list[i].nama + '</option>';
            }
            $(element).html(option);
            $(element).trigger("change");
        }
        $(loading).hide();
    });
}
;
function fillDokter( element, loading) {
    // $(loading).show();
    $.getJSON( base_url+"/api/odcods_bed/get_bed_all", function(data, status) {
        if (status === "success") {
            var option = '';
            option += '<option value="" selected="selected">-Pilih Bed / Mesin- </option>';
            for (var i = 0; i < data.depo_list.length; i++) {

                option += '<option value="' + data.depo_list[i].id + '">'
                    + data.depo_list[i].kode+" - "+ data.depo_list[i].nama + '</option>';

            }

            $(element).html(option);
            $(element).trigger("change");

        }
        $(loading).hide();
    });
}
;
function fillPasien( element, loading) {
    // $(loading).show();
    $.getJSON( base_url+"/api/odc_ods/jadwal_odcods/get_pasien_odc_ods", function(data, status) {
        if (status === "success") {
            var option = '';
            option += '<option value="" selected="selected">-Pilih Pasien- </option>';
            for (var i = 0; i < data.depo_list.length; i++) {

                option += '<option value="' + data.depo_list[i].id +'">'
                    + data.depo_list[i].no_rekam_medis+" - "+ data.depo_list[i].nama + '</option>';

            }

            $(element).html(option);
            $(element).trigger("change");

        }
        $(loading).hide();
    });
}
;
function fillBed( element, loading) {
    // $(loading).show();
    $.getJSON( base_url+"/api/odcods_bed/get_bed_all", function(data, status) {
        if (status === "success") {
            var option = '';
            option += '<option value="" selected="selected">-Pilih Bed / Mesin- </option>';
            for (var i = 0; i < data.depo_list.length; i++) {

                option += '<option value="' + data.depo_list[i].id + '">'
                    + data.depo_list[i].kode+" - "+ data.depo_list[i].nama + '</option>';

            }

            $(element).html(option);
            $(element).trigger("change");

        }
        $(loading).hide();
    });
}
;

function add_jadwal()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#addJadwal').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Jadwal'); // Set Title to Bootstrap modal title
}

function edit_person(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : base_url+"/odc_ods/jadwal_hemodialisa/ajax_edit/"+ id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="poli_add"]').select2().val(data.layanan_id).trigger("change");
            $('[name="pasien_add"]').select2().val(data.pasien_id).trigger("change");
            $('[name="tgl2"]').val(data.tanggal);
            $('[name="tanggal"]').val(data.tanggal);
            $('[name="jam_mulai"]').val(data.jam_mulai);
            $('[name="jam_akhir"]').val(data.jam_akhir);
            $('[name="bed_add"]').select2().val(data.bed_id).trigger("change");
            // $('[name="ruangan_praktek"]').val(data.catatan);

            $('#addJadwal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable
    var url;

    if(save_method == 'add') {
        url = base_url+"/odc_ods/jadwal_hemodialisa/ajax_add";
    } else {
        url = base_url+"/odc_ods/jadwal_hemodialisa/ajax_update";
    }
    console.log($('#form').serialize());

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            console.log("INIIIIIIIII");
            console.log(data);
            if(data.status) //if success close modal and reload ajax table
            {
                toastr.success('Data Telah Tersimpan', 'BERHASIL!');
                $('#addJadwal').modal('hide');
                the_table.draw();
            }
            else
            {
                // for (var i = 0; i < data.inputerror.length; i++)
                // {
                //     $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                //     $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                // }
                toastr.error('Error adding / update data , MESIN / BED sudah terpakai', 'GAGAL!');
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
            // toastr.success('Simpan Jadwal Berhasil !', 'Sukses');
            // the_table.destroy();
            // create_table();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            toastr.error('Error adding / update data', 'GAGAL!');

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable

        }
    });
}

function delete_person(id)
{

    swal({
            title: "Apakah anda yakin ?",
            text: "data yang telah dihapus tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "YA, Hapus",
            cancelButtonText: "TIDAK!, Batalkan",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url : base_url+"/odc_ods/jadwal_hemodialisa/ajax_delete/"+id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        //if success reload ajax table
                        $('#modal_form').modal('hide');
                        the_table.draw();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error deleting data');
                    }
                });
                toastr.success('Data Telah Terhapus', 'BERHASIL!');

            } else {
                swal("Batal", "Data Tidak Terhapus :)", "error");
            }
        });

}