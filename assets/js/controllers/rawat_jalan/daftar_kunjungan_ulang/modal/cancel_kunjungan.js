/**
 * Created by agungrizkyana on 10/7/16.
 */
app.controller('ModalCancelKunjunganController', modalCancelKunjunganController);
modalCancelKunjunganController.$inject = ['$scope', 'api','toastr', '$http', '$uibModalInstance', '$log', '$compile', 'items'];
function modalCancelKunjunganController($scope, api,toastr, $http, $uibModalInstance, $log, $compile, items) {

    $scope.data = {
        id : items,
        alasan : ''
    };

    $scope.save = save;

    $scope.close = close;
    $scope.dismiss = dismiss;

    function save() {
        api.kunjunganUlang.batalkanKunjungan(angular.copy($scope.data)).$promise.then(function(result){
            console.log(result);
            toastr.success('Batalkan Kunjungan Ulang Rawat Jalan Berhasil !', 'Sukses');

           close(result);
        }).catch(function(err){
            toastr.error('Terdapat kesalahan', 'Gagal');
            console.log(err);
        });
        // close({});
    }

    function dismiss() {
        $uibModalInstance.dismiss();
    }
    function close(result) {
        console.log("data : ", result);
        $uibModalInstance.close(result);
    }

}