/**
 * Created by agungrizkyana on 10/5/16.
 */
app.controller('ModalAddRacikanController', modalAddRacikanController);

/**
 * Modal Add Racikan
 * @type {string[]}
 */
modalAddRacikanController.$inject = ['$rootScope', '$scope', 'api', '$http', '$uibModalInstance', '$log', 'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder', '$compile', 'items', '$filter', '$resource'];
function modalAddRacikanController($rootScope, $scope, api, $http, $uibModalInstance, $log, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder, $compile, items, $filter, $resource) {
    
   // var url = base_url + "/api/master/obat/non_racikan/" + items.kelompok_pasien_id;
    $rootScope.searchObat = base_url + '/api/master/obat/non_racikan?kelompok_pasien='+items.data.kelompok_pasien_id+'&s=';
    var _tarif = [];

    $scope.obat = {
        catatan: items.catatan
    };
    $scope.dataObat = [];
    $scope.signa = [];
    $scope.signaEdit = [];
    $scope.selectedObat = selectedObat;
    $scope.remove = remove;

    $scope.total = 0;

    $scope.isEdit = false;

    // isEdit

    if (items.isEdit) {

        $scope.isEdit = true;

        $scope.dataObat = items.dataObat.tarif;

        var strSigna = "";
        _.each(_.map(items.dataObat.signa, function (o) {
            $scope.signa.push(o.id);
            return o.label;
        }), function (o) {
            strSigna += o + ", ";
        });

        $scope.signaEdit = strSigna;

        // $scope.signa = $scope.signaEdit;


        onChangeTotal();
    }

    $scope.close = function () {
        var racikan = {
            nama: items.racikan,
            dataObat: angular.copy($scope.dataObat),
            signa: angular.copy($scope.signa),
            signaEdit: angular.copy($scope.signaEdit),
            isEdit: items.isEdit,
            idx: (items.idx) ? items.idx : 0
        };
        console.log("data : ", racikan);
        $uibModalInstance.close({
            racikan : racikan,
            catatan : $scope.obat.catatan
        });
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.onChangeQty = onChangeQty;
    $scope.onChangeDiskon = onChangeDiskon;

    $scope.$on('CHANGE:TOTAL', onChangeTotal);

    $scope.selectedSigna = selectedSigna;

    function selectedSigna(val) {

       // permintaan sementara 08/01/2018
        // if ($scope.isEdit) {
        //     for (var i = 0; i < val.length; i++) {
        //         $scope.signa.push(val[i]);
        //     }
        // } else {
            $scope.signa = val;
       // }

    }

    function onChangeQty(idx, qty) {
        if(qty <= 0 || qty > $scope.dataObat[idx].stock){
            $scope.dataObat[idx].qty = "";
        }else{
            $scope.dataObat[idx].jumlah = $scope.dataObat[idx].harga * $scope.dataObat[idx].qty;
            console.log($scope.dataObat[idx].jumlah);
            // $scope.dataObat[idx].stock -= qty;
            $scope.$emit('CHANGE:TOTAL');
        }

    }

    function onChangeDiskon(idx, diskon) {
        if (diskon) {
            if (diskon >= 1) {
                var qty = $scope.dataObat[idx].qty;
                var harga = qty * $scope.dataObat[idx].harga;
                $scope.dataObat[idx].jumlah = harga - ((harga / diskon) * 100);
                $scope.$emit('CHANGE:TOTAL');
            }
        }
    }

    function selectedObat(item) {
        if (item) {
            var obat = item.originalObject;
            obat.qty = 1;
            obat.diskon = 0;
            obat.jumlah = obat.harga;
            $scope.dataObat.push(obat);
            $scope.total = _.sum(_.map($scope.dataObat, function (val) {
                return parseInt(val.jumlah);
            }));
        }
    }

    function remove(index) {
        $scope.dataObat.splice(index, 1);
        onChangeTotal();
    }

    function onChangeTotal() {
        $scope.total = _.sum(_.map($scope.dataObat, function (val) {
            return parseInt(val.jumlah);
        }));
    }


}

