var Kasir = function () {

	var oTableLookupTarifPelayanan = null;
	var oTableDaftarPasien;
	var OBJECT = null;
	var numericOptions = {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'}
	
	var showMessage = function(title, msg, type) {
		var sClass = '';
		switch (type) {
			case 'primary':
				sClass = 'bg-primary';
				break;
			case 'danger':
				sClass = 'bg-danger';
				break;
			case 'success':
				sClass = 'bg-success';
				break;
			case 'warning':
				sClass = 'bg-warning';
				break;
			case 'info':
				sClass = 'bg-info';
				break;
		}
		new PNotify({
            title: title,
            text: msg,
            addclass: sClass
        });
	};

	var fillForm = function(uid, pelayanan_uid = '') {
		$.getJSON(url_get_data + '?uid=' + uid + '&pelayanan_uid=' + pelayanan_uid, function(data, status) {
            if (status === 'success') {
            	console.log(data);
            	OBJECT = data.kasir;
                $('#tanggal').val(data.kasir.tanggal);
				$('#disp_tanggal').val(data.kasir.disp_tanggal);
				$('#no_kwitansi').val(data.kasir.no_kwitansi);
				$('#disp_no_rm').val(data.kasir.disp_no_rm);
				$('#disp_nama').val(data.kasir.disp_nama);
				$('#disp_alamat').val(data.kasir.disp_alamat);
				$('#disp_kelompok_pasien').val(data.kasir.disp_kelompok_pasien);
				$('#disp_perusahaan').val(data.kasir.disp_perusahaan);
				
				switch (parseInt(data.kasir.jenis_kelompok_pasien)) {
					case 1:
					case 2:
					case 3:
					case 6:
						$('#perusahaan_section').hide();
						break;
					case 4:
					case 5:
						$('#perusahaan_section').show();
						break;
				}
				
				switch (parseInt(data.kasir.jenis_layanan)) {
					case 1:
					case 4:
						$('#poliklinik_section').show();
						$('#no_reg_rawat_inap_section').hide();
						$('#kelas_section').hide();
						$('#ruang_section').hide();
						//$('#penunjang_medis_section').hide();
						break;
					case 2:
					case 22:
						$('#poliklinik_section').hide();
						$('#no_reg_rawat_inap_section').hide();
						$('#kelas_section').hide();
						$('#ruang_section').hide();
						//$('#penunjang_medis_section').hide();
						break;
					case 3:
						$('#poliklinik_section').hide();
						$('#no_reg_rawat_inap_section').show();
						$('#kelas_section').show();
						$('#ruang_section').show();
						//$('#penunjang_medis_section').hide();
						break;
					case 28: // ODS/ODC
						$('#poliklinik_section').hide();
						$('#no_reg_rawat_inap_section').hide();
						$('#kelas_section').hide();
						$('#ruang_section').hide();
						$('#penunjang_medis_section').hide();
						break;
				}
				
				$('#disp_no_reg_rawat_inap').val(data.kasir.no_reg_rawat_inap);
				$('#disp_layanan').val(data.kasir.disp_jenis_layanan);
				$('#disp_poliklinik').val(data.kasir.disp_layanan);
				$('#disp_kelas').val(data.kasir.disp_kelas);
				$('#disp_ruang').val(data.kasir.disp_ruang);
				$('#disp_penunjang_medis').val(data.kasir.disp_layanan);
				$('#disp_dokter').val(data.kasir.disp_dokter);
				
				var rows = '';
				var harga_satuan = 0;
				var jumlah = 0;
				var jasa_sarana_discount = 0;
				var dokter_discount = 0;
				var tenaga_ahli_discount = 0;
				var rumah_sakit_discount = 0;
				var sub_total = 0;
				var asuransi = 0;
				var tunai = 0;
				var nama = '';
				var tarif_id = '';
				var discount = 0;
				var bpjs = 0;
				var tunai = 0;
				var isContinue = true;
				var isJenisTindakan = false;
				if (data.kasir.detail_list.length > 0) {
					for (var i = 0; i < data.kasir.detail_list.length; i++) {
						isContinue = true;
						if (parseInt(data.kasir.jenis_kelompok_pasien) == 2) {
							isJenisTindakan = (parseInt(data.kasir.detail_list[i].jenis_tindakan) == 23) ||
                                              (parseInt(data.kasir.detail_list[i].jenis_tindakan) == 11) ||
											  (parseInt(data.kasir.detail_list[i].jenis_tindakan) == 24) ||
											  (parseInt(data.kasir.detail_list[i].jenis_tindakan) == 99);
							if ((isJenisTindakan) && (parseInt(data.kasir.detail_list[i].is_bpjs) == 0)) {
								isContinue = true;
							}
							else {
								isContinue = false;
							}
						}
						if (isContinue) {
							harga_satuan = parseFloat(data.kasir.detail_list[i].dokter_1) +
										   parseFloat(data.kasir.detail_list[i].dokter_2) +
										   parseFloat(data.kasir.detail_list[i].dokter_3) +
										   parseFloat(data.kasir.detail_list[i].dokter_4) +
										   parseFloat(data.kasir.detail_list[i].insentif_karyawan) +
										   parseFloat(data.kasir.detail_list[i].rumah_sakit) + 
										   parseFloat(data.kasir.detail_list[i].jasa_sarana) +
										   parseFloat(data.kasir.detail_list[i].lain_lain);
							
							if (parseInt(data.kasir.id) === 0) {
								jasa_sarana_discount = 0;
								if (parseInt(data.kasir.detail_list[i].jasa_sarana_jenis_disc) === 1) {
									if (parseFloat(data.kasir.detail_list[i].jasa_sarana) > 0 && parseFloat(data.kasir.detail_list[i].jasa_sarana_discount) > 0) {
										jasa_sarana_discount = parseFloat(data.kasir.detail_list[i].jasa_sarana) * (parseFloat(data.kasir.detail_list[i].jasa_sarana_discount)/100);
									}
								}
								else {
									jasa_sarana_discount = parseFloat(data.kasir.detail_list[i].jasa_sarana_discount);
								}
								dokter_1_discount = 0;
								if (parseInt(data.kasir.detail_list[i].dokter_1_jenis_disc) === 1) {
									if (parseFloat(data.kasir.detail_list[i].dokter_1) > 0 && parseFloat(data.kasir.detail_list[i].dokter_1_discount) > 0) {
										dokter_1_discount = parseFloat(data.kasir.detail_list[i].dokter_1) * (parseFloat(data.kasir.detail_list[i].dokter_1_discount)/100);
									}
								}
								else {
									dokter_1_discount = parseFloat(data.kasir.detail_list[i].dokter_1_discount);
								}
								dokter_2_discount = 0;
								if (parseInt(data.kasir.detail_list[i].dokter_2_jenis_disc) === 1) {
									if (parseFloat(data.kasir.detail_list[i].dokter_2) > 0 && parseFloat(data.kasir.detail_list[i].dokter_2_discount) > 0) {
										dokter_2_discount = parseFloat(data.kasir.detail_list[i].dokter_2) * (parseFloat(data.kasir.detail_list[i].dokter_2_discount)/100);
									}
								}
								else {
									dokter_2_discount = parseFloat(data.kasir.detail_list[i].dokter_2_discount);
								}
								dokter_3_discount = 0;
								if (parseInt(data.kasir.detail_list[i].dokter_3_jenis_disc) === 1) {
									if (parseFloat(data.kasir.detail_list[i].dokter_3) > 0 && parseFloat(data.kasir.detail_list[i].dokter_3_discount) > 0) {
										dokter_3_discount = parseFloat(data.kasir.detail_list[i].dokter_3) * (parseFloat(data.kasir.detail_list[i].dokter_3_discount)/100);
									}
								}
								else {
									dokter_3_discount = parseFloat(data.kasir.detail_list[i].dokter_3_discount);
								}
								dokter_4_discount = 0;
								if (parseInt(data.kasir.detail_list[i].dokter_4_jenis_disc) === 1) {
									if (parseFloat(data.kasir.detail_list[i].dokter_4) > 0 && parseFloat(data.kasir.detail_list[i].dokter_4_discount) > 0) {
										dokter_4_discount = parseFloat(data.kasir.detail_list[i].dokter_4) * (parseFloat(data.kasir.detail_list[i].dokter_4_discount)/100);
									}
								}
								else {
									dokter_4_discount = parseFloat(data.kasir.detail_list[i].dokter_4_discount);
								}
								insentif_karyawan_discount = 0;
								if (parseInt(data.kasir.detail_list[i].insentif_karyawan_jenis_disc) === 1) {
									if (parseFloat(data.kasir.detail_list[i].insentif_karyawan) > 0 && parseFloat(data.kasir.detail_list[i].insentif_karyawan_discount) > 0) {
										insentif_karyawan_discount = parseFloat(data.kasir.detail_list[i].insentif_karyawan) * (parseFloat(data.kasir.detail_list[i].insentif_karyawan_discount)/100);
									}
								}
								else {
									insentif_karyawan_discount = parseFloat(data.kasir.detail_list[i].insentif_karyawan_discount);
								}
								rumah_sakit_discount = 0;
								if (parseInt(data.kasir.detail_list[i].rumah_sakit_jenis_disc) === 1) {
									if (parseFloat(data.kasir.detail_list[i].rumah_sakit) > 0 && parseFloat(data.kasir.detail_list[i].rumah_sakit_discount) > 0) {
										rumah_sakit_discount = parseFloat(data.kasir.detail_list[i].rumah_sakit) * (parseFloat(data.kasir.detail_list[i].rumah_sakit_discount)/100);
									}
								}
								else {
									rumah_sakit_discount = parseFloat(data.kasir.detail_list[i].rumah_sakit_discount);
								}
								lain_lain_discount = 0;
								if (parseInt(data.kasir.detail_list[i].lain_lain_jenis_disc) === 1) {
									if (parseFloat(data.kasir.detail_list[i].lain_lain) > 0 && parseFloat(data.kasir.detail_list[i].lain_lain_discount) > 0) {
										lain_lain_discount = parseFloat(data.kasir.detail_list[i].lain_lain) * (parseFloat(data.kasir.detail_list[i].lain_lain_discount)/100);
									}
								}
								else {
									lain_lain_discount = parseFloat(data.kasir.detail_list[i].lain_lain_discount);
								}
								discount = jasa_sarana_discount + dokter_1_discount + dokter_2_discount + dokter_3_discount + dokter_4_discount + insentif_karyawan_discount + rumah_sakit_discount + lain_lain_discount;
							}
							else {
								discount = parseFloat(data.kasir.detail_list[i].discount);
							}

							/* BEGIN tambahan */
							var quantity = parseFloat(data.kasir.detail_list[i].quantity);
							var disp_quantity = quantity;
							switch (data.kasir.detail_list[i].kelompok_tindakan) {
								case 'Obat':
								case 'BMHP':
									if(quantity < 0) {
										disp_quantity = Math.abs(quantity);
									}
									break;
								case 'Racikan':
									quantity = 1;
									break;
							}
							/* ENG tambahan */

							jumlah = (harga_satuan - discount) * quantity;
							var disp_jumlah = jumlah;
							if(jumlah < 0) {
								disp_jumlah = Math.abs(jumlah);
							}
							sub_total += jumlah;
							
							if (parseInt(data.kasir.detail_list[i].is_bpjs) == 1) {
								asuransi += jumlah;
							}
							else {
								tunai += jumlah;
							}

							rows = '<tr>';
							rows += '	<td>';
							rows += '		<input type="hidden" id="detail_id_' + (i + 1) + '" name="detail_id[]" value="' + data.kasir.detail_list[i].id + '" />';
							switch (data.kasir.detail_list[i].kelompok_tindakan) {
								case 'Tindakan':
									tarif_id = data.kasir.detail_list[i].tarif_pelayanan_id;
									break;
								case 'Obat':
								case 'BMHP':
									tarif_id = data.kasir.detail_list[i].obat_id;
									break;
							}
							nama = '';
							switch (data.kasir.detail_list[i].kelompok_tindakan) {
								case 'Tindakan':
									nama = data.kasir.detail_list[i].nama_tarif_pelayanan;
									break;
								case 'CT-Scan':
									nama = data.kasir.detail_list[i].json_data[0];
									break;
								case 'Obat':
								case 'BMHP':
									nama = data.kasir.detail_list[i].nama_obat;
									if(quantity < 0) {
										nama = data.kasir.detail_list[i].nama_obat + ' (Retur)';
									}
									break;
								case 'Visite Dokter':
									nama = data.kasir.detail_list[i].nama_dokter;
									break;
								case 'Racikan':
									nama = data.kasir.detail_list[i].json_data.nama_racikan + ' (Isi: ' + numeral(data.kasir.detail_list[i].quantity).format('0,0') + ')';
								case 'Custom':
									nama = data.kasir.detail_list[i].keterangan;
									break;
							}
							rows += '		<input type="hidden" id="tindakan_id_' + (i + 1) + '" name="tindakan_id[]" value="' + data.kasir.detail_list[i].tindakan_id + '" />';
							rows += '		<input type="hidden" id="tanggal_tindakan_' + (i + 1) + '" name="tanggal_tindakan[]" value="' + data.kasir.detail_list[i].tanggal + '" />';
							rows += '		<input type="hidden" id="no_bukti_' + (i + 1) + '" name="no_bukti[]" value="' + data.kasir.detail_list[i].no_register + '" />';
							rows += '		<input type="hidden" id="tarif_id_' + (i + 1) + '" name="tarif_id[]" value="' + data.kasir.detail_list[i].tarif_pelayanan_id + '" />';
							rows += '		<input type="hidden" id="obat_id_' + (i + 1) + '" name="obat_id[]" value="' + data.kasir.detail_list[i].obat_id + '" />';
							rows += '		<input type="hidden" id="deskripsi_' + (i + 1) + '" name="deskripsi[]" value="' + nama + '" />';
							rows += '		<input type="hidden" id="dokter_id_' + (i + 1) + '" name="detail_dokter_id[]" value="' + data.kasir.detail_list[i].dokter_id + '" />';
							rows += '		<input type="hidden" id="dokter_1_' + (i + 1) + '" name="dokter_1[]" value="' + data.kasir.detail_list[i].dokter_1 + '" />';
							rows += '		<input type="hidden" id="dokter_2_' + (i + 1) + '" name="dokter_2[]" value="' + data.kasir.detail_list[i].dokter_2 + '" />';
							rows += '		<input type="hidden" id="dokter_3_' + (i + 1) + '" name="dokter_3[]" value="' + data.kasir.detail_list[i].dokter_3 + '" />';
							rows += '		<input type="hidden" id="dokter_4_' + (i + 1) + '" name="dokter_4[]" value="' + data.kasir.detail_list[i].dokter_4 + '" />';
							rows += '		<input type="hidden" id="insentif_karyawan_' + (i + 1) + '" name="insentif_karyawan[]" value="' + data.kasir.detail_list[i].insentif_karyawan + '" />';
							rows += '		<input type="hidden" id="rumah_sakit_' + (i + 1) + '" name="rumah_sakit[]" value="' + data.kasir.detail_list[i].rumah_sakit + '" />';
							rows += '		<input type="hidden" id="jasa_sarana_' + (i + 1) + '" name="jasa_sarana[]" value="' + data.kasir.detail_list[i].jasa_sarana + '" />';
							rows += '		<input type="hidden" id="discount_' + (i + 1) + '" name="detail_discount[]" value="' + discount + '" />';
							rows += '		<input type="hidden" id="kelompok_pasien_id_' + (i + 1) + '" name="detail_kelompok_pasien_id[]" value="' + data.kasir.kelompok_pasien_id + '" />';
							rows += '		<input type="hidden" id="jenis_tindakan_' + (i + 1) + '" name="jenis_tindakan[]" value="' + data.kasir.detail_list[i].jenis_tindakan + '" />';
							rows += '		<input type="hidden" id="is_bpjs_' + (i + 1) + '" name="is_bpjs[]" value="' + data.kasir.detail_list[i].is_bpjs + '" />';
							switch (data.kasir.detail_list[i].kelompok_tindakan) {
								case 'Racikan':
									rows += '		<label style="margin-bottom: 0;">' + data.kasir.detail_list[i].json_data.nama_racikan + ' <span class="text-slate-300 text-size-mini">(Isi: ' + numeral(data.kasir.detail_list[i].quantity).format('0,0') + ')</span></label>';
									break;
								default:
									rows += '		<label style="margin-bottom: 0;">' + nama + '</label>';
									break;
							}
							rows += '	</td>';
							rows += '	<td style="text-align: right;">';
							rows += '		<label style="margin-bottom: 0;" id="disp_harga_satuan_' + (i + 1) + '">' + harga_satuan + '</label>';
							rows += '	</td>';
							rows += '	<td style="text-align: right;">';
							rows += '		<input type="hidden" id="old_quantity_' + (i + 1) + '" name="old_quantity[]" value="' + quantity + '" />';
							rows += '		<input type="hidden" id="quantity_' + (i + 1) + '" name="quantity[]" value="' + quantity + '" />';
							rows += '		<label style="margin-bottom: 0;" id="label_quantity_' + (i + 1) + '" >' + disp_quantity + '</label>';
							rows += '		<input class="form-control quantity-row" type="text" id="disp_quantity_' + (i + 1) + '" maxlength="20" value="' + disp_quantity + '" autocomplete="off" style="text-align: right; display: none;" />';
							rows += '	</td>';
							rows += '	<td style="text-align: right;">';
							rows += '		<label style="margin-bottom: 0;" id="disp_discount_' + (i + 1) + '">' + discount + '</label>';
							rows += '	</td>';
							rows += '	<td style="text-align: right;">';
							rows += '		<label style="margin-bottom: 0;" id="disp_jumlah_' + (i + 1) + '">' + disp_jumlah + '</label>';
							rows += '	</td>';
							rows += '	<td style="text-align: center;">';
							rows += '		<input type="hidden" id="is_bpjs_' + (i + 1) + '" name="is_bpjs[]" value="' + data.kasir.detail_list[i].is_bpjs + '" />';
							rows += '		<input class="is-bpjs-row" type="checkbox" id="disp_is_bpjs_' + (i + 1) + '" name="disp_is_bpjs"' + (parseInt(data.kasir.detail_list[i].is_bpjs) === 1 ? ' checked="checked" ' : ' ') + '/>';
							rows += '	</td>';
							switch (data.kasir.detail_list[i].kelompok_tindakan) {
								case 'Obat':
								case 'Racikan':
									// rows += '	<td style="text-align: center;">';
									// rows += '		<button class="btn btn-link text-primary-600 edit-button" type="button" style="padding: 0;">';
									// rows += '			<i class="icon-pencil7"></i>';
									// rows += '		</button>';
									// rows += '		<button class="btn btn-link text-danger-600 batal-button" type="button" style="padding: 0;display: none;">';
									// rows += '			<i class="icon-undo"></i>';
									// rows += '		</button>';
									// rows += '	</td>';
									// break;
								default:
									//rows += '	<td style="text-align: center;">&nbsp;</td>';
									
							}
							rows += '</tr>';
							$("#detail_table tbody").append(rows);
							$("#detail_table tbody").find('#label_quantity_' + (i + 1)).autoNumeric('init', {aSep: '.', aDec: ',', mDec: 0, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
							$("#detail_table tbody").find('#disp_quantity_' + (i + 1)).autoNumeric('init', {aSep: '.', aDec: ',', mDec: 0, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
							$("#detail_table tbody").find('#disp_harga_satuan_' + (i + 1)).autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
							$("#detail_table tbody").find('#disp_discount_' + (i + 1)).autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
							$("#detail_table tbody").find('#disp_jumlah_' + (i + 1)).autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
							
							if (parseInt(data.kasir.jenis_kelompok_pasien) === 2) {
								if (data.kasir.detail_list[i].is_bpjs) {
									bpjs += jumlah;
								}
								else {
									tunai += jumlah;
								}
							}
						}
						
					}
				}

				//var disp_sub_total = Math.abs(sub_total);
                                var disp_sub_total = sub_total;
                                if (disp_sub_total < 0) {
                                    $('#jenis_pembayaran').val(2);
                                    
                                    $('#kartu_section').hide();
                                    $('#bayar_tunai_section').hide();
                                    $('#uang_diterima_section').hide();
                                    $('#kembalian_section').hide();
                                }
                                else {
                                    $('#jenis_pembayaran').val(1);
                                    
                                    $('#kartu_section').show();
                                    $('#bayar_tunai_section').show();
                                    $('#uang_diterima_section').show();
                                    $('#kembalian_section').show();
                                }
				$('.section_pembayaran').show();
				if(sub_total < 0) {
					$('.section_pembayaran').hide();
					//$('#td_label_bayar_tunai').html('Total harus kembali');
					//$('#td_label_uang_diterima').html('Uang kembali');
				}
				
				rows = '';
				$("#kartu_table tbody").html('');
				if (data.kasir.kartu_list.length > 0) {
					for (var i = 0; i < data.kasir.kartu_list.length; i++) {
						rows = '<tr>';
						rows += '	<td>';
						rows += '		<input type="hidden" id="kartu_detail_id_' + (i + 1) + '" name="kartu_detail_id[]" value="' + data.kasir.kartu_list[i].id + '" />';
						rows += '		<input type="hidden" id="kartu_kp_bank_id_' + (i + 1) + '" name="kartu_kp_bank_id[]" value="' + data.kasir.kartu_list[i].kp_bank_id + '" />';
						rows += '		<input type="hidden" id="kartu_kp_bank_nama_' + (i + 1) + '" value="' + data.kasir.kartu_list[i].kp_bank_nama + '" />';
						rows += '	</td>';
						rows += '	<td>';
						rows += '		<input type="hidden" id="kartu_jumlah_' + (i + 1) + '" name="kartu_jumlah[]" value="' + data.kasir.kartu_list[i].jumlah + '" />';
						rows += '	</td>';
						rows += '</tr>';
						$("#kartu_table tbody").append(rows);
					}
				}
				
				$('#sub_total').val(sub_total);
				$('#disp_sub_total').autoNumeric('set', disp_sub_total);
				
				$('#jenis_discount').val(data.kasir.jenis_discount);
				$('#discount').val(data.kasir.discount);
				var discount = 0;
				switch (parseInt(data.kasir.jenis_discount)) {
					case 1:
						$('#discount_symbol').text('%');
						$('#disp_discount_persen').autoNumeric('set', data.kasir.discount);
						if (parseInt(data.kasir.discount) > 0) {
							discount = sub_total * (parseInt(data.kasir.discount)/100);
						}
						break;
					case 2:
						$('#discount_symbol').text('Rp');
						$('#disp_discount_nominal').autoNumeric('set', data.kasir.discount);
						discount = parseFloat(data.kasir.discount);
						break;
				}
				
				tunai = tunai - discount;
				
				var hutang_pasien = 0;
				$('#hutang_pasien').val(hutang_pasien);
				$('#disp_hutang_pasien').autoNumeric('set', hutang_pasien);
				
				var ditanggung = 0;
				var mcs = data.kasir.mcs
				
				total_harus_bayar = (sub_total - discount) + hutang_pasien;
				
				/*
				switch (parseInt(data.kasir.jenis_kelompok_pasien)) {
					case 1:
						asuransi = 0;
						total_harus_bayar = (sub_total - discount) + hutang_pasien;
						break;
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
						if (parseInt(data.kasir.jenis_kelompok_pasien) === 2) {
							asuransi = bpjs;
							ditanggung = asuransi;
							total_harus_bayar = tunai;
						}
						else {
							asuransi = (sub_total - discount) + hutang_pasien;
							ditanggung = asuransi;
							total_harus_bayar = 0;
						}
						break;
				}
				*/

				var disp_total_harus_bayar = Math.abs(total_harus_bayar);
				
				//$('#asuransi').val(asuransi);
				//$('#disp_asuransi').autoNumeric('set', asuransi);
				
				$('#ditanggung_section').hide();
				$('#mcs_section').hide();
				/*
				switch (parseInt(data.kasir.jenis_kelompok_pasien)) {
					case 1:
					case 3:
					case 4:
					case 5:
					case 6:
						$('#ditanggung_section').hide();
						$('#mcs_section').hide();
						break;
					case 2:
						$('#ditanggung_section').show();
						$('#mcs_section').show();
						break;
				}
				*/
			
				// total_harus_bayar = (mcs);
				// switch (parseInt(data.kasir.jenis_kelompok_pasien)) {
					// case 1:
						// break;
					// case 2:
						// break;
					// case 3:
						// ditanggung = asuransi;
						// break;
					// case 4:
						// break;
					// case 5:
						// break;
					// case 6:
						// break;
				// }
				
				/* $('#ditanggung').val(ditanggung);
				$('#disp_ditanggung').autoNumeric('set', ditanggung);
				
				var mcs = asuransi - ditanggung;
				$('#mcs').val(mcs);
				$('#disp_msc').autoNumeric('set', mcs); */
				
				$('#total_harus_bayar').val(total_harus_bayar);
				$('#disp_total_harus_bayar').autoNumeric('set', disp_total_harus_bayar);
				
				$('#bayar_tunai').val(total_harus_bayar);
				$('#disp_bayar_tunai').autoNumeric('set', disp_total_harus_bayar);
				
				$('#kembalian').val(0);
				$('#disp_kembalian').autoNumeric('set', 0);
				
				$('#id').val(data.kasir.id);
				$('#uid').val(data.kasir.uid);
				$('#shift_id').val(data.kasir.shift_id);
				$('#pelayanan_id').val(data.kasir.pelayanan_id);
				$('#pasien_id').val(data.kasir.pasien_id);
				$('#jenis_kelompok_pasien').val(data.kasir.jenis_kelompok_pasien);
				$('#kelompok_pasien_id').val(data.kasir.kelompok_pasien_id);
				$('#perusahaan_id').val(data.kasir.perusahaan_id);
				$('#layanan_id').val(data.kasir.layanan_id);
				$('#jenis_layanan').val(data.kasir.jenis_layanan);
				$('#dokter_id').val(data.kasir.dokter_id);
				$('#poliklinik_id').val(data.kasir.poliklinik_id);
				$('#kelas_id').val(data.kasir.kelas_id);
				$('#ruang_id').val(data.kasir.ruang_id);
				$('#kp_pelayanan_id').val(data.kasir.layanan_perkiraan_id);
				$('#kp_kas_id').val(data.kasir.kp_kas_id);
				$('#kp_piutang_id').val(data.kasir.kp_piutang_id);
				
				$('#no_jaminan').val(data.kasir.no_jaminan);
				$('#inacbgs').val(data.kasir.inacbgs);

				// Update Validation
				//updateValidationUangDiterima(total_harus_bayar);

				// Update Autonumeric
				$("#disp_discount_nominal").autoNumeric('update', { 'vMax':  sub_total });

				$("#btn-toggle-discount").prop('disabled', false);
				$("#disp_discount_nominal").prop('readonly', false);
				$("#disp_discount_persen").prop('readonly', false);
				$("#kartu_button").prop('disabled', false);
				$("#disp_uang_diterima").prop('disabled', false);


				$("#disp_discount_nominal").trigger('blur');
				$("#disp_uang_diterima").autoNumeric('set', $("#disp_bayar_tunai").autoNumeric('get')).trigger('keyup');
				
				/*
				// Disable input yang tidak diperlukan berdasarkan kelompok pasien
				switch (parseInt(data.kasir.kelompok_pasien_id)) {
					case 1: // Umum
						$("#btn-toggle-discount").prop('disabled', false);
						$("#disp_discount_nominal").prop('readonly', false);
						$("#disp_discount_persen").prop('readonly', false);
						$("#kartu_button").prop('disabled', false);
						$("#disp_uang_diterima").prop('disabled', false);


						$("#disp_discount_nominal").trigger('blur');
						$("#disp_uang_diterima").autoNumeric('set', $("#disp_bayar_tunai").autoNumeric('get')).trigger('keyup');
						break;
					case 2: // BPJS
					case 3: // MCS
					case 4: // Asuransi
					case 5: // Kontraktor
					case 6: // Internal
						$("#btn-toggle-discount").prop('disabled', true);
						$("#disp_discount_nominal").prop('readonly', true);
						$("#disp_discount_persen").prop('readonly', true);
						$("#kartu_button").prop('disabled', true);
						$("#disp_uang_diterima").prop('disabled', true);
						break;
				}
				*/
				$('#discount_section').show();
				$('#asuransi_section').hide();
				$('#total_harus_bayar_section').show();
				$('#kartu_section').show();
            }
        });
	};
	
	var handleLookupTarifPelayanan = function() {
        
        oTableLookupTarifPelayanan = $('#lookup_tarif_pelayanan_table').DataTable({
            "processing"    : true,
            "serverSide"    : true,
			"ajax"			: {
                "url": url_load_lookup_tarif_pelayanan,
                "type": "GET",
				"data"    : function(d) {
								d.root_uid = $('#tarif_pelayanan_uid').val();
								d.jenis_kelompok_pasien = $('#jenis_kelompok_pasien').val();
								d.kelompok_pasien_id = $('#kelompok_pasien_id').val();
								d.kelas_id = $('#kelas_id').val();
							},
            },
            "order"         : [[ 0, "asc" ]],
			"columns": [
				{ "data": "id", "name": "id", "visible": false, "targets": [ 0 ] },
				{ "data": "uid", "name": "uid", "visible": false, "targets": [ 1 ] },
				{ "data": "kode", "name": "kode", 
					"render": function(data, type, row, meta) {
						return '<a class="select-tarif-pelayanan" data-id="' + row.id + '" data-uid="' + row.uid + '" href="#">' + data + '</a>';
					},
					"width": "20%",
					"targets": [ 2 ]},
				{ "data": "nama", "name": "nama", "targets": [ 3 ]}
			],
            "language": {
                "search": "Cari: ",
                "lengthMenu": "  _MENU_ records",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            }
        });
		
		var tableWrapper = $('#lookup_tarif_pelayanan_table_wrapper');
        tableWrapper.find('.dataTables_length select').select2();
        
    };
	
	var reloadLookupTarifPelayanan = function() {
		if (oTableLookupTarifPelayanan == null) {
			handleLookupTarifPelayanan();
		}
		else {
			oTableLookupTarifPelayanan.ajax.reload();
		}
	}
	
	var fill = function(template, data) {
        $.each(data, function(key, value) {
            var placeholder = "<%" + key + "%>";
            var value = data[key];
            while (template.indexOf(placeholder) !== -1) {
                template = template.replace(placeholder, value);
            }
        });
        return template;
    };

    var getLineNo = function(tr, control) {
        var id = tr.find(control).attr('id');
        var aId = id.split('_');
        return parseInt(aId[aId.length - 1]);
    };
	
	var formHandle = function() {
		
		$('#label_sub_total').autoNumeric('init', numericOptions);
		$('#disp_discount_persen').autoNumeric('init', numericOptions);
		$('#disp_discount_nominal').autoNumeric('init', numericOptions);
		$('#label_total').autoNumeric('init', numericOptions);
		$('#disp_uang_diterima').autoNumeric('init', numericOptions);
		$('#label_kembalian').autoNumeric('init', numericOptions);
		
		$('#tambah_button').on('click', function() {
			var $tr;

            var lineNo = parseInt($('#kasir_line_no').val());
            lineNo++;
            $('#kasir_line_no').val(lineNo);
			
			currLineNo = lineNo;

            var templateString = $('#kasir-detail-template').html();
            var newString = fill(templateString, {line_no: lineNo});
			$('#kasir_footer_section').before(newString);
            $tr = $('#kasir_footer_section').prev('tr');
			
			$('#label_harga_satuan_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_harga_satuan_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_quantity_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_quantity_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_discount_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_discount_' + lineNo).autoNumeric('init', numericOptions);
			$('#label_jumlah_' + lineNo).autoNumeric('init', numericOptions);
			$('#disp_jumlah_' + lineNo).autoNumeric('init', numericOptions);
			
            $('#kasir_detail_table').find('.edit-button').addClass('disabled');
            $('#kasir_detail_table').find('.hapus-button').addClass('disabled');
			
            $('#tambah_button').addClass('disabled');

            $('#simpan_1_button').addClass('disabled');
            $('#batal_1_button').addClass('disabled');
            
            $('#simpan_2_button').addClass('disabled');
            $('#batal_2_button').addClass('disabled');
            
            $('#disp_uraian_' + lineNo).focus();
		});
		
		$('#kasir_detail_table').on('click', '.tambah-simpan-button', function() {
            $tr = $(this).parent().parent();
			
            var lineNo = getLineNo($tr, 'input');
            
			var kode = $('#disp_kode_' + lineNo).val();
            $('#label_kode_' + lineNo).text(kode).show();
            $('#disp_kode_' + lineNo).hide();
			
			var uraian = $('#disp_uraian_' + lineNo).val();
            $('#label_uraian_' + lineNo).text(uraian).show();
            $('#disp_uraian_' + lineNo).hide();

            var hargaSatuan = parseFloat($('#disp_harga_satuan_' + lineNo).autoNumeric('get'));
            $('#harga_satuan_' + lineNo).val(hargaSatuan);
            $('#label_harga_satuan_' + lineNo).autoNumeric('set', hargaSatuan).show();
            $('#disp_harga_satuan_' + lineNo).hide();

            var quantity = parseFloat($('#disp_quantity_' + lineNo).autoNumeric('get'));
            $('#quantity_' + lineNo).val(quantity);
            $('#label_quantity_' + lineNo).autoNumeric('set', quantity).show();
            $('#disp_quantity_' + lineNo).hide();
			
			var discount = parseFloat($('#disp_discount_' + lineNo).autoNumeric('get'));
            $('#discount_' + lineNo).val(discount);
            $('#label_discount_' + lineNo).autoNumeric('set', discount).show();
            $('#disp_discount_' + lineNo).hide();
			
			var jumlah = parseFloat($('#disp_jumlah_' + lineNo).autoNumeric('get'));
            $('#jumlah_' + lineNo).val(jumlah);
            $('#label_jumlah_' + lineNo).autoNumeric('set', jumlah).show();
            $('#disp_jumlah_' + lineNo).hide();
			
			//var oldSubTotal = parseFloat($('#label_sub_total').autoNumeric('get'));
			//var newSubTotal = oldSubTotal + jumlah;
			//$('#sub_total').val(newSubTotal);
			//$('#label_sub_total').autoNumeric('set', newSubTotal);
			
			//calcTotal();
			
			$tr.find('td:nth-child(1)').css('padding', '8px');
			$tr.find('td:nth-child(2)').css('padding', '8px');
			$tr.find('td:nth-child(3)').css('padding', '8px');
			$tr.find('td:nth-child(4)').css('padding', '8px');
			$tr.find('td:nth-child(5)').css('padding', '8px');
			$tr.find('td:nth-child(6)').css('padding', '8px');
			$tr.find('td:nth-child(7)').css('padding', '8px');
			
            $tr.find('.tambah-simpan-button').removeClass('tambah-simpan-button').addClass('edit-button').html('<i class="icon-pencil7"></i>');
            $tr.find('.tambah-batal-button').removeClass('tambah-batal-button').addClass('hapus-button').html('<i class="icon-trash"></i>');
			
            $('#kasir_detail_table').find('.edit-button').removeClass('disabled');
            $('#kasir_detail_table').find('.hapus-button').removeClass('disabled');
			
            $('#tambah_button').removeClass('disabled');
			
            $('#simpan_1_button').removeClass('disabled');
            $('#batal_1_button').removeClass('disabled');
            
            $('#simpan_2_button').removeClass('disabled');
            $('#batal_2_button').removeClass('disabled');
			
            return false;
        });
		
		$('#kasir_detail_table').on('click', '.tambah-batal-button', function(event) {
            event.preventDefault;

            $(this).parent().parent().remove();

            $('#kasir_detail_table').find('.edit-button').removeClass('disabled');
            $('#kasir_detail_table').find('.hapus-button').removeClass('disabled');

            $('#tambah_button').removeClass('disabled');

            $('#simpan_1_button').removeClass('disabled');
            $('#batal_1_button').removeClass('disabled');
            
            $('#simpan_2_button').removeClass('disabled');
            $('#batal_2_button').removeClass('disabled');

            return false;
        });
		
		$('#kasir_detail_table').on('click', '.edit-button', function() {
            $tr = $(this).parent().parent();

            var lineNo = getLineNo($tr, 'label');
			
			$('#label_kode_' + lineNo).hide();
            $('#disp_kode_' + lineNo).show();
            
            $('#label_uraian_' + lineNo).hide();
            $('#disp_uraian_' + lineNo).show();

            $('#label_harga_satuan_' + lineNo).hide();
            $('#disp_harga_satuan_' + lineNo).show();

            $('#label_quantity_' + lineNo).hide();
            $('#disp_quantity_' + lineNo).show();
			
			$('#label_discount_' + lineNo).hide();
            $('#disp_discount_' + lineNo).show();
			
			$('#label_jumlah_' + lineNo).hide();
            $('#disp_jumlah_' + lineNo).show();
			
			$tr.find('td:nth-child(1)').css('padding', '2px');
			$tr.find('td:nth-child(2)').css('padding', '2px');
			$tr.find('td:nth-child(3)').css('padding', '2px');
			$tr.find('td:nth-child(4)').css('padding', '2px');
			$tr.find('td:nth-child(5)').css('padding', '2px');
			$tr.find('td:nth-child(6)').css('padding', '2px');
			$tr.find('td:nth-child(7)').css('padding', '2px');

			$tr.find('.edit-button').removeClass('edit-button').addClass('edit-simpan-button').html('<i class="icon-floppy-disk"></i>');
			$tr.find('.hapus-button').removeClass('hapus-button').addClass('edit-batal-button').html('<i class="icon-undo"></i>');

            $('#kasir_detail_table').find('.edit-button').addClass('disabled');
            $('#kasir_detail_table').find('.hapus-button').addClass('disabled');

            $('#tambah_button').addClass('disabled');

            $('#simpan_1_button').addClass('disabled');
            $('#batal_1_button').addClass('disabled');
            
            $('#simpan_2_button').addClass('disabled');
            $('#batal_2_button').addClass('disabled');
            
            return false;
        });

        $('#kasir_detail_table').on('click', '.edit-simpan-button', function() {
            $tr = $(this).parent().parent();

            var lineNo = getLineNo($tr, 'input');

			var kode = $('#disp_kode_' + lineNo).val();
			$('#kode_' + lineNo).val(kode);
            $('#label_kode_' + lineNo).text(kode).show();
            $('#disp_kode_' + lineNo).hide();
			
            var uraian = $('#disp_uraian_' + lineNo).val();
			$('#uraian_' + lineNo).val(uraian);
            $('#label_uraian_' + lineNo).text(uraian).show();
            $('#disp_uraian_' + lineNo).hide();

            var hargaSatuan = parseFloat($('#disp_harga_satuan_' + lineNo).autoNumeric('get'));
            $('#harga_satuan_' + lineNo).val(hargaSatuan);
            $('#label_harga_satuan_' + lineNo).text(hargaSatuan).show();
            $('#disp_harga_satuan_' + lineNo).hide();
			
			var quantity = parseFloat($('#disp_quantity_' + lineNo).autoNumeric('get'));
            $('#quantity_' + lineNo).val(quantity);
            $('#label_quantity_' + lineNo).text(quantity).show();
            $('#disp_quantity_' + lineNo).hide();
			
			var discount = parseFloat($('#disp_discount_' + lineNo).autoNumeric('get'));
            $('#discount_' + lineNo).val(discount);
            $('#label_discount_' + lineNo).text(discount).show();
            $('#disp_discount_' + lineNo).hide();
			
			var jumlah = parseFloat($('#disp_jumlah_' + lineNo).autoNumeric('get'));
            $('#jumlah_' + lineNo).val(jumlah);
            $('#label_jumlah_' + lineNo).text(jumlah).show();
            $('#disp_jumlah_' + lineNo).hide();

			$tr.find('td:nth-child(1)').css('padding', '8px');
			$tr.find('td:nth-child(2)').css('padding', '8px');
			$tr.find('td:nth-child(3)').css('padding', '8px');
			$tr.find('td:nth-child(4)').css('padding', '8px');
			$tr.find('td:nth-child(5)').css('padding', '8px');
			$tr.find('td:nth-child(6)').css('padding', '8px');
			$tr.find('td:nth-child(7)').css('padding', '8px');
			
			$tr.find('.edit-simpan-button').removeClass('edit-simpan-button').addClass('edit-button').html('<i class="icon-pencil7"></i>');
			$tr.find('.edit-batal-button').removeClass('edit-batal-button').addClass('hapus-button').html('<i class="icon-trash"></i>');

            $('#kasir_detail_table').find('.edit-button').removeClass('disabled');
            $('#kasir_detail_table').find('.hapus-button').removeClass('disabled');

            $('#tambah_button').removeClass('disabled');

            $('#simpan_1_button').removeClass('disabled');
            $('#batal_1_button').removeClass('disabled');
            
            $('#simpan_2_button').removeClass('disabled');
            $('#batal_2_button').removeClass('disabled');

            return false;
        });

        $('#kasir_detail_table').on('click', '.edit-batal-button', function() {

            $tr = $(this).parent().parent();

            var lineNo = getLineNo($tr, 'input');
			
			var kode = $('#kode_' + lineNo).val();
            $('#label_kode_' + lineNo).show();
            $('#disp_kode_' + lineNo).val(uraian).hide();
            
 			var uraian = $('#uraian_' + lineNo).val();
            $('#label_uraian_' + lineNo).show();
            $('#disp_uraian_' + lineNo).val(uraian).hide();

			var hargaSatuan = parseFloat($('#harga_satuan_' + lineNo).val());
            $('#label_harga_satuan_' + lineNo).show();
            $('#disp_harga_satuan_' + lineNo).val(hargaSatuan).hide();
			
			var quantity = parseFloat($('#quantity_' + lineNo).val());
            $('#label_quantity_' + lineNo).show();
            $('#disp_quantity_' + lineNo).val(quantity).hide();
			
			var discount = parseFloat($('#discount_' + lineNo).val());
            $('#label_discount_' + lineNo).show();
            $('#disp_discount_' + lineNo).val(discount).hide();
			
			var jumlah = parseFloat($('#jumlah_' + lineNo).val());
            $('#label_jumlah_' + lineNo).show();
            $('#disp_jumlah_' + lineNo).val(jumlah).hide();
			
			$tr.find('td:nth-child(1)').css('padding', '8px');
			$tr.find('td:nth-child(2)').css('padding', '8px');
			$tr.find('td:nth-child(3)').css('padding', '8px');
			$tr.find('td:nth-child(4)').css('padding', '8px');
			$tr.find('td:nth-child(5)').css('padding', '8px');
			$tr.find('td:nth-child(6)').css('padding', '8px');
			$tr.find('td:nth-child(7)').css('padding', '8px');

			$tr.find('.edit-simpan-button').removeClass('edit-simpan-button').addClass('edit-button').html('<i class="icon-pencil7"></i>');
			$tr.find('.edit-batal-button').removeClass('edit-batal-button').addClass('hapus-button').html('<i class="icon-trash"></i>');

            $('#kasir_detail_table').find('.edit-button').removeClass('disabled');
            $('#kasir_detail_table').find('.hapus-button').removeClass('disabled');

            $('#tambah_button').removeClass('disabled');

            $('#simpan_1_button').removeClass('disabled');
            $('#batal_1_button').removeClass('disabled');
            
            $('#simpan_2_button').removeClass('disabled');
            $('#batal_2_button').removeClass('disabled');

            return false;
        });

		$('#kasir_detail_table').on('click', '.hapus-button', function(event) {
            event.preventDefault;
            $(this).parent().parent().remove();
            return false;
        });
		
		$('#kasir_detail_table').on('click', '.tarif-pelayanan-button', function(e) {
			$tr = $(this).parent().parent().parent().parent();
			var lineNo = getLineNo($tr, 'input');
			
			$('#lookup_tarif_pelayanan_modal').data('counter', lineNo);
			
			//$('#tarif_pelayanan_uid').val($('#tarif_rawat_inap_uid').val());
			reloadLookupTarifPelayanan();
			
			$('#lookup_tarif_pelayanan_modal').modal({backdrop: 'static'});
			$('#lookup_tarif_pelayanan_modal').modal('show');
			
			return false;
		});
		
		$('#lookup_tarif_pelayanan_table').on('click', '.select-tarif-pelayanan', function() {
			var uid = $(this).data('uid');
			var counter =  $('#lookup_tarif_pelayanan_modal').data('counter');
			//fillTarifPelayanan(uid, counter);
			/*
			getTarif:
				cara_bayar_id
				perusahaan_id
				kelas_id
				golongan_operasi
			return:
				tarif_pelayanan
				tarif_pelayanan_detail
				tarif_pelayanan_komponen
			*/
			$("#lookup_tarif_pelayanan_modal").modal("hide");
			return false;
		});
		
	}
	
    return {

        init: function() {
			
			formHandle();
		
			$.validator.addMethod(
				"uangDiterima",
				function (value, element, requiredValue) {
					return parseFloat($(element).autoNumeric('get')) >= requiredValue;
				},
				"Uang diterima harus diisi."
			);
		
			$('#disp_sub_total').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			$('#disp_discount_persen').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '100', vMin: '-99999999999999.99'});
			$('#disp_discount_nominal').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			$('#disp_hutang_pasien').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			$('#disp_asuransi').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			$('#disp_ditanggung').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			$('#disp_mcs').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			$('#disp_total_harus_bayar').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			$('#disp_bayar_kredit').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			$('#disp_uang_diterima').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			$('#disp_kembalian').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			$('#label_mdl_total_kartu').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			
			$('#disp_bayar_tunai').autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
			
			// Click Select
			$('#disp_sub_total').on('click', function (e) {
				$(this).select();
			});
			$('#disp_discount_persen').on('click', function (e) {
				$(this).select();
			});
			$('#disp_discount_nominal').on('click', function (e) {
				$(this).select();
			});
			$('#disp_hutang_pasien').on('click', function (e) {
				$(this).select();
			});
			$('#disp_asuransi').on('click', function (e) {
				$(this).select();
			});
			$('#disp_total_harus_bayar').on('click', function (e) {
				$(this).select();
			});
			$('#disp_bayar_kredit').on('click', function (e) {
				$(this).select();
			});
			$('#disp_uang_diterima').on('click', function (e) {
				$(this).select();
			});
			$('#disp_kembalian').on('click', function (e) {
				$(this).select();
			});
			$('#label_mdl_total_kartu').on('click', function (e) {
				$(this).select();
			});
			$('#disp_bayar_tunai').on('click', function (e) {
				$(this).select();
			});
		
			$('#disp_tanggal').daterangepicker({
				autoUpdateInput: true,
				autoapply: true,
				singleDatePicker: true, 
				locale: {
					format: 'DD/MM/YYYY'
				}
			}, function(chosen_date) {
				$('#tanggal').val(chosen_date.format('YYYY-MM-DD hh:mm:ss'));
			});
			
			$('#discount_persen').on('click', function(e) {
				$(this).parent().parent().parent().removeClass('open').find('button').attr('aria-expanded', 'false');
				$('#discount_symbol').text('%');
				$('#jenis_discount').val(1);
				$('#disp_discount_persen').show();
				$('#disp_discount_nominal').hide();
				$('#disp_discount_persen').focus();

				$('#disp_discount_persen').trigger('change');
				return false;
			});
			
			$('#discount_nominal').on('click', function(e) {
				$(this).parent().parent().parent().removeClass('open').find('button').attr('aria-expanded', 'false');
				$('#jenis_discount').val(2);
				$('#discount_symbol').text('Rp');
				$('#disp_discount_persen').hide();
				$('#disp_discount_nominal').show();
				$('#disp_discount_nominal').focus();

				$('#disp_discount_nominal').trigger('change');
				return false;
			});
			
			$('#disp_discount_persen').on('change keyup input blur', function() {
				var discountPersen = numeral($('#disp_discount_persen').autoNumeric('get'))._value;
				var sub_total = numeral($('#sub_total').val())._value;
				var discountNominal = 0;
				if (discountPersen > 0) {
					discountNominal = sub_total * (discountPersen/100);
				}
				var total_harus_bayar = sub_total - discountNominal;
				var bayar_kredit = parseFloat($("#bayar_kredit").val());
				$('#discount').val(discountPersen);
				$('#disp_discount_nominal').autoNumeric('set', discountNominal);
				$('#total_harus_bayar').val(total_harus_bayar);
				$('#disp_total_harus_bayar').autoNumeric('set', total_harus_bayar);
				
				$('#bayar_tunai').val(total_harus_bayar - bayar_kredit);
				$('#disp_bayar_tunai').autoNumeric('set', total_harus_bayar - bayar_kredit);
				if(total_harus_bayar < 0) {
					$('#disp_bayar_tunai').autoNumeric('set', Math.abs(total_harus_bayar - bayar_kredit));
				}
				
				var uang_diterima = $('#disp_uang_diterima').autoNumeric('get');
				var bayar_tunai = parseFloat($('#bayar_tunai').val());
				var kembalian = uang_diterima - (bayar_tunai);
				
				$('#kembalian').val(kembalian);
				$('#disp_kembalian').autoNumeric('set', kembalian);

				// Update Validation
				//updateValidationUangDiterima(total_harus_bayar - bayar_kredit);
			});
			
			$('#disp_asuransi').on('change keyup input blur', function() {
				var asuransi = parseFloat($('#disp_asuransi').autoNumeric('get'));
				$('#asuransi').val(asuransi);
				
				var subTotal = parseFloat($('#sub_total').val());
				var totalHarusBayar = subTotal - asuransi;
				
				$('#total_harus_bayar').val(totalHarusBayar);
				$('#disp_total_harus_bayar').autoNumeric('set', totalHarusBayar);
				
				var bayarKredit = $('#disp_bayar_kredit').autoNumeric('get');
				
				totalHarusBayar -= bayarKredit;
				
				$('#bayar_tunai').val(totalHarusBayar);
				$('#disp_bayar_tunai').autoNumeric('set', totalHarusBayar);
				
				$('#uang_diterima').val(totalHarusBayar);
				$('#disp_uang_diterima').autoNumeric('set', totalHarusBayar);
			});
			
			$('#disp_discount_nominal').on('change keyup input blur', function() {
				switch (parseInt(OBJECT.kelompok_pasien_id)) {
					case 1: // Umum
						var discount = numeral($('#disp_discount_nominal').autoNumeric('get'))._value;
						var sub_total = numeral($('#sub_total').val())._value;
						var total_harus_bayar = sub_total - discount;
						var bayar_kredit = parseFloat($("#bayar_kredit").val());
						var discountPersen = discount > 0 ? (discount / sub_total) * 100 : 0; 
						$('#discount').val(discount);
						$('#disp_discount_persen').autoNumeric('set', discountPersen);
						$('#total_harus_bayar').val(total_harus_bayar);
						$('#disp_total_harus_bayar').autoNumeric('set', total_harus_bayar);
						
						$('#bayar_tunai').val(total_harus_bayar - bayar_kredit);
						$('#disp_bayar_tunai').autoNumeric('set', total_harus_bayar - bayar_kredit);
						if(total_harus_bayar < 0) {
							$('#disp_bayar_tunai').autoNumeric('set', Math.abs(total_harus_bayar - bayar_kredit));
						}
						
						var uang_diterima = parseFloat($('#uang_diterima').val());
						var bayar_tunai = parseFloat($('#bayar_tunai').val());
						var kembalian = uang_diterima - (bayar_tunai);
						
						$('#kembalian').val(kembalian);
						$('#disp_kembalian').autoNumeric('set', kembalian);

						// Update Validation
						//updateValidationUangDiterima(total_harus_bayar - bayar_kredit);
						break;
					case 2: // BPJS
					case 3: // MCS
					case 4: // Asuransi
					case 5: // Kontraktor
					case 6: // Internal
						break;
				}
			});
			
			$('#disp_uang_diterima').on('change keyup input blur', function() {
				var sub_total = $('#sub_total').val();
				$('#uang_diterima').val($('#disp_uang_diterima').autoNumeric('get'));
				if(parseFloat(sub_total < 0)) {
					$('#uang_diterima').val(Math.abs($('#disp_uang_diterima').autoNumeric('get')) * -1);
				}
				var bayar_tunai = parseFloat($('#bayar_tunai').val());
				if(bayar_tunai < 0) {
					bayar_tunai = Math.abs(bayar_tunai);
				}
				var kembalian = parseFloat($('#disp_uang_diterima').autoNumeric('get')) - bayar_tunai;
				$('#kembalian').val(kembalian);
				$('#disp_kembalian').autoNumeric('set', kembalian);
				
				var uang_diterima = parseFloat(Math.abs($('#disp_uang_diterima').autoNumeric('get')));
				var kembalian = uang_diterima - bayar_tunai;
				
				$('#kembalian').val(kembalian);
				$('#disp_kembalian').autoNumeric('set', kembalian);
			});
			
			$('#disp_ditanggung').on('focus', function() {
				$(this).select();
			});
			
			$('#disp_ditanggung').on('change keyup input blur', function() {
				$('#ditanggung').val(parseFloat($(this).autoNumeric('get')));
				var asuransi = parseFloat($('#asuransi').val());
				var ditanggung = parseFloat($('#ditanggung').val());
				var mcs = asuransi - ditanggung;
				var total_harus_bayar = 0;
				
				switch (parseInt($('#jenis_kelompok_pasien').val())) {
					case 3:
						total_harus_bayar -= mcs;
						break;
					case 1:
					case 2:
					case 4:
					case 5:
					case 6:
						break;
				}
				
				$('#mcs').val(mcs);
				$('#disp_mcs').autoNumeric('set', mcs);
				
				$('#total_harus_bayar').val(total_harus_bayar);
				$('#disp_total_harus_bayar').autoNumeric('set', total_harus_bayar);
				
				var bayar_kredit = parseFloat($('#disp_bayar_kredit').autoNumeric('get'));
				
				var bayar_tunai = total_harus_bayar - bayar_kredit;
				
				$('#bayar_tunai').val(bayar_tunai);
				$('#disp_bayar_tunai').autoNumeric('set', bayar_tunai);
				
				//updateValidationUangDiterima(total_harus_bayar);
				
			});
			
			$('#kartu_button').on('click', function() {
				$('#mdl_kartu_jumlah_tunai').val($('#total_harus_bayar').val());
				$('#disp_jumlah_tunai').text('Rp ' + $('#disp_total_harus_bayar').text());
				$('#tambah_mdl_kartu_button').removeClass('disabled');
				
				$('#mdl_kartu_table tbody').html('');
				
				var oRows = $("#kartu_table tbody tr");
				var rows = '';
				oRows.each(function(index, el) {
					// var counter = index + 1;
					// var id = $("#mdl_kartu_detail_id_" + counter).val();
					// var bank = $("#mdl_bank_" + counter).val();
					// var no_kartu = $("#mdl_no_kartu_" + counter).val();
					// var perkiraan_id = $("#mdl_perkiraan_id_" + counter).val();
					// var perkiraan = $("#label_mdl_perkiraan_" + counter).html();
					// var jumlah = $("#mdl_jumlah_" + counter).val();

					// console.log(id, bank, no_kartu, perkiraan_id, perkiraan, jumlah);

					rows = '<tr>';
					rows += '	<td style="padding: 2px;">';
					rows += '		<input type="hidden" id="mdl_kartu_detail_id_' + (index + 1) + '" name="mdl_kartu_detail_id_[]" value="' + $('#kartu_detail_id_' + (index + 1)).val() + '" />';
					rows += '		<input type="hidden" id="mdl_bank_' + (index + 1) + '" name="mdl_bank[]" value="' + $('#kartu_bank_' + (index + 1)).val() + '" />';
					rows += '		<label id="label_mdl_bank_' + (index + 1) + '" style="margin: 0px 10px;">' + $('#kartu_bank_' + (index + 1)).val() + '</label>';
					rows += '		<input class="bank-row form-control" type="text" id="disp_mdl_bank_' + (index + 1) + '" value="' + $('#kartu_bank_' + (index + 1)).val() + '" autocomplete="off" style="display: none;">';
					rows += '	</td>';
					rows += '	<td style="padding: 2px;">';
					rows += '		<input type="hidden" id="mdl_no_kartu_' + (index + 1) + '" name="mdl_no_kartu[]" value="' + $('#kartu_no_kartu_' + (index + 1)).val() + '" />';
					rows += '		<label id="label_mdl_no_kartu_' + (index + 1) + '" style="margin: 0px 10px;">' + $('#kartu_no_kartu_' + (index + 1)).val() + '</label>';
					rows += '		<input class="no-kartu-row form-control" type="text" id="disp_mdl_no_kartu_' + (index + 1) + '" value="' + $('#kartu_no_kartu_' + (index + 1)).val() + '" autocomplete="off" style="display: none;">';
					rows += '	</td>';
					rows += '	<td style="padding: 2px;">';
					rows += '		<input type="hidden" id="mdl_perkiraan_id_' + (index + 1) + '" name="mdl_perkiraan_id[]" value="' + $('#kartu_perkiraan_id_' + (index + 1)).val() + '" />';
					rows += '		<label id="label_mdl_perkiraan_' + (index + 1) + '" style="margin: 0px 10px;">' + $('#kartu_perkiraan_nama_' + (index + 1)).val() + '</label>';
					rows += '		<select class="perkiraan-row form-control" id="mdl_disp_perkiraan_id_' + (index + 1) + '" data-load="0" style="display: none;"></select>';
					rows += '	</td>';
					rows += '	<td style="padding: 2px; text-align: right;">';
					rows += '		<input type="hidden" id="mdl_jumlah_' + (index + 1) + '" name="mdl_jumlah[]" value="' + $('#kartu_jumlah_' + (index + 1)).val() + '" />';
					rows += '		<label id="label_mdl_jumlah_' + (index + 1) + '" style="margin: 0px 10px;">' + $('#kartu_jumlah_' + (index + 1)).val() + '</label>';
					rows += '		<input class="jumlah-row form-control" type="text" id="disp_mdl_jumlah_' + (index + 1) + '" value="' + $('#kartu_jumlah_' + (index + 1)).val() + '" autocomplete="off" style="text-align: right; display: none;">';
					rows += '	</td>';
					rows += '	<td style="padding: 2px; text-align: center;">';
					rows += '		<button class="btn btn-link text-primary-600 edit_button" type="button" style="padding: 0;"><i class="icon-pencil7"></i></button>';
					rows += '		<button class="btn btn-link text-danger-600 hapus_button" type="button" style="padding: 0;"><i class="icon-trash"></i></button>';
					rows += '	</td>';
					rows += '</tr>';
					$("#mdl_kartu_table tbody").append(rows);
					$("#mdl_kartu_table tbody").find('#label_mdl_jumlah_' + (index + 1)).autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
					$("#mdl_kartu_table tbody").find('#disp_mdl_jumlah_' + (index + 1)).autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
				});
				
				$('#kartu_modal').modal({backdrop: 'static'});
				$('#kartu_modal').modal('show');
				return false;
			});
			
			$("#tambah_mdl_kartu_button").click(function() {
				var $tr = $('#row_mdl_kartu_detail_clone').clone();
				
				var rowCount = $('#mdl_kartu_table tr').length;

				var counter = parseInt($("#counter_mdl_kartu").val());
				counter++;
				$("#counter_mdl_kartu").val(counter);

				$tr.attr('id', '')
				   .attr('style', '');
				
				// KOLOM: BANK
				$tr.find('#clone_mdl_kartu_detail_id')
					.attr('id', 'mdl_kartu_detail_id_' + counter)
					.attr('name', 'mdl_kartu_detail_id_[]')
					.attr('value', 'new_mdl_kartu_detail_id__id_' + counter);

				$tr.find('#clone_mdl_bank')
					.attr('id', 'mdl_bank_' + counter)
					.attr('name', 'mdl_bank[]');

				$tr.find('#clone_label_mdl_bank')
					.attr('id', 'label_mdl_bank_' + counter);

				$tr.find('#clone_disp_mdl_bank')
					.attr('id', 'disp_mdl_bank_' + counter);
				
				// KOLOM: NO. KARTU
				$tr.find('#clone_mdl_no_kartu')
					.attr('id', 'mdl_no_kartu_' + counter)
					.attr('name', 'mdl_no_kartu[]');

				$tr.find('#clone_label_mdl_no_kartu')
					.attr('id', 'label_mdl_no_kartu_' + counter);

				$tr.find('#clone_disp_mdl_no_kartu')
					.attr('id', 'disp_mdl_no_kartu_' + counter);
				
				// KOLOM: NO. AKUN
				$tr.find('#clone_mdl_perkiraan_id')
					.attr('id', 'mdl_perkiraan_id_' + counter)
					.attr('name', 'mdl_perkiraan_id[]');

				$tr.find('#clone_label_mdl_perkiraan')
					.attr('id', 'label_mdl_perkiraan_' + counter);

				$tr.find('#clone_mdl_disp_perkiraan_id')
					.attr('id', 'mdl_disp_perkiraan_id_' + counter);
				
				$.getJSON(url_get_perkiraan_bank, function(data, status) {
					if (status === "success") {
						var option_perkiraan = '';
						var id = 0;
						if (data.perkiraan_bank_list.length > 0) {
							id = data.perkiraan_bank_list[0];
						}
						for (var i = 0; i < data.perkiraan_bank_list.length; i++) {
							selected = id === parseInt(data.perkiraan_bank_list[i].id) ? ' selected="selected"' : '';
							option_perkiraan += '<option value="' + data.perkiraan_bank_list[i].id + '"' + selected + '>' + data.perkiraan_bank_list[i].kode + ' - ' + data.perkiraan_bank_list[i].nama + '</option>';
						}
						$tr.find('#mdl_disp_perkiraan_id_' + counter).html(option_perkiraan);
					}
				});
				$tr.find('#mdl_disp_perkiraan_id_' + counter).data('load', 1);
				
				// KOLOM: JUMLAH
				$tr.find('#clone_mdl_jumlah')
					.attr('id', 'mdl_jumlah_' + counter)
					.attr('name', 'mdl_jumlah[]');
				
				$tr.find('#clone_label_mdl_jumlah')
					.attr('id', 'label_mdl_jumlah_' + counter)
					.autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
				
				$tr.find('#clone_disp_mdl_jumlah')
					.attr('id', 'disp_mdl_jumlah_' + counter)
					.autoNumeric('init', {aSep: '.', aDec: ',', mDec: 2, vMax: '99999999999999.99', vMin: '-99999999999999.99'});
					
				if (parseInt(rowCount) === 1) {
					var jumlah = $('#mdl_kartu_jumlah_tunai').val();
					$tr.find('#disp_mdl_jumlah_' + counter).autoNumeric('set', jumlah);
				}
				
				$('#mdl_kartu_table tbody').append($tr);
				
				$('#mdl_button_1').attr('id', '').addClass('tambah_simpan_button').html('<i class="icon-floppy-disk"></i>');
				$('#mdl_button_2').attr('id', '').addClass('tambah_batal_button').html('<i class="icon-undo"></i>');

				$tr.find('.edit_button').addClass('disabled');
				$tr.find('.edit_button').prop('disabled', true);
				$tr.find('.hapus_button').addClass('disabled');
				$tr.find('.hapus_button').prop('disabled', true);

				$('#tambah_mdl_kartu_button').addClass('disabled');
				$('#tambah_mdl_kartu_button').prop('disabled', true);
				
				$('#simpan_mdl_kartu_button').addClass('disabled');
				$('#simpan_mdl_kartu_button').prop('disabled', true);
				$('#tutup_mdl_kartu_button').addClass('disabled');
				$('#tutup_mdl_kartu_button').prop('disabled', true);
				
				$('#mdl_kartu_table tbody').find('#disp_mdl_bank_' + counter).focus();

				return false;
			});
			
			$('#mdl_kartu_table').on('click', '.tambah_simpan_button', function(e) {
				e.preventDefault;
				
				$tr = $(this).parent().parent();

				var counter = getCounter($tr);
				
				var nama_bank = $('#disp_mdl_bank_' + counter).val();
				$('#mdl_bank_' + counter).val(nama_bank);
				$('#label_mdl_bank_' + counter).text(nama_bank).show();
				$('#disp_mdl_bank_' + counter).hide();
				
				var no_kartu = $('#disp_mdl_no_kartu_' + counter).val();
				$('#mdl_no_kartu_' + counter).val(no_kartu);
				$('#label_mdl_no_kartu_' + counter).text(no_kartu).show();
				$('#disp_mdl_no_kartu_' + counter).hide();
				
				var perkiraan_id = $tr.find('#mdl_disp_perkiraan_id_' + counter).val();
				$tr.find("#mdl_perkiraan_id_" + counter).val(perkiraan_id);
				var perkiraan = $("#mdl_disp_perkiraan_id_" + counter + " option[value='" + perkiraan_id + "']").text();
				$tr.find('#label_mdl_perkiraan_' + counter).text(perkiraan).show();
				$tr.find('#mdl_disp_perkiraan_id_' + counter).hide();
				
				var jumlah = parseFloat($tr.find('#disp_mdl_jumlah_' + counter).autoNumeric('get'));
				$tr.find('#mdl_jumlah_' + counter).val(jumlah);
				$tr.find('#label_mdl_jumlah_' + counter).autoNumeric('set', jumlah).show();
				$tr.find('#disp_mdl_jumlah_' + counter).hide();
				
				var oldTotal = parseFloat($('#mdl_total_kartu').val());
				var total = oldTotal + jumlah;
				
				$('#mdl_total_kartu').val(total);
				$('#label_mdl_total_kartu').autoNumeric('set', total);
				
				$('.tambah_simpan_button').removeClass('tambah_simpan_button').addClass('edit_button').html('<i class="icon-pencil7"></i>');
				$('.tambah_batal_button').removeClass('tambah_batal_button').addClass('hapus_button').html('<i class="icon-trash"></i>');
				
				$tr.find('.edit_button').removeClass('disabled');
				$tr.find('.edit_button').prop('disabled', false);
				$tr.find('.hapus_button').removeClass('disabled');
				$tr.find('.hapus_button').prop('disabled', false);
				
				$('#tambah_mdl_kartu_button').removeClass('disabled');
				$('#tambah_mdl_kartu_button').prop('disabled', false);

				$('#tambah_mdl_kartu_button').removeClass('disabled');
				$('#tambah_mdl_kartu_button').prop('disabled', false);
				
				$('#simpan_mdl_kartu_button').removeClass('disabled');
				$('#simpan_mdl_kartu_button').prop('disabled', false);
				$('#tutup_mdl_kartu_button').removeClass('disabled');
				$('#tutup_mdl_kartu_button').prop('disabled', false);

				return false;
			});
			
			$('#mdl_kartu_table').on('click', '.tambah_batal_button', function(e) {
				e.preventDefault;

				$tr = $(this).parent().parent().remove();
				
				$tr.find('.edit_button').removeClass('disabled');
				$tr.find('.edit_button').prop('disabled', false);
				$tr.find('.hapus_button').removeClass('disabled');
				$tr.find('.hapus_button').prop('disabled', false);
				
				$('#tambah_mdl_kartu_button').removeClass('disabled');
				$('#tambah_mdl_kartu_button').prop('disabled', false);
				
				$('#simpan_mdl_kartu_button').removeClass('disabled');
				$('#simpan_mdl_kartu_button').prop('disabled', false);
				$('#tutup_mdl_kartu_button').removeClass('disabled');
				$('#tutup_mdl_kartu_button').prop('disabled', false);

				return false;
			});
			
			$('#mdl_kartu_table').on('click', '.edit_button', function() {
				$tr = $(this).parent().parent();

				var counter = getCounter($tr);

				$tr.find('#label_mdl_bank_' + counter).hide();
				$tr.find('#disp_mdl_bank_' + counter).show();
				
				$tr.find('#label_mdl_no_kartu_' + counter).hide();
				$tr.find('#disp_mdl_no_kartu_' + counter).show();
				
				var perkiraan_id = parseInt($('#mdl_perkiraan_id_' + counter).val());
				$('#label_mdl_perkiraan_' + counter).hide();
				$('#mdl_disp_perkiraan_id_' + counter).show();
				
				if (parseInt($('#mdl_disp_perkiraan_id_' + counter).data('load')) === 0) {
					$.getJSON(url_get_perkiraan_bank, function(data, status) {
						if (status === "success") {
							var option_perkiraan = '';
							if (data.perkiraan_bank_list.length > 0) {
								id = data.perkiraan_bank_list[0];
							}
							for (var i = 0; i < data.perkiraan_bank_list.length; i++) {
								selected = (perkiraan_id === parseInt(data.perkiraan_bank_list[i].id) ? ' selected="selected"' : '');
								option_perkiraan += '<option value="' + data.perkiraan_bank_list[i].id + '"' + selected + '>' + data.perkiraan_bank_list[i].nama + '</option>';
							}
							$('#mdl_disp_perkiraan_id_' + counter).html(option_perkiraan);
						}
					});
				}
				
				var jumlah = $tr.find('#mdl_jumlah_' + counter).val();
				$tr.find('#label_mdl_jumlah_' + counter).hide();
				$tr.find('#disp_mdl_jumlah_' + counter).show();
				
				$tr.find('.edit_button').removeClass('edit_button').addClass('edit_simpan_button').html('<i class="icon-floppy-disk"></i>');
				$tr.find('.hapus_button').removeClass('hapus_button').addClass('edit_batal_button').html('<i class="icon-undo"></i>');
				
				$tr.find('.edit_button').addClass('disabled');
				$tr.find('.edit_button').prop('disabled', true);
				$tr.find('.hapus_button').addClass('disabled');
				$tr.find('.hapus_button').prop('disabled', true);

				$('#tambah_mdl_kartu_button').addClass('disabled');
				$('#tambah_mdl_kartu_button').prop('disabled', true);
				
				$('#simpan_mdl_kartu_button').addClass('disabled');
				$('#simpan_mdl_kartu_button').prop('disabled', true);
				$('#tutup_mdl_kartu_button').addClass('disabled');
				$('#tutup_mdl_kartu_button').prop('disabled', true);

				return false;
			});
			
			$('#mdl_kartu_table').on('click', '.edit_simpan_button', function() {
				$tr = $(this).parent().parent();

				var counter = getCounter($tr);
				
				var nama_bank = $('#disp_mdl_bank_' + counter).val();
				$('#mdl_bank_' + counter).val(nama_bank);
				$('#label_mdl_bank_' + counter).text(nama_bank).show();
				$('#disp_mdl_bank_' + counter).hide();
				
				var no_kartu = $('#disp_mdl_no_kartu_' + counter).val();
				$('#mdl_no_kartu_' + counter).val(no_kartu);
				$('#label_mdl_no_kartu_' + counter).text(no_kartu).show();
				$('#disp_mdl_no_kartu_' + counter).hide();
				
				var perkiraan_id = $tr.find('#mdl_disp_perkiraan_id_' + counter).val();
				$tr.find("#mdl_perkiraan_id_" + counter).val(perkiraan_id);
				var perkiraan = $("#mdl_disp_perkiraan_id_" + counter + " option[value='" + perkiraan_id + "']").text();
				$tr.find('#label_mdl_perkiraan_' + counter).text(perkiraan).show();
				$tr.find('#mdl_disp_perkiraan_id_' + counter).hide();
				
				var oldJumlah = parseFloat($('#mdl_jumlah_' + counter).val());
				var newJumlah = parseFloat($('#disp_mdl_jumlah_' + counter).autoNumeric('get'));
				$tr.find('#mdl_jumlah_' + counter).val(newJumlah);
				$tr.find('#label_mdl_jumlah_' + counter).autoNumeric('set', newJumlah).show();
				$tr.find('#disp_mdl_jumlah_' + counter).hide();
				
				var oldTotal = parseFloat($('#mdl_total_kartu').val());
				var newTotal = (oldTotal - oldJumlah) + newJumlah;
				
				$('#mdl_total_kartu').val(newTotal);
				$('#label_mdl_total_kartu').autoNumeric('set', newTotal);
				
				$('.edit_simpan_button').removeClass('edit_simpan_button').addClass('edit_button').html('<i class="icon-pencil7"></i>');
				$('.edit_batal_button').removeClass('edit_batal_button').addClass('hapus_button').html('<i class="icon-trash"></i>');
				
				$tr.find('.edit_button').removeClass('disabled');
				$tr.find('.edit_button').prop('disabled', false);
				$tr.find('.hapus_button').removeClass('disabled');
				$tr.find('.hapus_button').prop('disabled', false);
				
				$('#tambah_mdl_kartu_button').removeClass('disabled');
				$('#tambah_mdl_kartu_button').prop('disabled', false);
				
				$('#simpan_mdl_kartu_button').removeClass('disabled');
				$('#simpan_mdl_kartu_button').prop('disabled', false);
				$('#tutup_mdl_kartu_button').removeClass('disabled');
				$('#tutup_mdl_kartu_button').prop('disabled', false);

				return false;
			});
			
			$('#mdl_kartu_table').on('click', '.edit_batal_button', function() {

				$tr = $(this).parent().parent();

				var counter = getCounter($tr);
				
				var bank = $tr.find('#label_mdl_bank_' + counter).val()
				$tr.find('#label_mdl_bank_' + counter).show();
				$tr.find('#disp_mdl_bank_' + counter).val(bank).hide();
				
				var no_kartu = $tr.find('#label_mdl_bank_' + counter).val()
				$tr.find('#label_mdl_no_kartu_' + counter).show();
				$tr.find('#disp_mdl_no_kartu_' + counter).val(no_kartu).hide();
				
				$tr.find('#label_mdl_perkiraan_' + counter).show();
				$tr.find('#mdl_disp_perkiraan_id_' + counter).hide();
				
				$tr.find('#label_mdl_jumlah_' + counter).show();
				$tr.find('#disp_mdl_jumlah_' + counter).hide();

				$('.edit_simpan_button').removeClass('edit_simpan_button').addClass('edit_button').html('<i class="icon-pencil7"></i>');
				$('.edit_batal_button').removeClass('edit_batal_button').addClass('hapus_button').html('<i class="icon-trash"></i>');
				
				$tr.find('.edit_button').removeClass('disabled');
				$tr.find('.edit_button').prop('disabled', false);
				$tr.find('.hapus_button').removeClass('disabled');
				$tr.find('.hapus_button').prop('disabled', false);
				
				$('#tambah_mdl_kartu_button').removeClass('disabled');
				$('#tambah_mdl_kartu_button').prop('disabled', false);
				
				$('#simpan_mdl_kartu_button').removeClass('disabled');
				$('#simpan_mdl_kartu_button').prop('disabled', false);
				$('#tutup_mdl_kartu_button').removeClass('disabled');
				$('#tutup_mdl_kartu_button').prop('disabled', false);

				return false;
			});
			
			$('#mdl_kartu_table').on('click', '.hapus_button', function(e) {
				e.preventDefault;
				
				$tr = $(this).parent().parent();

				var counter = getCounter($tr);
				
				var oldJumlah = parseFloat($tr.find('#mdl_jumlah_' + counter).val());
				var oldTotal = parseFloat($('#mdl_total_kartu').val());
				var newTotal = (oldTotal - oldJumlah);
				
				$('#mdl_total_kartu').val(newTotal);
				$('#label_mdl_total_kartu').autoNumeric('set', newTotal);
				
				$tr.remove();
				
				return false;
			});
			
			var getCounter = function(tr) {
				var id = tr.find('input').attr('id');
				var aId = id.split('_');
				return parseInt(aId[aId.length - 1]);
			};
			
			$('#simpan_mdl_kartu_button').on('click', function() {
			
				$('#kartu_table tbody').html('');
			
				var oRows = $("#mdl_kartu_table tbody tr");
				var rows = '';
				oRows.each(function(index) {
					rows = '<tr>';
					rows += '	<td>';
					rows += '		<input type="hidden" id="kartu_detail_id_' + (index + 1) + '" name="kartu_detail_id[]" value="' + $('#mdl_kartu_detail_id_' + (index + 1)).val() + '" />';
					rows += '		<input type="hidden" id="kartu_bank_' + (index + 1) + '" name="kartu_bank[]" value="' + $('#mdl_bank_' + (index + 1)).val() + '" />';
					rows += '	</td>';
					rows += '	<td>';
					rows += '		<input type="hidden" id="kartu_no_kartu_' + (index + 1) + '" name="kartu_no_kartu[]" value="' + $('#mdl_no_kartu_' + (index + 1)).val() + '" />';
					rows += '	</td>';
					rows += '	<td>';
					rows += '		<input type="hidden" id="kartu_perkiraan_id_' + (index + 1) + '" name="kartu_perkiraan_id[]" value="' + $('#mdl_perkiraan_id_' + (index + 1)).val() + '" />';
					rows += '		<input type="hidden" id="kartu_perkiraan_nama_' + (index + 1) + '" value="' + $('#mdl_disp_perkiraan_id_' + (index + 1) + ' option:selected').text() + '" />';
					rows += '	</td>';
					rows += '	<td>';
					rows += '		<input type="hidden" id="kartu_jumlah_' + (index + 1) + '" name="kartu_jumlah[]" value="' + $('#mdl_jumlah_' + (index + 1)).val() + '" />';
					rows += '	</td>';
					rows += '</tr>';
					$("#kartu_table tbody").append(rows);
				});
				
				var jumlahKartu = parseFloat($('#mdl_total_kartu').val());
				$('#bayar_kredit').val(jumlahKartu);
				$('#disp_bayar_kredit').autoNumeric('set', jumlahKartu);
				
				var total_harus_bayar = parseFloat($('#total_harus_bayar').val());
				$('#bayar_tunai').val(total_harus_bayar - jumlahKartu);
				$('#disp_bayar_tunai').autoNumeric('set', total_harus_bayar - jumlahKartu);
				
				$('#uang_diterima').val(total_harus_bayar - jumlahKartu);
				$('#disp_uang_diterima').autoNumeric('set', total_harus_bayar - jumlahKartu);
				
				var uang_diterima = parseFloat($('#disp_bayar_tunai').autoNumeric('get'));
				var bayar_tunai = parseFloat($('#disp_bayar_tunai').autoNumeric('get'));
				var kembalian = uang_diterima - bayar_tunai;
				
				$('#kembalian').val(kembalian);
				$('#disp_kembalian').autoNumeric('set', kembalian);
				
				//updateValidationUangDiterima(total_harus_bayar - jumlahKartu);
			
				$('#kartu_modal').modal('hide');
			});
			
			$('#tutup_mdl_kartu_button').on('click', function() {
				$('#kartu_modal').modal('hide');
			});
			
			$('#batal_1_button, #batal_2_button').on('click', function() {
				if ($('#uid').val() === '') {
					window.location = url_daftar_pasien;
				}
				else {
					window.location = url_daftar_bayar;
				}
				return false;
			});
            
            //fillForm(uid, pelayanan_uid);
			
			var kasirApp = {
				initKasirForm: function () {
					$("#kasir_form").validate({
						rules: {
						},
						messages: {
						},
						submitHandler: function(form) {
							kasirApp.addKasir($(form));
						}
					});
				},
				addKasir: function(form) {
					blockElement('#main_section');
					var url = url_simpan;
					var postData = form.serialize();
					$.post(url, postData, function(data, status) {
						if (status === "success") {
							showMessage('Simpan', 'Record telah di simpan!', 'success');
							return;
						}
						showMessage('Simpan', 'Record gagal di simpan!', 'danger');
					}, 'json');
				}
			};
			kasirApp.initKasirForm();
            
            $(document).ajaxComplete(function(event, xhr, settings ) {
				if (typeof xhr.responseJSON != 'undefined') {
					switch (xhr.responseJSON.tag) {
						case 'simpan':
							$('#main_section').unblock();
							window.location = url_view + '?uid=' + xhr.responseJSON.uid + '&from=form';
							break;
					};
				}
            });
			
        }

    };

}();