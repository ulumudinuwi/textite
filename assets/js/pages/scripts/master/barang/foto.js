$(() => {
	let itr = 0;
	let EL = {
		elCrop: '#section-crop_foto',
		input: '#upload-foto',
		modal: '#foto-modal',
		content: '#content-foto',
		btnTambah: '#modal-btn_tambah_foto',
		noPreviewImage: '.no-preview-image',
	}; 

	let uploadCrop = $(EL.elCrop).croppie({
	    enableExif: true,
	    enableResize: true,
	    viewport: {
	        width: 500,
	        height: 500,
	        type: 'square'
	    },
	    boundary: {
	        width: 600,
	        height: 600,
	    },
	    /*url: '<?php echo base_url('assets/img/placeholder.jpg'); ?>'*/
	});

	let checkContent = () => {
		if($(EL.content).html() !== "") {
			$(EL.noPreviewImage).hide();
		} else $(EL.noPreviewImage).show();
	}

	let imgPreview = (input) => {
		if (input.files && input.files[0]) {
            let reader = new FileReader();
            
            reader.onload = function (e) {
				$(EL.elCrop).show();
            	uploadCrop.croppie('bind', {
            		url: e.target.result
            	}).then(function(){
            		console.log('jQuery bind complete');
            	});
            	
            }
            
            reader.readAsDataURL(input.files[0]);
        }
        else messageWarning("Peringatan !", "Browser yang digunakan tidak mendukung FileReader API");
	}

	let popupResultCrop = (result) => {
		/*let html;
		html = '<img src="' + result.src + '" />';
		swal({
			title: '',
			html: true,
			text: html,
			allowOutsideClick: true
		});
		setTimeout(function(){
			$('.sweet-alert').css('margin', function() {
				var top = -1 * ($(this).height() / 2),
					left = -1 * ($(this).width() / 2);

				return top + 'px 0 0 ' + left + 'px';
			});
		}, 1);*/

		console.log('Data Image:', result);
		makeContent({ id: 0, foto: result.src, status: 'Belum tersimpan'});
		setTimeout(() => {
			$(EL.modal).modal('hide');
		}, 200);
	}

	let makeContent = (data) => {
		itr++; 

		let labelStatusClass = 'btn-warning';
		let iconStatusClass = '<i class="icon-database-add"></i>';
		if(parseInt(data.id) !== 0) {
			labelStatusClass = 'btn-success';
			iconStatusClass = '<i class="icon-database-check"></i>';
		}

		let div = $('<div/>')
			.addClass(`col-sm-3 div-foto_${itr}`)
			.appendTo(EL.content);
			let panel = $('<div/>')
				.addClass(`panel panel-default`)
				.appendTo(div);
			let panelBody = $('<div/>')
				.addClass(`panel-body no-padding`)
				.appendTo(panel);
				let img = $('<img/>')
					.attr('src', data.foto)
					.css('width', '100%')
					.appendTo(panelBody);
				let inputFotoId = $('<input/>')
					.attr('type', 'hidden')
					.attr('name', 'foto_id[]')
					.val(data.id)
					.html(data.id)
					.appendTo(panelBody);
				let inputFoto = $('<textarea/>')
					.attr('name', 'foto[]')
					.val(data.foto)
					.html(data.foto)
					.appendTo(panelBody);
				inputFoto.hide();
			let panelFooter = $('<div/>')
				.addClass(`panel-footer no-padding-right`)
				.appendTo(panel);
				let divFooterLeft = $('<div/>')
					.addClass(`col-xs-8`)
					.appendTo(panelFooter);
					let labelStatus = $('<btn/>')
						.addClass(`btn ${labelStatusClass} btn-xs text-left`)
						.attr('title', data.status)
						.data('toggle', 'tooltip')
						.html(iconStatusClass)
						.appendTo(divFooterLeft);
					labelStatus.tooltip();
				let divFooterRight = $('<div/>')
					.addClass(`col-xs-4 text-right`)
					.appendTo(panelFooter);
						let btnDelete = $('<button/>')
							.prop('type', 'button')
							.data('id', data.id)
							.addClass('btn btn-danger btn-xs delete-row')
							.html('<i class="fa fa-trash"></trash>')
							.appendTo(divFooterRight);
		
		checkContent();
		
		// Event Handler
		btnDelete.click(function(e) {
			let id = $(this).data('id');

			if(parseInt(id) !== 0) {
				swal({
			        title: "Anda yakin akan menghapus foto tersebut?",
			        text: "foto tersebut akan hilang dari database untuk selamanya.",
			        showCancelButton: true,
			        confirmButtonColor: "#F44336",
			        confirmButtonText: "Ya",
			        cancelButtonText: "Batal",
			        closeOnConfirm: false,
			        showLoaderOnConfirm: true,
			        animation: "slide-from-top",
			        type: "warning"
			      },
			      function() {
			      	blockElement(div);
			        $.post(url.deletePhoto, {id: id}, function (data, status) {
			          if (status === "success") {
			            swal.close();
			            setTimeout(() => {
				   			div.remove();
							checkContent();
				            successMessage('Berhasil !', 'Foto berhasil dihapus.');
				   		}, 300);
			          }
			        })
			        .fail(function (error) {
			            errorMessage('Gagal !', 'Terjadi kesalahan saat akan menghapus foto.');
			        });
			      });
			} else {
				div.remove();
				checkContent();
			}
		});
	}

	$(EL.input).change(function(e) {
		e.preventDefault();
		let ext = this.value.match(/\.(.+)$/)[1];
	    switch (ext) {
	        case 'jpg':
	        case 'jpeg':
	        case 'png':
	        case 'gif':
	            console.log('Type File OK');
	            break;
	        default:
	            this.value = '';
	            $(EL.input).parent().find('.filename').html('No file selected');
	            warningMessage('Peringatan', 'Tipe file tidak diperbolehkan.');
	            return;
	           	break;
	    }
		imgPreview(this);
	});

	$(EL.btnTambah).click(function() {
		uploadCrop.croppie('result', {
			type: 'canvas',
			size: 'viewport'
		}).then(function (resp) {
			popupResultCrop({
				src: resp
			});
		});
	});

	$(EL.modal).on('hidden.bs.modal', function () {
        $(EL.elCrop).hide();
        $(EL.input).val("");
        $(EL.input).parent().find('.filename').html('No file selected');
    });

    if(UID !== "") {
   		setTimeout(() => {
   			for (var i = 0; i < dataBarang.fotos.length; i++) {
   				makeContent(dataBarang.fotos[i]);
   			}
   		}, 800); 	
    }
});