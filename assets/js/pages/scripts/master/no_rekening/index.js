$(() => {
    let initialize = () => {
        // DATATABLE
        TABLEDT = TABLE.dataTable({
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": URL.loadData,
            "columns": [
                {
                    "data": "nama_pemilik",
                    "className": "text-left"
                },
                {
                    "data": "nama_bank",
                    "render": function (data, type, row, meta) {
                        var link = URL.form.replace(':UID', row.uid);
                        if (row.deleted_flag == 1) {
                            return `<strike><a href="${link}">${data}</a></strike>`;
                        }
                        return `<a href="${link}">${data}</a>`;
                    },
                    "className": "text-left"
                },
                {
                    "data": "no_rekening",
                    "render": function (data, type, row, meta) {
                        if (row.deleted_flag == 1) {
                            return `<strike>${data}</strike>`;
                        }
                        return data;
                    },
                    "className": "text-center"
                },/*
                {
                    "data": "uid",
                    "render": function (data, type, row, meta) {
                        let url_edit = URL.form.replace(':UID', data);
                        let btnEdit = `<a href="${url_edit}" class="btn btn-primary btn-xs"><i class="icon-pencil5"></i></a>`;
                        let btnDelete = `<button type="button" class="btn btn-danger btn-xs delete" data-uid="${data}"><i class="icon-trash"></i></button>`;
                        let btnRestore = `<button type="button" class="btn btn-info btn-xs restore" data-uid="${data}"><i class="icon-reset"></i></button>`;

                        if (row.deleted_flag == 1) {
                            return btnRestore;
                        } else {
                            return btnEdit + '&nbsp;' + btnDelete;
                        }
                    },
                    "className": "text-center"
                }*/
            ],
            "order": [ [0, "asc"] ],
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                // remove filters
                for (var i = 0; i < data.columns.length; i++) {
                    switch (i) {
                        default:
                            data.columns[i].search.search = "";
                            break;
                    }
                }
            },
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                if (TABLE_TIMER) clearTimeout(TABLE_TIMER);
                TABLE_TIMER = setTimeout(function () {
                    blockElement(TABLE.selector);
                    $.getJSON( sSource, aoData, function (json) {
                        fnCallback(json);
                    });
                }, 500);
            },
            "fnDrawCallback": function (oSettings) {
                var n = oSettings._iRecordsTotal;
                TABLE.unblock();
                TABLE.find('[data-popup=tooltip]').tooltip();
            }
        });

        // DELETE
        TABLE.on('click', '.delete', function (e) {
            e.preventDefault();
            let data = {
                uid: $(this).data('uid')
            };
            confirmDialog({
                title: 'Hapus Data Tersebut?',
                text: '',
                btn_confirm: 'Hapus',
                url: URL.delete,
                data: data,
                onSuccess: (res) => {
                    successMessage('Success', 'Data Berhasil Dihapus.');
                    // window.location.reload(false);
                    TABLEDT.fnDraw(false);
                },
                onError: (error) => {
                    errorMessage('Error', 'Data Gagal Dihapus.');
                },
            });
        });

        TABLE.on('click', '.restore', function (e) {
            e.preventDefault();
            let data = {
                uid: $(this).data('uid')
            };
            confirmDialog({
                title: 'Restore Data Tersebut?',
                text: '',
                btn_confirm: 'Restore',
                url: URL.restore,
                data: data,
                onSuccess: (res) => {
                    successMessage('Success', 'Data Berhasil Direstore.');
                    // window.location.reload(false);
                    TABLEDT.fnDraw(false);
                },
                onError: (error) => {
                    errorMessage('Error', 'Data Gagal Direstore.');
                },
            });
        });

        BTN_REFRESH.on('click', function () {
            TABLEDT.fnDraw(false);
        });
    }

    initialize();
});