$(() => {
    let initialize = () => {
        let tbody = $('#table').find('tbody'),
            totalPendapatan = 0,
            totalbeban = 0;
        $.getJSON(URL.loadData, (res, status) => {
            tbody.empty();
            if (res.data) {
                recursiveParent(res.data.harta, 'Harta');
                recursiveParent(res.data.kewajiban, 'Kewajiban');
                recursiveParent(res.data.modal, 'Modal');
                recursiveParent(res.data.pendapatan, 'Pendapatan');
                recursiveParent(res.data.beban_atas_pendapatan, 'Beban Atas Pendapatan');
                recursiveParent(res.data.bebanoperasional, 'Beban Operasional');
                recursiveParent(res.data.bebannonoperasional, 'Beban Non Operasional');
                recursiveParent(res.data.pendapatanlainlain, 'Pendapatan Lain - Lain');
                recursiveParent(res.data.bebanlainlain, 'Beban Lain - Lain');
            }else{
                tbody.append(`
                            <tr>
                                <td colspan="2" class="text-bold text-center">Tidak Ada Data</td>
                            </tr>
                            `);  
            }
        });
    }

    function recursiveParent(resData, nameParent){
        let tbody = $('#table').find('tbody');
        if (resData.length > 0) {
            tbody.append(`
                    <tr>
                        <td class="text-center">-</td>
                        <td colspan="2" class="text-bold">${nameParent}</td>
                    </tr>
                    `);  
            recursiveData(resData, tbody, '', '0');
        }
    }
    function recursiveData(data, tbody, parent, kode_sub){
        for (let d of data) {
            if(parent){
                tbody.append(`
                    <tr>
                        <td>${kode_sub+' - '+d.kode}</td>
                        <td><strong><span class="gi">|—</span></strong><strong><span class="gi">|—</span></strong>${d.nama}</td>
                        <td class="text-left">
                            <a href="${URL.form.replace(':UID', d.uid)}" class="btn btn-primary btn-xs"><i class="icon-pencil5"></i></a>
                            <button type="button" class="btn btn-danger btn-xs delete" data-uid="${d.uid}"><i class="icon-trash"></i></button>
                        </td>
                    </tr>
                `);
            }else{
                if (d.sub_parent.length > 0) {
                    tbody.append(`
                        <tr>
                            <td>${d.kode}</td>
                            <td class="text-bold"><strong><span class="gi">|—</span></strong>${d.nama}</td>
                            <td class="text-left">
                                <a href="${URL.form.replace(':UID', d.uid)}" class="btn btn-primary btn-xs"><i class="icon-pencil5"></i></a>
                                <button type="button" class="btn btn-danger btn-xs delete hide" data-uid="${d.uid}"><i class="icon-trash"></i></button>
                            </td>
                        </tr>
                    `);
                }else{
                    tbody.append(`
                        <tr>
                            <td>${d.kode}</td>
                            <td class="text-bold"><strong><span class="gi">|—</span></strong>${d.nama}</td>
                            <td class="text-left">
                                <a href="${URL.form.replace(':UID', d.uid)}" class="btn btn-primary btn-xs"><i class="icon-pencil5"></i></a>
                                <button type="button" class="btn btn-danger btn-xs delete" data-uid="${d.uid}"><i class="icon-trash"></i></button>
                            </td>
                        </tr>
                    `);
                }
            }
            if(d.sub_parent){
                recursiveData(d.sub_parent, tbody, d.parent_id, d.kode);
            }
        }
    }

    // DELETE
    TABLE.on('click', '.delete', function (e) {
        e.preventDefault();
        let data = {
            uid: $(this).data('uid')
        };
        confirmDialog({
            title: 'Hapus Data Tersebut?',
            text: '',
            btn_confirm: 'Hapus',
            url: URL.delete,
            data: data,
            onSuccess: (res) => {
                successMessage('Success', 'Data Berhasil Dihapus.');
                initialize();
            },
            onError: (error) => {
                errorMessage('Error', 'Data Gagal Dihapus.');
            },
        });
    });

    BTN_REFRESH.on('click', function () {
        initialize();
    });

    initialize();
});