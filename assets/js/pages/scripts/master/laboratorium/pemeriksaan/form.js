$(function () {
    /**
     * HELPERS
     */
    let formatNilaiNormal = (data) => {
        var id = 'nilai_normal-' + makeid(10);
        data.elementID = id;
        var content = data.value;
        var label = data.nama;
        var jenisKelamin = '';
        var umur = '';
        switch (data.datatype) {
            default: // Undefined
                content = data.value;
                break;
            case 'range': // Range
                content = data.value;
                break;
            case 'boolean': // Boolean
                content = parseInt(data.value) === 1 ? 'Positif' : 'Negatif';;
                break;
            case 'text': // Text
                content = data.value;
                break;
            case 'lt': // LESSTHAN
                content = '< ' + data.value;
                break;
            case 'gt': // GREATERTHAN
                content = '> ' + data.value;
                break;
            case 'lte': // LESSTHANEQUAL
                content = '<= ' + data.value;
                break;
            case 'gte': // GREATERTHANEQUAL
                content = '>= ' + data.value;
                break;
        }

        if (parseInt(data.jenis_kelamin) == 1) { // Laki-Laki
            jenisKelamin = 'Laki-Laki';
        } else if (parseInt(data.jenis_kelamin) == 2) { // Perempuan
            jenisKelamin = 'Perempuan';
        }

        var umurContent = '';
        var umur_min_flag = parseInt(data.umur_min_flag);
        var umur_min_tahun = parseInt(data.umur_min_tahun) != 0 ? `${data.umur_min_tahun} Thn` : '';
        var umur_min_bulan = parseInt(data.umur_min_bulan) != 0 ? `${data.umur_min_bulan} Bln` : '';
        var umur_min_hari = parseInt(data.umur_min_hari) != 0 ? `${data.umur_min_hari} Hr` : '';

        var umur_max_flag = parseInt(data.umur_max_flag);
        var umur_max_tahun = parseInt(data.umur_max_tahun) ? `${data.umur_max_tahun} Thn` : '';
        var umur_max_bulan = parseInt(data.umur_max_bulan) ? `${data.umur_max_bulan} Bln` : '';
        var umur_max_hari = parseInt(data.umur_max_hari) ? `${data.umur_max_hari} Hr` : '';


        if (umur_min_flag == 1 && umur_max_flag == 0) {
            umurContent = `>= ${umur_min_tahun} ${umur_min_bulan} ${umur_min_hari}`;
        } else if (umur_min_flag == 0 && umur_max_flag == 1) {
            umurContent = `<= ${umur_max_tahun} ${umur_max_bulan} ${umur_max_hari}`;
        } else if (umur_min_flag == 1 && umur_max_flag == 1) {
            umurContent = `${umur_min_tahun} ${umur_min_bulan} ${umur_min_hari} - ${umur_max_tahun} ${umur_max_bulan} ${umur_max_hari}`;
        }
        umurContent.trim();


        var a = $("<a/>")
            .prop('id', id)
            .prop('href', '#')
            .addClass('text-left nilai_normal')
            .data('nilai_normal', JSON.stringify(data))
            .html((label ? label : '') + ' ' + (jenisKelamin ? jenisKelamin : '') + ' ' + (umurContent ? '(' + umurContent + ')' : '') + (label || umurContent ? ': ' : '') + content);

        if (parseInt(data.status) == 0) {
            a.addClass('text-danger');
        }

        return a;
    }
    let parseNilaiNormal = (postData) => {
        let obj = {};
        obj.id = postData.id;
        obj.uid = postData.uid;
        obj.item_id = postData.item_id;
        obj.nama = postData.nama;
        obj.datatype = postData.datatype;
        obj.jenis_kelamin = postData.jenis_kelamin;
        obj.value = postData.value;
        obj.umur_min_flag = numeral(postData.umur_min_flag || 0)._value;
        obj.umur_min_tahun = numeral(postData.umur_min_tahun || 0)._value;
        obj.umur_min_bulan = numeral(postData.umur_min_bulan || 0)._value;
        obj.umur_min_hari = numeral(postData.umur_min_hari || 0)._value;
        obj.umur_max_flag = numeral(postData.umur_max_flag || 0)._value;
        obj.umur_max_tahun = numeral(postData.umur_max_tahun || 0)._value;
        obj.umur_max_bulan = numeral(postData.umur_max_bulan || 0)._value;
        obj.umur_max_hari = numeral(postData.umur_max_hari || 0)._value;
        
        switch (obj.datatype) {
            case 'range':
                obj.value = `${postData.input_range_min} - ${postData.input_range_max}`;
                break;
            case 'boolean':
                obj.value = postData.input_boolean || 0;
                break;
            case 'text':
                obj.value = postData.input_text;
                break;
            case 'lt':
                obj.value = numeral(postData.input_lt)._value;
                break;
            case 'gt':
                obj.value = numeral(postData.input_gt)._value;
                break;
            case 'lte':
                obj.value = numeral(postData.input_lte)._value;
                break;
            case 'gte':
                obj.value = numeral(postData.input_gte)._value;
                break;
        }

        return obj;
    }
    let parseNilaiNormalFormData = (postData) => {
        let obj = {};
        for (let i = 0; i < postData.length; i++) {
            obj[postData[i].name] = postData[i].value;
        }
        
        nilai_normal = parseNilaiNormal(obj);

        return nilai_normal;
    }
    let updateNilaiNormalRow = (tr, nilai_normal) => {
        console.log(tr, nilai_normal);
        tr.find('[name="nn_id[]"]').val(nilai_normal.id);
        tr.find('[name="nn_uid[]"]').val(nilai_normal.uid);
        tr.find('[name="nn_item_id[]"]').val(nilai_normal.item_id);
        tr.find('[name="nn_datatype[]"]').val(nilai_normal.datatype);
        tr.find('[name="nn_nama[]"]').val(nilai_normal.nama);
        tr.find('[name="nn_jenis_kelamin[]"]').val(nilai_normal.jenis_kelamin);
        tr.find('[name="nn_umur_min_flag[]"]').val(nilai_normal.umur_min_flag);
        tr.find('[name="nn_umur_min_tahun[]"]').val(nilai_normal.umur_min_tahun);
        tr.find('[name="nn_umur_min_bulan[]"]').val(nilai_normal.umur_min_bulan);
        tr.find('[name="nn_umur_min_hari[]"]').val(nilai_normal.umur_min_hari);
        tr.find('[name="nn_umur_max_flag[]"]').val(nilai_normal.umur_max_flag);
        tr.find('[name="nn_umur_max_tahun[]"]').val(nilai_normal.umur_max_tahun);
        tr.find('[name="nn_umur_max_bulan[]"]').val(nilai_normal.umur_max_bulan);
        tr.find('[name="nn_umur_max_hari[]"]').val(nilai_normal.umur_max_hari);
        tr.find('[name="nn_value[]"]').val(nilai_normal.value);

        tr.find('td:eq(0)').html(formatNilaiNormal(nilai_normal));
    }
    let updatePemeriksaanRow = (tr, data) => {
        let tdKode = tr.find('td:eq(0)');
        let tdNama = tr.find('td:eq(1)');
        let tdSatuan = tr.find('td:eq(2)');
        let tdNilaiNormal = tr.find('td:eq(3)');

        tr.find('[name="item_id[]"]').val(data.id);
        tr.find('[name="item_pemeriksaan_id[]"]').val(data.pemeriksaan_id);
        tr.find('[name="item_kode[]"]').val(data.kode);
        tr.find('[name="item_nama[]"]').val(data.nama);
        tr.find('[name="item_satuan[]"]').val(data.satuan);
        tr.find('[name="item_status[]"]').val(data.status);
        tr.find('[name="item_nilai_normal[]"]').val(json_encode(data.nilai_normal));


        tdKode.html(data.kode);
        tdNama.html(data.nama);
        tdSatuan.html(data.satuan);
        tdNilaiNormal.html('');
        for (let i = 0; i < data.nilai_normal.length; i++) {
            tdNilaiNormal.append(formatNilaiNormal(data.nilai_normal[i]));
            tdNilaiNormal.append('<br/>');
        }
    }

    let disableForm = () => {
        FORM.find('button').prop('disabled', true);
        BTN_CANCEL.prop('disabled', false);
        BTN_RESTORE.prop('disabled', false);
        BTN_DELETE.prop('disabled', false);
    }
    let enableForm = () => {
        FORM.find('button').prop('disabled', false);
        BTN_CANCEL.prop('disabled', false);
        BTN_RESTORE.prop('disabled', false);
        BTN_DELETE.prop('disabled', false);
    }

    let fetchLookupObat = (cb) => {
        $.getJSON(URL.fetchLookupObat, function (res, status) {
            if (status === 'success') {
                TABLE_LOOKUP_OBAT_DT.clear().draw();
                TABLE_LOOKUP_OBAT_DT.rows.add(res.data).draw();

                if (cb) {
                    cb();
                }
            } else {
                //
            }
        });
    }

    let fetchParent = (parent_id) => {
        $.getJSON(URL.fetchParent, (res, status) => {
            if (status === 'success') {
                let data = res.data;

                FORM.find('[name=parent_id]').empty();
                let temp;
                for (let i = 0; i < data.length; i++) {
                    temp = $("<option/>")
                        .prop('value', data[i].id)
                        .html('&mdash;'.repeat(data[i].lvl) + ' ' + (data[i].kode ? `${data[i].kode} - ` : '') + data[i].nama)
                        .appendTo(FORM.find('[name=parent_id]'));
                }

                FORM.find('[name=parent_id]').val(parent_id).trigger('change');

                if (data.length <= 0) {
                    $("<option/>")
                        .prop('value', 0)
                        .html('Root')
                        .appendTo(FORM.find('[name=parent_id]'));
                    FORM.find('[name=parent_id]').val(1).trigger('change');
                }
            }
        });
    }

    let fillForm = (uid) => {
        $.getJSON(URL.getData.replace(':UID', uid), (res, status) => {
            if (status === 'success') {
                let data = res.data;

                $("#id").val(data.id);
                $("#uid").val(data.uid);
                $("#kode").val(data.kode);
                $("#nama").val(data.nama);
                $(`[name=jenis][value=${data.jenis}]`).prop('checked', true).trigger('change');
                $("#old_parent_id").val(data.parent_id);
                $("#parent_id").val(data.parent_id);
                $("#deskripsi").val(data.deskripsi);
                $("#disp_deskripsi").summernote('code', data.deskripsi);
                fetchParent(data.parent_id);

                // Items
                TABLE_PEMERIKSAAN.find('tbody').empty();
                for (let item of data.items) {
                    console.log('PEMERIKSAAN', item);
                    addPemeriksaan(item);
                }

                // Obat
                TABLE_OBAT.find('tbody').empty();
                for (let obat of data.obats) {
                    console.log('OBAT', obat);
                    addObat(obat);
                }

                if (data.deleted_flag == 1) {
                    BTN_DELETE.hide();
                    BTN_RESTORE.show();
                    LABEL_DELETED.show();
                    disableForm();
                } else {
                    BTN_RESTORE.hide();
                    BTN_DELETE.show();
                    LABEL_DELETED.hide();
                    enableForm();
                }
            }
        });
    }

    let fillFormPemeriksaan = (tr, data) => {
        tr.data('active', 1);
        console.log('fillFormPemeriksaan', tr, data);
        FORM_PEMERIKSAAN_ITEM.find('[name=id]').val(data.id);
        FORM_PEMERIKSAAN_ITEM.find('[name=pemeriksaan_id]').val(data.pemeriksaan_id);
        FORM_PEMERIKSAAN_ITEM.find('[name=tr_selector]').val(tr.selector);
        FORM_PEMERIKSAAN_ITEM.find('[name=kode]').val(data.kode);
        FORM_PEMERIKSAAN_ITEM.find('[name=nama]').val(data.nama);
        FORM_PEMERIKSAAN_ITEM.find('[name=satuan]').val(data.satuan);
        FORM_PEMERIKSAAN_ITEM.find('[name=status]').val(data.status);

        // Nilai Normal
        TABLE_ITEM_NILAI_NORMAL.find('tbody').empty();
        for (let i = 0; i < data.nilai_normal.length; i++) {
            addNilaiNormal(data.nilai_normal[i]);
        }

        console.log(tr.data());

        MODAL_PEMERIKSAAN_ITEM.modal('show');
    }

    let fillFormNilaiNormal = (tr, data) => {
        TABLE_ITEM_NILAI_NORMAL.find('tbody > tr').data('active', 0);
        tr.data('active', 1);
        // FORM_NILAI_NORMAL
        FORM_NILAI_NORMAL.find('[name=id]').val(data.id);
        FORM_NILAI_NORMAL.find('[name=uid]').val(data.uid);
        FORM_NILAI_NORMAL.find('[name=item_id]').val(data.item_id);
        FORM_NILAI_NORMAL.find('[name=datatype]').val(data.datatype).trigger('change');
        FORM_NILAI_NORMAL.find('[name=nama]').val(data.nama);
        FORM_NILAI_NORMAL.find('[name=jenis_kelamin]').val(data.jenis_kelamin);
        FORM_NILAI_NORMAL.find(`[name=umur_min_flag][value=${data.umur_min_flag || 0}]`).prop('checked', true).trigger('change');
        FORM_NILAI_NORMAL.find('[name=umur_min_tahun]').val(data.umur_min_tahun);
        FORM_NILAI_NORMAL.find('[name=umur_min_bulan]').val(data.umur_min_bulan);
        FORM_NILAI_NORMAL.find('[name=umur_min_hari]').val(data.umur_min_hari);
        FORM_NILAI_NORMAL.find(`[name=umur_max_flag][value=${data.umur_max_flag || 0}]`).prop('checked', true).trigger('change');
        FORM_NILAI_NORMAL.find('[name=umur_max_tahun]').val(data.umur_max_tahun);
        FORM_NILAI_NORMAL.find('[name=umur_max_bulan]').val(data.umur_max_bulan);
        FORM_NILAI_NORMAL.find('[name=umur_max_hari]').val(data.umur_max_hari);
        FORM_NILAI_NORMAL.find('[name=value]').val(data.value);

        switch (data.datatype) {
            case 'range':
                let values = data.value.split('-');
                let value_min = numeral(values[0] || 0)._value;
                let value_max = numeral(values[1] || 0)._value;
                FORM_NILAI_NORMAL.find('[name=input_range_min]').autoNumeric('set', value_min);
                FORM_NILAI_NORMAL.find('[name=input_range_max]').autoNumeric('set', value_max);
                break;
            case 'boolean':
                let value = data.value || 0;
                FORM_NILAI_NORMAL.find(`[name=input_boolean][value=${value}]`).prop('checked', true).trigger('change');
                break;
            case 'text':
                FORM_NILAI_NORMAL.find('[name=input_text]').val(data.value);
                break;
            case 'lt':
                FORM_NILAI_NORMAL.find('[name=input_lt]').autoNumeric('set', data.value);
                break;
            case 'gt':
                FORM_NILAI_NORMAL.find('[name=input_gt]').autoNumeric('set', data.value);
                break;
            case 'lte':
                FORM_NILAI_NORMAL.find('[name=input_lte]').autoNumeric('set', data.value);
                break;
            case 'gte':
                FORM_NILAI_NORMAL.find('[name=input_gte]').autoNumeric('set', data.value);
                break;
        }

        // console.log(tr.data());
        // console.log('fillFormNilaiNormal', tr, data);

        MODAL_NILAI_NORMAL.modal('show');
    }

    let addPemeriksaan = (data, mode) => {
        let tbody = TABLE_PEMERIKSAAN.find('tbody');

        let tr = $("<tr/>")
            .appendTo(tbody);
        let input_id = $('<input/>')
            .prop('type', 'hidden')
            .prop('name', 'item_id[]')
            .val(data.id)
            .appendTo(tr);
        let input_pemeriksaan_id = $('<input/>')
            .prop('type', 'hidden')
            .prop('name', 'item_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(tr);
        let input_kode = $('<input/>')
            .prop('type', 'hidden')
            .prop('name', 'item_kode[]')
            .val(data.kode)
            .appendTo(tr);
        let input_nama = $('<input/>')
            .prop('type', 'hidden')
            .prop('name', 'item_nama[]')
            .val(data.nama)
            .appendTo(tr);
        let input_satuan = $('<input/>')
            .prop('type', 'hidden')
            .prop('name', 'item_satuan[]')
            .val(data.satuan)
            .appendTo(tr);
        let input_status = $('<input/>')
            .prop('type', 'hidden')
            .prop('name', 'item_status[]')
            .val(data.status)
            .appendTo(tr);
        let input_nilai_normal = $('<textarea/>')
            .addClass('hide')
            .prop('name', 'item_nilai_normal[]')
            .val(json_encode(data.nilai_normal))
            .appendTo(tr);

        let tdKode = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        tdKode.html(data.kode);

        let tdNama = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        tdNama.html(data.nama);

        let tdSatuan = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        tdSatuan.html(data.satuan);

        let tdNilaiNormal = $("<td/>")
            .addClass('text-left')
            .appendTo(tr);
        tdNilaiNormal.html('');
        for (let i = 0; i < data.nilai_normal.length; i++) {
            tdNilaiNormal.append(formatNilaiNormal(data.nilai_normal[i]));
            tdNilaiNormal.append('<br/>');
        }

        let tdStatus = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnStatus = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-sm text-center')
            .appendTo(tdStatus);
        if (parseInt(data.status) == 1) {
            btnStatus
                .removeClass('bg-slate')
                .addClass('bg-success')
                .html(LABEL_STATUS_ACTIVE);
        } else {
            btnStatus
                .removeClass('bg-success')
                .addClass('bg-slate')
                .html(LABEL_STATUS_INACTIVE);
        }

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnEdit = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-primary btn-xs')
            .html('<i class="fa fa-pencil"></i>')
            .appendTo(tdAction);
        let btnDel = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-danger btn-xs btn-b')
            .html('<i class="fa fa-trash-o"></i>')
            .appendTo(tdAction);

        // Handler
        btnDel.on('click', (e) => {
            tr.remove();
        });

        btnEdit.on('click', (e) => {
            fillFormPemeriksaan(tr, {
                id: input_id.val(),
                pemeriksaan_id: input_pemeriksaan_id.val(),
                kode: input_kode.val(),
                nama: input_nama.val(),
                satuan: input_satuan.val(),
                status: input_status.val(),
                nilai_normal: json_decode(input_nilai_normal.val()),
            });
        });

        btnStatus.on('click', (e) => {
            if (parseInt(input_status.val()) == 1) {
                input_status.val(0);
                btnStatus
                    .removeClass('bg-success')
                    .addClass('bg-slate')
                    .html(LABEL_STATUS_INACTIVE);
            } else {
                input_status.val(1);
                btnStatus
                    .removeClass('bg-slate')
                    .addClass('bg-success')
                    .html(LABEL_STATUS_ACTIVE);
            }
            return false;
        });

        return tr;
    }

    let addNilaiNormal = (data, mode) => {
        let tbody = TABLE_ITEM_NILAI_NORMAL.find('tbody');

        let tr = $("<tr/>")
            .data('active', 0)
            .appendTo(tbody);
        let input_id = $('<input/>').prop('type', 'hidden').prop('name', 'nn_id[]')
            .val(data.id)
            .appendTo(tr);
        let input_uid = $('<input/>').prop('type', 'hidden').prop('name', 'nn_uid[]')
            .val(data.uid)
            .appendTo(tr);
        let input_item_id = $('<input/>').prop('type', 'hidden').prop('name', 'nn_item_id[]')
            .val(data.item_id)
            .appendTo(tr);
        let input_datatype = $('<input/>').prop('type', 'hidden').prop('name', 'nn_datatype[]')
            .val(data.datatype)
            .appendTo(tr);
        let input_nama = $('<input/>').prop('type', 'hidden').prop('name', 'nn_nama[]')
            .val(data.nama)
            .appendTo(tr);
        let input_jenis_kelamin = $('<input/>').prop('type', 'hidden').prop('name', 'nn_jenis_kelamin[]')
            .val(data.jenis_kelamin)
            .appendTo(tr);
        let input_umur_min_flag = $('<input/>').prop('type', 'hidden').prop('name', 'nn_umur_min_flag[]')
            .val(data.umur_min_flag)
            .appendTo(tr);
        let input_umur_min_tahun = $('<input/>').prop('type', 'hidden').prop('name', 'nn_umur_min_tahun[]')
            .val(data.umur_min_tahun)
            .appendTo(tr);
        let input_umur_min_bulan = $('<input/>').prop('type', 'hidden').prop('name', 'nn_umur_min_bulan[]')
            .val(data.umur_min_bulan)
            .appendTo(tr);
        let input_umur_min_hari = $('<input/>').prop('type', 'hidden').prop('name', 'nn_umur_min_hari[]')
            .val(data.umur_min_hari)
            .appendTo(tr);
        let input_umur_max_flag = $('<input/>').prop('type', 'hidden').prop('name', 'nn_umur_max_flag[]')
            .val(data.umur_max_flag)
            .appendTo(tr);
        let input_umur_max_tahun = $('<input/>').prop('type', 'hidden').prop('name', 'nn_umur_max_tahun[]')
            .val(data.umur_max_tahun)
            .appendTo(tr);
        let input_umur_max_bulan = $('<input/>').prop('type', 'hidden').prop('name', 'nn_umur_max_bulan[]')
            .val(data.umur_max_bulan)
            .appendTo(tr);
        let input_umur_max_hari = $('<input/>').prop('type', 'hidden').prop('name', 'nn_umur_max_hari[]')
            .val(data.umur_max_hari)
            .appendTo(tr);
        let input_value = $('<input/>').prop('type', 'hidden').prop('name', 'nn_value[]')
            .val(data.value)
            .appendTo(tr);
        
        let tdKriteria = $("<td/>")
            .appendTo(tr);
        tdKriteria.html(formatNilaiNormal(data));

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnEdit = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-primary btn-xs')
            .html('<i class="fa fa-pencil"></i>')
            .appendTo(tdAction);
        let btnDel = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-danger btn-xs btn-b')
            .html('<i class="fa fa-trash-o"></i>')
            .appendTo(tdAction);

        // Handlers
        btnDel.on('click', (e) => {
            tr.remove();
        });

        btnEdit.on('click', (e) => {
            fillFormNilaiNormal(tr, {
                id: input_id.val(),
                uid: input_uid.val(),
                item_id: input_item_id.val(),
                datatype: input_datatype.val(),
                nama: input_nama.val(),
                jenis_kelamin: input_jenis_kelamin.val(),
                umur_min_flag: input_umur_min_flag.val(),
                umur_min_tahun: input_umur_min_tahun.val(),
                umur_min_bulan: input_umur_min_bulan.val(),
                umur_min_hari: input_umur_min_hari.val(),
                umur_max_flag: input_umur_max_flag.val(),
                umur_max_tahun: input_umur_max_tahun.val(),
                umur_max_bulan: input_umur_max_bulan.val(),
                umur_max_hari: input_umur_max_hari.val(),
                value: input_value.val(),
            });
        });


        return tr;
    }

    let addObat = (data, mode) => {
        let tbody = TABLE_OBAT.find('tbody');

        let tr = $("<tr/>")
            .data('uid', makeid(10))
            .data('obat_id', data.obat_id)
            .appendTo(tbody);
        let input_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_id[]')
            .val(data.id)
            .appendTo(tr);
        let input_pemeriksaan_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_pemeriksaan_id[]')
            .val(data.pemeriksaan_id)
            .appendTo(tr);
        let input_obat_id = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_obat_id[]')
            .val(data.obat_id)
            .appendTo(tr);
        let input_quantity = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_quantity[]')
            .val(data.quantity)
            .appendTo(tr);
        let input_status = $("<input/>")
            .prop('type', 'hidden')
            .prop('name', 'obat_status[]')
            .val(data.status)
            .appendTo(tr);

        let tdKode = $("<td/>")
            .appendTo(tr);
        tdKode.html(data.obat_kode);

        let tdNama = $("<td/>")
            .appendTo(tr);
        tdNama.html(data.obat);

        let tdQuantity = $("<td/>")
            .addClass('text-right')
            .appendTo(tr);
        let labelQuantity = $("<span/>")
            .addClass('text-right')
            .appendTo(tdQuantity)
            .html(numeral(data.quantity).format(',.##'));
        let dispInputQuantity = $("<input/>")
            .prop('type', 'text')
            .addClass('form-control text-right disp_input_quantity')
            .appendTo(tdQuantity);
        dispInputQuantity.autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': 'p'});
        dispInputQuantity.autoNumeric('set', data.quantity);

        let tdStatus = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnStatus = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-sm text-center')
            .appendTo(tdStatus);
        if (parseInt(data.status) == 1) {
            btnStatus
                .removeClass('bg-slate')
                .addClass('bg-success')
                .html(LABEL_STATUS_ACTIVE);
        } else {
            btnStatus
                .removeClass('bg-success')
                .addClass('bg-slate')
                .html(LABEL_STATUS_INACTIVE);
        }

        let tdAction = $("<td/>")
            .addClass('text-center')
            .appendTo(tr);
        let btnDone = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-success btn-xs')
            .html('<i class="fa fa-check"></i>')
            .appendTo(tdAction);
        btnDone.hide();
        let btnDel = $("<button/>")
            .prop('type', 'button')
            .addClass('btn btn-danger btn-xs btn-b')
            .html('<i class="fa fa-trash-o"></i>')
            .appendTo(tdAction);

        // Handler
        btnDel.on('click', (e) => {
            tr.remove();
        });

        tr.on('click', (e) => {
            if (tr.data('done') == 1) {
                tr.data('done', 0);
                return;
            }
            tbody.find('tr').each((i, el) => {
                if ($(el).data('uid') != tr.data('uid')) {
                    $(el).trigger('input_close');
                }
            });

            btnDone.show();
            btnDel.hide();

            labelQuantity.hide()
            dispInputQuantity.show();

            dispInputQuantity.focus();
        }).on('input_close', () => {
            btnDone.hide();
            btnDel.show();

            labelQuantity.show();
            dispInputQuantity.hide();
        });

        btnDone.on('click', (e) => {
            tr.data('done', 1);
            btnDone.hide();
            btnDel.show();

            labelQuantity.show();
            dispInputQuantity.hide();
        });

        dispInputQuantity.on('keyup change blur', (e) => {
            input_quantity.val(dispInputQuantity.autoNumeric('get'));
            labelQuantity.html(dispInputQuantity.autoNumeric('get'));

            if (dispInputQuantity.autoNumeric('get') == "") {
                dispInputQuantity.autoNumeric('set', 0);
                dispInputQuantity.trigger('change');
            }
        });

        btnStatus.on('click', (e) => {
            if (parseInt(input_status.val()) == 1) {
                input_status.val(0);
                btnStatus
                    .removeClass('bg-success')
                    .addClass('bg-slate')
                    .html(LABEL_STATUS_INACTIVE);
            } else {
                input_status.val(1);
                btnStatus
                    .removeClass('bg-slate')
                    .addClass('bg-success')
                    .html(LABEL_STATUS_ACTIVE);
            }
            return false;
        });

        dispInputQuantity.trigger('change');

        if (mode == 'view') {
            btnDone.hide();
            btnDel.show();

            labelQuantity.show();
            dispInputQuantity.hide();
        } else {
            btnDone.show();
            btnDel.hide();

            labelQuantity.hide();
            dispInputQuantity.show();

            dispInputQuantity.focus();
        }
    }

    let initialize = (uid) => {
        // INITIALIZE DATATABLE
        TABLE_LOOKUP_OBAT_DT = TABLE_LOOKUP_OBAT.DataTable({
            data: [],
            columns: COLUMNS_OPT_LOOKUP_OBAT,
            fnDrawCallback: function (oSettings) {
                TABLE_LOOKUP_OBAT.find('tbody tr input[type=checkbox]').uniform();
            }
        });

        FORM.find('[name=jenis]').on('change', function () {
            let val = $(this).val();
            if (val == 'kelompok') {
                $("#section-pemeriksaan").hide('slow');
                $("#section-obat").hide('slow');
            } else {
                $("#section-pemeriksaan").show('slow');
                $("#section-obat").show('slow');
            }
        });

        BTN_TAMBAH_PEMERIKSAAN.on('click', (e) => {
            let tr = addPemeriksaan({status: 1, nilai_normal: []});
            fillFormPemeriksaan(tr, {
                id: 0,
                pemeriksaan_id: 0,
                kode: null,
                nama: null,
                satuan: null,
                status: 1,
                nilai_normal: [],
            });
        });

        BTN_TAMBAH_NILAI_NORMAL.on('click', (e) => {
            let tr = addNilaiNormal({});
            fillFormNilaiNormal(tr, {});
        });

        /**
         * NILAI NORMAL
         */
        (() => {
            FORM_NILAI_NORMAL.find('[name=datatype]').on('change', function (e) {
                let val = $(this).val();
                switch (val) {
                    case 'range':
                        FORM_NILAI_NORMAL.find(".group-range").show();
                        FORM_NILAI_NORMAL.find(".group-boolean").hide();
                        FORM_NILAI_NORMAL.find(".group-text").hide();
                        FORM_NILAI_NORMAL.find(".group-lt").hide();
                        FORM_NILAI_NORMAL.find(".group-gt").hide();
                        FORM_NILAI_NORMAL.find(".group-lte").hide();
                        FORM_NILAI_NORMAL.find(".group-gte").hide();
                        break;
                    case 'boolean':
                        FORM_NILAI_NORMAL.find(".group-range").hide();
                        FORM_NILAI_NORMAL.find(".group-boolean").show();
                        FORM_NILAI_NORMAL.find(".group-text").hide();
                        FORM_NILAI_NORMAL.find(".group-lt").hide();
                        FORM_NILAI_NORMAL.find(".group-gt").hide();
                        FORM_NILAI_NORMAL.find(".group-lte").hide();
                        FORM_NILAI_NORMAL.find(".group-gte").hide();
                        break;
                    case 'text':
                        FORM_NILAI_NORMAL.find(".group-range").hide();
                        FORM_NILAI_NORMAL.find(".group-boolean").hide();
                        FORM_NILAI_NORMAL.find(".group-text").show();
                        FORM_NILAI_NORMAL.find(".group-lt").hide();
                        FORM_NILAI_NORMAL.find(".group-gt").hide();
                        FORM_NILAI_NORMAL.find(".group-lte").hide();
                        FORM_NILAI_NORMAL.find(".group-gte").hide();
                        break;
                    case 'lt':
                        FORM_NILAI_NORMAL.find(".group-range").hide();
                        FORM_NILAI_NORMAL.find(".group-boolean").hide();
                        FORM_NILAI_NORMAL.find(".group-text").hide();
                        FORM_NILAI_NORMAL.find(".group-lt").show();
                        FORM_NILAI_NORMAL.find(".group-gt").hide();
                        FORM_NILAI_NORMAL.find(".group-lte").hide();
                        FORM_NILAI_NORMAL.find(".group-gte").hide();
                        break;
                    case 'gt':
                        FORM_NILAI_NORMAL.find(".group-range").hide();
                        FORM_NILAI_NORMAL.find(".group-boolean").hide();
                        FORM_NILAI_NORMAL.find(".group-text").hide();
                        FORM_NILAI_NORMAL.find(".group-lt").hide();
                        FORM_NILAI_NORMAL.find(".group-gt").show();
                        FORM_NILAI_NORMAL.find(".group-lte").hide();
                        FORM_NILAI_NORMAL.find(".group-gte").hide();
                        break;
                    case 'lte':
                        FORM_NILAI_NORMAL.find(".group-range").hide();
                        FORM_NILAI_NORMAL.find(".group-boolean").hide();
                        FORM_NILAI_NORMAL.find(".group-text").hide();
                        FORM_NILAI_NORMAL.find(".group-lt").hide();
                        FORM_NILAI_NORMAL.find(".group-gt").hide();
                        FORM_NILAI_NORMAL.find(".group-lte").show();
                        FORM_NILAI_NORMAL.find(".group-gte").hide();
                        break;
                    case 'gte':
                        FORM_NILAI_NORMAL.find(".group-range").hide();
                        FORM_NILAI_NORMAL.find(".group-boolean").hide();
                        FORM_NILAI_NORMAL.find(".group-text").hide();
                        FORM_NILAI_NORMAL.find(".group-lt").hide();
                        FORM_NILAI_NORMAL.find(".group-gt").hide();
                        FORM_NILAI_NORMAL.find(".group-lte").hide();
                        FORM_NILAI_NORMAL.find(".group-gte").show();
                        break;
                    default: 
                        FORM_NILAI_NORMAL.find(".group-range").hide();
                        FORM_NILAI_NORMAL.find(".group-boolean").hide();
                        FORM_NILAI_NORMAL.find(".group-text").hide();
                        FORM_NILAI_NORMAL.find(".group-lt").hide();
                        FORM_NILAI_NORMAL.find(".group-gt").hide();
                        FORM_NILAI_NORMAL.find(".group-lte").hide();
                        FORM_NILAI_NORMAL.find(".group-gte").hide();
                        break;
                }
            });
    
            FORM_NILAI_NORMAL.find('[name=input_range_min]')
                .autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': ''});
            FORM_NILAI_NORMAL.find('[name=input_range_max]')
                .autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': '', 'pSign': ''});
            FORM_NILAI_NORMAL.find('[name=input_lt]')
                .autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': 'X < ', 'pSign': 'p'});
            FORM_NILAI_NORMAL.find('[name=input_gt]')
                .autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': 'X > ', 'pSign': 'p'});
            FORM_NILAI_NORMAL.find('[name=input_lte]')
                .autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': 'X <= ', 'pSign': 'p'});
            FORM_NILAI_NORMAL.find('[name=input_gte]')
                .autoNumeric('init', {'mDec': 2, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': 'X >= ', 'pSign': 'p'});

            /**
             * UMUR
             */
            FORM_NILAI_NORMAL.find('[name=umur_min_flag]').on('change', function (e) {
                switch (parseInt($(this).val())) {
                    case 0:
                        FORM_NILAI_NORMAL.find('[name=umur_min_tahun]').prop('readonly', true);
                        FORM_NILAI_NORMAL.find('[name=umur_min_bulan]').prop('readonly', true);
                        FORM_NILAI_NORMAL.find('[name=umur_min_hari]').prop('readonly', true);
                        break;
                    case 1:
                        FORM_NILAI_NORMAL.find('[name=umur_min_tahun]').prop('readonly', false);
                        FORM_NILAI_NORMAL.find('[name=umur_min_bulan]').prop('readonly', false);
                        FORM_NILAI_NORMAL.find('[name=umur_min_hari]').prop('readonly', false);
                        break;
                }
            });
            FORM_NILAI_NORMAL.find('[name=umur_max_flag]').on('change', function (e) {
                console.log(parseInt($(this).val()), $(this).val());
                switch (parseInt($(this).val())) {
                    case 0:
                        FORM_NILAI_NORMAL.find('[name=umur_max_tahun]').prop('readonly', true);
                        FORM_NILAI_NORMAL.find('[name=umur_max_bulan]').prop('readonly', true);
                        FORM_NILAI_NORMAL.find('[name=umur_max_hari]').prop('readonly', true);
                        break;
                    case 1:
                        FORM_NILAI_NORMAL.find('[name=umur_max_tahun]').prop('readonly', false);
                        FORM_NILAI_NORMAL.find('[name=umur_max_bulan]').prop('readonly', false);
                        FORM_NILAI_NORMAL.find('[name=umur_max_hari]').prop('readonly', false);
                        break;
                }
            });

            FORM_NILAI_NORMAL.find('[name=umur_min_tahun]')
                .autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' Tahun', 'pSign': 's'});
            FORM_NILAI_NORMAL.find('[name=umur_min_bulan]')
                .autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' Bulan', 'pSign': 's'});
            FORM_NILAI_NORMAL.find('[name=umur_min_hari]')
                .autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' Hari', 'pSign': 's'});

            FORM_NILAI_NORMAL.find('[name=umur_max_tahun]')
                .autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' Tahun', 'pSign': 's'});
            FORM_NILAI_NORMAL.find('[name=umur_max_bulan]')
                .autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' Bulan', 'pSign': 's'});
            FORM_NILAI_NORMAL.find('[name=umur_max_hari]')
                .autoNumeric('init', {'mDec': 0, 'aSep': '.', 'aDec': ',', 'vMin': 0, 'aSign': ' Hari', 'pSign': 's'});

            FORM_NILAI_NORMAL.find('[name=datatype]').trigger('change');
            FORM_NILAI_NORMAL.find('[name=umur_min_flag][value=0]').prop('checked', true);
            FORM_NILAI_NORMAL.find('[name=umur_min_flag]').trigger('change');
            FORM_NILAI_NORMAL.find('[name=umur_max_flag][value=0]').prop('checked', true);
            FORM_NILAI_NORMAL.find('[name=umur_max_flag]').trigger('change');

            FORM_NILAI_NORMAL.find('input, select, textarea').on('change', () => {
                let nilai_normal = parseNilaiNormalFormData(FORM_NILAI_NORMAL.serializeArray());
                let nilai_normal_str = formatNilaiNormal(nilai_normal);
                // console.log(nilai_normal, nilai_normal_str);
                FORM_NILAI_NORMAL.find('.disp_nilai_normal').html(nilai_normal_str);
            });
        })();

        BTN_TAMBAH_OBAT.on('click', (e) => {
            MODAL_LOOKUP_OBAT.modal('show');
            blockElement(MODAL_LOOKUP_OBAT.find('.modal-dialog').selector);
            setTimeout(() => {
                TABLE_LOOKUP_OBAT_DT.rows().nodes().each((i, rowIdx) => {
                    let tr = $(TABLE_LOOKUP_OBAT_DT.row(rowIdx).node());
                    tr.find('input[type=checkbox]').prop('checked', false);
                    // tr.find('input[type=checkbox]').uniform();
                });
                TABLE_LOOKUP_OBAT_DT.draw(false);
                MODAL_LOOKUP_OBAT.find('.modal-dialog').unblock();
            }, 500);
        });

        BTN_TAMBAH_LOOKUP_OBAT_SIMPAN.on('click', (e) => {
            blockElement(MODAL_LOOKUP_OBAT.find('.modal-dialog').selector);
            setTimeout(() => {
                TABLE_LOOKUP_OBAT_DT.rows().nodes().each((i, rowIdx) => {
                    let tr = $(TABLE_LOOKUP_OBAT_DT.row(rowIdx).node());
                    let obat = {
                        id: 0,
                        pemeriksaan_id: 0,
                        obat_id: tr.find('input[type=checkbox]').data('id'),
                        obat_kode: tr.find('input[type=checkbox]').data('kode'),
                        obat: tr.find('input[type=checkbox]').data('nama'),
                        quantity: 1,
                        status: 1,
                    };

                    if (tr.find('input[type=checkbox]').is(':checked')) {
                        let existsTr = false;
                        TABLE_OBAT.find('tbody').find('tr').each((i, el) => {
                            console.log($(el), $(el).data('obat_id'));
                            if ($(el).data('obat_id') == obat.obat_id) {
                                existsTr = $(el);
                            }
                        });

                        if (existsTr) {
                            let dispInputQuantity = existsTr.find('.disp_input_quantity');
                            let quantity = numeral(dispInputQuantity.autoNumeric('get'))._value;
                            dispInputQuantity.autoNumeric('set', quantity+1);
                            dispInputQuantity.trigger('change');

                            let bg = existsTr.find('td').css('background-color');
                            let highlightBg = 'rgba(96,125,139,0.5)';
                            existsTr.find('td')
                                .animate({backgroundColor: highlightBg}, 400)
                                .delay(400)
                                .animate({backgroundColor: bg}, 400);
                        } else {
                            addObat(obat);
                        }
                    }
                });

                MODAL_LOOKUP_OBAT.find('.modal-dialog').unblock();
                MODAL_LOOKUP_OBAT.modal('hide');
            }, 500);
        });

        /**
         * FORM VALIDATION
         */
        (() => {
            /**
             * FORM
             */
            FORM.validate({
                rules: {
                    kode: { required: true },
                    nama: { required: true },
                    jenis: { required: true },
                    parent_id: { required: true },
                },
                messages: {
                    kode: "Kode Diperlukan.",
                    nama: "Nama Diperlukan.",
                    jenis: "Jenis Diperlukan.",
                    parent_id: "Parent Diperlukan.",
                },
                focusInvalid: true,
                errorPlacement: function(error, element) {
                    var inputGroup = $(element).closest('.input-group');
                    var checkbox = $(element).closest('.checkbox-inline');

                    if (inputGroup.length) {
                        error.insertAfter(inputGroup);
                    } else if (checkbox.length) {
                        checkbox.append(error);
                    } else {
                        $(element).closest("div").append(error);
                    }
                },
                submitHandler: function (form) {
                    blockPage();
                    $("#deskripsi").val($("#disp_deskripsi").summernote('code'));
                    var postData = $(form).serializeArray();
                    var formData = new FormData($(form)[0]);

                    for (var i = 0; i < postData.length; i++) {
                        if (postData[i].name != 'foto' && postData[i].name.search(/\[\]/) === -1) {
                            formData.delete(postData[i].name);
                            formData.append(postData[i].name, postData[i].value);
                        }
                    }

                    $.ajax({
                        url: URL.save,
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function (result) {
                            result = JSON.parse(result);
                            var data = result.data;
                            console.log(data);
                            successMessage('Success', "Pemeriksaan berhasil disimpan.");

                            $("#btn-simpan").hide();

                            setTimeout(() => {
                                window.location.assign(URL.index);
                            }, 3000);
                        },
                        error: function () {
                            $.unblockUI();
                            errorMessage('Error', "Terjadi kesalahan saat hendak menyimpan pemeriksaan.");
                        },
                        complete: function () {
                            $.unblockUI();
                        }
                    });
                }
            });

            /**
             * FORM PEMERIKSAAN ITEM
             */
            FORM_PEMERIKSAAN_ITEM.validate({
                rules: {
                    kode: { required: true },
                    nama: { required: true },
                    satuan: { required: false },
                },
                messages: {
                    kode: "Kode Diperlukan.",
                    nama: "Nama Diperlukan.",
                    satuan: "Satuan Diperlukan.",
                },
                focusInvalid: true,
                errorPlacement: function(error, element) {
                    var inputGroup = $(element).closest('.input-group');
                    var checkbox = $(element).closest('.checkbox-inline');

                    if (inputGroup.length) {
                        error.insertAfter(inputGroup);
                    } else if (checkbox.length) {
                        checkbox.append(error);
                    } else {
                        $(element).closest("div").append(error);
                    }
                },
                submitHandler: function (form) {
                    let postData = $(form).serializeArray();
                    let obj = {};
                    let activeTr = null;

                    let fieldname;
                    for (var i = 0; i < postData.length; i++) {
                        fieldname = postData[i].name;
                        if (fieldname.search(/\[\]/) === -1) {
                            obj[fieldname] = postData[i].value;
                        } else {
                            if (! obj[fieldname]) {
                                obj[fieldname] = [];
                            }

                            obj[fieldname].push(postData[i].value);
                        }
                    }

                    obj.nilai_normal = [];
                    for (let i = 0; i < obj['nn_id[]'].length; i++) {
                        obj.nilai_normal.push({
                            id: obj['nn_id[]'][i],
                            datatype: obj['nn_datatype[]'][i],
                            item_id: obj['nn_item_id[]'][i],
                            jenis_kelamin: obj['nn_jenis_kelamin[]'][i],
                            nama: obj['nn_nama[]'][i],
                            uid: obj['nn_uid[]'][i],
                            umur_max_bulan: obj['nn_umur_max_bulan[]'][i],
                            umur_max_flag: obj['nn_umur_max_flag[]'][i],
                            umur_max_hari: obj['nn_umur_max_hari[]'][i],
                            umur_max_tahun: obj['nn_umur_max_tahun[]'][i],
                            umur_min_bulan: obj['nn_umur_min_bulan[]'][i],
                            umur_min_flag: obj['nn_umur_min_flag[]'][i],
                            umur_min_hari: obj['nn_umur_min_hari[]'][i],
                            umur_min_tahun: obj['nn_umur_min_tahun[]'][i],
                            value: obj['nn_value[]'][i],
                        })
                    }

                    TABLE_PEMERIKSAAN.find('tbody').find('tr').each((i, el) => {
                        if ($(el).data('active') == 1) {
                            activeTr = $(el);
                        }
                    });

                    delete obj['nn_id[]'];
                    delete obj['nn_datatype[]'];
                    delete obj['nn_item_id[]'];
                    delete obj['nn_jenis_kelamin[]'];
                    delete obj['nn_nama[]'];
                    delete obj['nn_uid[]'];
                    delete obj['nn_umur_max_bulan[]'];
                    delete obj['nn_umur_max_flag[]'];
                    delete obj['nn_umur_max_hari[]'];
                    delete obj['nn_umur_max_tahun[]'];
                    delete obj['nn_umur_min_bulan[]'];
                    delete obj['nn_umur_min_flag[]'];
                    delete obj['nn_umur_min_hari[]'];
                    delete obj['nn_umur_min_tahun[]'];
                    delete obj['nn_value[]'];


                    console.log('PEMERIKSAAN ITEM', obj);
                    updatePemeriksaanRow(activeTr, obj);
                    MODAL_PEMERIKSAAN_ITEM.modal('hide');
                }
            });

            /**
             * FORM NILAI NORMAL
             */
            FORM_NILAI_NORMAL.validate({
                rules: {
                    //
                },
                messages: {
                    //
                },
                focusInvalid: true,
                errorPlacement: function(error, element) {
                    var inputGroup = $(element).closest('.input-group');
                    var checkbox = $(element).closest('.checkbox-inline');

                    if (inputGroup.length) {
                        error.insertAfter(inputGroup);
                    } else if (checkbox.length) {
                        checkbox.append(error);
                    } else {
                        $(element).closest("div").append(error);
                    }
                },
                submitHandler: function (form) {
                    let postData = $(form).serializeArray();
                    let nilai_normal = parseNilaiNormalFormData(postData);
                    let activeTr = null;

                    TABLE_ITEM_NILAI_NORMAL.find('tbody').find('tr').each((i, el) => {
                        if ($(el).data('active') == 1) {
                            activeTr = $(el);
                        }
                    });

                    updateNilaiNormalRow(activeTr, nilai_normal);
                    MODAL_NILAI_NORMAL.modal('hide');
                }
            });
        })();

        /**
         * BUTTON
         */
        BTN_DELETE.on('click', (e) => {
            let data = {
                uid: FORM.find('[name=uid]').val()
            };
            confirmDialog({
                title: 'Hapus Data Tersebut?',
                text: '',
                btn_confirm: 'Hapus',
                url: URL.delete,
                data: data,
                onSuccess: (res) => {
                    successMessage('Success', 'Data Berhasil Dihapus.');
                    // window.location.reload(false);
                    fillForm(FORM.find('[name=uid]').val());
                },
                onError: (error) => {
                    errorMessage('Error', 'Data Gagal Dihapus.');
                },
            });
        });

        BTN_RESTORE.on('click', (e) => {
            let data = {
                uid: FORM.find('[name=uid]').val()
            };
            confirmDialog({
                title: 'Restore Data Tersebut?',
                text: '',
                btn_confirm: 'Restore',
                url: URL.restore,
                data: data,
                onSuccess: (res) => {
                    successMessage('Success', 'Data Berhasil Direstore.');
                    // window.location.reload(false);
                    fillForm(FORM.find('[name=uid]').val());
                },
                onError: (error) => {
                    errorMessage('Error', 'Data Gagal Direstore.');
                },
            });
        });

        BTN_CANCEL.on('click', (e) => {
            window.location.assign(URL.index);
        });

        if (uid) {
            fillForm(uid);
        } else {
            fetchParent(0);
            BTN_DELETE.hide();
            BTN_RESTORE.hide();
            LABEL_DELETED.hide();
        }

        fetchLookupObat();
    }


    initialize(UID);
});