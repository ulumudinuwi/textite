$(function () {
    let fetchData = () => {
        $.getJSON(URL.fetchData, (res, status) => {
            if (status === 'success') {
                CONTAINER.empty();
                renderData(res.data, CONTAINER);
            } else {
                errorMessage('Error', 'Internal Server Error');
            }
        });
    };

    let renderData = (data, parent_container) => {
        parent_container = parent_container || CONTAINER;
        for (let key in data) {
            let d = data[key];
            if (d.lvl == 0) {
                console.log('ROOT', d.nama);
                renderData(d.children, parent_container);
            } else {
                console.log('RENDER', d);

                let container;
                if (d.lvl == 1) {
                    console.log('CREATE PANEL DATA');
                    container = createPanelData(d, parent_container);
                } else if (d.lvl > 1 && d.children) {
                    console.log('CREATE LIST DATA');
                    container = createListData(d, parent_container);
                } else {
                    console.log('CREATE ELEMENT DATA');
                    container = createElementData(d, parent_container);
                }

                renderData(d.children, container);
            }

            // CONTAINER.find('')
        }
    }

    let createPanelData = (data, container) => {
        let col = $("<div/>")
            .addClass('col-sm-12 col-md-6 col-lg-6 mb-15')
            .appendTo(container);

        let panel = $("<div/>")
            .addClass('panel panel-white')
            .appendTo(col);
        let panelHeading = $("<div/>")
            .addClass('panel-heading')
            .appendTo(panel);
        let panelCollapse = $("<div/>")
            .prop('id', `collapse-${data.uid}`)
            .addClass('panel-collapse collapse in')
            .appendTo(panel);
        let panelBody = $("<div/>")
            .addClass('panel-body')
            .appendTo(panelCollapse);

        // PANEL HEADING
        let panelHeadingTitle = $("<h6/>")
            .addClass('panel-title')
            .appendTo(panelHeading);
        let panelHeadingTitleContent = $("<a/>")
            .prop('data-toggle', 'collapse')
            .prop('href', `#collapse-${data.uid}`)
            // .html(`${data.kode} - ${data.nama}`)
            .appendTo(panelHeadingTitle);
        let link = $("<a/>")
            .data('data', data)
            .prop('href', URL.form.replace(':UID', data.uid))
            .addClass('pemeriksaan')
            .html(`<strong>${data.kode} - ${data.nama}</strong>`)
            .appendTo(panelHeadingTitleContent);

        if (data.status == 1) {
            link.addClass('active')
        } else if (data.status == 0 && data.deleted_flag == 0) {
            link.addClass('inactive');
        } else {
            link.addClass('deleted');
        }

        // PANEL BODY
        let panelBodyRow = $("<div/>")
            .addClass('row content-here')
            .appendTo(panelBody);

        return col;
    }

    let createListData = (data, container) => {
        let content = container.find('.content-here');
        let col = $("<div/>")
            .addClass('col-sm-6')
            .appendTo(content);

        let fieldset = $("<fieldset/>")
            .appendTo(col);
        let legend = $("<legend/>")
            .addClass('mb-5')
            .appendTo(fieldset);
        let link = $("<a/>")
            .data('data', data)
            .prop('href', URL.form.replace(':UID', data.uid))
            .addClass('pemeriksaan')
            .html(`<strong>${data.kode} - ${data.nama}</strong>`)
            .appendTo(legend);
        let row = $("<div/>")
            .addClass('row content-here')
            .appendTo(fieldset);

        if (data.status == 1) {
            link.addClass('active')
        } else if (data.status == 0 && data.deleted_flag == 0) {
            link.addClass('inactive');
        } else {
            link.addClass('deleted');
        }

        return col;
    }

    let createElementData = (data, container) => {
        // IF CONTAINER HAS FIELDSET COL 12
        let content = container.find('.content-here');
        let col = $("<div/>")
            .appendTo(content);

        if (container.find('fieldset').length > 0) {
            col.addClass('col-sm-12');
        } else {
            col.addClass('col-sm-6');
        }

        let link = $("<a/>")
            .data('data', data)
            .prop('href', URL.form.replace(':UID', data.uid))
            .addClass('pemeriksaan')
            .html(`${data.kode} - ${data.nama}`)
            .appendTo(col);

        if (data.status == 1) {
            link.addClass('active')
        } else if (data.status == 0 && data.deleted_flag == 0) {
            link.addClass('inactive');
        } else {
            link.addClass('deleted');
        }

        /*
        <div class="col-sm-6">
            <a href="#" class="pemeriksaan deleted">Gula Darah</a>
        </div>

        <div class="col-md-12">
            <a href="#" class="pemeriksaan inactive">Gula Darah</a>
        </div>
        */
    }

    let initialize = () => {
        fetchData();
    }

    initialize();
});