<?php

use Sse\Event;
use Sse\SSE;

class notifikasiEvent implements Event {
    private $CI;
    private $last_checksum = null;

    public function __construct() {
        $this->CI =& get_instance();
    }

    public function check() {
        $row = $this->CI->db->select('(IFNULL(SUM(created_at), 0)) as checksum')
                ->where('role_id', $this->session->userdata('role_id'))
                ->get('t_notifikasi')
                ->row();
        
        $checksum = $row->checksum;
        if ($checksum != $this->last_checksum) {
            $this->last_checksum = $checksum;
            return true;
        }

        return false;
    }

    public function update() {
        return json_encode(['data' => true, 'timestamp' => time()]);
    }
}