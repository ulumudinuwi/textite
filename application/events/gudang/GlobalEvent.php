<?php

use Sse\Event;
use Sse\SSE;

class GlobalEvent implements Event {
    private $CI;
    private $table;
    private $hash = null;

    public function __construct($uid) {
        header('Content-Type: text/event-stream');
        
        $this->CI =& get_instance();
        $this->table = base64_decode($uid);
    }

    public function check() {
        // Check Sum Beds
        $query = $this->CI->db->query('CHECKSUM TABLE '.$this->table);
        $row = $query->row();
        $hash = $row->Checksum;
        if ($hash != $this->hash) {
            $this->hash = $hash;
            return true;
        }

        return false;
    }

    public function update() {
        return json_encode(['data' => true]);
    }
}