<?php

use Carbon\Carbon;
use Hhxsv5\SSE\SSE;
use Hhxsv5\SSE\Update;

class CheckoutEvent {
    private $CI;
    private $member_id;
    private $event_name = '';
    public $last_checksum = '';

    public function __construct($uid, $event_name) {
        $this->CI =& get_instance();
        $this->event_name = $event_name;
        $this->member_id = base64_decode($uid);
    }

    public function start()
    {
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        header('Connection: keep-alive');
        header('X-Accel-Buffering: no');

        $sse = new SSE();
        $sse->start(new Update(function () {
            $new_checksum = $this->CI->db->select('(IFNULL(SUM(status), 0) + IFNULL(SUM(status), 0)) as checksum')
                            ->where('member_id', $this->member_id)
                            ->get('t_pos_checkout')
                            ->row()->checksum;
            
            if($this->last_checksum != $new_checksum){
                $message = [
                    'last_checksum' => $this->last_checksum,
                    'new_checksum' => $new_checksum,
                    'times' => Carbon::now()->format('d-m-Y'),
                ];
                $this->last_checksum = $new_checksum;
            }

            if (!empty($message)) {
                return json_encode(['message' => $message]);
            }
            return false;
        }), $this->event_name);
    }

    public function update() {

    }
}