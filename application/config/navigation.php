<?php

/**
 * Main Navigation.
 * Primarily being used in views/layouts/admin.php
 *
 */
$config['navigation'] = array(
    'dashboard' => array(
        'uri' => 'home',
        'title' => 'Dashboard',
        'icon' => 'fa fa-dashboard',
    ),
    'konsultasi' => array(
        'title' => 'Konsultasi',
        'uri' => 'konsultasi',
        'icon' => 'fa fa-pencil'
    ),
    'laporan' => array(
        'title' => 'Laporan QC',
        'uri' => 'laporan',
        'icon' => 'fa fa-list'
    ),
    'pegawai' => array(
        'title' => 'Pegawai',
        'icon' => 'fa fa-users',
        'uri' => 'pegawai'
    ),
    'pertanyaan' => array(
        'title' => 'Daftar Pertanyaan',
        'icon' => 'fa fa-question-circle',
        'uri' => 'pertanyaan',
    ),
    'user-management' => array(
        'uri' => 'auth/user',
        'title' => 'User Management',
        'icon' => 'fa fa-user'
    ),
    'acl' => array(
        'title' => 'ACL',
        'icon' => 'fa fa-unlock-alt',
        'children' => array(
            'rules' => array(
                'uri' => 'acl/rule',
                'title' => 'Rules'
            ),
            'roles' => array(
                'uri' => 'acl/role',
                'title' => 'Roles'
            ),
            'resources' => array(
                'uri' => 'acl/resource',
                'title' => 'Resources'
            )
        )
    ),
    'utils' => array(
        'title' => 'Utils',
        'icon' => 'fa fa-wrench',
        'children' => array(
            // 'style_guides' => array(
            //     'uri' => 'utils/style_guides',
            //     'title' => 'Style Guides'
            // ),
            // 'master_kurs' => array(
            //     'title' => 'Kurs',
            //     'uri' => 'master/kurs'
            // ),
            'system_logs' => array(
                'uri' => 'utils/logs/system',
                'title' => 'System Logs'
            ),
            'deploy_logs' => array(
                'uri' => 'utils/logs/deploy',
                'title' => 'Deploy Logs'
            ),
            'info' => array(
                'uri' => 'utils/info',
                'title' => 'Info'
            )
        )
    ),
);
