<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;

class Welcome extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->language('welcome');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        
        $this->template->build('welcome/welcome_message');
    }

    public function barcode($text){
        header('Content-type: image/png');
        $this->load->library('ci_barcode');
        $this->ci_barcode->generate($text);
    }

    public function test_pdf()
    {
        $this->load->library('ci_barcode');
        $data['tes'] = 'test';
        $data['barcode'] = $this->ci_barcode->barcode_image('test');
        $document = $this->load->view('rawat_jalan/documents/label', $data, true);

        $mpdf = new mPDF();
        $mpdf->showImageErrors = true;
        $mpdf->WriteHTML($document);
        $mpdf->Output('Label Pasien Rawat Jalan.pdf', "I");
    }

    public function test_inacbg()
    {
        $this->load->helper('inacbg_helper');

        $this->CI = &get_instance();

        $key = $this->CI->config->item('inacbg')['key'];
        $inacbg_url = $this->CI->config->item('inacbg')['url_debug'];

        $request = array(
            "metadata" => array(
                "method" => "new_claim"
            ),
            "data" => array(
                "nomor_kartu" => "0000668870001",
                "nomor_sep" => "0301R00112140006067",
                "nomor_rm" => "02.19243.1239",
                "nama_pasien" => "Ujang Sadikin",
                "tgl_lahir" => "1981-01-01 04:00:00",
                "gender" => "2"
            )
        );


        $json_request = json_encode($request);
        $encrypted = mc_encrypt($json_request, $key);

        $client = new Client();
        $res = $client->request('POST', $inacbg_url, [
            'body' => $encrypted
        ]);

        echo $res->getStatusCode();
        echo mc_decrypt($res->getBody(), $key);
//// 200
//        echo $res->getHeaderLine('content-type');
// 'application/json; charset=utf8'
//        echo $inacbg_url;

//        echo "$encrypted\n"; // untuk dikirim ke Web Service
//        $decrypted = mc_decrypt($encrypted, $key);
//        echo "$decrypted\n"; // Test hasil decrypt


    }

}
