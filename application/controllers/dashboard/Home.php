<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Home extends Admin_Controller {

	public function index()
	{
		$this->template
				->set_css('timeline')
				->set_css('../bower_components/morrisjs/morris')
				->set_js('../bower_components/raphael/raphael.min', TRUE)
				->set_js('../bower_components/morrisjs/morris.min', TRUE)
				->set_js('morris-data', TRUE)
				->build('dashboard/index');
	}
}
/*
    plugins/forms/validation/validate.min
    plugins/forms/validation/localization/messages_id
    plugins/tables/datatables/datatables.min
    plugins/forms/selects/bootstrap_multiselect
    plugins/forms/inputs/touchspin.min
    plugins/forms/styling/switch.min
    plugins/forms/styling/uniform.min
    plugins/forms/selects/select2.min
    plugins/ui/nicescroll.min
    pages/form_validation
    pages/form_select2
    pages/layout_fixed_custom
    pages/datatables_basic
 */