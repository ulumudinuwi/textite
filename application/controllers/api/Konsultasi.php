<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Konsultasi extends CI_Controller {

    protected $table_def = "m_kain";
    protected $table_def_laporan = "t_laporan";
    protected $ori_dir, $thumb_dir;
    
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('harga_barang');
        $this->load->model('Konsultasi_model', 'main');
    }

    public function load_data(){
        $browse  = $this->input->post('browse') ? : '';

        $aColumns = array('nip', 'nama',  'Jabatan', 'email');
        /* 
         * Paging
         */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
         * Ordering
         */
        $sOrder = "";
        $aOrders = array();
        for ($i = 0; $i < count($aColumns); $i++) {
            if($_POST['columns'][$i]['orderable'] == "true") {
                if($i == $_POST['order'][0]['column']) {
                    switch ($aColumns[$i]) {
                        default:
                            $aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
                            break;
                    }
                }
            }
        }
        if (count($aOrders) > 0) {
            $sOrder = implode(', ', $aOrders);
        }
        if (!empty($sOrder)) {
            $sOrder = "ORDER BY ".$sOrder;
        }

        /*
         * Where
         */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        case 'kode':
                            $aLikes[] = "{$this->table_def}.kode LIKE '%".$_POST['search']['value']."%' OR {$this->table_def}.alias LIKE '%".$_POST['search']['value']."%'";
                            break;
                        case 'deskripsi':
                            $aLikes[] = "{$this->table_def}.deskripsi LIKE '%".$_POST['search']['value']."%' OR {$this->table_def}.alias LIKE '%".$_POST['search']['value']."%'";
                            break;
                        case 'total':
                            $aLikes[] = "{$this->table_def}.total LIKE '%".$_POST['search']['value']."%' OR {$this->table_def}.alias LIKE '%".$_POST['search']['value']."%'";
                            break;
                        default:
                            $aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder);

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
         * Output
         */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $data = get_object_vars($obj);
            $data['no'] = ($i+1);
            $rows[] = $data;
            $i++;
        }
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function get_data() {
        if(!$this->input->is_ajax_request()) 
            exit();

        $uid = $this->input->get('uid');
        $output = array();
        if($uid) {
            $obj = $this->main->get_by("WHERE {$this->table_def}.uid = '{$uid}'");

        } else {
            $obj = new stdClass();
            $obj->id = 0;
            $obj->uid = "";
            $obj->kode = "";
            $obj->name = "";
            $obj->merk = "";
            $obj->jenis_id = "";
            $obj->kategori_id = "";
            $obj->pabrik_id = "";
            //$obj->kelompok_id = "";
            $obj->berat_barang = "";
            $obj->deskripsi = "";
            $obj->harga_pembelian = "";
            $obj->satuan_pembelian_id = "";
            $obj->diskon_mou = 0;
            $obj->satuan_penggunaan_id = "";
            $obj->isi_satuan_penggunaan = 1;
            $obj->fotos = array();
            $obj->kelompoks = array();
        }

        $output['data'] = $obj;
        echo json_encode($output);
    }  

    public function save() {
        if(!$this->input->is_ajax_request()) exit();

        $obj = $this->_getDataObject();
        if (isset($obj->uid) && $obj->uid != "") {
            $result = $this->main->update($obj);
        } else $result = $this->main->create($obj);

        if(!$result) {
            $this->output->set_status_header(500);
            echo json_encode(['message' => 'Terjadi kesalahan ketika memproses data!']); exit();
        }

        $this->output->set_status_header(200);
        echo json_encode(['message' => 'Berhasil disimpan.']);
    }

    public function update_status() {
        if (!$this->input->is_ajax_request())
          exit();

        $uid = $this->input->post('uid');
        $status = $this->input->post('status');

        $result = $this->main->update_status($uid, $status);
        echo json_encode($result);
    }

    public function delete() {
        if (!$this->input->is_ajax_request())
        exit();

        $uid = $this->input->post('uid');
        $result = $this->main->delete($uid);

        if(!$result) {
            $this->output->set_status_header(500);
            echo json_encode(['message' => 'Terjadi kesalahan ketika memproses data!']); exit();
        }

        $this->output->set_status_header(200);
        echo json_encode(['message' => 'Bahan Baku berhasil terhapus!']);
    }

    private function _getDataObject() {
        $browse = $this->input->post('browse') ? : '';
        
        $obj = new stdClass();

        $obj->id = $this->input->post('id');
        $obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "") ? $this->input->post('uid') : "";
        $obj->nama = $this->input->post('nama_kain');
        $obj->barcode = $this->input->post('barcode');
        $obj->spek_kain = $this->input->post('spek_kain');
        $obj->kesimpulan = $this->input->post('kesimpulan');
        $obj->catatan = $this->input->post('catatan');
        $obj->data_pertanyaan = $this->input->post('jawaban');

        return $obj;
    }
}
