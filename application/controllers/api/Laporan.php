<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

    protected $table_def = "t_laporan";
    protected $table_def_kain = "m_kain";
    protected $ori_dir, $thumb_dir;
    
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('harga_barang');
        $this->load->model('Laporan_model', 'main');
    }

    public function load_data(){
        $browse  = $this->input->post('browse') ? : '';

        $aColumns = array('nama',  'barcode', 'spek_kain');
        /* 
         * Paging
         */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
         * Ordering
         */
        $sOrder = "";
        $aOrders = array();
        for ($i = 0; $i < count($aColumns); $i++) {
            if($_POST['columns'][$i]['orderable'] == "true") {
                if($i == $_POST['order'][0]['column']) {
                    switch ($aColumns[$i]) {
                        default:
                            $aOrders[] = $this->table_def_kain.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
                            break;
                    }
                }
            }
        }
        if (count($aOrders) > 0) {
            $sOrder = implode(', ', $aOrders);
        }
        if (!empty($sOrder)) {
            $sOrder = "ORDER BY ".$sOrder;
        }

        /*
         * Where
         */
        $sWhere = "";
        $aWheres = array();
        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();
        if($_POST['search']['value'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {
                if($_POST['columns'][$i]['searchable'] == "true") {
                    switch ($aColumns[$i]) {
                        default:
                            $aLikes[] = "{$this->table_def_kain}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                            break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder);

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
         * Output
         */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $data = get_object_vars($obj);
            $data['no'] = ($i+1);
            $rows[] = $data;
            $i++;
        }
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function get_data() {
        if(!$this->input->is_ajax_request()) 
            exit();

        $uid = $this->input->get('uid');
        $output = array();
        if($uid) {
            $obj = $this->main->get_by("WHERE {$this->table_def}.uid = '{$uid}'");
            $obj->kain = $this->db->select('*')
                                ->where('m_kain.id', $obj->kain_id)
                                ->get($this->table_def_kain)->result();
        } else {
            $obj = new stdClass();
            $obj->id = 0;
            $obj->uid = "";
            $obj->kode = "";
            $obj->name = "";
            $obj->merk = "";
            $obj->jenis_id = "";
            $obj->kategori_id = "";
            $obj->pabrik_id = "";
            //$obj->kelompok_id = "";
            $obj->berat_barang = "";
            $obj->deskripsi = "";
            $obj->harga_pembelian = "";
            $obj->satuan_pembelian_id = "";
            $obj->diskon_mou = 0;
            $obj->satuan_penggunaan_id = "";
            $obj->isi_satuan_penggunaan = 1;
            $obj->fotos = array();
            $obj->kelompoks = array();
        }

        $output['data'] = $obj;
        echo json_encode($output);
    }  

    public function save() {
        if(!$this->input->is_ajax_request()) exit();

        $obj = $this->_getDataObject();
        if (isset($obj->uid) && $obj->uid != "") {
            $result = $this->main->update($obj);
        } else $result = $this->main->create($obj);

        if(!$result) {
            $this->output->set_status_header(500);
            echo json_encode(['message' => 'Terjadi kesalahan ketika memproses data!']); exit();
        }

        $this->output->set_status_header(200);
        echo json_encode(['message' => 'Berhasil disimpan.']);
    }

    public function update_status() {
        if (!$this->input->is_ajax_request())
          exit();

        $uid = $this->input->post('uid');
        $status = $this->input->post('status');

        $result = $this->main->update_status($uid, $status);
        echo json_encode($result);
    }

    public function delete() {
        if (!$this->input->is_ajax_request())
        exit();

        $uid = $this->input->post('uid');
        $result = $this->main->delete($uid);

        if(!$result) {
            $this->output->set_status_header(500);
            echo json_encode(['message' => 'Terjadi kesalahan ketika memproses data!']); exit();
        }

        $this->output->set_status_header(200);
        echo json_encode(['message' => 'Bahan Baku berhasil terhapus!']);
    }

    private function _getDataObject() {
        $browse = $this->input->post('browse') ? : '';
        
        $obj = new stdClass();

        $obj->id = $this->input->post('id');
        $obj->uid = $this->input->post('uid') && ($this->input->post('uid') != "") ? $this->input->post('uid') : "";
        $obj->nip = $this->input->post('nip');
        $obj->nama = $this->input->post('nama');
        $obj->jabatan = $this->input->post('jabatan');
        $obj->email = $this->input->post('email');

        return $obj;
    }

    public function load_data_modal(){
        $aColumns = array('description', 'name');
        /* 
        * Paging
        */
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $iLimit = intval( $_POST['length'] );
            $iOffset = intval( $_POST['start'] );
        }

        /*
        * Ordering
        */
        $sOrder = "";
        $aOrders = array();
        for ($i = 0; $i < count($aColumns); $i++) {
            if($_POST['columns'][$i]['orderable'] == "true") {
                if($i == $_POST['order'][0]['column']) 
                    $aOrders[] = $aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
            }
        }
        if (count($aOrders) > 0) {
            $sOrder = implode(', ', $aOrders);
        }
        if (!empty($sOrder)) {
            $sOrder = "ORDER BY ".$sOrder;
        }

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        $aWheres[] = "{$this->table_def}.status = 1";
        if ($this->session->userdata('bpom') == 1) {
            $aWheres[] = "{$this->table_def}.bpom = ".$this->session->userdata('bpom');
        }
        if (count($aWheres) > 0) {
            $sWhere = implode(' AND ', $aWheres);
        }
        if (!empty($sWhere)) {
            $sWhere = "WHERE ".$sWhere;
        }

        $aLikes = array();
            if($_POST['search']['value'] != "") {
                for ($i = 0; $i < count($aColumns); $i++) {
                    if($_POST['columns'][$i]['searchable'] == "true") {
                        switch ($aColumns[$i]) {
                            case 'description':
                                $aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
                                break;
                            case 'name':
                                $aLikes[] = "({$this->table_def}.name LIKE '%".$_POST['search']['value']."%' OR {$this->table_def}.alias LIKE '%".$_POST['search']['value']."%')";
                                break;
                            default:
                        break;
                    }
                }
            }
        }

        if (count($aLikes) > 0) {
            $sLike = "(".implode(' OR ', $aLikes).")";
            $sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
        }

        $aSelect = array(
            "{$this->table_def}.id",
            "{$this->table_def}.name",
            "{$this->table_def}.description",
            "{$this->table_def}.status",
        );

        $list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

        $rResult = $list['data'];
        $iFilteredTotal = $list['total_rows'];
        $iTotal = $list['total_rows'];

        /*
        * Output
        */
        $output = array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iFilteredTotal,
            "data" => array(),
        );

        $rows = array();
        $i = $iOffset;
        foreach ($rResult as $obj) {
            $data = get_object_vars($obj);
            $data['no'] = ($i+1);
            $rows[] = $data;
            $i++;
        }
        $output['data'] = $rows;

        echo json_encode($output);
    }

    public function print_001(){
        $mode = $this->input->get("d");

        // $pabrik_id  = $_GET['pabrik_id'];

        /*
        * Where
        */
        $sWhere = "";
        $aWheres = array();
        // if($pabrik_id != "") $aWheres[] = "pabrik_id = ".$pabrik_id;
        if (count($aWheres) > 0) $sWhere = implode(' AND ', $aWheres);
        if (!empty($sWhere)) $sWhere = "WHERE ".$sWhere;

        $list = $this->main->get_all(100, 0, 0, $sWhere);
        
        ob_start();

        switch ($mode) {
            case 'pdf':
                $data = array(
                    'title' => "LAPORAN HASIL KONSULTASI",
                    'rows' => $list['data'],
                    'total_rows' => $list['total_rows'],
                    'current_date' => konversi_to_id(date("d M Y")),
                );

                $html = $this->load->view('laporan/laporan-001-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new \Mpdf\Mpdf([
                    'mode' => 'utf-8',
                    'format' => 'A4',
                    'orientation' => 'L',
                ]);
                $mpdf->setHTMLHeader('<p style="text-align: right;">HALAMAN: {PAGENO} / {nb}</p>');
                $mpdf->WriteHTML($html);
                $mpdf->Output('Stock Barang.pdf', "I");
                break;
        }
    }

    public function print_002(){
        $mode = $this->input->get("d");
        $uid = $this->input->get("uid");

        $data = array();

        $obj = $this->main->get_by("WHERE {$this->table_def}.uid = '{$uid}'");
        $obj->kain = $this->db->select('*')
                            ->where('m_kain.id', $obj->kain_id)
                            ->get($this->table_def_kain)->result();
        ob_start();

        switch ($mode) {
            case 'pdf':
                $data = array(
                    'title' => "LAPORAN HASIL KONSULTASI",
                    'current_date' => konversi_to_id(date("d M Y")),
                    //data Kain
                    'nama_kain' => $obj->nama,
                    'barcode' => $obj->barcode,
                    'spek_kain' => $obj->spek_kain,
                    'deskripsi' => $obj->deskripsi,
                    'catatan' => $obj->catatan,
                    'data_pertanyaan' => json_decode($obj->data_pertanyaan),
                );

                $html = $this->load->view('laporan/laporan-detail-pdf', $data, TRUE);
                
                # Create PDF
                $mpdf = new \Mpdf\Mpdf([
                    'mode' => 'utf-8',
                    'format' => 'A4',
                    'orientation' => 'P',
                ]);

                $mpdf->WriteHTML($html);
                $mpdf->Output('Stock Barang.pdf', "I");
                break;
        }
    }
}
