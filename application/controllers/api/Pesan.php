<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan extends CI_Controller 
{
	protected $table_def = "t_kontak";
	
	function __construct() {
		parent::__construct();
		$this->lang->load('barang');

		$this->load->model('Pesan_model', 'main');
	}

	/**
	 * Load data
	 */
	public function load_data(){
		$tanggal_dari  = $_POST['tanggal_dari'];
		$tanggal_sampai  = $_POST['tanggal_sampai'];
		
		$aColumns = array('name', 'email');
		/* 
		 * Paging
		 */
		if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
			$iLimit = intval( $_POST['length'] );
			$iOffset = intval( $_POST['start'] );
		}

		/*
		 * Ordering
		 */
		$sOrder = "";
		$aOrders = array();
		for ($i = 0; $i < count($aColumns); $i++) {
			if($_POST['columns'][$i]['orderable'] == "true") {
				if($i == $_POST['order'][0]['column']) {
					switch ($aColumns[$i]) {
						default:
							$aOrders[] = $this->table_def.'.'.$aColumns[$i].' '.($_POST['order'][0]['dir'] == 'asc' ? 'asc' : 'desc');
							break;
					}
				}
			}
		}
		if (count($aOrders) > 0) {
			$sOrder = implode(', ', $aOrders);
		}
		if (!empty($sOrder)) {
			$sOrder = "ORDER BY ".$sOrder;
		}

		/*
		 * Where
		 */
		$sWhere = "";
		$aWheres = array();
		if($tanggal_dari != "") $aWheres[] = "DATE({$this->table_def}.created_at) >= '{$tanggal_dari}'";
		if($tanggal_sampai != "") $aWheres[] = "DATE({$this->table_def}.created_at) <= '{$tanggal_sampai}'";
		if (count($aWheres) > 0) {
			$sWhere = implode(' AND ', $aWheres);
		}
		if (!empty($sWhere)) {
			$sWhere = "WHERE ".$sWhere;
		}

		$aLikes = array();
		if($_POST['search']['value'] != "") {
			for ($i = 0; $i < count($aColumns); $i++) {
				if($_POST['columns'][$i]['searchable'] == "true") {
					switch ($aColumns[$i]) {
						default:
							$aLikes[] = "{$this->table_def}.{$aColumns[$i]} LIKE '%".$_POST['search']['value']."%'";
							break;
					}
				}
			}
		}

		if (count($aLikes) > 0) {
			$sLike = "(".implode(' OR ', $aLikes).")";
			$sWhere = !empty($sWhere) ? $sWhere." AND ".$sLike : "WHERE ".$sLike;
		}

		$aSelect = array(
			"{$this->table_def}.id",
			"{$this->table_def}.uid",
			"{$this->table_def}.name",
			"{$this->table_def}.email",
			"{$this->table_def}.subject",
			"{$this->table_def}.description",
		);
		$list = $this->main->get_all($iLimit, $iOffset, $sWhere, $sOrder, $aSelect);

		$rResult = $list['data'];
		$iFilteredTotal = $list['total_rows'];
		$iTotal = $list['total_rows'];

		/*
		 * Output
		 */
		$output = array(
			"draw" => intval($_POST['draw']),
			"recordsTotal" => $iTotal,
			"recordsFiltered" => $iFilteredTotal,
			"data" => array(),
		);

		$rows = array();
		$i = $iOffset;
		foreach ($rResult as $obj) {
			$data = get_object_vars($obj);
			$data['no'] = ($i+1);
			$rows[] = $data;
			$i++;
		}
		$output['data'] = $rows;

		echo json_encode($output);
	}

	public function save() {
		
		if (!$this->input->is_ajax_request())
			exit();

		$obj = $this->_getDataObject();
		if($obj->uid == ''){
			$result = $this->main->create($obj);
		}else{
			$result = $this->main->update($obj);
		}

		if(!$result) 
			$this->output->set_status_header(500);

		$this->output->set_status_header(200);
		echo json_encode($result);
	}

	public function get_data($uid = 0) {
		if (!$this->input->is_ajax_request())
			exit();

		if ($uid) {
			$obj = $this->main->get_by("WHERE {$this->table_def}.uid = \"{$uid}\"");
			$obj->details = $this->db->select('id, kas_bank_id, keterangan, jumlah, perkiraan_id as perkiraan, status')
				            ->where('kas_bank_id', $obj->id)
				            ->get('t_kas_bank_detail')
				            ->result();
		}
		echo json_encode(['data' => $obj]);
	}

    public function read() {
        if (! $this->input->is_ajax_request())
            exit();

        $uid = $this->input->post('uid');
        $result = $this->main->read($uid);

        if (! $result) {
            $this->output->set_status_header(504);
            return;
        }

        $this->output->set_status_header(200)
            ->set_output(json_encode(['data' => $result]));
    }

    public function verify($user_id = 0)
    {
        $user_data = array(
            'userid' => "",
            'username' => "",
            'role' => "",
            'display_name' => "",
            'email' => "",
            'logged_in' => FALSE,
            'access_list' => null,
            'service_list' => null,
            'current_path' => null
        );

        $this->session->set_userdata($user_data);
        if ($user_id != "0") {
            $user_id = base64_decode($user_id);
            $this->db->set('verifikasi_email', 1)
                ->where('id', $user_id)
                ->update('auth_users');
        }

        $this->session->set_flashdata('flash_notification', 'Terima Kasih telah melakukan validasi email');
        //header("Location: http://optimelife.com/");
        header("Location: ".$this->config->item('api_base_web')."");
		exit();
    }
}