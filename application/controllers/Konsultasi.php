<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Konsultasi extends Admin_Controller 
{
	protected $page_title = '<i class="icon-pencil"></i> Konsultasi';
	protected $def_uri = 'konsultasi';


	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 
	 */
	public function index($uid = "")
	{
		$this->data['uid'] = $uid;
		if ($uid === "") 
			$this->data['page_title'] = '<i class="icon-pencil"></i> Tambah Konsultasi';
		else 
			$this->data['page_title'] = '<i class="icon-pencil"></i> Edit Konsultasi';
		$this->template
          	->set_css('../bower_components/Croppie/croppie')
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
			->set_js('core/libraries/jquery_ui/effects.min', TRUE)
			->set_js('plugins/forms/validation/validate.min.js', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          	->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          	->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          	->set_js('pages/wizard_steps.js', TRUE)
          	->set_js('plugins/forms/wizards/steps.min.js', TRUE)
          	->set_js('../bower_components/Croppie/croppie', TRUE)
			->set_script($this->def_uri . '/script-form')
			->build($this->def_uri . '/form', $this->data);
	}

	public function form($uid = "")
	{
		$this->data['uid'] = $uid;
		if ($uid === "") 
			$this->data['page_title'] = '<i class="icon-pencil"></i> Tambah Konsultasi';
		else 
			$this->data['page_title'] = '<i class="icon-pencil"></i> Edit Konsultasi';
		$this->template
          	->set_css('../bower_components/Croppie/croppie')
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/interactions.min', TRUE)
			->set_js('core/libraries/jquery_ui/widgets.min', TRUE)
			->set_js('core/libraries/jquery_ui/effects.min', TRUE)
			->set_js('plugins/forms/validation/validate.min.js', TRUE)
			->set_js('plugins/tables/datatables/datatables.min', FALSE)
			->set_js('plugins/notifications/bootbox.min', FALSE)
			->set_js('plugins/notifications/sweet_alert.min', FALSE)
			->set_js('plugins/buttons/spin.min', FALSE)
			->set_js('plugins/buttons/ladda.min', FALSE)
			->set_js('plugins/ui/moment/moment.min', FALSE)
			->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
			->set_js('plugins/editors/wysihtml5/wysihtml5.min.js', TRUE)
          	->set_js('plugins/editors/wysihtml5/toolbar.js', TRUE)
          	->set_js('plugins/editors/wysihtml5/parsers.js', TRUE)
          	->set_js('../bower_components/Croppie/croppie', TRUE)
			->set_script($this->def_uri . '/script-form')
			->build($this->def_uri . '/form', $this->data);
	}

}