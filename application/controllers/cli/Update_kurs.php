<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_kurs extends CI_Controller
{
    public function __construct() {
        parent::__construct();

        $this->load->model('master/Kurs_model', 'main');
    }

    public function convert_currency(){
        $listKurs = $this->main->get_all(0, 0)['data'];
        foreach ($listKurs as $key => $row) {
            $fromCurrency = $row->country_id;
            $toCurrency = 'IDR';
            $query =  "{$fromCurrency}_{$toCurrency}";
            $value = $row->currency;
            
            if(!$json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey=9568862c52a93ad0f1ad")) {
                echo "Failed to converting currency --> URL can't be accessed .".PHP_EOL;
            } else {
                $obj = json_decode($json, true);
                $value = floatval($obj["$query"]);

                $this->db->set('currency', $value)
                        ->where('id', $row->id)
                        ->update('m_kurs');

                echo "Success to converting currency for {$row->country} .".PHP_EOL;
            }
        }

    }
}