<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pdf
{

    var $CI;

    function __construct()
    {
        // Get the instance
        $this->CI = & get_instance();
    }

    function load($param=NULL)
    {
	
		require_once __DIR__ . '/../../vendor/mpdf/mpdf/mpdf.php';
		
        if ($param== NULL)
        {
            $param = '"en-GB-x","A4","","",10,10,10,10,6,3';
        }

        return new mPDF($param);
    }

}
