<?php

use Rakit\Validation\Rule;

class MinimalUangRule extends Rule
{
    /** @var string */
    protected $message = "The :attribute minimum is :min";

    /** @var array */
    protected $fillableParams = ['min','nominal'];
    
    public function check($value): bool
    {
        $this->requireParameters($this->fillableParams);

        $min = $this->parameter('min');

        if (!is_numeric($value)) {
            return false;
        }
        
        return $value >= $min;
    }
}
