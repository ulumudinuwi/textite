<?php
namespace Ryuna;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Auth From CI_BEAM
 * 
 * @package Ryuna
 * @author Ari Ardiansyah 
 */

class Auth {

    protected static $CI;

    /**
     * Initialize Assign the CodeIgniter super-object
     */
    public static function init() {
        if(empty($CI)) self::$CI =& get_instance();
    }
    
    /**
     * Return response Json
     * 
     * @return boolean
     */
    public static function check() {
        self::init();
        if(!self::$CI->session) return false;
        return true;
    }

    /**
     * User
     * 
     * @return mix
     */
    public static function user() {
        self::init();
        self::$CI->load->model('auth/User_model');
        $user_id = self::$CI->session->auth_user;
        $user = (Object) DB::table('auth_users')->find($user_id);
        // dd($user);
        if(self::check()) return $user;
        return false;
    }
}