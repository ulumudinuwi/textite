<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pegawai_model extends CI_Model {

	protected $table_def = "m_pegawai";
	protected $table_def_foto = "m_pegawai_foto";

	public function __construct() {
        parent::__construct();

        $this->load->library('image_lib');
    }
	
	private function _get_select($customSelect = "") {
		if($customSelect != "") {
			$select = $customSelect;
		} else {
			$select = array(
				"{$this->table_def}.*",
			);
		}
		return 'SELECT '.implode(', ', $select).' ';
	}
	
	private function _get_from() {
		$from = "FROM {$this->table_def}";
		return $from;
	}

	private function _get_join() {
		$join = " ";
		// $join = "LEFT JOIN {$this->table_def_foto} foto ON {$this->table_def}.id = foto.pertanyaan_id ";
		return $join;
	}
	
	public function get_by($sWhere = "", $customSelect = "") {
		$sql = $this->_get_select($customSelect)." ";
		$sql .= $this->_get_from()." ";
		$sql .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql .= " ".$sWhere;
		}
        $query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
    }
	
	public function get_all($iLimit = 10, $iOffset = 0, $sWhere = "", $sOrder = "", $customSelect = "") {
		
		$data = array();
		$sql_count = "SELECT COUNT({$this->table_def}.id) AS numrows ";
		$sql_count .= $this->_get_from()." ";
		$sql_count .= $this->_get_join();
		if (!empty($sWhere)) {
			$sql_count .= " ".$sWhere." ";
		}
		$query = $this->db->query($sql_count);
		if ($query->num_rows() == 0) {
			$data['total_rows'] = 0;
		}
		else {
			$row = $query->row();
			$data['total_rows'] = (int) $row->numrows;
		}
		
		$select = $this->_get_select($customSelect);
		$from = $this->_get_from();
		$join = $this->_get_join();
		$sql = $select." ".$from." ".$join." ";
		if (!empty($sWhere)) {
			$sql .= $sWhere." ";
		}
		if (!empty($sOrder)) {
			$sql .= $sOrder." ";
		}
		if ($iLimit > 0) {
			$sql .= "LIMIT ".$iOffset.", ".$iLimit;
		}
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$data['data'] = $query->result();
		}
		else {
			$data['data'] = array();
		}
		return $data;
	}

	public function create($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
		unset($data['uid']);
		$this->db->set('uid', 'UUID()', FALSE);
		$data['created_by'] = $this->session->userdata('auth_user');
		$data['created_at'] = date('Y-m-d H:i:s');

		$this->db->insert($this->table_def, $data);
		$obj->id =  $this->db->insert_id();

		$user = [
			'uid' => uuid(),
            'first_name' => $data['nama'],
            'username' => $data['email'],
            'email' => $data['email'],
            'password' => $this->passwordhash->HashPassword('12345678'),
            'pegawai_id' => $obj->id,
            'role_id' => 2,
            'registered' => timestamp(),
            'created_at' => timestamp(),
            'created_by' => auth_id(),
            'update_at' => timestamp(),
            'update_by' => auth_id(),
		];
		
		$this->db->insert('auth_users', $user);

		$auth_users_roles = [
            'user_id' => $obj->id,
            'role_id' => 2,
            'created_at' => timestamp(),
            'created_by' => auth_id(),
            'update_at'  => timestamp(),
            'update_by'  => auth_id(),
		];
		
		$this->db->insert('auth_users_roles', $auth_users_roles);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $obj->id;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function update($obj) {
		$this->db->trans_start();

		$data = get_object_vars($obj);
        $password = $this->passwordhash->HashPassword($data['password']);

		unset($data['uid']);
		unset($data['password']);
		unset($data['kode']);

		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');

		$this->db->where('uid', $obj->uid);
		$this->db->update($this->table_def, $data);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $obj->id;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function delete($uid) {
		$this->db->trans_start();

		$this->db->where('uid', $uid);
		$this->db->delete($this->table_def);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

	public function update_status($uid, $status) {
		$this->db->trans_start();

		$data = array();
		$data['update_by'] = $this->session->userdata('auth_user');
		$data['update_at'] = date('Y-m-d H:i:s');
		$data['status'] = $status;

		$this->db->where('uid', $uid);
		$this->db->update($this->table_def, $data);

		$this->db->trans_complete();
		if ($this->db->trans_status() === TRUE) {
		  $this->db->trans_commit();
		  return $uid;
		} else {
		  $this->db->trans_rollback();
		  return false;
		}
	}

}

?>