<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Model
 * 
 * @package App
 * @category Model
 * @author Ardi Soebrata
 */
class User_model extends MY_Model {

	protected $table = 'auth_users';
	protected $role_table = 'acl_roles';
	private $ci;

	function __construct()
	{
		parent::__construct();

		$this->ci = & get_instance();
		$this->ci->load->library('PasswordHash', array('iteration_count_log2' => 8, 'portable_hashes' => FALSE));
	}

	/**
	 * Insert data to User Model
	 * 
	 * @param array $data
	 * @return boolean
	 */
	public function insert($data)
	{
		$this->db->trans_start();

		$roles = $data["roles_id"];
		unset($data["roles_id"]);
		$data['registered'] = date('Y-m-d H:i:s');
		$user_id = parent::insert($this->prep_data($data));

		$this->_saveRoles($user_id, $roles);


		if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
			return $user_id;
        } else {
            $this->db->trans_rollback();
            return false;
        }
	}

	/**
	 * Update data to User Model
	 * 
	 * @param int $id
	 * @param array $data
	 * @return boolean
	 */
	public function update($id, $data)
	{
		$this->db->trans_start();

		$roles = $data["roles_id"];
		unset($data["roles_id"]);
		$result = parent::update($id, $this->prep_data($data));

		$this->_saveRoles($id, $roles);

		if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
			return $result;
        } else {
            $this->db->trans_rollback();
            return false;
        }
	}

	/**
	 * Prepare input data
	 * 
	 * @param array $data
	 * @return array
	 */
	private function prep_data($data)
	{
		// Remove confirm-password field
		unset($data['confirm-password']);

		// Hash password field if not empty
		if (isset($data['password']))
		{
			if (strlen(trim($data['password'])) > 0)
				$data['password'] = $this->ci->passwordhash->HashPassword($data['password']);
			else
				unset($data['password']);
		}
		return $data;
	}

	/**
	 * Compare user input password to stored hash
	 * 
	 * @param string $password
	 * @param string $userpass
	 * @return boolean
	 */
	public function check_password($password, $userpass)
	{
		// check password
		return $this->ci->passwordhash->CheckPassword($password, $userpass);
	}

	/**
	 * Get user by id
	 * 
	 * @param int $id
	 * @return array|boolean
	 */
	function get_by_id($id)
	{
		$this->db->select($this->table . '.*, ' . $this->role_table . '.name AS role_name') //, ' . $this->unit_kerja . '.nama AS unit_kerja')
				->join($this->role_table, $this->role_table . '.id = ' . $this->table . '.role_id', 'left');
				//->join($this->unit_kerja, $this->unit_kerja . '.id = ' . $this->table . '.unitkerja_id', 'left');

		// dd($id);
		$obj = parent::get_by_id($id);

		$obj->roles = $this->db->select('acl_roles.id, acl_roles.name')
				->join('auth_users_roles', 'auth_users_roles.role_id = acl_roles.id', 'left')
				->where('auth_users_roles.user_id', $obj->id)
				->get('acl_roles')->result();
		$obj->roles_id = array();
		foreach ($obj->roles as $role) {
			$obj->roles_id[] = $role->id;
		}

		return $obj;

		// return parent::get_by_id($id);
	}

	/**
	 * Get user by username
	 * 
	 * @param string $username
	 * @return object user
	 */
	function get_by_username($username)
	{
		$this->db->select($this->table . '.*, ' . $this->role_table . '.name AS role_name')
				->join($this->role_table, $this->role_table . '.id = ' . $this->table . '.role_id', 'left');
		$query = $this->db->get_where($this->table, array($this->table . '.username' => $username));
		if ($query->num_rows() > 0)
			return $query->row();
		else
			return FALSE;
	}

	/**
	 * Check if username is available
	 * 
	 * @param string $username
	 * @param int $id
	 * @return boolean
	 */
	function is_username_unique($username, $id = 0)
	{
		$this->db->where('username', $username);
		if ($id > 0)
			$this->db->where($this->id_field . ' <>', $id);
		$query = $this->db->get($this->table);
		return ($query->num_rows() == 0);
	}

	/**
	 * Check if email is available
	 * 
	 * @param string $email
	 * @param int $id
	 * @return boolean
	 */
	function is_email_unique($email, $id = 0)
	{
		$this->db->where('email', $email);
		if ($id > 0)
			$this->db->where($this->id_field . ' <>', $id);
		$query = $this->db->get($this->table);
		return ($query->num_rows() == 0);
	}

	function datatable()
	{
		$this->datatables->select("$this->table.id, first_name, last_name, username, email, acl_roles.name AS role, registered, '' AS action")
				->join('acl_roles', "acl_roles.id = $this->table.role_id", 'left')
				->from($this->table);
		return $this->datatables->generate();
	}

	private function _saveRoles($user_id, $dataRoles) 
	{
		if (! count($dataRoles) > 0) return;
		
		$result = $this->db->where('user_id', $user_id)
					->get('auth_users_roles')
					->result();

		$roles = array();
		foreach ($result as $row) {
			$temp = new stdClass();
			$temp->role_id = $row->role_id;
			$temp->user_id = $user_id;
			$temp->mode = 'DELETE';
			$roles[$row->role_id] = $temp;
		}

		foreach ($dataRoles as $role_id) {
			if (array_key_exists($role_id, $roles)) { // Update
				$roles[$role_id]->mode = 'UPDATE';
			} else { // ADD
				$temp = new stdClass();
				$temp->role_id = $role_id;
				$temp->user_id = $user_id;
				$temp->mode = 'ADD';
				$roles[$role_id] = $temp;
			}
		}

		// Save to Database
		foreach ($roles as $role) {
			$data = get_object_vars($role);
			unset($data['mode']);
			switch ($role->mode) {
				case 'ADD':
					$this->db->insert('auth_users_roles', $data);
					break;
				case 'UPDATE':
					$this->db->where('user_id', $role->user_id);
					$this->db->where('role_id', $role->role_id);
					$this->db->update('auth_users_roles', $data);
					break;
				case 'DELETE':
					$this->db->where('user_id', $role->user_id);
					$this->db->where('role_id', $role->role_id);
					$this->db->delete('auth_users_roles');
					break;
			}
		}
	}
}
