<?php defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Rest extends REST_Controller {
	public $secretKey = 'PRIMA-uvXL68GB5THBN8cUIFuM';

	public function __construct() {
		parent::__construct();
	}

	public function checkToken() {
		$authKey = $this->input->get_request_header('Authorization');
		try {
			if (preg_match('/Bearer\s(\S+)/', $authKey, $matches)) $authKey = str_replace('"', "", $matches[1]);

			$decodeAuthKey = JWT::decode($authKey, $this->secretKey, array('HS256'));
			$curUser = $this->user_model->get_by_id($decodeAuthKey->id);
			if($curUser) return $curUser;
		} catch (Exception $e) {
			exit(json_encode(['status' => 0, 'message' => 'Token tidak valid']));
		}
	}

	public function failToken($message) {
		return $this->response([
	            				'status' => 0,
              					'message' => $message
              					], REST_Controller::HTTP_BAD_REQUEST);
	}
}