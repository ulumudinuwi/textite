<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['field_id'] = 'ID';
$lang['field_uid'] = 'UID';
$lang['field_kode'] = 'Kode';
$lang['field_nama'] = 'Nama';
$lang['field_keterangan'] = 'Keterangan';
$lang['field_deskripsi'] = 'Deskripsi';
$lang['field_jenis'] = 'Jenis';
$lang['field_kategori'] = 'Kategori';
$lang['field_pilih'] = 'Pilih';
$lang['field_status'] = 'Status';
$lang['field_all'] = 'Semua';

$lang['status_active'] = 'Aktif';
$lang['status_inactive'] = 'Non Aktif';

$lang['btn_save'] = 'Simpan';
$lang['btn_cancel'] = 'Batal';
$lang['btn_close'] = 'Tutup';
$lang['btn_back'] = 'Kembali';
$lang['btn_add'] = 'Tambah';
$lang['btn_delete'] = 'Hapus';
$lang['btn_restore'] = 'Restore';
$lang['btn_edit'] = 'Edit';