<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['master_ukuran_foto_title'] = 'Master Ukuran Foto';
$lang['master_ukuran_foto_icon'] = 'icon-images3';
$lang['master_ukuran_foto_add'] = 'Tambah Ukuran Foto';
$lang['master_ukuran_foto_edit'] = 'Edit Ukuran Foto';

$lang['master_pemeriksaan_title'] = 'Master Pemeriksaan Radiologi';
$lang['master_pemeriksaan_icon'] = 'icon-certificate';
$lang['master_pemeriksaan_add'] = 'Tambah Pemeriksaan Radiologi';
$lang['master_pemeriksaan_edit'] = 'Edit Pemeriksaan Radiologi';

$lang['section_data'] = 'Data';
$lang['section_ukuran_foto'] = 'Ukuran Foto';
$lang['section_obat'] = 'Penggunaan Obat/BMHP';
// LABORATORIUM

$lang['field_kode'] = 'Kode';
$lang['field_nama'] = 'Nama';
$lang['field_satuan'] = 'Satuan';
$lang['field_nilai_normal'] = 'Nilai Rujukan';
$lang['field_jenis'] = 'Kelompok / Rincian';
$lang['field_jenis_kelompok'] = 'Kelompok';
$lang['field_jenis_rincian'] = 'Rincian';
$lang['field_parent_id'] = 'Parent';
$lang['field_deskripsi'] = 'Deskripsi';
$lang['field_status'] = 'Status';
$lang['field_quantity'] = 'Qty';
$lang['field_action'] = '&nbsp;';
$lang['field_obat'] = 'Obat/BMHP';
$lang['field_select'] = '&nbsp;';
$lang['field_ukuran'] = 'Ukuran';

// MODALS
$lang['modal_lookup_obat_title'] = 'Pilih Obat/BMHP';