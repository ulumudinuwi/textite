<?php
use Illuminate\Database\Capsule\Manager as DB;

if (!function_exists('get_no_kuitansi')) {
    function get_no_kuitansi(){
        return DB::select('SELECT func_get_no_kuitansi() as no_kuitansi')[0]['no_kuitansi'];
    }
}
if (!function_exists('get_no_invoice')) {
    function get_no_invoice($param){
        return DB::select('SELECT func_get_no_invoice("'.$param.'") as no_invoice')[0]['no_invoice'];
    }
}
if (!function_exists('get_no_invoice_online')) {
    function get_no_invoice_online($param){
        return DB::select('SELECT func_get_no_invoice_online("'.$param.'") as no_invoice')[0]['no_invoice'];
    }
}
if (!function_exists('get_no_fbp')) {
    function get_no_fbp(){
        return DB::select('SELECT func_get_no_fbp() as no_fbp')[0]['no_fbp'];
    }
}
if (!function_exists('get_no_member')) {
    function get_no_member(){
        return DB::select('SELECT func_get_no_member() as no_member')[0]['no_member'];
    }
}
if (!function_exists('point_masuk')) {
    function point_masuk($object,$jumlah_point,$transaksi_id,$ket = ''){
        // $dokter = DB::table('m_dokter_member')->where('uid', $object->member->uid)->update([
        //     'point' => $object->member->point += $jumlah_point,
        //     'updated_at' => timestamp(),
        //     'updated_by' => auth_id(),
        // ]);

        $point_record = DB::table('t_points_record')->insert([
            'uid' => uuid(),
            // 'member_id' => $object->member->id,
            // 'sales_id' => $object->sales->id,
            'jumlah_point' => $jumlah_point,
            'transaksi_id' => $transaksi_id,
            'keterangan' => $ket,
            'jenis' => 'masuk',
            'created_at' => timestamp(),
            'created_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);
    }
}
if (!function_exists('point_keluar')) {
    function point_keluar($object,$jumlah_point,$transaksi_id,$ket = ''){
        $dokter = DB::table('m_dokter_member')->where('uid', $object->member->uid)->update([
            'point' => $object->member->point -= $jumlah_point,
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);

        $point_record = DB::table('t_points_record')->insert([
            'uid' => uuid(),
            // 'member_id' => $object->member->id,
            // 'sales_id' => $object->sales->id,
            'jumlah_point' => $jumlah_point,
            'transaksi_id' => $transaksi_id,
            'keterangan' => $ket,
            'jenis' => 'keluar',
            'created_at' => timestamp(),
            'created_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);
    }
}
if (!function_exists('cicilan_ke')) {
    function cicilan_ke($pos_id){
        $_scope_pos = DB::table('t_pos')->where('id', $pos_id)->first();
        if($_scope_pos && !$_scope_pos['status_lunas']){
            $cicilan_ke = 1;
            $_scope_pembayaran = DB::table('t_pos_pembayaran')->where('pos_id', $pos_id)->orderBy('cicilan_ke','DESC')->first();

            if($_scope_pembayaran) $cicilan_ke = $_scope_pembayaran['cicilan_ke'] += 1;
            
            return ['cicilan_ke' => $cicilan_ke];
        }

        return ['cicilan_ke' => 0];
    }
}
if (!function_exists('find_role')) {
    function find_role($role_name){
        $role = DB::table('acl_roles')->where('name','LIKE',"%".$role_name."%")->first();
        return $role;
    }
}
if (!function_exists('sales_id')) {
    function sales_id(){
        return DB::table('auth_users')->where('id', auth_id())->first()['sales_id'];
    }
}
if (!function_exists('get_role')) {
    function get_role($role_id){
        $query = DB::table('acl_roles')->where('id', $role_id)->first();
        return $query;
    }
}
if (!function_exists('role_name')) {
    function role_name($role_id){
        $get_role = get_role($role_id);
        if($get_role) return $get_role['name'];
        return 'Guest';
    }
}
if (!function_exists('compare_string')) {
    function compare_string($str1, $str2){
        if (strcasecmp($str1, $str2) == 0) return true;
        return false;
    }
}

if (!function_exists('strripos_string')) {
    function strripos_string($str1, $str2){
        if (stripos($str1, $str2) === false) return false;
        return true;
    }
}

if (!function_exists('is_manager')) {
    function is_manager(){
        $get_user = DB::table('auth_users')->where('id', auth_id())->first();
        $role_name = role_name($get_user['role_id']);
        $is_manager = compare_string('Sales Manager',$role_name);
        return $is_manager;
    }
}
if (!function_exists('is_admin')) {
    function is_admin(){
        $get_user = DB::table('auth_users')->where('id', auth_id())->first();
        $role_name = role_name($get_user['role_id']);
        $is_admin = strripos_string($role_name, 'Admini');
        return $is_admin;
    }
}
if (!function_exists('is_sales')) {
    function is_sales(){
        $get_user = DB::table('auth_users')->where('id', auth_id())->first();
        if(!$get_user['sales_id']) return false;
        $role_name = role_name($get_user['role_id']);
        $is_sales = compare_string('Sales',$role_name);
        return $is_sales;
    }
}
if (!function_exists('is_direktur')) {
    function is_direktur(){
        $get_user = DB::table('auth_users')->where('id', auth_id())->first();
        $role_name = role_name($get_user['role_id']);
        $is_direktur = strripos_string($role_name, 'Direktur');
        return $is_direktur;
    }
}
if (!function_exists('get_bawahan_sales_id')) {
    function get_bawahan_sales_id(){
        if(is_manager()){
            $get_all_user = DB::table('m_sales')->where('manager_sales_id', sales_id())->lists('id');
            return $get_all_user;
        }
    }
}
if (!function_exists('diskon_mou')) {
    function diskon_mou($object){
        $getDetail = DB::table('t_pos_detail')->where('id', $object)->first();
        $getBarang = DB::table('m_promo')->whereRaw('barang_id = '.$getDetail['barang_id'].' AND DATE(expired_at) >='.date('Y-m-d'))->first();
        $getDokter = DB::table('m_dokter_member')->where('id', $getDetail['member_id'])->first();

        if ($getDetail) {
            $grandTotal = $getDetail['harga'];
            $grandQty = $getDetail['qty'];
            if (!$getBarang) {

                if ($getDokter['st_diskon_mou'] == 1 && strtotime($getDokter['tgl_mulai_mou']) <= strtotime(date('Y-m-d')) && strtotime($getDokter['tgl_jatuh_tempo']) >= strtotime(date('Y-m-d'))) {
                    $diskonMou  = $getDokter['diskon_mou'];
                    $totalMou   = $grandTotal * $grandQty * $diskonMou / 100;
                    $point_record = DB::table('t_diskon_mou')->insert([
                        'uid' => uuid(),
                        'pos_id' => $getDetail['pos_id'],
                        'pos_detail_id' => $getDetail['id'],
                        'barang_id' => $getDetail['barang_id'],
                        // 'member_id' => $getDetail['member_id'],
                        // 'sales_id' => $getDetail['sales_id'],
                        'diskon' => $diskonMou,
                        'total_diskon' => $totalMou,
                        'status' => '1',
                        'created_at' => timestamp(),
                        'created_by' => auth_id(),
                        'update_at' => timestamp(),
                        'update_by' => auth_id(),
                    ]);
                }
            }
        }


    }
}

if (!function_exists('point_masuk_online')) {
    function point_masuk_online($data, $jumlah_point, $transaksi_id, $ket = ''){
        $dokter = DB::table('m_dokter_member')->where('id', $data->member_id)->update([
            'point' => $data->point += $jumlah_point,
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);

        $point_record = DB::table('t_points_record')->insert([
            'uid' => uuid(),
            // 'member_id' => $data->member_id,
            // 'sales_id' => $data->sales_id,
            'jumlah_point' => $jumlah_point,
            'transaksi_id' => $transaksi_id,
            'keterangan' => $ket,
            'jenis' => 'masuk',
            'created_at' => timestamp(),
            'created_by' => auth_id(),
            'updated_at' => timestamp(),
            'updated_by' => auth_id(),
        ]);
    }
}