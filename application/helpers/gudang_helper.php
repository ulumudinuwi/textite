<?php
	if( !function_exists('generateKode')) {
		function generateKode($prefix, $table) {
			$CI =& get_instance();
			$year = date('y');
			$month = date('m');
			$day = date('d');

			$no_urut = 0;
			$query = $CI->db->query("SELECT RIGHT(kode, 4) last_code FROM {$table} WHERE YEAR(created_at) = YEAR(CURRENT_TIMESTAMP) AND MONTH(created_at) = MONTH(CURRENT_TIMESTAMP) ORDER BY created_at DESC LIMIT 1");
			if($query->num_rows() > 0) $no_urut = (int) $query->row()->last_code;
			$no_urut += 1;
			
			return $prefix.'/'.$year.$month.'/'.str_pad($no_urut, 4, "0", STR_PAD_LEFT);
		}
	}
