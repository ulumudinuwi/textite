<?php
if(! function_exists('procStockMasuk')) {
	function procStockMasuk($obj, $detail, $tipe) {
		$CI =& get_instance();
		$table_def_stock = 't_logistik_stock';
		$table_def_stock_detail = 't_logistik_stock_detail';
		$table_def_kartu_stock = 't_logistik_kartu_stock';
		$table_def_barang = 'm_barang';

		$dipesan = 0;
		if($tipe == $CI->config->item('tipe_kartu_stock_penerimaan')) $dipesan = $detail->dipesan;
		$checkExists = $CI->db->where('barang_id', $detail->barang_id)
								->get($table_def_stock);
		if($checkExists->num_rows() > 0) {
			$stock_id = $checkExists->row()->id;
			$CI->db->set('qty', 'qty + '.$detail->qty, FALSE)
					->set('dipesan', $dipesan)
					->set('update_at', date('Y-m-d H:i:s'))
					->set('update_by', $CI->auth->userid())
					->where('id', $stock_id)
					->update($table_def_stock);
		} else {
			$objNew = array(
				'barang_id' => $detail->barang_id,
				'qty' => $detail->qty,
				'created_at' => date('Y-m-d H:i:s'),
				'created_by' => $CI->auth->userid(),
			);
			$CI->db->insert($table_def_stock, $objNew);
			$stock_id = $CI->db->insert_id();
		}

		# GET AND UPDATE STOCK DETAIL
		$CI->db->where('stock_id', $stock_id);
		if($tipe == $CI->config->item('tipe_kartu_stock_penerimaan')) 
			$CI->db->where('harga', $detail->harga);
		$checkExists = $CI->db->get($table_def_stock_detail);

		if($checkExists->num_rows() > 0) {
			$stock_detail_id = $checkExists->row()->id;
			$CI->db->set('qty_awal', 'qty_awal + '.$detail->qty, FALSE)
					->set('qty', 'qty + '.$detail->qty, FALSE)
					->set('update_at', date('Y-m-d H:i:s'))
					->set('update_by', $CI->auth->userid())
					->where('id', $stock_detail_id)
					->update($table_def_stock_detail);
		} else {
			$harga = isset($detail->harga) ? $detail->harga : 0;
			if($tipe != $CI->config->item('tipe_kartu_stock_penerimaan')) {
				$objBarang = $CI->db->query("SELECT harga_pembelian, isi_satuan_penggunaan FROM {$table_def_barang} WHERE id = {$detail->barang_id}")->row();
				if($objBarang) $harga = $objBarang->harga_pembelian / $objBarang->isi_satuan_penggunaan;
			}

			$objNew = array(
				'stock_id' => $stock_id,
				'harga' => $harga,
				'qty' => $detail->qty,
				'qty_awal' => $detail->qty,
				'created_at' => date('Y-m-d H:i:s'),
				'created_by' => $CI->auth->userid(),
			);
			$CI->db->insert($table_def_stock_detail, $objNew);
			$stock_detail_id = $CI->db->insert_id();
		}

		$objStock = $CI->db->where('id', $stock_id)
						->get($table_def_stock)->row();

		$objStockDetail = $CI->db->where('id', $stock_detail_id)
						->get($table_def_stock_detail)->row();

		# KARTU STOCK
		$kartu = new stdClass();
		$kartu->tipe    = $tipe;
		$kartu->tanggal = $obj->tanggal;
		$kartu->kode = $obj->kode;
		$kartu->transaksi_id = $obj->id;
		$kartu->transaksi_detail_id = $detail->id;
		$kartu->barang_id = $detail->barang_id;
		$kartu->stock_detail_id = $stock_detail_id;
		$kartu->masuk 	  = $detail->qty;
		$kartu->sisa 	  = $objStockDetail->qty;
		$kartu->sisa_sum  = $objStock->qty;
		$kartu->keterangan = isset($obj->keterangan) ? $obj->keterangan : '';
		$kartu->created_by = $CI->auth->userid();
        $kartu->created_at = date('Y-m-d H:i:s');
        $CI->db->insert($table_def_kartu_stock, $kartu);
	}
}

if(! function_exists('procStockKeluar')) {
	function procStockKeluar($obj, $detail, $tipe) {
		$CI =& get_instance();
		$table_def_stock = 't_logistik_stock';
		$table_def_stock_detail = 't_logistik_stock_detail';
		$table_def_kartu_stock = 't_logistik_kartu_stock';

		$objStock = $CI->db->where('barang_id', $detail->barang_id)
							->get($table_def_stock)->row();

		$tmpQty = $detail->qty;
		do {
			$objStockDetail = $CI->db->where('stock_id', $objStock->id)
									->where('qty >', 0)
									->order_by('id', 'ASC')
									->limit(1)
									->get($table_def_stock_detail)->row();

            if($objStockDetail) {
                $tmpStockSisa = $tmpQty - $objStockDetail->qty;
                $stock_sisa = $tmpQty - $objStockDetail->qty;
                if($stock_sisa > 0) {
                    $stock_sisa = 0;
                    $sendJumlah = $objStockDetail->qty;
                } else {
                    $stock_sisa = $objStockDetail->qty - $tmpQty;
                    $sendJumlah = $tmpQty;
                }
                $tmpQty = $tmpStockSisa;
                $CI->db->set('qty', $stock_sisa)
                		->where('id', $objStockDetail->id)
                		->update($table_def_stock_detail);

                $CI->db->set('qty', 'qty - '.$sendJumlah, FALSE)
						->set('update_at', date('Y-m-d H:i:s'))
						->set('update_by', $CI->auth->userid())
						->where('id', $objStock->id)
						->update($table_def_stock);

				$stock = $CI->db->select('qty')
								->where('barang_id', $detail->barang_id)
								->get($table_def_stock)->row()->qty;

                # KARTU STOCK
                $kartu = new stdClass();
				$kartu->tipe    = $tipe;
				$kartu->tanggal = $obj->tanggal;
				$kartu->kode = $obj->kode;
				$kartu->transaksi_id = $obj->id;
				$kartu->transaksi_detail_id = $detail->id;
				$kartu->barang_id = $detail->barang_id;
				$kartu->stock_detail_id = $objStockDetail->id;
				$kartu->keluar 	  = $sendJumlah;
				$kartu->sisa 	  = $stock_sisa;
				$kartu->sisa_sum  = $stock;
				$kartu->keterangan = $obj->keterangan;
				$kartu->created_by = $CI->auth->userid();
		        $kartu->created_at = date('Y-m-d H:i:s');
		        $CI->db->insert($table_def_kartu_stock, $kartu);
            } else break;
		} while ($tmpQty > 0);					
	}
}

if(! function_exists('procStockSO')) {
	function procStockSO($obj, $detail, $tipe) {
		$CI =& get_instance();
		$table_def_stock = 't_logistik_stock';
		$table_def_stock_detail = 't_logistik_stock_detail';
		$table_def_kartu_stock = 't_logistik_kartu_stock';
		$table_def_barang = 'm_barang';

		$stock_id = $detail->stock_id;
		$objStock = $CI->db->select('id, barang_id, qty')
						->where('id', $stock_id)
						->get($table_def_stock)->row();


		// UPDATE STOCK BARANG
		$fisik = $detail->pengecekan2_stock_fisik;		
		$CI->db->set('qty', $fisik, FALSE)
                		->set('status', 1)
						->set('update_at', date('Y-m-d H:i:s'))
						->set('update_by', $CI->auth->userid())
						->where('id', $stock_id)
						->update($table_def_stock);
		
		// UPDATE STOCK DETAIL BARANG
		$selisih = $detail->pengecekan2_selisih;		
		$absSelisih = abs($selisih);
		if($selisih < 0) {
			$stock = 0;
			$dataStockDetail = $CI->db->select('id, qty, qty_awal')
										->where('stock_id', $stock_id)
										->order_by('id', 'desc')
										->get($table_def_stock_detail)->result();
			if(count($dataStockDetail) > 0) {
				foreach ($dataStockDetail as $i => $row) {
					$nextRow = isset($dataStockDetail[$i + 1]) ? $dataStockDetail[$i + 1] : null;
					$stock_detail_id = $row->id;

					$jumlahMasuk = $row->qty;
					$jumlahStock = $row->qty;
					if(!is_null($nextRow)) {
						if($row->qty < $row->qty_awal) {
							$jumlahMasuk = $absSelisih;
							$tmpSelisih = $row->qty_awal - $row->qty;
							$jumlahStock = $row->qty + $jumlahMasuk;
							if($jumlahMasuk > $row->qty_awal) {
								$jumlahMasuk = $row->qty_awal;
								$jumlahStock = $row->qty_awal;
								$absSelisih -= $tmpSelisih; 
							}
						}
					} else $jumlahStock = $row->qty + $absSelisih;

					$stock += $jumlahStock;

					$CI->db->set('qty_awal', $jumlahStock, FALSE)
                		->set('qty', $jumlahStock, FALSE)
                		->where('id', $stock_detail_id)
                		->update($table_def_stock_detail);

                	# KARTU STOCK
		            $kartu = new stdClass();
					$kartu->tipe    = $tipe;
					$kartu->tanggal = $obj->tanggal;
					$kartu->kode = $obj->kode;
					$kartu->transaksi_id = $obj->id;
					$kartu->transaksi_detail_id = $detail->id;
					$kartu->barang_id = $objStock->barang_id;
					$kartu->stock_detail_id = $stock_detail_id;
					$kartu->masuk 	  = $jumlahStock;
					$kartu->sisa 	  = $jumlahStock;
					$kartu->sisa_sum  = $stock;
					$kartu->keterangan = 'Stock Opname Barang';
					$kartu->created_by = $CI->auth->userid();
			        $kartu->created_at = date('Y-m-d H:i:s');
			        $CI->db->insert($table_def_kartu_stock, $kartu);

			        if($absSelisih <= 0) break;
				}
			} else {
				$harga = 0;
				$objBarang = $CI->db->query("SELECT harga_pembelian, isi_satuan_penggunaan FROM {$table_def_barang} WHERE id = {$objStock->barang_id}")->row();
				if($objBarang) $harga = $objBarang->harga_pembelian / $objBarang->isi_satuan_penggunaan;

				$objNew = array(
					'stock_id' => $stock_id,
					'harga' => $harga,
					'qty' => $fisik,
					'qty_awal' => $fisik,
					'created_at' => date('Y-m-d H:i:s'),
					'created_by' => $CI->auth->userid(),
				);
				$CI->db->insert($table_def_stock_detail, $objNew);

				$stock_detail_id = $CI->db->insert_id();
				$jumlahStock = $fisik;
				$stock = $fisik;

				# KARTU STOCK
	            $kartu = new stdClass();
				$kartu->tipe    = $tipe;
				$kartu->tanggal = $obj->tanggal;
				$kartu->kode = $obj->kode;
				$kartu->transaksi_id = $obj->id;
				$kartu->transaksi_detail_id = $detail->id;
				$kartu->barang_id = $objStock->barang_id;
				$kartu->stock_detail_id = $stock_detail_id;
				$kartu->masuk 	  = $jumlahStock;
				$kartu->sisa 	  = $jumlahStock;
				$kartu->sisa_sum  = $stock;
				$kartu->keterangan = 'Stock Opname Barang';
				$kartu->created_by = $CI->auth->userid();
		        $kartu->created_at = date('Y-m-d H:i:s');
		        $CI->db->insert($table_def_kartu_stock, $kartu);
			}
		} else {
			$stock = $objStock->qty;
			$dataStockDetail = $CI->db->select('id, qty, qty_awal')
										->where('stock_id', $stock_id)
										->where('qty >', 0)
										->order_by('id', 'asc')
										->get($table_def_stock_detail)->result();

			foreach ($dataStockDetail as $i => $row) {
				$stock_detail_id = $row->id;
				$jumlahMasuk = $row->qty;
				$jumlahStock = $row->qty;

				$jumlahMasuk = $absSelisih;
				$jumlahStock = $row->qty - $jumlahMasuk;
				if($jumlahStock < 0) {
					$jumlahMasuk = $row->qty;
					$jumlahStock = 0;
					$absSelisih -= $jumlahMasuk; 
				}

				$CI->db->set('qty', $jumlahStock, FALSE)
            		->where('id', $stock_detail_id)
            		->update($table_def_stock_detail);

            	# KARTU STOCK
	            $kartu = new stdClass();
				$kartu->tipe    = $tipe;
				$kartu->tanggal = $obj->tanggal;
				$kartu->kode = $obj->kode;
				$kartu->transaksi_id = $obj->id;
				$kartu->transaksi_detail_id = $detail->id;
				$kartu->barang_id = $objStock->barang_id;
				$kartu->stock_detail_id = $stock_detail_id;
				$kartu->keterangan = 'Stock Opname Barang';
				$kartu->created_by = $CI->auth->userid();
		        $kartu->created_at = date('Y-m-d H:i:s');
				$kartu->masuk 	  = $jumlahStock;
				$kartu->sisa 	  = $jumlahStock;
				$kartu->sisa_sum  = $stock - $jumlahMasuk;
		        $CI->db->insert($table_def_kartu_stock, $kartu);
			}
		}
	}
}