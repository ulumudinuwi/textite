<?php
	function saveDetailStock($obj, $settingObat, $sVar) {
		$CI =& get_instance();
		$CI->load->model('satelite/Satelite_stock_detail_model');
		$type_masuk = 1;
	    $type_keluar = 2;
	    $type_so = 3;
	    $type_another = 4;
	    $type_afkir = 5;
	    $dataModeAdd = 1;
	    $dataModeEdit = 2;
	    $dataModeDelete = 3;

        foreach ($obj->obats as $obat) {
        	$CI->db->where('unit_usaha_id', $CI->session->userdata('unit_usaha_id'));
            $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
            $CI->db->where('obat_id', $obat->obat_id);
            $stock = $CI->db->get($sVar->table_def_satelite_gudang)->row();

            switch ($obat->data_mode) {
                case $dataModeAdd: # ADD
                    $data = get_object_vars($obat);
                    $data[$sVar->key_id] = $obj->transaksi->id;
                    $data['created_by'] = $CI->session->userdata('auth_user');
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['update_by'] = $CI->session->userdata('auth_user');
                    $data['update_at'] = date('Y-m-d H:i:s');
                    unset($data['data_mode']);

                    $CI->db->insert($sVar->table_def_detail_obat, $data);
                    $obat->id = $CI->db->insert_id();

                    // Jika Quantity Melebihi dari settingan atau tidak ada dalam settingan maka masukan ke tagihan.
                    $obatNotExists = ! array_key_exists($obat->obat_id, $settingObat);
                    $obatLebih = array_key_exists($obat->obat_id, $settingObat) ? $obat->quantity > $settingObat[$obat->obat_id]->quantity : false;
                    $quantityLebih = array_key_exists($obat->obat_id, $settingObat) ? $obat->quantity - $settingObat[$obat->obat_id]->quantity : 0;

                    if ($obatNotExists) {
                        $obatRow = $CI->db->select('harga_beli as harga')
                                        ->where('id', $obat->obat_id)
                                        ->get('m_obat')
                                        ->row();
                        $quantityLebih = $obat->quantity;
                    }
                    $harga = array_key_exists($obat->obat_id, $settingObat) ? $settingObat[$obat->obat_id]->harga : $obatRow->harga;
                    // if ($obatNotExists || $obatLebih) {
                    //     $CI->db->set('uid', 'UUID()', FALSE);
                    //     $CI->db->insert('t_tindakan', [
                    //         'transaksi_id' => $obj->transaksi->id,
                    //         'transaksi_detail_id' => $obat->id,
                    //         'pelayanan_id' => $obj->transaksi->pelayanan_id,
                    //         'pasien_id' => $obj->pasien_id,
                    //         'tanggal' => date('Y-m-d H:i:s'),
                    //         'no_register' => $obj->kode_transaksi,
                    //         'layanan_id' => $obj->layanan_id,
                    //         'obat_id' => $obat->obat_id,
                    //         'tarif_pelayanan_id' => 0,
                    //         'quantity' => $quantityLebih,
                    //         'jasa_sarana' => $harga,
                    //         'jenis_tindakan' => $sVar->jenis_tindakan,
                    //         'is_bpjs' => 0,
                    //         'close' => 0,
                    //     ]);
                    // }

                    $sisa;
                    if ($stock) {
                        $sisa = $stock->jumlah - $obat->quantity;
                        $CI->db->where('id', $stock->id);
                        $CI->db->update($sVar->table_def_satelite_gudang, 
                            [
                                'jumlah' => $sisa
                            ]);

                        // Kartu Stock
                        $kartuStock = array(
                            'unit_usaha_id' => $CI->session->userdata('unit_usaha_id'),
                            'unit_kerja_id' => $CI->session->userdata('unit_kerja_id'),
                            'satelite_id' => $obj->transaksi->id,
                            'satelite_gudang_id' => $stock->id,
                            'tanggal' => date('Y-m-d', strtotime($obj->transaksi->tanggal_ditindak)).' '.date('H:i:s'),
                            'no_ro' => $obj->transaksi->kode_transaksi,
                            'kamar_obat_id' => $obj->kamar_obat_id,
                            'obat_id' => $obat->obat_id,
                            'tipe' => $type_another,
                            'qty' => $obat->quantity,
                            'sisa' => $sisa,
                            'keterangan' => $sVar->keterangan,
                            'created_at' => date('Y-m-d H:i:s'),
                            'created_by' => $CI->session->userdata('auth_user'),
                            'update_at' => date('Y-m-d H:i:s'),
                            'update_by' => $CI->session->userdata('auth_user'),
                        );

                        $CI->db->insert($sVar->table_def_satelite_kartu_stock, $kartuStock);

                        # Get & Update No. Batch & exp. date
		                $aDataBarang = array();
		                $i = 0;
		                $temp_qty = $obat->quantity;
		                do {
		                    $oStock = $CI->Satelite_stock_detail_model->get_by("WHERE ({$sVar->table_def_stock_ed}.satelite_gudang_id = {$stock->id} AND {$sVar->table_def_stock_ed}.sisa > 0) AND DATE(expired_date) >= NOW() ORDER BY {$sVar->table_def_stock_ed}.expired_date ASC");
		                    if(empty($oStock)) {
		                    	$oStock = $CI->Satelite_stock_detail_model->get_by("WHERE ({$sVar->table_def_stock_ed}.satelite_gudang_id = {$stock->id} AND {$sVar->table_def_stock_ed}.sisa > 0) ORDER BY {$sVar->table_def_stock_ed}.expired_date ASC");
		                    }

		                    if($oStock) {
		                        $temp_stock_sisa = $temp_qty - $oStock->sisa;
		                        $stock_sisa = $temp_qty - $oStock->sisa;
		                        if($stock_sisa > 0) {
		                            $stock_sisa = 0;
		                            $sendJumlah = $oStock->sisa;
		                        } else {
		                            $stock_sisa = $oStock->sisa - $temp_qty;
		                            $sendJumlah = $temp_qty;
		                        }
		                        $temp_qty = $temp_stock_sisa;
		                        $CI->db->set('sisa', $stock_sisa);
		                        $CI->db->where('id', $oStock->id);
		                        $CI->db->update($sVar->table_def_stock_ed);

		                        # Kartu Persediaan
		                        $kartu = new stdClass();
		                        $kartu->unit_usaha_id = $CI->session->userdata('unit_usaha_id');
		                        $kartu->unit_kerja_id = $CI->session->userdata('unit_kerja_id');
		                        $kartu->satelite_id = $obj->transaksi->id;
		                        $kartu->satelite_gudang_id = $stock->id;
		                        $kartu->tanggal = date('Y-m-d', strtotime($obj->transaksi->tanggal_ditindak)).' '.date('H:i:s');
		                        $kartu->no_ro   = $obj->transaksi->kode_transaksi;
		                        $kartu->tipe    = $type_another;
		                        $kartu->obat_id = $obat->obat_id;
		                        $kartu->kamar_obat_id = $obj->kamar_obat_id;
		                        $kartu->qty       = $sendJumlah;
		                        $kartu->sisa      = $stock_sisa;
		                        $kartu->harga = $oStock->harga;
		                        $kartu->keterangan = $sVar->keterangan;
		                        $kartu->created_by = $CI->session->userdata('auth_user');
		                        $kartu->created_at = date('Y-m-d H:i:s');
		                        $CI->db->insert($sVar->table_def_kartu_persediaan, $kartu);

		                        # detail barang
		                        $data_barang = new StdClass();
		                        $data_barang->id            = $oStock->id;
		                        $data_barang->expired_date  = $oStock->expired_date;
		                        $data_barang->no_batch      = $oStock->no_batch;
		                        $data_barang->harga         = $oStock->harga;
		                        $data_barang->jumlah        = $sendJumlah;
		                        $data_barang->sisa          = $sendJumlah;
		                        $aDataBarang[$i] = $data_barang;

		                        $i++;
		                    } else {
		                        break;
		                    } 
		                } while ($temp_qty > 0);

		                $dJsonData = new StdClass();
		                $dJsonData->detail_barang = $aDataBarang;
		                $CI->db->set('json_data', json_encode($dJsonData));
		                $CI->db->where('id', $obat->id);
		                $CI->db->update($sVar->table_def_detail_obat);
                    }

                    break;
                case $dataModeEdit: # Update
                    $data = get_object_vars($obat);
                    $data['update_by'] = $CI->session->userdata('auth_user');
                    $data['update_at'] = date('Y-m-d H:i:s');
                    unset($data['id']);
                    unset($data['data_mode']);
                    unset($data['old_quantity']);

                    $CI->db->where($sVar->key_id, $obj->transaksi->id);
                    $CI->db->where('obat_id', $obat->obat_id);
                    $CI->db->update($sVar->table_def_detail_obat, $data);

	                $CI->db->where('satelite_id', $obj->transaksi->id);
                    $CI->db->where('satelite_gudang_id', $stock->id);
                    $CI->db->where('no_ro', $obj->transaksi->kode_transaksi);
                    $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
                    $CI->db->where('obat_id', $obat->obat_id);
		            $CI->db->where('tipe', $type_another);
                    $CI->db->where('keterangan', $sVar->keterangan);
		            $rKartuStock = $CI->db->get($sVar->table_def_satelite_kartu_stock)->row();

		            if($obat->quantity != $obat->old_quantity) {
		            	if(!empty($obat->json_data)) {
			            	$jsonData = json_decode($obat->json_data);
			            	if(property_exists($jsonData, 'detail_barang')) {
				                if(count($jsonData->detail_barang) > 0) {
				                    foreach ($jsonData->detail_barang as $value) { 
				                        $CI->db->set('sisa', 'sisa + '.$value->sisa, FALSE);
				                        $CI->db->where('id', $value->id);
				                        $CI->db->update($sVar->table_def_stock_ed);   

				                        # get data row Kartu Persediaan
				                        $CI->db->where('satelite_id', $obj->transaksi->id);
					                    $CI->db->where('satelite_gudang_id', $stock->id);
					                    $CI->db->where('no_ro', $obj->transaksi->kode_transaksi);
					                    $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
					                    $CI->db->where('harga', $value->harga);
					                    $CI->db->where('obat_id', $obat->obat_id);
							            $CI->db->where('tipe', $type_another);
					                    $CI->db->where('keterangan', $sVar->keterangan);
				                        $rKP = $CI->db->get($sVar->table_def_kartu_persediaan)->row();

				                        # update data Kartu Persediaan
				                        $sisa = ($rKP->sisa + $rKP->qty);
				                        $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
				                        $CI->db->where('harga', $value->harga);
				                        $CI->db->where('obat_id', $obat->obat_id);
				                        $CI->db->where('id >', $rKP->id);
				                        $lKP = $CI->db->get($sVar->table_def_kartu_persediaan);

				                        if($lKP->num_rows() > 0) {
				                            foreach ($lKP->result() as $ks) {
				                                if($ks->tipe == $type_keluar || $ks->tipe == $type_afkir || $ks->tipe == $type_another) {
				                                    $sisa = $sisa - $ks->qty;
				                                } else if($ks->tipe == $type_masuk){
				                                    $sisa = $sisa + $ks->qty;
				                                } else if($ks->tipe == $type_so) {
				                                    $sisa = $ks->qty;
				                                }
				                                # update data sisa selelah id awal yang diupdate
				                                $CI->db->set('sisa', $sisa);
				                                $CI->db->where('id', $ks->id);
				                                $CI->db->update($sVar->table_def_kartu_persediaan);
				                            }
				                        }
				                        # delete data Kartu Persediaan
				                        $CI->db->where('id', $rKP->id);
				                        $CI->db->delete($sVar->table_def_kartu_persediaan);
				                    }
				                }
				            }
		            	}

		            	$CI->db->where('unit_usaha_id', $CI->session->userdata('unit_usaha_id'));
	                    $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
	                    $CI->db->where('obat_id', $obat->obat_id);
	                    $stock = $CI->db->get($sVar->table_def_satelite_gudang)->row();

	                    # Get & Update No. Batch & exp. date
		                $aDataBarang = array();
		                $i = 0;
		                $temp_qty = $obat->quantity;
		                do {
		                    $oStock = $CI->Satelite_stock_detail_model->get_by("WHERE ({$sVar->table_def_stock_ed}.satelite_gudang_id = {$stock->id} AND {$sVar->table_def_stock_ed}.sisa > 0) AND DATE(expired_date) >= NOW() ORDER BY {$sVar->table_def_stock_ed}.expired_date ASC");
		                    if(empty($oStock)) {
		                    	$oStock = $CI->Satelite_stock_detail_model->get_by("WHERE ({$sVar->table_def_stock_ed}.satelite_gudang_id = {$stock->id} AND {$sVar->table_def_stock_ed}.sisa > 0) ORDER BY {$sVar->table_def_stock_ed}.expired_date ASC");
		                    }

		                    if($oStock) {
		                        $temp_stock_sisa = $temp_qty - $oStock->sisa;
		                        $stock_sisa = $temp_qty - $oStock->sisa;
		                        if($stock_sisa > 0) {
		                            $stock_sisa = 0;
		                            $sendJumlah = $oStock->sisa;
		                        } else {
		                            $stock_sisa = $oStock->sisa - $temp_qty;
		                            $sendJumlah = $temp_qty;
		                        }
		                        $temp_qty = $temp_stock_sisa;
		                        $CI->db->set('sisa', $stock_sisa);
		                        $CI->db->where('id', $oStock->id);
		                        $CI->db->update($sVar->table_def_stock_ed);

		                        # Kartu Persediaan
		                        $kartu = new stdClass();
		                        $kartu->unit_usaha_id = $CI->session->userdata('unit_usaha_id');
		                        $kartu->unit_kerja_id = $CI->session->userdata('unit_kerja_id');
		                        $kartu->satelite_id = $obj->transaksi->id;
		                        $kartu->satelite_gudang_id = $stock->id;
		                        $kartu->tanggal = date('Y-m-d', strtotime($obj->transaksi->tanggal_ditindak)).' '.date('H:i:s');
		                        $kartu->no_ro   = $obj->transaksi->kode_transaksi;
		                        $kartu->tipe    = $type_another;
		                        $kartu->obat_id = $obat->obat_id;
		                        $kartu->kamar_obat_id = $obj->kamar_obat_id;
		                        $kartu->qty       = $sendJumlah;
		                        $kartu->sisa      = $stock_sisa;
		                        $kartu->harga = $oStock->harga;
		                        $kartu->keterangan = $sVar->keterangan;
		                        $kartu->created_by = $CI->session->userdata('auth_user');
		                        $kartu->created_at = date('Y-m-d H:i:s');
		                        $CI->db->insert($sVar->table_def_kartu_persediaan, $kartu);

		                        # detail barang
		                        $data_barang = new StdClass();
		                        $data_barang->id            = $oStock->id;
		                        $data_barang->expired_date  = $oStock->expired_date;
		                        $data_barang->no_batch      = $oStock->no_batch;
		                        $data_barang->harga         = $oStock->harga;
		                        $data_barang->jumlah        = $sendJumlah;
		                        $data_barang->sisa          = $sendJumlah;
		                        $aDataBarang[$i] = $data_barang;

		                        $i++;
		                    } else {
		                        break;
		                    } 
		                } while ($temp_qty > 0);

		                $dJsonData = new StdClass();
		                $dJsonData->detail_barang = $aDataBarang;

		                $CI->db->set('json_data', json_encode($dJsonData));
		                $CI->db->where('id', $obat->id);
		                $CI->db->update($sVar->table_def_detail_obat);

		                # update data kartu stock
		                $sisa = ($rKartuStock->sisa + $obat->old_quantity) - $obat->quantity;
		                $CI->db->set('qty', $obat->quantity);
		                $CI->db->set('sisa', $sisa);
		                $CI->db->set('update_by', $CI->session->userdata('auth_user'));
		                $CI->db->set('update_at', date('Y-m-d H:i:s'));
		                $CI->db->where('id', $rKartuStock->id);
		                $CI->db->update($sVar->table_def_satelite_kartu_stock);

		                $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
		                $CI->db->where('obat_id', $obat->obat_id);
		                $CI->db->where('id >', $rKartuStock->id);
		                $lKartuStock = $CI->db->get($sVar->table_def_satelite_kartu_stock);

		                if($lKartuStock->num_rows() > 0) {
		                    foreach ($lKartuStock->result() as $ks) {
		                        if($ks->tipe == $type_keluar || $ks->tipe == $type_afkir || $ks->tipe == $type_another) {
		                            $sisa = $sisa - $ks->qty;
		                        } else if($ks->tipe == $type_masuk){
		                            $sisa = $sisa + $ks->qty;
		                        } else if($ks->tipe == $type_so) {
		                            $sisa = $ks->qty;
		                        }
		                        # update data sisa selelah id awal yang diupdate
		                        $CI->db->set('sisa', $sisa);
		                        $CI->db->where('id', $ks->id);
		                        $CI->db->update($sVar->table_def_satelite_kartu_stock);
		                    }
		                }

		                # update data stock terakhir
		                $CI->db->set('jumlah', $sisa);
		                $CI->db->where('id', $stock->id);
		                $CI->db->update($sVar->table_def_satelite_gudang);

		                // Jika Quantity Melebihi dari settingan atau tidak ada dalam settingan maka masukan ke tagihan.
	                    $obatNotExists = ! array_key_exists($obat->obat_id, $settingObat);
	                    $obatLebih = array_key_exists($obat->obat_id, $settingObat) ? $obat->quantity > $settingObat[$obat->obat_id]->quantity : false;
	                    $quantityLebih = array_key_exists($obat->obat_id, $settingObat) ? $obat->quantity - $settingObat[$obat->obat_id]->quantity : 0;

	                    if ($obatNotExists) {
	                        $obatRow = $CI->db->select('harga_beli as harga')
	                                        ->where('id', $obat->obat_id)
	                                        ->get('m_obat')
	                                        ->row();
	                        $quantityLebih = $obat->quantity;
	                    }
	                    $harga = array_key_exists($obat->obat_id, $settingObat) ? $settingObat[$obat->obat_id]->harga : $obatRow->harga;

	                    $tindakanRow = $CI->db->where('transaksi_id', $obj->id)
	                                        ->where('transaksi_detail_id', $obat->id)
	                                        ->where('pelayanan_id', $obj->transaksi->pelayanan_id)
	                                        ->where('obat_id', $obat->obat_id)
	                                        ->where('jenis_tindakan', $sVar->jenis_tindakan)
	                                        ->get('t_tindakan')
	                                        ->row();
	                    //
	                    if (! $tindakanRow) {
	                        if ($obatNotExists || $obatLebih) {
	                            $CI->db->set('uid', 'UUID()', FALSE);
	                            $CI->db->insert('t_tindakan', [
	                                'transaksi_id' => $obj->transaksi->id,
	                                'transaksi_detail_id' => $obat->id,
	                                'pelayanan_id' => $obj->transaksi->pelayanan_id,
	                                'pasien_id' => $obj->pasien_id,
	                                'tanggal' => date('Y-m-d H:i:s'),
	                                'no_register' => $obj->transaksi->kode_transaksi,
	                                'layanan_id' => $obj->layanan_id,
	                                'obat_id' => $obat->obat_id,
	                                'tarif_pelayanan_id' => 0,
	                                'quantity' => $quantityLebih,
	                                'jasa_sarana' => $harga,
	                                'jenis_tindakan' => $sVar->jenis_tindakan,
	                                'is_bpjs' => 0,
	                                'close' => 0,
	                            ]);
	                        }
	                    } else {
	                        if (($obatNotExists || $obatLebih) && $tindakanRow->close == 0) {
	                            $CI->db->where('id', $tindakanRow->id);
	                            $CI->db->update('t_tindakan', [
	                                'quantity' => $quantityLebih,
	                                'jasa_sarana' => $harga
	                            ]);
	                        } else {
	                            $CI->db->where('id', $tindakanRow->id);
	                            $CI->db->delete('t_tindakan');
	                        }
	                    }
		            }
                    break;
                case $dataModeDelete: # Delete
                    $CI->db->where('id', $obat->id);
                    $CI->db->delete($sVar->table_def_detail_obat);

                    if(!empty($obat->json_data)) {
	                    $dJsonData = json_decode($obat->json_data);
	                    if(property_exists($dJsonData, 'detail_barang')) {
				            if(is_array($dJsonData->detail_barang) && count($dJsonData->detail_barang) > 0) {
				                foreach ($dJsonData->detail_barang as $data_barang) {
				                    $oStock = $CI->Satelite_stock_detail_model->get_by("WHERE {$sVar->table_def_stock_ed}.id = {$data_barang->id}");
				                    if($oStock) {
				                        $CI->db->set('sisa', 'sisa + '.$data_barang->sisa, FALSE);
				                        $CI->db->where('id', $oStock->id);
				                        $CI->db->update($sVar->table_def_stock_ed);

				                        # get data row Kartu Persediaan
				                        $CI->db->where('satelite_id', $obj->transaksi->id);
					                    $CI->db->where('satelite_gudang_id', $stock->id);
					                    $CI->db->where('no_ro', $obj->transaksi->kode_transaksi);
					                    $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
					                    $CI->db->where('harga', $data_barang->harga);
					                    $CI->db->where('obat_id', $obat->obat_id);
							            $CI->db->where('tipe', $type_another);
					                    $CI->db->where('keterangan', $sVar->keterangan);
				                        $rKP = $CI->db->get($sVar->table_def_kartu_persediaan)->row();

				                        # update data Kartu Persediaan
				                        $sisa = ($rKP->sisa + $rKP->qty);
				                        $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
				                        $CI->db->where('harga', $data_barang->harga);
				                        $CI->db->where('obat_id', $obat->obat_id);
				                        $CI->db->where('id >', $rKP->id);
				                        $lKP = $CI->db->get($sVar->table_def_kartu_persediaan);

				                        if($lKP->num_rows() > 0) {
				                            foreach ($lKP->result() as $ks) {
				                                if($ks->tipe == $type_keluar || $ks->tipe == $type_afkir || $ks->tipe == $type_another) {
				                                    $sisa = $sisa - $ks->qty;
				                                } else if($ks->tipe == $type_masuk){
				                                    $sisa = $sisa + $ks->qty;
				                                } else if($ks->tipe == $type_so) {
				                                    $sisa = $ks->qty;
				                                }
				                                # update data sisa selelah id awal yang diupdate
				                                $CI->db->set('sisa', $sisa);
				                                $CI->db->where('id', $ks->id);
				                                $CI->db->update($sVar->table_def_kartu_persediaan);
				                            }
				                        }
				                        # delete data Kartu Persediaan
				                        $CI->db->where('id', $rKP->id);
				                        $CI->db->delete($sVar->table_def_kartu_persediaan);
				                    }
				                }
				            }
				        }
                    }

                    $CI->db->where('satelite_id', $obj->transaksi->id);
                    $CI->db->where('satelite_gudang_id', $stock->id);
                    $CI->db->where('no_ro', $obj->transaksi->kode_transaksi);
                    $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
                    $CI->db->where('obat_id', $obat->obat_id);
		            $CI->db->where('tipe', $type_another);
                    $CI->db->where('keterangan', $sVar->keterangan);
		            $rKartuStock = $CI->db->get($sVar->table_def_satelite_kartu_stock)->row();

		            # update data kartu stock
		            $sisa = ($rKartuStock->sisa + $rKartuStock->qty);
		            $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
		            $CI->db->where('obat_id', $obat->obat_id);
		            $CI->db->where('id >', $rKartuStock->id);
		            $lKartuStock = $CI->db->get($sVar->table_def_satelite_kartu_stock);

                    // UPDATE Potong Stock di satelite
                    $CI->db->where('unit_usaha_id', $CI->session->userdata('unit_usaha_id'));
                    $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
                    $CI->db->where('obat_id', $obat->obat_id);
                    $stock = $CI->db->get($sVar->table_def_satelite_gudang)->row();

                    if($lKartuStock->num_rows() > 0) {
		                foreach ($lKartuStock->result() as $ks) {
		                    if($ks->tipe == $type_keluar || $ks->tipe == $type_afkir || $ks->tipe == $type_another) {
		                        $sisa = $sisa - $ks->qty;
		                    } else if($ks->tipe == $type_masuk){
		                        $sisa = $sisa + $ks->qty;
		                    } else if($ks->tipe == $type_so) {
		                        $sisa = $ks->qty;
		                    }
		                    # update data sisa selelah id awal yang diupdate
		                    $CI->db->set('sisa', $sisa);
		                    $CI->db->where('id', $ks->id);
		                    $CI->db->update($sVar->table_def_satelite_kartu_stock);
		                }
		            }
		            # delete data kartu stock
		            $CI->db->where('id', $rKartuStock->id);
		            $CI->db->delete($sVar->table_def_satelite_kartu_stock);

		            # update data stock terakhir
		            $CI->db->set('jumlah', $sisa);
		            $CI->db->where('kamar_obat_id', $obj->kamar_obat_id);
		            $CI->db->where('obat_id', $obat->obat_id);
		            $CI->db->update($sVar->table_def_satelite_gudang);

                    // Hapus di tindakan jika status close masih 0
                    $CI->db->where('transaksi_id', $obj->id);
                    $CI->db->where('transaksi_detail_id', $obat->id);
                    $CI->db->where('pelayanan_id', $obj->transaksi->pelayanan_id);
                    $CI->db->where('obat_id', $obat->obat_id);
                    $CI->db->where('jenis_tindakan', $sVar->jenis_tindakan);
                    $CI->db->where('close', '0');
                    $CI->db->delete('t_tindakan');
                    break;
            }
        }
    }
?>