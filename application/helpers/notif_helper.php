<?php 
    function insertNotif($data) {
        $CI =& get_instance();

        $dataInsert = array(
            'role_id' => $data->role_id,
            'description' => $data->description,
            'link' => $data->link,
            'created_at' => date('Y-m-d H:i:s'),
        );
        $CI->db->set('uid', 'UUID()', FALSE);
        $insert = $CI->db->insert('t_notifikasi', $dataInsert);

        return true;
    }
?>