<?php
if(! function_exists('getHargaDasar')) {
	function getHargaDasar($def_id = 0, $browse = "gudang_farmasi") {
		$table_def_gudang_farmasi_detail = 't_gudang_farmasi_stock_detail';
		$table_def_gudang_farmasi = 't_gudang_farmasi_stock';
		$table_def_logistik_detail = 't_logistik_stock_detail';
		$table_def_logistik = 't_logistik_stock';
		$table_def_farmasi_detail = 't_farmasi_stock_detail';
		$table_def_farmasi = 't_farmasi_stock';

		$CI =& get_instance();
		$CI->load->helper('options');

		$metodeHarga = get_option('set_metode_harga_barang') ? : 'max';
		switch ($metodeHarga) {
			case 'min':
				$sqlHarga = "IFNULL(MIN(harga), 0)";
				break;
			case 'avg':
				$sqlHarga = "IFNULL(AVG(harga), 0)";
				break;
			default:
				$sqlHarga = "IFNULL(MAX(harga), 0)";
				break;
		}

		switch ($browse) {
			/*case 'farmasi':
				$table_detail = $table_def_farmasi_detail;
				$table = $table_def_farmasi;
				break;*/
			case 'logistik':
				$table_detail = $table_def_logistik_detail;
				$table = $table_def_logistik;
				break;
			default:
				$table_detail = $table_def_gudang_farmasi_detail;
				$table = $table_def_gudang_farmasi;
				break;
		}

		$harga = 0;
		if($browse == "farmasi") {
			$dataStock = $CI->db->query("SELECT id FROM {$table} WHERE barang_id = {$def_id}")->row();
			if($dataStock) {
				$dataHarga = $CI->db->query("SELECT {$sqlHarga} harga FROM {$table_detail} WHERE stock_id = {$dataStock->id} AND qty > 0")->row();
				if($dataHarga->harga <= 0) 
					$dataHarga = $CI->db->query("SELECT (harga_pembelian / isi_satuan_penggunaan) harga FROM m_barang WHERE id = {$def_id}")->row();

				$harga = $dataHarga->harga;
			}
		} else {
			$dataHarga = $CI->db->query("SELECT {$sqlHarga} harga FROM {$table_detail} WHERE stock_id = {$def_id} AND qty > 0")->row();
			if($dataHarga->harga <= 0) 
				$dataHarga = $CI->db->query("SELECT (harga_pembelian / isi_satuan_penggunaan) harga FROM m_barang WHERE id = (SELECT barang_id FROM {$table} WHERE id = {$def_id})")->row();

			$harga = $dataHarga->harga;
		}
		return $harga;
	}
}