<?php

require_once('vendor/autoload.php');

function format_umur_satuan($satuan) {
    return $satuan == 1 ? 'Thn' : ($satuan == 2 ? 'Bln' : 'Hari');
}

function format_nilai_normal($nilai_normal = array()) {
    $content = array();
    foreach ($nilai_normal as $row) {
        $temp = '';
        $jenis_kelamin = '';
        $umur = '';
        $label = $row->nama;
        switch (intval($row->datatype)) {
            case 0: // Undefined
                $temp = $row->value;
                break;
            case 1: // Range
                $temp = $row->value;
                break;
            case 2: // Boolean
                $temp = intval($row->value) === 1 ? 'Positif' : 'Negatif';
                break;
            case 3: // Text
                $temp = $row->value;
                break;
            case 4: // LESSTHAN
                $temp = '< ' . $row->value;
                break;
            case 5: // GREATERTHAN
                $temp = '> ' . $row->value;
                break;
            case 6: // LESSTHANEQUAL
                $temp = '<= ' . $row->value;
                break;
            case 7: // GREATERTHANEQUAL
                $temp = '>= ' . $row->value;
                break;
        }

        if (intval($row->jenis_kelamin) == 1) { // Laki-Laki
            $jenis_kelamin = 'Laki-Laki';
        } else if (intval($row->jenis_kelamin) == 2) { // Perempuan
            $jenis_kelamin = 'Perempuan';
        }

        $umur_min = intval($row->umur_min);
        $umur_min_satuan = intval($row->umur_min_satuan);
        $umur_max = intval($row->umur_max);
        $umur_max_satuan = intval($row->umur_max_satuan);

        if ($umur_min == $umur_max && $umur_min_satuan == $umur_max_satuan && $umur_min == 0) {
            $umur = '';
        } else if ($umur_min == 0 && $umur_max == 0) {
            $umur = '';
        } else if ($umur_min == 0 && $umur_max >= 0) {
            $umur = '< ' . $umur_max . ' ' . format_umur_satuan($umur_max_satuan);
        } else if ($umur_min >= 0 && $umur_max == 0) {
            $umur = '> ' . $umur_min . ' ' . format_umur_satuan($umur_min_satuan);
        } else if ($umur_min == $umur_max && $umur_min_satuan == $umur_max_satuan) {
            $umur = $umur_min . ' ' . format_umur_satuan($umur_min_satuan);
        } else {
            $umur = $umur_min . ' ' . format_umur_satuan($umur_min_satuan) . ' - ' . $umur_max . ' ' . format_umur_satuan($umur_max_satuan);
        }

        $content[] = ($label ? $label : '') . ' ' . ($umur ? '(' . $umur . ')' : '') . ($label || $umur ? ': ' : '') . $temp;
    }

    return implode("\r\n", $content);
}

function laboratorium_get_nilai_normal_datatype($nilai_normal = array()) {
    $content = array();
    $datatype = "undefined";
    foreach ($nilai_normal as $row) {
        switch (intval($row->datatype)) {
            case 0: // Undefined
                $datatype = "undefined";
                break;
            case 1: // Range
                $datatype = "range";
                break;
            case 2: // Boolean
                $datatype = "boolean";
                break;
            case 3: // Text
                $datatype = "text";
                break;
            case 4: // LESSTHAN
                $datatype = "lt";
                break;
            case 5: // GREATERTHAN
                $datatype = "gt";
                break;
            case 6: // LESSTHANEQUAL
                $datatype = "lte";
                break;
            case 7: // GREATERTHANEQUAL
                $datatype = "gte";
                break;
        }
    }

    return $datatype;
}

function laboratorium_parse_tindakan($rawData, $stack = array()) {
    if (count($rawData) <= 1) return $rawData;
    $data = array();
    $stack = array();
    foreach ($rawData as $node) {
        // print_r($node);
        while (count($stack) && ($peak = $stack[count($stack) -1]) && $stack[count($stack) -1]->rgt < $node->lft) {
            $peak = $stack[count($stack) -1];
            array_pop($stack);

            if (count($stack)) {
                if (array_search($peak, $stack[count($stack) -1]->children) == -1) {
                    $data[] = $peak;
                }
            } else {
                $data[] = $peak;
            }
        }

        if (count($stack)) {
            if ( !isset($stack[count($stack) -1]->children)) {
                $stack[count($stack) -1]->folder = true;
                $stack[count($stack) -1]->expanded = true;
                $stack[count($stack) -1]->children = array();
            }
            array_push($stack[count($stack) -1]->children, $node);
        }
        array_push($stack, $node);
    }
    // array_push($data, $node);
    if (count($stack)) {
        while(count($stack)) {
            $peak = array_pop($stack);

            if (count($stack)) {
                if (array_search($peak, $stack[count($stack) -1]->children) == -1) {
                    $data[] = $peak;
                }
            } else {
                $data[] = $peak;
            }
        }
    }

    return $data;
}

function laboratorium_get_tindakan_detail($obj, &$details) {
    $CI =& get_instance();
    $CI->load->model('master/laboratorium/Pemeriksaan_model');
    for ($i = 0, $n = count($details); $i < $n; $i++) {
        $pemeriksaanResult = $CI->db->where('tarif_pelayanan_id', $details[$i]->tarif_pelayanan_id)
                                    ->where('status', 1)
                                    ->get('m_laboratorium_pemeriksaan')
                                    ->result();
        $details[$i]->pemeriksaan = array();
        foreach ($pemeriksaanResult as $pemeriksaan) {
            $pemeriksaan->detail_id = $details[$i]->id;
            $details[$i]->pemeriksaan[] = laboratorium_parse_tindakan_detail($obj, $pemeriksaan);
        }

        if (property_exists($details[$i], 'children') && count($details[$i]->children) > 0) {
            laboratorium_get_tindakan_detail($obj, $details[$i]->children);
        }
    }
}

function laboratorium_parse_tindakan_detail($obj, $tindakan) {
    $CI =& get_instance();

    $detail = new stdClass();
    $detail->detail_id = $tindakan->detail_id;
    $detail->test_code = $tindakan->kode_hasil;
    $detail->test_name = $tindakan->nama;
    $detail->result_value = null;
    $detail->unit = $tindakan->satuan;
    $detail->flag = 'N';
    $detail->reference_range = format_nilai_normal(laboratorium_get_nilai_normal($obj, $tindakan));
    $detail->data_type = laboratorium_get_nilai_normal_datatype(laboratorium_get_nilai_normal($obj, $tindakan));
    $detail->status = 'F';
    $detail->test_comment = 'SIMRS iMedis';
    $detail->validated_by = $CI->session->userdata('auth_user');
    $detail->validated_on = date('YmdHis');
    $detail->title = $tindakan->nama;

    return $detail;
}

function laboratorium_get_nilai_normal($obj, $tindakan) {
    $CI =& get_instance();

    $from = new DateTime(date('Y-m-d', strtotime($obj->tgl_lahir)));
    $to = new DateTime('today');
    $umur_tahun = $from->diff($to)->y;
    $umur_bulan = $from->diff($to)->m;
    $umur_hari = $from->diff($to)->d;

    $nilaiNormalResult = $CI->db->where('pemeriksaan_id', $tindakan->id)
                                ->get('m_laboratorium_pemeriksaan_nilai_normal')
                                ->result();
    $nilai_normal = array();
    foreach ($nilaiNormalResult as $row) {
        $include = false;
        $include_jenis_kelamin = false;
        $include_umur_min = false;
        $include_umur_max = false;

        $jenis_kelamin = false;
        $umur_min = false;
        $umur_max = false;

        if ($row->jenis_kelamin != 0) {
            $include_jenis_kelamin = true;
            $row->jenis_kelamin = $row->jenis_kelamin == 1 ? 1 : 0;
        }

        if ($row->umur_min > 0) {
            $include_umur_min = true;
        }

        if ($row->umur_max > 0) {
            $include_umur_max = true;
        }

        if ($include_jenis_kelamin) {
            $jenis_kelamin = $obj->jenis_kelamin == $row->jenis_kelamin;
        }

        if ($include_umur_min) {
            switch ($row->umur_min_satuan) {
                case 1: # Thn
                    $umur_min = $umur_tahun >= $row->umur_min;
                    break;
                case 2: # Bln
                    $umur_min = $umur_tahun == 0 && $umur_bulan >= $row->umur_min;
                    break;
                case 3: # Hari
                    $umur_min = $umur_tahun == 0 && $umur_bulan == 0 && $umur_hari >= $row->umur_min;
                    break;
            }
        }

        if ($include_umur_max) {
            switch ($row->umur_max_satuan) {
                case 1: # Thn
                    $umur_max = $umur_tahun < $row->umur_max;
                    break;
                case 2: # Bln
                    $umur_max = $umur_tahun == 0 && $umur_bulan < $row->umur_max;
                    break;
                case 3: # Hari
                    $umur_max = $umur_tahun == 0 && $umur_bulan == 0 && $umur_hari < $row->umur_max;
                    break;
            }
        }


        if ($include_jenis_kelamin && !$jenis_kelamin) continue;
        if ($include_umur_min && !$umur_min) continue;
        if ($include_umur_max && !$umur_max) continue;

        $nilai_normal[] = $row;
    }

    return $nilai_normal;
}

function laboratorium_get_hasil_lis($obj, &$details) {
    $CI =& get_instance();
    $CI->load->model('laboratorium/Laboratorium_model');
    $CI->load->model('master/laboratorium/Pemeriksaan_model');
    $CI->load->model('master/Settings_model');

    # Settings
    $settings = $CI->Settings_model->get_all(0, 0, "WHERE m_settings.type = ".$CI->Settings_model::TYPE_LABORATORIUM, "", true);

    $lisHandler = $CI->Laboratorium_model->initializeLIS();
    foreach ($details as &$detail) {
        if (!property_exists($detail, 'tarif_pelayanan_id')) continue;
        $kodeObj = $CI->db->select('kode, kode_lis')->where('tarif_pelayanan_id', $detail->tarif_pelayanan_id)->get('m_laboratorium_kode')->row();

        if (! $kodeObj) continue;

        $kode = $settings['kode_lis'] ? $kodeObj->kode_lis : $kodeObj->kode;

        # is Detail has Childrens
        $hasChildren = isset($detail->children);

        switch ($settings['koneksi_lis']) {
            case 'off':
                break;
            default:
                $lis_id = 'SIMRS iMedis';

                if (isset($detail->lis_id) && !empty($detail->lis_id)) {
                    $result = json_decode($detail->hasil_lis);

                    if (count($result) > 1) {
                        $hasil_lis = array_map(function ($item) use ($detail) {
                            $item->detail_id = $detail->id;
                            return $item;
                        }, $result);

                        // if (isset($detail->children)) {
                        //     $detail->children = array_merge($hasil_lis, $detail->children);
                        // } else {
                        //     $detail->children = $hasil_lis;
                        // }
                    }
                } else {
                    $result = $CI->Laboratorium_model->getResultDetail($obj->kode_transaksi, $kode);
                    if ($settings['koneksi_lis'] == 'file') {
                        $lis_id = $lisHandler->getResultId();
                    }

                    if (count($result)) {
                        $detail->hasil_lis = array_map(function ($item) {
                            $item->unit = htmlentities($item->unit);
                            return $item;
                        }, $result);
                        $detail->lis_id = $lis_id;

                        $hasil_lis = array_map(function ($item) use ($detail) {
                            $item->detail_id = $detail->id;
                            return $item;
                        }, $result);

                        // if (isset($detail->children)) {
                        //     $detail->children = array_merge($hasil_lis, $detail->children);
                        // } else {
                        //     $detail->children = $hasil_lis;
                        // }
                    } else {
                        $pemeriksaanResult = $CI->db->where('tarif_pelayanan_id', $detail->tarif_pelayanan_id)
                                                    ->where('status', 1)
                                                    ->get('m_laboratorium_pemeriksaan')
                                                    ->result();
                        $detail->pemeriksaan = array();
                        foreach ($pemeriksaanResult as $pemeriksaan) {
                            $pemeriksaan->detail_id = $detail->id;
                            $detail->pemeriksaan[] = laboratorium_parse_tindakan_detail($obj, $pemeriksaan);
                            $detail->lis_id = null;
                        }
                    }
                }
                break;
        }

        if ($hasChildren) {
            $detail->children = laboratorium_get_hasil_lis($obj, $detail->children);
        }
    }

    return $details;
}

/**
 * List Obat Per Jenis Pemeriksaan yang quantity satelite lebih dari quantity yang diperlukan Jenis Pemeriksaan
 */
function laboratorium_get_obat_pemeriksaan($obj, $patologi = false) {
    $CI =& get_instance();

    # Layanan
    $sql = "SELECT id, tarip_layanan_id FROM m_layanan WHERE jenis = 4 AND nama = 'Laboratorium'";
    $query = $CI->db->query($sql);
    $layanan = $query->row();
    $layanan_laboratorium_id = $layanan->id;

    # Kamar Obat
    // $sql = "SELECT * FROM m_kamar_obat WHERE layanan_id = ".$layanan_laboratorium_id;
    // $query = $CI->db->query($sql);
    // $kamar_obat = $query->row();
    $kamar_obat = get_kamar_obat($layanan_laboratorium_id);

    $result = laboratorium_get_obat_pemeriksaan_loop($obj, $obj->tindakan, $kamar_obat, $patologi);
    $obats = array();
    foreach ($result as $row) {
        if (array_key_exists($row->obat_id, $obats)) {
            $obats[$row->obat_id]->quantity += $row->quantity;
        } else {
            $obats[$row->obat_id] = $row;
        }
    }

    return array_values($obats);
}

function laboratorium_get_obat_pemeriksaan_loop($obj, $tindakans, $kamar_obat, $patologi = false) {
    $obats = array();
    foreach ($tindakans as $tindakan) {
        $obats = array_merge($obats, laboratorium_get_obat_pemeriksaan_get($obj, $tindakan, $kamar_obat, $patologi));

        if (property_exists($tindakan, 'children') && count($tindakan->children) > 0) {
            $obats = array_merge($obats, laboratorium_get_obat_pemeriksaan_loop($obj, $tindakan->children, $kamar_obat, $patologi));
        }
    }

    return $obats;
}

function laboratorium_get_obat_pemeriksaan_get($obj, $tindakan, $kamar_obat, $patologi = false) {
    $isBmhpActive = get_setting('opt_penggunaan_bmhp');

    // if ($isBmhpActive == 0) return [];

    $CI =& get_instance();
    $tarif_pelayanan_id = $tindakan->tarif_pelayanan_id;

    if (! $patologi) {
        $aSelect = array(
            '0 as id',
            $obj->id.' as laboratorium_id',
            'm_laboratorium_obat.obat_id',
            'm_obat.kode as obat_kode',
            'm_obat.nama as obat',
            'm_laboratorium_obat.quantity as quantity',
            'm_obat.satuan_id as satuan_id',
            'm_satuan.nama as satuan',
            'IFNULL(t_satelite_gudang.jumlah, 0) as tersedia',
        );
        $query = $CI->db->select(implode(',', $aSelect))
                        ->from('m_laboratorium_obat')
                        ->join('m_obat', 'm_laboratorium_obat.obat_id = m_obat.id', 'left')
                        ->join('m_satuan', 'm_obat.satuan_id = m_satuan.id', 'left')
                        ->join('t_satelite_gudang', 't_satelite_gudang.obat_id = m_laboratorium_obat.obat_id', 'left')
                        ->where('m_laboratorium_obat.status', 1)
                        // ->where('t_satelite_gudang.jumlah >= m_laboratorium_obat.quantity', NULL, FALSE)
                        ->where('t_satelite_gudang.kamar_obat_id', $kamar_obat->id)
                        ->where('m_laboratorium_obat.tarif_pelayanan_id', $tarif_pelayanan_id)
                        ->get();
    } else {
        $sql = "SELECT id, tarip_layanan_id FROM m_layanan WHERE jenis = 4 AND nama = 'Laboratorium Patologi'";
        $query = $CI->db->query($sql);
        $layanan = $query->row();

        $aSelect = array(
            '0 as id',
            $obj->id.' as laboratorium_id',
            'm_tindakan_obat.obat_id',
            'm_obat.kode as obat_kode',
            'm_obat.nama as obat',
            'm_tindakan_obat.quantity as quantity',
            'm_obat.satuan_id as satuan_id',
            'm_satuan.nama as satuan',
        );
        $query = $CI->db->select(implode(',', $aSelect))
                        ->from('m_tindakan_obat')
                        ->join('m_obat', 'm_tindakan_obat.obat_id = m_obat.id', 'left')
                        ->join('m_satuan', 'm_obat.satuan_id = m_satuan.id', 'left')
                        ->join('t_satelite_gudang', 't_satelite_gudang.obat_id = m_tindakan_obat.obat_id', 'left')
                        ->where('m_tindakan_obat.status', 1)
                        // ->where('t_satelite_gudang.jumlah >= m_tindakan_obat.quantity', NULL, FALSE)
                        ->where('t_satelite_gudang.kamar_obat_id', $kamar_obat->id)
                        ->where('m_tindakan_obat.layanan_id', $layanan->id)
                        ->where('m_tindakan_obat.tarif_pelayanan_id', $tarif_pelayanan_id)
                        ->get();
    }

    return $query->result();
}

/**
 * Fungsi untuk menentukan apakah hasil pemeriksaan melebihi nilai normal atau tidak
 */
function laboratorium_get_flag_pemeriksaan($details) {
    foreach ($details as $detail) {
        if (property_exists($detail, 'hasil_lis'))
        if (stripos($detail->hasil_lis, '[') !== FALSE) {
            $resultHasil = json_decode($detail->hasil_lis);
            foreach ($resultHasil as $hasil) {
                switch ($hasil->data_type) {
                    case "undefined": // Undefined
                        $value = $hasil->result_value;
                        $reference_value = explode(': ', $hasil->reference_range);
                        $reference_value = array_pop($reference_value);
                        if (stripos($reference_value, $value) !== FALSE) {
                            $hasil->flag = "N";
                        } else {
                            $hasil->flag = "NN";
                        }
                        break;
                    case "range": // Range
                        $value = $hasil->result_value;
                        $reference_value = explode(': ', $hasil->reference_range);
                        $reference_value = array_pop($reference_value);
                        list($min, $max) = explode(' - ', $reference_value);

                        // Convert Value
                        $value = floatval($value);
                        $min = floatval($min);
                        $max = floatval($max);
                        if ($value >= $min && $value <= $max) { // Normal
                            $hasil->flag = "N";
                        } elseif ($value < $min) {
                            $hasil->flag = "L";
                        } else {
                            $hasil->flag = "H";
                        }
                        break;
                    case "boolean": // Boolean
                        $value = $hasil->result_value;
                        $reference_value = explode(': ', $hasil->reference_range);
                        $reference_value = array_pop($reference_value);
                        $reference_value = trim($reference_value);

                        if (stripos($reference_value, $value) !== FALSE) {
                            $hasil->flag = 'N';
                        } else {
                            $hasil->flag = 'NN';
                        }
                        break;
                    case "text": // Text
                        $value = $hasil->result_value;
                        $reference_value = explode(': ', $hasil->reference_range);
                        $reference_value = array_pop($reference_value);
                        $reference_value = trim($reference_value);

                        if (stripos($reference_value, $value) !== FALSE) {
                            $hasil->flag = 'N';
                        } else {
                            $hasil->flag = 'NN';
                        }
                        break;
                    case "lt": // LESSTHAN
                        $value = floatval($hasil->result_value);
                        $reference_value = explode(': ', $hasil->reference_range);
                        $reference_value = array_pop($reference_value);
                        $reference_value = floatval(trim($reference_value, '<'));

                        if ($value < $reference_value) {
                            $hasil->flag = 'N';
                        } else {
                            $hasil->flag = 'NN';
                        }
                        break;
                    case "gt": // GREATERTHAN
                        $value = floatval($hasil->result_value);
                        $reference_value = explode(': ', $hasil->reference_range);
                        $reference_value = array_pop($reference_value);
                        $reference_value = floatval(trim($reference_value, '>'));

                        if ($value > $reference_value) {
                            $hasil->flag = 'N';
                        } else {
                            $hasil->flag = 'NN';
                        }
                        break;
                    case "lte": // LESSTHANEQUAL
                        $value = floatval($hasil->result_value);
                        $reference_value = explode(': ', $hasil->reference_range);
                        $reference_value = array_pop($reference_value);
                        $reference_value = floatval(trim($reference_value, '<='));

                        if ($value <= $reference_value) {
                            $hasil->flag = 'N';
                        } else {
                            $hasil->flag = 'NN';
                        }
                        break;
                        break;
                    case "gte": // GREATERTHANEQUAL
                        $value = floatval($hasil->result_value);
                        $reference_value = explode(': ', $hasil->reference_range);
                        $reference_value = array_pop($reference_value);
                        $reference_value = floatval(trim($reference_value, '>='));

                        if ($value >= $reference_value) {
                            $hasil->flag = 'N';
                        } else {
                            $hasil->flag = 'NN';
                        }
                        break;
                }
            }
            $detail->hasil_lis = json_encode($resultHasil);
        }
    }
    return $details;
}