<script>
var url = {
    loadDatabase: "<?php echo site_url('api/utils/setting_database/load_data'); ?>",
    getDatabase: "<?php echo site_url('api/utils/setting_database/get_data/:UID'); ?>",
    formDatabase: "<?php echo site_url('utils/settings/form_database/:UID'); ?>"
};

var tableDatabase = $("#table_database"),
    tableDatabaseDt,
    tableDatabaseTimer = null;

tableDatabaseDt = tableDatabase.dataTable({
    "sPaginationType": "full_numbers",
    "iDisplayLength": 20,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": url.loadDatabase,
    "columns": [
        {
            "data": "uid",
            "orderable": false,
            "render": function (data, type, row, meta) {
                if (row.connected) {
                    return '<button type="button" data-uid="' + data + '" class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm"><i class="icon-database"></i> ' + row.label + '</button>';
                }

                return '<button type="button" data-uid="' + data + '" class="btn border-grey text-grey btn-flat btn-icon btn-rounded btn-sm"><i class="icon-database"></i> ' + row.label + '</button>';
            },
            "className": "text-center"
        },
        { 
            "data": "unit",
            "orderable": false,
            "render": function (data, type, row, meta) {
                var template = '<a href="' + url.formDatabase + '">' + data + '</a>';
                return template.replace(':UID', row.uid);
            },
            "className": "text-left"
        },
        { 
            "data": "value", // hostname
            "orderable": false,
            "render": function (data, type, row, meta) {
                var setting = row.value;
                return setting.hostname ? setting.hostname + ":" + setting.port : "";
            },
            "className": "text-center"
        },
        { 
            "data": "uid",
            "orderable": false,
            "render": function (data, type, row, meta) {
                if (row.connected) {
                    return '<label class="label label-success">Connected</label>';
                }
                return '<label class="label label-default">Not Connected</label>';
            },
            "className": "text-center"
        }
    ],
    "fnServerData": function ( sSource, aoData, fnCallback ) {
        if (tableDatabaseTimer) clearTimeout(tableDatabaseTimer);
        tableDatabaseTimer = setTimeout(function () {
            blockElement(tableDatabase.selector);
            $.getJSON( sSource, aoData, function (json) {
                fnCallback(json);
            });
        }, 500);
    },
    "fnDrawCallback": function (oSettings) {
        var n = oSettings._iRecordsTotal;
        tableDatabase.unblock();
    }
});

$("#btn-refresh-database").on('click', function (e) {
    tableDatabaseDt.fnDraw(false);
});

$('a[href="#tab-database"]').click(function (e) {
    if ($(this).data('toggle') == 'tab') {
        tableDatabaseDt.fnDraw(false);
    }
});
</script>