<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $template['metas']; ?>

        <title><?php echo $template['title']; ?></title>

        <link rel="icon" href="<?php echo assets_url('img/logo/logo.png') ?>" type="image/png">

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo css_url('icons/icomoon/styles.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('icons/webfont-medical-icons/wfmi-style.css'); ?>" rel="stylesheet">
        <link href="<?php echo css_url('bootstrap.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('core.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('components.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('colors.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('vmt.custom.css') ?>" rel="stylesheet">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo js_url('plugins/loaders/pace.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('core/libraries/jquery.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('core/libraries/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/loaders/blockui.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo js_url('core/app.min.js') ?>"></script>
        <!-- /core JS files -->

        <script type="text/javascript" src="<?php echo js_url('core/app.js') ?>"></script>
        <!-- /theme JS files -->

    </head>
	
    <body class="login-container">

		<!-- Main navbar -->
		<div class="navbar navbar-inverse">
			<div class="navbar-header">
                <?php 
                    $appname = $this->config->item('app_name');
                    $appversion = $this->config->item('app_version');
                ?>
				<a class="navbar-brand" href="<?php echo site_url() ?>">
                    <!-- <img src="<?= base_url(get_option('rs_logo')) ?>" style="margin-top: -10px;height:40px"> -->
                    <!-- <span style="font-size: 20px;color: #fff;position: relative; left: 50px;">
                        <?php echo $template['title']; ?>
                    </span> -->
                </a>

				<ul class="nav navbar-nav pull-right visible-xs-block">
					<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				</ul>
			</div>
		</div>
		<!-- /main navbar -->
		
		<!-- Page container -->
		<div class="page-container">

			<!-- Page content -->
			<div class="page-content">

				<!-- Main content -->
				<div class="content-wrapper">

					<!-- Content area -->
					<div class="content">

						<?php echo $template['content']; ?>

						<!-- Footer -->
						<div class="footer text-muted text-center">
							&copy; <?php echo date('Y'); ?> 
						</div>
						<!-- /footer -->

					</div>
					<!-- /content area -->

				</div>
				<!-- /main content -->

			</div>
			<!-- /page content -->

		</div>
		<!-- /page container -->

    </body>
</html>
