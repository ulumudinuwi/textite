<script>
	$('select').select2();
	let UID = "<?php echo $uid; ?>",
		form = '#form',
		daftarPertanyaan = null,
		jumlahPertanyaan = null,
		tabelJawaban = $('#table_jawaban').find('tbody'),
		jawaban = [];


	var url = {
		index: "<?php echo site_url('konsultasi'); ?>",
		save: "<?php echo site_url('api/konsultasi/save'); ?>",
		deletePhoto: "<?php echo site_url('api/konsultasi/delete_photo'); ?>",
		getData: "<?php echo site_url('api/konsultasi/get_data'); ?>?uid=:UID",
		getPertanyaan: "<?php echo site_url('api/pertanyaan/get_all'); ?>",
	};

	function fillElement(obj, element) {
		let parent = element.parent();
		parent.find('.loading-select').show();
		$.getJSON(obj.url, function(data, status) {
			if (status === 'success') {
				var option = '';
				option += '<option value="">- Pilih -</option>';
				for (var i = 0; i < data.list.length; i++) {
					let selected = ""
					//if (parseInt(obj.value) === parseInt(data.list[i].id)) selected = 'selected="selected"';

					let value_name = data.list[i].nama; 
					let data_kode = "";
					if (data.list[i].kode) {
						data_kode = `data-kode="${data.list[i].kode}"`;
						value_name = data.list[i].kode + ' - ' + data.list[i].nama;
					} 
					option += '<option value="' + data.list[i].id + '" ' + data_kode + ' ' + selected + '>' + value_name + '</option>';
				}
				element.html(option).val(obj.value).change();
			}
			parent.find('.loading-select').hide();
		});
	}

	function fillForm(uid) {
		blockElement($(form));
		$.getJSON(url.getData.replace(':UID', uid), function(data, status) {
			if (status === 'success') {
				data = data.data;
				dataBarang = data;
				if(uid != "") $('.el-hidden').show();

				var label = Object.keys(data);
				for (var i = 0; i < label.length; i++) {
					switch(label[i]) {
						case "id":
						case "uid":
						case "name":
							$('#' + label[i]).val(data[label[i]]);
							break;
						case "description":
							$('#' + label[i]).data("wysihtml5").editor.setValue(data.description);
							break;
					}
				}
				$('#isi_satuan_penggunaan').addClass('hide');
				$(form).unblock();
			}
		});
	}

	function setPertanyaan(){
		if (jawaban.length == daftarPertanyaan.length) {
			//save Pertanyaan
			$('#modalPertanyaan').modal('hide');

			var tabledata, J01, J02, J03, J04, J05, J06, J07, K01=false, K02=false;
			for (var i = 0; i < jawaban.length; i++) {
				tabelJawaban.empty();
				var jawab = jawaban[i].jawab == 1 ? 'Ya, Total Point : ' + jawaban[i].number : 'Tidak'; 

					tabledata += '<tr>'
									+'<td class="text-center">'+jawaban[i].kode+'</td>'
									+'<td>'+jawaban[i].pertanyaan_deskripsi+'</td>'
									+'<td>'+jawab+'</td>'
								+'</tr>';

				//rule Pakar hasil j01 - j07 true or false
				switch(jawaban[i].kode) {
				  case 'J01':
				    J01 = jawaban[i].number >= daftarPertanyaan[i].total ? true : false; 
				    break;
				  case 'J02':
				    J02 = jawaban[i].number >= daftarPertanyaan[i].total ? true : false; 
				    break;
				  case 'J03':
				    J03 = jawaban[i].number >= daftarPertanyaan[i].total ? true : false; 
				    break;
				  case 'J04':
				    J04 = jawaban[i].number >= daftarPertanyaan[i].total ? true : false; 
				    break;
				  case 'J05':
				    J05 = jawaban[i].number >= daftarPertanyaan[i].total ? true : false; 
				    break;
				  case 'J06':
				    J06 = jawaban[i].number >= daftarPertanyaan[i].total ? true : false; 
				    break;
				  case 'J07':
				    J07 = jawaban[i].number >= daftarPertanyaan[i].total ? true : false; 
				    break;
				}
			}
			tabelJawaban.append(tabledata);

			//penilaian jawaban - menarik kesimpulan K01 atau K02

			// R01
			if (J01 && J02 || J03) {
				K01 = true;
			}
			// R02
			else if (J01 && J02 || J07) {
				K01 = true;
			}
			// R03
			else if (J02 && J03 || J04) {
				K01 = true;
			}
			// R04
			else if (J02 && J03 || J07) {
				K01 = true;
			}
			// R05
			else if (J03 && J04 || J05) {
				K01 = true;
			}
			// R06
			else if (J03 && J06 || J07) {
				K01 = true;
			}
			// R07 - R012
			else{
				K02 = true;
			}

			var kesimpulan = K02 ? "Standar" : "Reject";
			$('#kesimpulan').html("Kain masuk kedalam Kategori "+kesimpulan);
			$('#jawaban').val(JSON.stringify(jawaban));
			return false
		}

		//set form
		$('#number').val('');

		$('.modal-title').html('Pertanyaan ' + daftarPertanyaan[jawaban.length].kode);
		$('#pertanyaan').html(daftarPertanyaan[jawaban.length].deskripsi);
		$('#pertanyaan_deskripsi').val(daftarPertanyaan[jawaban.length].deskripsi);
		$('#pertanyaan_id').val(daftarPertanyaan[jawaban.length].id);
		$('#total_point').val(daftarPertanyaan[jawaban.length].total);
		$('#kode').val(daftarPertanyaan[jawaban.length].kode);

		//modal pertanyaan
		$('#modalPertanyaan').modal('show');

	}

	$(document).ready(function() {
		$(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
		$(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

		$(".wysihtml5-min").wysihtml5({
		    "action": false,
		    "color": false,
		    "html": false,
		    "image": false,
		    "link": false,
		});


		$('.btn-save').on('click', function(e) {
			e.preventDefault();
			$(form).submit();
		});

		$(".btn-simpan-modal").click(function() {
			var jawab = $("input[type='radio'][name='jawab']:checked").val(),
				kode = $('#kode').val(),
				total_point = $('#total_point').val(),
				number = $('#number').val(),
				pertanyaan_deskripsi = $('#pertanyaan_deskripsi').val(),
				pertanyaan_id = $('#pertanyaan_id').val();

			let dataJawaban = {
				pertanyaan_id : pertanyaan_id,
				pertanyaan_deskripsi : pertanyaan_deskripsi,
				kode : kode,
				number : number,
				jawab : jawab,
				total_point : total_point,
			};

			if (jawab == 1 && number == "") {
				errorMessage('Peringatan', "Inputan harus diisi.");
				return false;
			}

			swal({
				title: "Konfirmasi?",
				type: "warning",
				text: "Apakah data yang diisi telah benar ? Jawaban Tidak dapat dirubah",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#2196F3",
				cancelButtonText: "Batal",
				cancelButtonColor: "#FAFAFA",
				closeOnConfirm: true,
				showLoaderOnConfirm: true,
			},
			function() {
				jawaban.push(dataJawaban);
				blockElement('#modalPertanyaan');

				setTimeout(() => {
					$('#modalPertanyaan').unblock()
					successMessage('Berhasil', "Jawaban tersimpan.");
					setPertanyaan();
				}, 1500);
			});
		});

		$('.btn-isi').on('click', function(e) {
			if (jawaban.length >0) {
				jawaban.splice(0);
				tabelJawaban.empty();
				tabelJawaban.append('<tr><td colspan="4" class="text-center">Pertanyaan Belum diisi.</td></tr>');
				$('#kesimpulan').html('');
			}

			if($('#nama_kain').val() == ''){
				errorMessage('Peringatan', 'Data kain harus diisi.');
				return false;
			}

			e.preventDefault();
			$.getJSON(url.getPertanyaan, function(data, status) {
				if (status === 'success') {
					data = data.data;
					jumlahPertanyaan = data.length;
					daftarPertanyaan = data;

					setPertanyaan();

				}
			});
		});

		$(".btn-batal").click(function() {
			window.location.assign(url.index);
		});

		$(form).validate({
			rules: {
				nama_kain: { required: true },
				barcode: { required: true },
				catatan: { required: true, minlength: 1 },
			},
			focusInvalid: true,
			errorPlacement: function(error, element) {
				var placement = $(element).closest('.input-group');
				if (placement.length > 0) {
					error.insertAfter(placement);
				} else {
					error.insertAfter($(element));
				}
			},
			submitHandler: function (form) {
				swal({
					title: "Konfirmasi?",
					type: "warning",
					text: "Apakah data yang dimasukan telah benar ?",
					showCancelButton: true,
					confirmButtonText: "Ya",
					confirmButtonColor: "#2196F3",
					cancelButtonText: "Batal",
					cancelButtonColor: "#FAFAFA",
					closeOnConfirm: true,
					showLoaderOnConfirm: true,
				},
				function() {
					$('.input-decimal').each(function() {
						$(this).val($(this).autoNumeric('get'));
					});

					$('.input-bulat').each(function() {
						$(this).val($(this).autoNumeric('get'));
					});

					$('input, textarea, select').prop('disabled', false);

					// Set Prefix Kode
					let kodeJenis = $("#jenis_id").find("option:selected").data("kode");
					let kodeKategori = $("#kategori_id").find("option:selected").data("kode");
					let kodePabrik = $("#pabrik_id").find("option:selected").data("kode");

					blockElement($(form));
					var formData = $(form).serialize();
					$.ajax({
						data: formData,
						type: 'POST',
						dataType: 'JSON', 
						url: url.save,
						success: function(data){
							$(form).unblock();
							successMessage('Berhasil', "Data berhasil disimpan.");
							window.location.assign(url.index);
						},
						error: function(data){
							$(form).unblock();
							errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
						}
					});
					return false;
				});
			}
		});

		fillForm(UID);
	});
</script>