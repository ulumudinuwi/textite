<div id="foto-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Tambah Foto</h6>
			</div>
			<div class="modal-body form-horizontal">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<div id="section-crop_foto" style="display: none;"></div>
						<input type="file" id="upload-foto" value="Choose file" accept="image/*" class="file-styled-primary">
					</div>
				</div>
			</div>
			<div class="modal-footer m-t-none">
				<button type="button" id="modal-btn_tambah_foto" class="btn btn-primary btn-labeled">
					<b><i class="fa fa-plus"></i></b>
					Tambah
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?= script_url('assets/js/pages/scripts/master/barang/foto.js'); ?>"></script>
<script type="text/javascript">
	$(".file-styled-primary").uniform({
		fileButtonClass: 'action btn bg-blue'
	});
</script>