<style>
	table > thead > tr > th, table > tbody > tr > td  {
		white-space: nowrap;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row mb-20">
						<div class="col-md-12">
							<fieldset>
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Kain</legend>

							</fieldset>
						</div>
						<div class="col-sm-12 col-md-12 col-lg-12">
							<div class="row kain">
								<div class="col-md-10">
									<div class="form-group">
										<label class="control-label col-md-3 input-required">Warna Kain</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="nama_kain" name="nama_kain" placeholder="Warna Kain...">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 input-required">Barcode Kain</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="barcode" name="barcode" placeholder="Barcode Kain ...">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3">Spek Kain</label>
										<div class="col-sm-7">
											<textarea class="form-control" id="spek_kain" name="spek_kain" rows="8" placeholder="Spek Kain ..."></textarea>
										</div>
									</div>
								</div>	

								<button type="button" class="btn btn-success btn-labeled btn-isi">
									<b><i class="icon-pencil"></i></b>
									Isi Pertanyaan
								</button>


								<div class="col-md-12">
								<hr/>
									<fieldset>
										<legend class="text-bold"><i class="icon-magazine position-left"></i> Hasil Konsultasi</legend>
									</fieldset>
								</div>
								<div class="col-sm-12 resume_pertanyaan">
									<div class="table-responsive">
										<table id="table_jawaban" class="table">
											<thead>
												<tr>
													<th class="text-center">Kode</th>
										            <th class="text-center">Pertanyaan</th>
										            <th class="text-center">Jawaban</th>
												</tr>
							 				</thead>
											<tbody>
												<tr>
													<td colspan="4" class="text-center">Pertanyaan Belum diisi.</td>
												</tr>
											</tbody>
										</table>
									</div>
									<hr/>
									<div class="panel-body">
										<div class="col-md-12">
											<div class="form-group col-sm-6">
												<label class="col-sm-12">Kesimpulan</label>
												<div class="col-sm-10">
													<textarea class="form-control" readonly id="kesimpulan" name="kesimpulan" rows="8" placeholder="Kesimpulan ..."></textarea>
												</div>
											</div>
											<div class="form-group col-sm-6">
												<label class="col-sm-12">Catatan</label>
												<div class="col-sm-10">
													<textarea class="form-control" id="catatan" name="catatan" rows="8" placeholder="Catatan ..."></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div>
						<input type="hidden" id="id" name="id" value="0">
						<input type="hidden" id="uid" name="uid" value="0">
						<input type="hidden" id="jawaban" name="jawaban" value="0">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-simpan">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div id="modalPertanyaan" class="modal fade" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Pertanyaan</h6>
			</div>
			<div class="modal-body form-horizontal">
				<div class="panel panel-default">
					<div class="row">
						<div class="col-md-12 text-center">

							<div class="alert bg-info alert-styled-left">
								<span class="text-semibold" id="pertanyaan">-</span>
						    </div>

							<div class="col-md-12 text-left">
								<div class="radio col-md-3" style="margin-left: 35%;">
									<label>
										<input type="radio" name="jawab" id="jawab" value="1" class="control-primary" checked="checked">
										Ya
									</label>
								</div>

								<div class="radio col-md-3">
									<label>
										<input type="radio" name="jawab" id="jawab" value="2" class="control-danger">
										Tidak
									</label>
								</div>
							</div>

							<div class="col-md-12" style="margin-top:10px;margin-bottom: 10px;">
								<input type="text" class="form-control" id="number" name="number" placeholder="Isi jika ya...">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer m-t-none">
				<input type="hidden" id="kode" value="">
				<input type="hidden" id="pertanyaan_id" value="">
				<input type="hidden" id="total_point" value="">
				<input type="hidden" id="pertanyaan_deskripsi" value="">
				<button type="button" class="btn btn-primary btn-simpan-modal">
					Simpan
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>