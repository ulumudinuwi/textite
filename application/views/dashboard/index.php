  
  <div class="row">
      <div class="col-md-12 text-center">
        <img src="<?= base_url('/assets/img/logo.png') ?>" style="width: 20%;" />
    </div>
    <div class="col-md-12">
        
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <!-- <li data-target="#myCarousel" data-slide-to="4"></li> -->
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item active">
            <img src="<?= base_url('/assets/img/img-1.jpg') ?>" alt="Image"
              style="width: 50%; margin: auto;"
            >
          </div>

          <div class="item">
            <img src="<?= base_url('/assets/img/img-2.jpg') ?>" alt="Image"
              style="width: 50%; margin: auto;"
            >
          </div>

          <div class="item">
            <img src="<?= base_url('/assets/img/img-3.jpg') ?>" alt="Image"
              style="width: 50%; margin: auto;"
            >
          </div>

          <!-- <div class="item">
            <img src="<?= base_url('/assets/img/img-4.jpg') ?>" alt="Image"
              style="width: 50%; margin: auto;"
            >
          </div> -->
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

    </div>
  </div>

<input type="hidden" name="bpom" value="<?php echo $this->session->userdata('bpom') ?>">