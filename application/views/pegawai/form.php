<style>
	table > thead > tr > th, table > tbody > tr > td  {
		white-space: nowrap;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row mb-20">
						<div class="col-md-12">
							<fieldset>
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Pegawai</legend>
							</fieldset>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-4 input-required">NIP</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="nip" name="nip" placeholder="NIP ...">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4 input-required">Nama</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama ...">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4 input-required">Jabatan</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan ...">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-4 input-required">Email / Username</label>
										<div class="col-md-7">
											<input type="text" class="form-control" id="email" name="email" placeholder="Email / Username...">
										</div>
									</div>
								</div>	
							</div>
						</div>
					<div>
						<input type="hidden" id="id" name="id" value="0">
						<input type="hidden" id="uid" name="uid" value="0">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

