<script>
	$('select').select2();
	let UID = "<?php echo $uid; ?>",
		form = '#form',
		dataBarang = null;


	var url = {
		index: "<?php echo site_url('pegawai'); ?>",
		save: "<?php echo site_url('api/pegawai/save'); ?>",
		deletePhoto: "<?php echo site_url('api/pegawai/delete_photo'); ?>",
		getData: "<?php echo site_url('api/pegawai/get_data'); ?>?uid=:UID",
		getKategori: "<?php echo site_url('api/kategori_jadwal/get_all'); ?>",
		getKelompok: "<?php echo site_url('api/kelompok_jadwal/get_all'); ?>",
	};

	function fillElement(obj, element) {
		let parent = element.parent();
		parent.find('.loading-select').show();
		$.getJSON(obj.url, function(data, status) {
			if (status === 'success') {
				var option = '';
				option += '<option value="">- Pilih -</option>';
				for (var i = 0; i < data.list.length; i++) {
					let selected = ""
					//if (parseInt(obj.value) === parseInt(data.list[i].id)) selected = 'selected="selected"';

					let value_name = data.list[i].nama; 
					let data_kode = "";
					if (data.list[i].kode) {
						data_kode = `data-kode="${data.list[i].kode}"`;
						value_name = data.list[i].kode + ' - ' + data.list[i].nama;
					} 
					option += '<option value="' + data.list[i].id + '" ' + data_kode + ' ' + selected + '>' + value_name + '</option>';
				}
				element.html(option).val(obj.value).change();
			}
			parent.find('.loading-select').hide();
		});
	}

	function fillForm(uid) {
		blockElement($(form));
		$.getJSON(url.getData.replace(':UID', uid), function(data, status) {
			if (status === 'success') {
				data = data.data;
				dataBarang = data;
				if(uid != "") $('.el-hidden').show();

				var label = Object.keys(data);
				for (var i = 0; i < label.length; i++) {
					switch(label[i]) {
						case "id":
						case "uid":
						case "nip":
						case "nama":
						case "jabatan":
						case "email":
							$('#' + label[i]).val(data[label[i]]);
							break;
						case "description":
							$('#' + label[i]).data("wysihtml5").editor.setValue(data.description);
							break;
					}
				}
				$('#isi_satuan_penggunaan').addClass('hide');
				$(form).unblock();
			}
		});
	}

	$(document).ready(function() {
		$(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});
		$(".input-bulat").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

		$(".wysihtml5-min").wysihtml5({
		    "action": false,
		    "color": false,
		    "html": false,
		    "image": false,
		    "link": false,
		});


		// $('#bpom').change(function() {
		// 	if ($(this).val() == 1) {
		// 		$('body').find('#ppn').prop('readonly', false);
		// 		$('body').find('#ppn').val('');
		// 	}else{
		// 		$('body').find('#ppn').prop('readonly', true);
		// 		$('body').find('#ppn').val('0');
		// 	}
		// });

		// $('#jenis_id').change(function() {
		// 	let obj = {
		// 		value: $('#tmp_kategori_id').val(),
		// 		url: url.getKategori + '?mode=by&jenis_id=' + $(this).val()
		// 	};
		// 	fillElement(obj, $('#kategori_id'));
		// });

		$('#btn-tambah_foto').click(function(e) {
			e.preventDefault();
			$('#foto-modal').modal('show');
		});

		$('.btn-save').on('click', function(e) {
			e.preventDefault();
			$(form).submit();
		});

		$(".btn-batal").click(function() {
			window.location.assign(url.index);
		});

		$(form).validate({
			rules: {
				nama: { required: true },
				deskripsi: { required: true, minlength: 1 },
			},
			focusInvalid: true,
			errorPlacement: function(error, element) {
				var placement = $(element).closest('.input-group');
				if (placement.length > 0) {
					error.insertAfter(placement);
				} else {
					error.insertAfter($(element));
				}
			},
			submitHandler: function (form) {
				swal({
					title: "Konfirmasi?",
					type: "warning",
					text: "Apakah data yang dimasukan telah benar??",
					showCancelButton: true,
					confirmButtonText: "Ya",
					confirmButtonColor: "#2196F3",
					cancelButtonText: "Batal",
					cancelButtonColor: "#FAFAFA",
					closeOnConfirm: true,
					showLoaderOnConfirm: true,
				},
				function() {
					$('.input-decimal').each(function() {
						$(this).val($(this).autoNumeric('get'));
					});

					$('.input-bulat').each(function() {
						$(this).val($(this).autoNumeric('get'));
					});

					$('input, textarea, select').prop('disabled', false);

					// Set Prefix Kode
					let kodeJenis = $("#jenis_id").find("option:selected").data("kode");
					let kodeKategori = $("#kategori_id").find("option:selected").data("kode");
					let kodePabrik = $("#pabrik_id").find("option:selected").data("kode");

					blockElement($(form));
					var formData = $(form).serialize();
					$.ajax({
						data: formData,
						type: 'POST',
						dataType: 'JSON', 
						url: url.save,
						success: function(data){
							$(form).unblock();
							successMessage('Berhasil', "Barang jadi berhasil disimpan.");
							window.location.assign(url.index);
						},
						error: function(data){
							$(form).unblock();
							errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
						}
					});
					return false;
				});
			}
		});

		fillForm(UID);
	});
</script>