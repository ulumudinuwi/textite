<div id="detail-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h6 class="modal-title">Detail Barang</h6>
      </div>
      <div class="modal-body form-horizontal">
        <div class="row mb-20">
          <div class="col-md-12">
            <fieldset>
              <legend class="text-bold"><i class="icon-magazine position-left"></i> Data Barang</legend>
            </fieldset>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-md-3">Kode</label>
                  <div class="col-md-7">
                    <div class="form-control-static" id="detail_kode"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Nama</label>
                  <div class="col-md-7">
                    <div class="form-control-static" id="detail_nama"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Deskripsi</label>
                  <div class="col-md-7">
                    <div class="form-control-static" id="detail_deskripsi"></div>
                  </div>
                </div>
              </div>  
            </div>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-md-3">Jenis</label>
                  <div class="col-md-7">
                    <div class="input-group">
                      <div class="form-control-static" id="detail_jenis"></div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Kategori</label>
                  <div class="col-md-7">
                    <div class="input-group">
                      <div class="form-control-static" id="detail_kategori"></div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Supplier</label>
                  <div class="col-md-7">
                    <div class="input-group">
                      <div class="form-control-static" id="detail_pabrik"></div>
                    </div>
                  </div>
                </div>
              </div>  
            </div>
          </div>
        </div>
        <div class="row mb-20">
          <div class="col-md-6">
            <fieldset>
              <legend class="text-bold"><i class="icon-magazine position-left"></i> Data Pembelian</legend>
              <div class="form-group">
                <label class="control-label col-md-4">Satuan</label>
                <div class="col-md-7">
                  <div class="form-control-static" id="detail_satuan_pembelian"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">Harga</label>
                <div class="col-md-7">
                  <div class="form-control-static" id="detail_harga_pembelian"></div>
                </div>
              </div>
            </fieldset>
          </div>
          <div class="col-md-6">
            <fieldset>
              <legend class="text-bold"><i class="icon-magazine position-left"></i> Data Penjualan</legend>
              <!-- <div class="form-group">
                <label class="control-label col-md-4">Satuan</label>
                <div class="col-md-7">
                  <div class="form-control-static" id="detail_satuan_penggunaan"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">Isi</label>
                <div class="col-md-7">
                  <div class="form-control-static" id="detail_isi_satuan_penggunaan"></div>
                  <span class="text-info text-size-mini"><i class="fa fa-info"></i> 1 satuan pembelian berapa satuan penggunaan</span>
                </div>
              </div> -->
              <div class="form-group">
                <label class="control-label col-md-4">Harga Jual</label>
                <div class="col-md-7">
                  <div class="form-control-static" id="detail_harga_penjualan"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">Harga Minimum</label>
                <div class="col-md-7">
                  <div class="form-control-static" id="detail_harga_minimum"></div>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
      </div>
      <div class="modal-footer m-t-none">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
    </div>
  </div>
</div>

<div id="import-modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-md">
    <form method="post" id="import_form" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h6 class="modal-title">Import Barang</h6>
        </div>

        <div class="modal-body form-horizontal">
          <div class="row mb-20">
            <div class="col-md-12">
              <fieldset>
                <legend class="text-bold"><i class="icon-magazine position-left"></i> File Excel Barang</legend>
              </fieldset>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">File</label>
                    <div class="col-md-9">
                      <input type="file" name="file" id="file" class="form-control" required accept=".xls, .xlsx" /></p>
                    </div>
                  </div>
                </div>  
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer m-t-none">
          <button type="submit" class="btn btn-success">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </form>
  </div>
</div>