<style>
	table > thead > tr > th, table > tbody > tr > td  {
		white-space: nowrap;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row mb-20">
						<div class="col-sm-12 col-md-6 col-lg-6">
							<fieldset>
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Barang</legend>
							</fieldset>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-3 input-required">Kode</label>
										<div class="col-md-7">
											<div class="form-control-static" id="label-kode"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 input-required">Nama</label>
										<div class="col-md-7">
											<div class="form-control-static" id="label-nama"></div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 input-required">Harga</label>
										<div class="col-md-7">
											<input type="text" class="form-control input-decimal" id="harga_penjualan" name="harga_penjualan" placeholder="Harga ...">
										</div>
									</div>
								</div>	
							</div>
						</div>
					</div>
					<div>
						<input type="hidden" id="id" name="id" value="0">
						<input type="hidden" id="uid" name="uid" value="">
						<input type="hidden" name="browse" value="master-harga_jual">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>