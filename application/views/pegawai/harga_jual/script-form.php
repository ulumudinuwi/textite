<script>
	let UID = "<?php echo $uid; ?>",
		form = '#form';

	var url = {
		index: "<?php echo site_url('master/barang/harga_jual'); ?>",
		save: "<?php echo site_url('api/master/barang/save'); ?>",
		getData: "<?php echo site_url('api/master/barang/get_data'); ?>?uid=:UID",
	};

	function fillForm(uid) {
		blockElement($(form));
		$.getJSON(url.getData.replace(':UID', uid), function(data, status) {
			if (status === 'success') {
				data = data.data;
				data.harga_penjualan = data.harga_penjualan != 0 ? data.harga_penjualan : data.hna_by_setting;
				
				var label = Object.keys(data);
				for (var i = 0; i < label.length; i++) {
					switch(label[i]) {
						case "id":
						case "uid":
							$('#' + label[i]).val(data[label[i]]);
							break;
						case "kode":
						case "nama":
							$('#label-' + label[i]).html(data[label[i]]);
							break;
						case "harga_penjualan":
							$('#' + label[i]).autoNumeric('set', data[label[i]]);
							break;
					}
				}

				$(form).unblock();
			}
		});
	}

	$(document).ready(function() {
		$(".input-decimal").autoNumeric('init', {aSep: '.', aDec: ',', mDec: '2'});

		$(".wysihtml5-min").wysihtml5({
		    "action": false,
		    "color": false,
		    "html": false,
		    "image": false,
		    "link": false,
		});

		$('.btn-save').on('click', function(e) {
			e.preventDefault();
			$(form).submit();
		});

		$(".btn-batal").click(function() {
			window.location.assign(url.index);
		});

		$(form).validate({
			rules: {
				harga_penjualan: { required: true, minlength: 1 },
			},
			focusInvalid: true,
			errorPlacement: function(error, element) {
				var placement = $(element).closest('.input-group');
				if (placement.length > 0) {
					error.insertAfter(placement);
				} else {
					error.insertAfter($(element));
				}
			},
			submitHandler: function (form) {
				swal({
					title: "Konfirmasi?",
					type: "warning",
					text: "Apakah data yang dimasukan telah benar??",
					showCancelButton: true,
					confirmButtonText: "Ya",
					confirmButtonColor: "#2196F3",
					cancelButtonText: "Batal",
					cancelButtonColor: "#FAFAFA",
					closeOnConfirm: true,
					showLoaderOnConfirm: true,
				},
				function() {
					$('.input-decimal').each(function() {
						$(this).val($(this).autoNumeric('get'));
					});

					blockElement($(form));
					var formData = $(form).serialize();
					$.ajax({
						data: formData,
						type: 'POST',
						dataType: 'JSON', 
						url: url.save,
						success: function(data){
							$(form).unblock();
							successMessage('Berhasil', "Harga jual berhasil disimpan.");
							window.location.assign(url.index);
						},
						error: function(data){
							$(form).unblock();
							errorMessage('Peringatan', "Terjadi kesalahan saat memproses data.");
						}
					});
					return false;
				});
			}
		});

		fillForm(UID);
	});
</script>