<?php echo messages(); ?>
<style type="text/css">
	.table thead tr th, .table tbody tr td {
		white-space: nowrap;
	}
  #btn-group_float {
    position: fixed;
    bottom: 10%;
    right: 20px;
    z-index: 10000;
    text-align: right;
  }
</style>

<div class="panel panel-flat">
  <hr class="no-margin-top no-margin-bottom">
	<div class="panel-body no-padding-top">
		<div class="table-responsive">
			<table id="table" class="table">
				<thead>
					<tr>
						<th >NIP</th>
            <th >Nama</th>
            <th >Jabatan</th>
            <th >Email / Username</th>
            <th class="text-center">Status</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
var table;
var btnRefresh = $("#btn-refresh"),
    modalDetail = "#detail-modal",
    modalImport = "#import-modal",
    tableDetail = $("#table-detail");

var url = {
  edit: "<?php echo site_url('pegawai/form/:UID'); ?>",
  delete: "<?php echo site_url('api/pegawai/delete'); ?>",
  loadData: "<?php echo site_url('api/pegawai/load_data'); ?>",
  getData: "<?php echo site_url('api/pegawai/get_data'); ?>?uid=:UID",
  getKategori: "<?php echo site_url('api/kategori_jadwal/get_all'); ?>",
  updateStatus: "<?php echo site_url('api/pegawai/update_status'); ?>",
  imporjadwal: "<?php echo site_url('api/pegawai/import'); ?>",
}

function fillElement(obj, element) {
  let parent = $(element).parent();
  parent.find('.loading-select').show();
  $.getJSON(obj.url, function(data, status) {
    if (status === 'success') {
      var option = '';
      option += '<option value="" selected="selected">- Pilih -</option>';
      for (var i = 0; i < data.list.length; i++) {
        var selected = ""
        if (parseInt(obj.value) === parseInt(data.list[i].id)) selected = 'selected="selected"';
        option += '<option value="' + btoa(data.list[i].id) + '"  ' + selected + '>' + data.list[i].kode + ' - ' + data.list[i].nama + '</option>';
      }
      $(element).html(option).trigger("change");
    }
    parent.find('.loading-select').hide();
  });
}

$(window).ready(function () {
  table = $("#table").DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": url.loadData,
        "type": "POST",
        "data": function(p) {
            p.jenis_id = $('#search_jenis').val();
            p.kategori_id = $('#search_kategori').val();
        }
    },
    "columns": [
      {
        "data": "id",
        "render": function (data, type, row, meta) {
          return '<b>'+row.nip ? row.nip : '-'+'</b>';
        }
      },
      {
        "data": "id",
        "render": function (data, type, row, meta) {
          return '<b>'+row.nama ? row.nama : '-'+'</b>';
        }
      },
      {
        "data": "id",
        "render": function (data, type, row, meta) {
          return '<b>'+row.jabatan ? row.jabatan : '-'+'</b>';
        }
      },
      {
        "data": "id",
        "render": function (data, type, row, meta) {
          return '<b>'+row.email ? row.email : '-'+'</b>';
        }
      },
      { 
        "data": "status",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) {
          if (data == 0) 
            return '<button type="button" class="toggle-status-row btn bg-success btn-xs" data-status="1" data-uid="' + row.uid + '">Aktifkan</button>';
          return '<button type="button" class="toggle-status-row btn bg-slate-400 btn-xs" data-status="0" data-uid="' + row.uid + '">Non Aktifkan</button>';
        },
        "className": "text-center"
      },
      {
        "data": "uid",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) {
          return '<a class="edit-row" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
        },
        "className": "text-center"
      }
    ],
  });

  btnRefresh = $("#btn-refresh");
  btnRefresh.click(function () {
    table.draw();
  });
});

$(document).ready(function() {
  $('#search_jenis, #search_kategori').change(function() {
    table.draw();
  });

  $('#search_jenis').change(function() {
    let obj = {
      value: $(this).val(),
      url: url.getKategori + '?mode=by&jenis_id=' + $(this).val()
    };
    fillElement(obj, '#search_kategori');
  });

  $("#table").on("click", ".edit-row", function () {
    var uid = $(this).data('uid');
    window.location.assign(url.edit.replace(':UID', uid));
  });

  $("#table").on("click", ".toggle-status-row", function () {
    var btn = $(this);
    btn.prop("disabled", true);
    var uid = $(this).data('uid');
    var statusData = $(this).data('status');

    var title;
    if (parseInt(statusData) === 1) title = "Mengaktifkan data";
    else title = "Menonaktifkan data";

    // Progress loader
    var cur_value = 1;
    var update = false;
    var progress;
    var timer;

    // Make a loader.
    var loader = new PNotify({
      title: title,
      text: '<div class="progress progress-striped active" style="margin:0">\
      <div class="progress-bar bg-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">\
      <span class="sr-only">0%</span>\
      </div>\
      </div>',
      addclass: 'bg-slate',
      icon: 'icon-spinner4 spinner',
      hide: false,
      buttons: {
        closer: true,
        sticker: false
      },
      history: {
        history: false
      },
      before_open: function(PNotify) {
        progress = PNotify.get().find("div.progress-bar");
        progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");

        // Pretend to do something.
        timer = setInterval(function() {
          if (cur_value >= 100) {
            // Remove the interval.
            window.clearInterval(timer);
            
            update = true;
            loader.remove();

            return;
          }
          cur_value += 5;
          progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");
        }, 65);
      },
      after_close: function(PNotify, timer_hide) {
        btn.prop("disabled", false);
        clearInterval(timer);
        
        if (update) {
          $.post(url.updateStatus, {uid: uid, status: statusData}, function (data, status) {
            if (status === "success") {

              if (parseInt(statusData) === 0) {
                btn.removeClass('bg-success');
                btn.addClass('bg-slate-400');
                btn.data('status', 0);
                btn.html("Inactive");
                successMessage('Berhasil', 'Data berhasil dinonaktifkan.');
              } else {
                btn.removeClass('bg-slate-400');
                btn.addClass('bg-success');
                btn.data('status', 1);
                btn.html("Active");
                successMessage('Berhasil', 'Data berhasil diaktifkan.');
              }
              table.draw();
            }
          }).fail(function (error) {
            if (parseInt(statusData) === 0) 
              errorMessage('Peringatan', 'Terjadi kesalahan saat menonaktifkan data.');
            else 
              errorMessage('Peringatan', 'Terjadi kesalahan saat mengaktifkan data.');
          });
        }

        update = false;
        
      }
    });
  });
  $('#search_jenis').change();

  $('.btn-import').on('click', function(event){
    $(modalImport).modal('show');
  });

  $('#import_form').on('submit', function(event){

    blockElement(modalImport + ' .modal-dialog');
    event.preventDefault();
    $.ajax({
      url: url.imporjadwal,
      method:"POST",
      data:new FormData(this),
      contentType:false,
      cache:false,
      processData:false,
      success:function(data){
        successMessage('Berhasil', 'Data berhasil Di Import.');
        $(modalImport + ' .modal-dialog').unblock();
        $(modalImport).modal('hide');
        table.draw();
      },
      error: function(data) {
        errorMessage('Gagal', 'Periksa Kembali Data.');
        $(modalImport + ' .modal-dialog').unblock();
      },
    })
  });
  
  $("#btn-print-excel").click(function () {
    window.location.assign(`<?php echo site_url('api/gudang_farmasi/stock/print_jadwal'); ?>`);
  });
});

</script>