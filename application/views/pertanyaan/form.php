<style>
	table > thead > tr > th, table > tbody > tr > td  {
		white-space: nowrap;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row mb-20">
						<div class="col-md-12">
							<fieldset>
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Pertanyaan</legend>
							</fieldset>
						</div>
						<div class="col-sm-12 col-md-12 col-lg-12">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-2 input-required">Kode</label>
										<div class="col-md-4">
											<input type="text" class="form-control" id="kode" name="kode" placeholder="Kode ...">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 input-required">Point</label>
										<div class="col-md-4">
											<input type="text" class="form-control input-decimal" id="total" name="total" placeholder="Point ...">
										</div>
									</div>
									<div class="form-group hideForm">
										<label class="control-label col-md-2 input-required">Deskripsi</label>
										<div class="col-md-7">
											<textarea type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi ..."></textarea>
										</div>
									</div>
									<div class="form-group hideForm">
										<label class="control-label col-md-2 input-required">Solusi</label>
										<div class="col-md-7">
											<textarea type="text" class="form-control" id="solusi" name="solusi" placeholder="Solusi ..."></textarea>
										</div>
									</div>
								</div>	
							</div>
						</div>
					</div>
					<div>
						<input type="hidden" id="id" name="id" value="0">
						<input type="hidden" id="uid" name="uid" value="0">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>