<?php echo messages(); ?>
<style type="text/css">
	.table thead tr th, .table tbody tr td {
		white-space: nowrap;
	}
  #btn-group_float {
    position: fixed;
    bottom: 10%;
    right: 20px;
    z-index: 10000;
    text-align: right;
  }
</style>
<div id="btn-group_float">
  <p>
    <button type="button" id="btn-print-pdf" class="btn bg-orange btn-float btn-rounded" title="Print PDF"  data-placement="left" data-delay="600">
      <b><i class="icon-file-pdf"></i></b>
    </button>
  </p>
</div>
<div class="panel panel-flat">
	<div class="panel-body no-padding-top">
		<div class="table-responsive">
			<table id="table" class="table">
				<thead>
					<tr>
						<th>Warna Kain</th>
            <th>Barcode</th>
            <th>Kesimpulan</th>
            <th>Catatan</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-print" tabindex="-1" role="dialog" aria-labelledby="modal-printLabel">
  <div class="modal-dialog modal-full">
    <div class="modal-content">
      <div class="modal-header">
        <div class="close">
          <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Close</button>
        </div>
        <h4 class="modal-title">Print Preview</h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var table;
var btnRefresh = $("#btn-refresh"),
    modalDetail = "#detail-modal",
    modalImport = "#import-modal",
    tableDetail = $("#table-detail");

var url = {
  edit: "<?php echo site_url('laporan/form/:UID'); ?>",
  delete: "<?php echo site_url('api/laporan/delete'); ?>",
  loadData: "<?php echo site_url('api/laporan/load_data'); ?>",
  getData: "<?php echo site_url('api/laporan/get_data'); ?>?uid=:UID",
  getKategori: "<?php echo site_url('api/master/kategori_testimoni/get_all'); ?>",
  updateStatus: "<?php echo site_url('api/laporan/update_status'); ?>",
  importestimoni: "<?php echo site_url('api/laporan/import'); ?>",
}

function fillElement(obj, element) {
  let parent = $(element).parent();
  parent.find('.loading-select').show();
  $.getJSON(obj.url, function(data, status) {
    if (status === 'success') {
      var option = '';
      option += '<option value="" selected="selected">- Pilih -</option>';
      for (var i = 0; i < data.list.length; i++) {
        var selected = ""
        if (parseInt(obj.value) === parseInt(data.list[i].id)) selected = 'selected="selected"';
        option += '<option value="' + btoa(data.list[i].id) + '"  ' + selected + '>' + data.list[i].kode + ' - ' + data.list[i].nama + '</option>';
      }
      $(element).html(option).trigger("change");
    }
    parent.find('.loading-select').hide();
  });
}

function fillDetail(uid) {
  $(modalDetail).modal('show');
  blockElement(modalDetail + ' .modal-dialog');
  $.getJSON(url.getData.replace(":UID", uid), function(res, status) {
    if(status === "success") {
      let data = res.data;

      var label = Object.keys(data);
      for (var i = 0; i < label.length; i++) {
        switch(label[i]) {
          case "kode":
          case "nama":
          case "jenis":
          case "kategori":
          case "pabrik":
          case "satuan_pembelian":
          case "satuan_penggunaan":
          case "deskripsi":
            $('#detail_' + label[i]).html(data[label[i]] ? data[label[i]] : '-');
            break;
          case "harga_pembelian":
          case "harga_penjualan":
          case "harga_minimum":
            $('#detail_' + label[i]).html(`Rp. ${numeral(data[label[i]]).format('0.0,')}`);
            break;
          case "isi_satuan_penggunaan":
            $('#detail_' + label[i]).html(numeral(data[label[i]]).format('0,0'));
            break;
          case "diskon_mou":
            $('#detail_' + label[i]).html(numeral(data[label[i]]).format('0.0,'));
            break;
        }
      }
    }
    $(modalDetail + ' .modal-dialog').unblock();

    $('[data-toggle=tooltip]').tooltip();
  });
}

$(window).ready(function () {
  table = $("#table").DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": url.loadData,
        "type": "POST",
        "data": function(p) {
            p.jenis_id = $('#search_jenis').val();
            p.kategori_id = $('#search_kategori').val();
        }
    },
    "columns": [
      {
        "data": "nama",
        "render": function (data, type, row, meta) {
          return data;
        }
      },
      { 
        "data": "barcode",
      },
      { 
        "data": "deskripsi",
      },
      { 
        "data": "catatan",
      },
      {
        "data": "uid",
        "orderable": false,
        "searchable": false,
        "render": function (data, type, row, meta) {
          return '<a class="edit-row" data-uid="' + row.uid + '"><i class="fa fa-edit"></i></a>';
        },
        "className": "text-center"
      }
    ],
  });

  btnRefresh = $("#btn-refresh");
  btnRefresh.click(function () {
    table.draw();
  });
});

$(document).ready(function() {
  $('#search_jenis, #search_kategori').change(function() {
    table.draw();
  });

  $('#search_jenis').change(function() {
    let obj = {
      value: $(this).val(),
      url: url.getKategori + '?mode=by&jenis_id=' + $(this).val()
    };
    fillElement(obj, '#search_kategori');
  });

  $('#table').on("click", ".detail-row", function() {
    var uid = $(this).data("uid");
    fillDetail(uid);
  });

  $("#table").on("click", ".edit-row", function () {
    var uid = $(this).data('uid');
    window.location.assign(url.edit.replace(':UID', uid));
  });

  $("#table").on("click", ".toggle-status-row", function () {
    var btn = $(this);
    btn.prop("disabled", true);
    var uid = $(this).data('uid');
    var statusData = $(this).data('status');

    var title;
    if (parseInt(statusData) === 1) title = "Mengaktifkan data";
    else title = "Menonaktifkan data";

    // Progress loader
    var cur_value = 1;
    var update = false;
    var progress;
    var timer;

    // Make a loader.
    var loader = new PNotify({
      title: title,
      text: '<div class="progress progress-striped active" style="margin:0">\
      <div class="progress-bar bg-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">\
      <span class="sr-only">0%</span>\
      </div>\
      </div>',
      addclass: 'bg-slate',
      icon: 'icon-spinner4 spinner',
      hide: false,
      buttons: {
        closer: true,
        sticker: false
      },
      history: {
        history: false
      },
      before_open: function(PNotify) {
        progress = PNotify.get().find("div.progress-bar");
        progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");

        // Pretend to do something.
        timer = setInterval(function() {
          if (cur_value >= 100) {
            // Remove the interval.
            window.clearInterval(timer);
            
            update = true;
            loader.remove();

            return;
          }
          cur_value += 5;
          progress.width(cur_value + "%").attr("aria-valuenow", cur_value).find("span").html(cur_value + "%");
        }, 65);
      },
      after_close: function(PNotify, timer_hide) {
        btn.prop("disabled", false);
        clearInterval(timer);
        
        if (update) {
          $.post(url.updateStatus, {uid: uid, status: statusData}, function (data, status) {
            if (status === "success") {

              if (parseInt(statusData) === 0) {
                btn.removeClass('bg-success');
                btn.addClass('bg-slate-400');
                btn.data('status', 0);
                btn.html("Inactive");
                successMessage('Berhasil', 'Data berhasil dinonaktifkan.');
              } else {
                btn.removeClass('bg-slate-400');
                btn.addClass('bg-success');
                btn.data('status', 1);
                btn.html("Active");
                successMessage('Berhasil', 'Data berhasil diaktifkan.');
              }
              table.draw();
            }
          }).fail(function (error) {
            if (parseInt(statusData) === 0) 
              errorMessage('Peringatan', 'Terjadi kesalahan saat menonaktifkan data.');
            else 
              errorMessage('Peringatan', 'Terjadi kesalahan saat mengaktifkan data.');
          });
        }

        update = false;
        
      }
    });
  });
  $('#search_jenis').change();

  $("#btn-print-pdf").click(function () {
    let iframeHeight = $(window).height() - 220;
    let param = `?d=pdf`;
    $('#modal-print .modal-body').html(`<iframe id="modal-iframe_print" src="<?php echo site_url('api/laporan/print_001'); ?>${param}" style="width: 100%; height: ${iframeHeight}px; border: 1px solid #e5e5e5;background-image: url(<?php echo image_url('spinner.gif') ?>); background-repeat: no-repeat; background-position: 50% 50%;"></iframe>`);
    $('#modal-print').modal('show');
  });
});

</script>