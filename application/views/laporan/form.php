<style type="text/css">
	.table thead tr th, .table tbody tr td {
		white-space: nowrap;
	}
  #btn-group_float {
    position: fixed;
    bottom: 10%;
    right: 20px;
    z-index: 10000;
    text-align: right;
  }
</style>
<div id="btn-group_float">
  <p>
    <button type="button" id="btn-print-pdf" class="btn bg-orange btn-float btn-rounded" title="Print PDF"  data-placement="left" data-delay="600">
      <b><i class="icon-file-pdf"></i></b>
    </button>
  </p>
</div>
<div class="row">
	<div class="col-md-12">
		<form id="form" class="form-horizontal" method="post">
			<div class="panel panel-white">
				<div class="panel-body">
					<div class="row mb-20">
						<div class="col-md-6">
							<fieldset>
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Data Kain</legend>
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset>
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Kesimpulan</legend>
							</fieldset>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-3 input-required">Warna Kain</label>
										<div class="col-md-7">
											<input type="text" readonly class="form-control" id="nama" name="nama" placeholder="Nama ...">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3 input-required">Barcode</label>
										<div class="col-md-7">
											<input type="text" readonly class="form-control" id="barcode" name="barcode" placeholder="Barcode ...">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3">Spek Kain</label>
										<div class="col-sm-7">
											<textarea readonly class="form-control" id="spek_kain" name="spek_kain" rows="8" placeholder="Spek Kain ..."></textarea>
										</div>
									</div>
								</div>	
							</div>
						</div>

						<div class="col-sm-6 col-md-6 col-lg-6">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-sm-12">Kesimpulan</label>
										<div class="col-sm-12">
											<textarea readonly class="form-control" id="deskripsi" name="deskripsi" rows="11" placeholder="Kesimpulan ..."></textarea>
										</div>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-sm-12">Catatan</label>
										<div class="col-sm-12">
											<textarea readonly class="form-control" id="catatan" name="catatan" rows="11" placeholder="Catatan ..."></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<fieldset>
								<legend class="text-bold"><i class="icon-magazine position-left"></i> Pertanyaan & Jawaban</legend>
							</fieldset>
						</div>

						<div class="col-sm-12 col-md-12 col-lg-12">
				
							<div class="table-responsive">
								<table id="table_jawaban" class="table">
									<thead>
										<tr>
											<th class="text-center">Kode</th>
								            <th class="text-center">Pertanyaan</th>
								            <th class="text-center">Jawaban</th>
										</tr>
					 				</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>

					</div>
					<div>
						<input type="hidden" id="id" name="id" value="0">
						<input type="hidden" id="uid" name="uid" value="0">
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" class="btn btn-success btn-labeled btn-save">
								<b><i class="icon-floppy-disk"></i></b>
								Cetak
							</button>
							<button type="button" class="btn btn-default btn-batal">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="modal fade" id="modal-print" tabindex="-1" role="dialog" aria-labelledby="modal-printLabel">
  <div class="modal-dialog modal-full">
    <div class="modal-content">
      <div class="modal-header">
        <div class="close">
          <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Close</button>
        </div>
        <h4 class="modal-title">Print Preview</h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>