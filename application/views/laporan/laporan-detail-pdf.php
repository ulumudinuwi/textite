<html>
<body>

<style type="text/css" media="print">
body {
    line-height: 1.2em;
    font-size: 8px;
    font-family: Arial, sans-serif;
}
h1, h2, h3, h4, h5, h6 {
    font-family: inherit;
    font-weight: 400;
    line-height: 1.5384616;
    color: inherit;
    margin-top: 0;
    margin-bottom: 5px;
}
h1 {
    font-size: 24px;
}
h2 {
    font-size: 16px;
}
h3 {
    font-size: 14px;
}
h4 {
    font-size: 12px;
}
h5 {
    font-size: 10px;
}
h6 {
    font-size: 8px;
}
table {
    border-collapse: collapse;
    font-size: 8px;
}
.table {
    border-spacing: 0;
    width: 100%;
    border: 1px solid #555;
    font-size: 10px;
}
.table thead th,
.table tbody td {
    border: 1px solid #555;
    vertical-align: middle;
    padding: 5px 10px;
    line-height: 1.5384616;
}
.table thead th {
    color: #fff;
    background-color: #607D8B;
    font-weight: bold;
    text-align: center;
}
.text-center {
    text-align: center;
}
</style>

<style>
.footer_current_date_user {
    text-align: right;
    color: #d10404;
    font-size: 8px;
    vertical-align: top;
    margin-top: 10px;
}
</style>
<h4 style="position:absolute;left: 7%; top: 3%;">CV. RUKUN ABADI</h4>
<h4 style="position:absolute;right: 7%; top: 3%;"><?php echo $title; ?></h4>
<br>

<div class="row">
    <table class="table table-bordered table-striped">
        <thead>
            <tr class="bg-slate">
                <th>DATA KAIN</th>
                <th>KESIMPULAN</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Warna Kain : <?php echo $nama_kain ?><br>
                    Barcode : <?php echo $barcode ?><br>
                    Spek Kain : <?php echo $spek_kain ?>
                </td>
                <td>
                    Hasil Konsultasi : <?php echo $deskripsi ?><br>
                    catatan : <?php echo $catatan ?>
                </td>
            </tr>
        </tbody>
    </table>

    <table class="table table-bordered table-striped">
        <thead>
            <tr class="bg-slate">
                <th colspan="4">Daftar Pertanyaan dan Jawaban</th>
            </tr>
            <tr class="bg-slate">
                <th>NO.</th>
                <th>KODE</th>
                <th>PERTANYAAN</th>
                <th>HASIL KONSULTASI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                if(count($data_pertanyaan) > 0):
                    $no = 1;
                    foreach ($data_pertanyaan as $i => $row):
                    $jawaban = ($row->jawab === '1') ? 'Ya, Total Point : ' . $row->number : 'Tidak' ;
            ?>
            <tr>
                <td class="text-center"><?php echo $no; ?></td>
                <td class="text-center"><?php echo $row->kode; ?></td>
                <td ><?php echo $row->pertanyaan_deskripsi; ?></td>
                <td class="text-center"><?php echo $jawaban; ?></td>
            </tr>
            <?php 
                $no++;
                endforeach; 
            ?>
            <?php else: ?>
            <tr>
                <td style="font-weight: bold;text-align: center;" colspan="4">TIDAK ADA DATA</td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>
<table style="width: 100%; margin-top: 20px;">
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;"><?php echo $current_date; ?></td>
    </tr>
    <tr>
        <td style="text-align: left; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: center; white-space: nowrap; width: 20%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 60%;">&nbsp;</td>
        <td style="text-align: center; white-space: nowrap; width: 20%;">_______________________</td>
    </tr>
</table>
</body>
</html>